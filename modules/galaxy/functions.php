<?php

function verifyModuleConfig() {
  // put some custom code in here to verify the module in some way.
  // if not required then just return a JSON response of '1':
  // return json_encode( array("resp"=>1) );
  
  
  global $person_id, $modname;
  global $vals; // a key/value array where the key is the db table col name and the value is the users value. e.g: $vals['api_key'] = 'XYZ'
//   error_log("verifying galaxy...".$person_id." ".$vals['url']." ".$vals['api_key']);

  $user_url = $vals['url']."api/users?key=".$vals['api_key'];
  $wf_url = $vals['url']."api/workflows?key=".$vals['api_key'];
  error_log($wf_url);
  
  $options = array(
    CURLOPT_RETURNTRANSFER => true
//     CURLOPT_USERPWD => $username . ":" . $password,   // authentication
//     CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
//     CURLOPT_POSTFIELDS => $json_string
    );
  
  $ch = curl_init( $user_url );
  curl_setopt_array( $ch, $options );
  $ch_result = "";
  $ch_result = curl_exec($ch);
  
  if(curl_errno($ch)) {
      error_log( 'Curl ERROR: ' . curl_error($ch) );
      return json_encode( array("resp"=>-1, "error"=>curl_error($ch)." ".$vals['url'] ) );
      
  }

  error_log("user_result=".$ch_result);
  $user_arr = json_decode($ch_result);
  if( ! isset($user_arr[0]) )
    return json_encode( array("resp"=>-1, "error"=>"User verification failed with given credentials." ) );
  
  $gal_user_id = $user_arr[0]->id;
  $gal_user_email = $user_arr[0]->email;
//   error_log("ID: ".$gal_user_id." EMAIL: ".$gal_user_email);
  
  $ch = curl_init( $wf_url );
  curl_setopt_array( $ch, $options );
  $ch_result = curl_exec($ch);
//   error_log("wf_result=".$ch_result);
  $wf_arr = json_decode($ch_result);
//   $wf_names = array();
  
  $resp_arr = array();
  $resp_arr['resp'] = 1;
  $resp_arr['verify_response'] = "Found the following workflows:<ul>";
//   $resp_arr['verify_response_data'] = $wf_names;
  
  foreach($wf_arr as $k=>$wf) 
    $resp_arr['verify_response'] .= "<li>".$wf->name."</li>";
//     error_log("WF: ".$wf->name." ".$wf->id);
  $resp_arr['verify_response'] .= "</ul>";

  return json_encode( $resp_arr );
//   return json_encode( array("resp"=>1) );

}


// function getUserGalaxyDetails($person_id) {
//   $sql = "select * from galaxy_config where person_id = '".$person_id."' limit 1";
//   error_log($sql);
//   // $res = mysql_query($sql);
//   $r = dbq($sql);
//   // $u = mysql_fetch_object($res, 'LimsUser');
//   $u = $r->fetch_object('GalaxyConfig');
//   return $u;
// }
?>