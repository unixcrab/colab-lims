<?php
if( !isset($_SESSION['lims']) ) session_start();
include_once "functions.php";
$g = getUserGalaxyDetails($_SESSION['lims']['person_id']);

// echo $subpage;
?>

<!--  MUST UPDATE SESSION WITH NEW DETAILS! -->

<!--<div id="user_feedback" class="alert alert-info">
Please provide as many details as possible.
</div>-->

<form name="change_galaxy_details" action="" >

  <label for="galaxy_api_key" id="galaxy_api_key_label"><strong>Galaxy API key</strong> (obtained in Galaxy/User/API Keys)</label>
  <input class="input-xlarge" type="text" placeholder=".input-xlarge" name="galaxy_api_key" id="galaxy_api_key" value="<?php print htmlspecialchars($g->api_key); ?>" />

  
  <br/>
  <button type="submit" class="btn" id="change_galaxy_details_submit"><? print $_SESSION['lims']['langdata']['save']; ?></button>


</form>