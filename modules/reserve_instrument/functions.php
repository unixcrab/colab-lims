<?php

function verifyModuleConfig() {
  // put some custom code in here to verify the module in some way.
  // if not required then just return a JSON response of '1':
  return json_encode( array("resp"=>1) );
	
	}
	
function customModuleRender() {
	global $tablePrefix;
	global $site_timezone;
	global $day_len;
	global $modname;
	global $button_reserve, $button_cancel, $button_trash;
	
	$now = time();
	$day = 86400;
	$day_work = 36000;
	$week = 604800;
	$day_today = date("j",$now);
	$month_today = date("n",$now);
	$year_today = date("Y",$now);
	$day_start = mktime(0, 0, 1,$month_today,$day_today,$year_today);
	//error_log("day_start=".makeTimeString($day_start));

	date_default_timezone_set($site_timezone);
	
	$instr_dates = getModuleData($modname,true,"instrument_id ASC,start_date ASC");
	$instruments = getInstruments($_SESSION['lims']['lab_id']);
	
	print "<h4>Instrument Reservation</h4>";
	?>
	<hr>
	<div id="user_feedback" class="alert alert-info" style="display: none;"></div>
	<form class="form-inline">
	<?php
	print "<label for='instrument'><strong>".$_SESSION['lims']['langdata']['reserve']."</strong> </label> <select id='instrument'>";
	foreach($instruments as $i) {
		print "<option value=".$i->id.">".$i->name."</option>";
	}
	print "</select>";
	?>
	
			<label class="control-label" for="dpd1"><strong><?php print $_SESSION['lims']['langdata']['day'];?></strong> </label> 
			<input type="text" class="input-small" id="dpd1" value="<?php print makeDateString($now);?>">
			
			<div class="input-append bootstrap-timepicker">
				<input type="text" class="input-mini" id="startTime" value="<?php print date("H:i",$now); ?>">
				<span class="add-on"><i class="icon-time"></i></span>
			</div>
			
			<!--
			<label class="control-label" for="dpd2"><strong><?php print $_SESSION['lims']['langdata']['to'];?></strong> </label>
			<input type="text" class="input-small" id="dpd2" value="<?php print makeDateString($now);?>">
			-->
			
			<label class="control-label" for="dpd1"><strong><?php print $_SESSION['lims']['langdata']['to'];?></strong> </label> 
			<div class="input-append bootstrap-timepicker">
				<input type="text" class="input-mini" id="endTime" value="<?php print date("H:i",($now+7200)); ?>">
				<span class="add-on"><i class="icon-time"></i></span>
			</div>
	
	<button class="btn btn-primary" id="reserveConfirm"><?php print $button_reserve; ?></button>
	</form>
	
	<!-- Delete Modal -->
	<div id="resDelModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="resDelModalLabel" aria-hidden="true" data-keyboard="false">
		<div class="modal-header">
			<h4 id="resDelModalLabel"><i class="icon-trash"></i> <?php print $_SESSION['lims']['langdata']['delete'];?>?</h4>
		</div>
		<div class="modal-body" id="resDelModalBody"></div>
		<div class="modal-footer">
			<!--<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true"><i class='icon-ban-circle icon-white'></i> Cancel</button>-->
			<button class="btn btn-warning" id="resDelCancel"><?php print $button_cancel; ?></button>
			<button type="submit" class="btn btn-danger" id="resDelConfirm"><?php print $button_trash; ?></button>
		</div>
	</div>

	<?php
	

	
	//$len = count($instr_dates) - 1;
	//print "<!-- len=".$len." -->\n";
	
	print "<table class='table  table-hover'>";
	for($bdate=$day_start;$bdate<($day_start+$week);$bdate+=$day) {
	
		$trclass="";
		if(date("w",$bdate)==6) $trclass = "class='warning'";
		if(date("w",$bdate)==0) $trclass = "class='error'";
		print "\n<tr ".$trclass.">\n<td style='width:80px;'>".makeDateString($bdate)."</td>\n<td><!-- bookings -->\n";
		
		//print "<div style='width: 100%; display: table;'>\n";
		
		foreach($instruments as $iid=>$instrument) {
			print "<div style='width: 100%; display: table;'>\n<div style='display: table-row;'>\n";
			print "<div style='display: inline; float:left; width:100px; padding: 0px;'>".$instrument->name."</div>\n";
			
			foreach($instr_dates as $instr_res_id=>$instr_res) {
				$icon_remove = "";
				
				
				if(
					( $instr_res->instrument_id==$instrument->id )&&
					( 
						( $instr_res->start_date>=$bdate)&&($instr_res->end_date<=($bdate+$day) ) 
						//|| (($instr_res->start_date<$bdate) && ($instr_res->end_date>$bdate))
						)
					) {
					$iuser = getUserDetails($instr_res->person_id);
					if($instr_res->person_id==$_SESSION['lims']['person_id']) $icon_remove = "<i id='resDelLink-".$instr_res_id."' class='icon-remove' style='display:none;'></i>";
					$perc_day = round((($instr_res->end_date-$instr_res->start_date)/$day_work)*100.0);
					print "<!-- perc_day=".$perc_day." -->\n";
					if($perc_day>=75.0) $perc_day=75;
					if(($perc_day>25.0)&&($perc_day<75.0)) $perc_day=25;
					if(($perc_day<25.0)&&($perc_day>12.0)) $perc_day=12;
					if($perc_day<10.0) $perc_day=10;
					print "<div id='res_id-".$instr_res_id."' data-trigger='hover' data-animation='true' data-placement='left' data-html='true' data-title=\"<span class='label label-important'>".$_SESSION['lims']['langdata']['reserved']."</span> ".$instrument->name."\" data-content='<b>".$_SESSION['lims']['langdata']['from']."</b>: ".makeDateString($instr_res->start_date)." ".makeTimeString($instr_res->start_date)."<br><b>".$_SESSION['lims']['langdata']['to']."</b>: ".makeDateString($instr_res->end_date)." ".makeTimeString($instr_res->end_date)."<br><hr><b>".$_SESSION['lims']['langdata']['user']."</b>: ".myhtmlentities($iuser->first_name." ".$iuser->last_name)."' style='display: inline; border:1px solid #fcc; border-left:3px solid #f00; background-color:#fee; padding-left: 5px; width:".$perc_day."%; float:left;'><small>".makeTimeString($instr_res->start_date)."-".makeTimeString($instr_res->end_date)." ".$icon_remove."</small></div>\n\n";
				}
			}
			print "</div><!--ROW-->\n</div><!--TABLE-->\n";
		}
		print "</td>\n</tr>\n";
	
	}
	
	
	print "</table>";
	
	?>

	<?php
}
?>


<script type="text/javascript" src="<?php print $site_root.'/core/js/datepicker/bootstrap-datepicker.'.trim($curr_lang,'"').'.js';?>"></script>
<script type="text/javascript" src="<?php print $site_root.'/core/js/timepicker/bootstrap-timepicker.min.js';?>"></script>

<script>
$(document).ready(function(){
	$('#resDelModal').modal('hide');
	$('#resDelCancel').click( function() {
		$('#resDelConfirm').unbind('click');
		$('#resDelModal').modal('hide');
	});
	
	$('[id^=resDelLink-]').mouseenter(function(){ $(this).css('cursor', 'pointer'); });
	
	$('[id^=resDelLink-]').click(function(){
		var rid = this.id.split("-")[1];
		//console.log('del '+rid);
		$('#resDelModal').modal('show');
		
			$('#resDelConfirm').click( function(){
			$.post("modules/<?php print $modname; ?>/ajax.php", { mod:'<?php print $modname; ?>', mode:'del', rid:rid }, 
				function(){
						$('#resDelModal' ).modal( "hide" ); 
						$('#resDelModal').on('hidden', function () {	location.reload() });
					});
			});
			
	});
	

			
	$('[id^=res_id-]').mouseenter(function(){
		var rid = this.id.split("-")[1];
		$('[id^=resDelLink-]').hide();
		$('#resDelLink-'+rid).show();
	});
	$('[id^=res_id-]').popover();
	
	$('#reserveConfirm').click(function(){
		var instr = $('#instrument :selected').val();
		var sdate = $('#dpd1').val();
		var edate = sdate; //$('#dpd2').val();
		var stime = $('#startTime').val();
		var etime = $('#endTime').val();
		
		var jqxhr = $.post('modules/<?php print $modname; ?>/ajax.php', { mod:'<?php print $modname; ?>', mode:'add', instr:instr, sdate:sdate, edate:edate, stime:stime, etime:etime }, 
			function(data) {
				if( data.resp == 1 ) location.reload(); //userFeedbackL("alert-success", "err_saved");
				else if( data.resp == -5 ) userFeedbackL("alert-error", "err_invalid_range");
				else if( data.resp == -10 ) userFeedbackL("alert-error", "err_not_available");
				else userFeedbackL("alert-error", "err_not_available");
				//console.log("reserveConfirm "+data.resp);
			}, "json" );
	
		return false;
	});

	$('#startTime').timepicker({showMeridian:false});
	$('#endTime').timepicker({showMeridian:false});
	
	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	var date_format_str = "dd/mm/yyyy";
	if( date_format == 2 ) date_format_str = "mm/dd/yyyy";
	
	var start_date = $('#dpd1').datepicker({
			weekStart: 1, 
			format: date_format_str,
			onRender: function(date) {
				return date.valueOf() < now.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function(ev) {
			//if (ev.date.valueOf() > end_date.date.valueOf()) {
			//	var newDate = new Date(ev.date)
			//	newDate.setDate(newDate.getDate());
			//	end_date.setValue(newDate);
			//}
			start_date.hide();
			//$('#dpd2')[0].focus();
		}).data('datepicker');
		
	/*	
	var end_date = $('#dpd2').datepicker({
			weekStart: 1, 
			format: date_format_str,
			onRender: function(date) {
				return date.valueOf() < start_date.date.valueOf() ? 'disabled' : '';
			}
		}).on('changeDate', function(ev) {
			end_date.hide();
		}).data('datepicker');
		*/
		
	
});
</script>