<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "../../core/config.common.php";
include $serv_root."/core/constants.php";
include $serv_root."/core/functions.misc.php";
//include $serv_root."/core/functions.db_connect.php";
include $serv_root."/core/functions.module.api.php";

//foreach($_POST as $k=>$v) error_log("reserve ajax ".$k." ".$v);

if( !isset($_POST['mode']) ) {
	print json_encode( array("resp"=>0) );
	return;
	}
$mode = $_POST['mode'];

if($mode == "del") {
 if(!isset($_POST['rid'])) {
	print json_encode( array("resp"=>0) );
	return;
	}
	$rid = $_POST['rid'];
	$modname = $_POST['mod'];
	
	//$data_array = array( "id"=>(int)$rid, "name"=>"some string"	);
	$data_array = array( "id"=>(int)$rid	);
		$ret = delModuleData($modname, $data_array);
	if($ret==1) {
		print json_encode( array("resp"=>1) );
		return;
	}
	
}

if($mode == "add") {
	$modname = $_POST['mod'];
	$instrument_id = (int)$_POST['instr'];
	$start_hr_min = explode(":",$_POST['stime']);
	$end_hr_min = explode(":",$_POST['etime']);
	
	if($_SESSION['lims']['dateformat']==FORMAT_DATE_DMY) {
		$start_ptime = strptime($_POST['sdate'], '%d/%m/%Y');
		$end_ptime = strptime($_POST['edate'], '%d/%m/%Y');
		}
	if($_SESSION['lims']['dateformat']==FORMAT_DATE_MDY) {
		$start_ptime = strptime($_POST['sdate'], '%m/%d/%Y');
		$end_ptime = strptime($_POST['edate'], '%m/%d/%Y');
		}
	
	$start_ts = mktime($start_hr_min[0], $start_hr_min[1], 0, $start_ptime['tm_mon']+1, $start_ptime['tm_mday'], $start_ptime['tm_year']+1900);
	$end_ts = mktime($end_hr_min[0], $end_hr_min[1], 0, $end_ptime['tm_mon']+1, $end_ptime['tm_mday'], $end_ptime['tm_year']+1900);
	
	if($end_ts<$start_ts) { // end date is before start date
		print json_encode( array("resp"=>-5) ); 
		return;
	}
	
	error_log($_SESSION['lims']['dateformat']." timestamp ".$start_ts." -- ".makeDateString($start_ts)." ".makeTimeString($start_ts));
	error_log($_SESSION['lims']['dateformat']." timestamp ".$end_ts." -- ".makeDateString($end_ts)." ".makeTimeString($end_ts));

	$instr_free = true;
	$current_reservations = getModuleData($modname,"instrument_id=".$instrument_id);
	foreach($current_reservations as $k=>$v) {
		//error_log($k." ".$v->instrument_id." ".$v->start_date." ".$v->end_date);
		if( ($start_ts>$v->start_date) && ($end_ts<$v->end_date) ) $instr_free = false;
		if( ($start_ts<$v->start_date) && ($end_ts>$v->start_date) ) $instr_free = false;
		if( ($start_ts<$v->end_date) && ($end_ts>$v->end_date) ) $instr_free = false;
		if( ($start_ts==$v->start_date) && ($end_ts==$v->end_date) ) $instr_free = false;
		}
	
	if(!$instr_free) {
		print json_encode( array("resp"=>-10) );
		error_log("instr ".$instrument_id." NOT FREE");
		return;
	}
	else {
		$data_array = array(
			"person_id"=>(int)$_SESSION['lims']['person_id'],
			"instrument_id"=>$instrument_id,
			"start_date"=>$start_ts,
			"end_date"=>$end_ts
			);
		$ret = addModuleData($modname, $data_array);
		if($ret==1) {
			print json_encode( array("resp"=>1) );
			return;
		}
	}
}








?>