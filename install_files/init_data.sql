﻿charset utf8;

insert into {COLAB_PREFIX}person (person_id,username,passwd,first_name,last_name) values(1,"admin","8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918","admin","admin");

insert into {COLAB_PREFIX}domains(id,domain_name) values(1,"Main Lab Group");

insert into {COLAB_PREFIX}laboratory(id,laboratory_id,domain_id,name) values(1,"Main Lab",1,"Main Lab");

insert into {COLAB_PREFIX}laboratory_personnel(lab_id,person_id,auth_level_id) values(1,1,100);

insert into {COLAB_PREFIX}storage_type(storage_type) values("Freezer (Upright)");
insert into {COLAB_PREFIX}storage_type(storage_type) values("Freezer (Box)");
insert into {COLAB_PREFIX}storage_type(storage_type) values("Refrigerator");
insert into {COLAB_PREFIX}storage_type(storage_type) values("Refrigerator/Freezer (Combined)");
insert into {COLAB_PREFIX}storage_type(storage_type) values("Cupboard");
insert into {COLAB_PREFIX}storage_type(storage_type) values("Shelves");
insert into {COLAB_PREFIX}storage_type(storage_type) values("Steel Locker");
insert into {COLAB_PREFIX}storage_type(storage_type) values("Trash");

insert into {COLAB_PREFIX}sample_type(sample_type) values("DNA");
insert into {COLAB_PREFIX}sample_type(sample_type) values("RNA");

INSERT INTO {COLAB_PREFIX}container_type(container_type, capacity, number_of_rows, number_of_columns, active) VALUES('96 Well Plate', 96, 8, 12, 1);

-- units
-- Mass
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,10,'pg',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,10,'ng',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,10,'µg',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,10,'mg',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,10,'g',1.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,10,'kg',1000.0);

-- volume
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,50,'pL',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,50,'nL',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,50,'µL',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,50,'mL',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,50,'L',1.0);

insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(20,50,'Cubic Foot',1.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(20,50,'Cubic Inch',1.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(20,50,'Cubic Yard',1.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(20,50,'Gallon',1.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(20,50,'Ounce',1.0);

-- pressure
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,60,'pPa',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,60,'nPa',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,60,'µPa',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,60,'mPa',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,60,'Pa',1.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,60,'kPa',1000.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,60,'MPa',1.0E6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,60,'GPa',1.0E9);

-- temperature
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,40,'°C',1.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(20,40,'°F',1.0);

-- length
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,70,'µm',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,70,'mm',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,70,'cm',0.1);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,70,'m',1.0);

insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(20,70,'yards',1.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(20,70,'feet',1.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(20,70,'inches',1.0);

-- concentration
-- Parts per million
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'ppm',1.0);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(20,30,'ppm',1.0);

insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'pg/pL',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'ng/pL',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'µg/pL',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'mg/pL',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'g/pL',1.0);

insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'pg/µL',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'ng/µL',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'µg/µL',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'mg/µL',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'g/µL',1.0);

insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'pg/mL',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'ng/mL',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'µg/mL',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'mg/mL',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'g/mL',1.0);

insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'pg/L',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'ng/L',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'µg/L',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'mg/L',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'g/L',1.0);
-- Molarity
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'pM',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'nM',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'µM',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'mM',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'M',1.0);
-- Molality
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'pm',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'nm',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'µm',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'mm',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'m',1.0);

-- moles/Litre
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'pm/pL',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'nm/pL',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'µm/pL',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'mm/pL',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'m/pL',1.0);

insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'pm/µL',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'nm/µL',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'µm/µL',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'mm/µL',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'m/µL',1.0);

insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'pm/mL',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'nm/mL',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'µm/mL',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'mm/mL',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'m/mL',1.0);

insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'pm/L',1.0E-12);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'nm/L',1.0E-9);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'µm/L',1.0E-6);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'mm/L',1.0E-3);
insert into {COLAB_PREFIX}units(unit_sys_id,unit_type_id,units,conversion_factor) values(1,30,'m/L',1.0);