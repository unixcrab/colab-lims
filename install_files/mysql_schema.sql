-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 31, 2013 at 09:02 AM
-- Server version: 5.5.29
-- PHP Version: 5.4.6-1ubuntu1.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lims`
--

-- --------------------------------------------------------

--
-- Table structure for table `alerts`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}alerts`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}alerts` (
  `alert_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `lang_index` varchar(64) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `ack` int(11) NOT NULL,
  PRIMARY KEY (`alert_id`),
  KEY `person_id` (`person_id`),
  KEY `timestamp` (`timestamp`),
  KEY `ack` (`ack`),
  KEY `ref_id` (`ref_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}audit`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}audit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `audit_key` bigint(20) NOT NULL,
  `affected_id` int(11) NOT NULL,
  `audit_descr` text COLLATE utf8_bin NOT NULL COMMENT 'A detailed description of the change',
  `timestamp` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `affected_id` (`affected_id`),
  KEY `audit_key` (`audit_key`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=105 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_levels`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}auth_levels`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}auth_levels` (
  `auth_level_id` int(11) NOT NULL AUTO_INCREMENT,
  `descr` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`auth_level_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=101 ;

-- --------------------------------------------------------

--
-- Table structure for table `carton`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}carton`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}carton` (
  `carton_id` int(11) NOT NULL AUTO_INCREMENT,
  `container_type_id` int(11) DEFAULT NULL,
  `carton_identifier` varchar(100) CHARACTER SET utf8 NOT NULL,
  `create_date_timestamp` bigint(20) DEFAULT NULL,
  `storage_id` int(11) NOT NULL DEFAULT '0',
  `shelf_id` int(11) NOT NULL DEFAULT '0',
  `internal_barcode` varchar(64) CHARACTER SET utf8 NOT NULL,
  `owner_id` int(11) NOT NULL COMMENT 'Owner of the carton',
  `allow_details_edit` int(11) NOT NULL DEFAULT '1',
  `allow_contents_edit` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`carton_id`),
  KEY `storage_id` (`storage_id`),
  KEY `create_date_timestamp` (`create_date_timestamp`),
  KEY `shelf_id` (`shelf_id`),
  KEY `internal_barcode` (`internal_barcode`),
  KEY `carton_identifier` (`carton_identifier`),
  KEY `person_id` (`owner_id`),
  KEY `allow_details_edit` (`allow_details_edit`,`allow_contents_edit`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `carton_sample`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}carton_sample`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}carton_sample` (
  `carton_sample_id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_id` int(11) NOT NULL,
  `row_num` int(11) NOT NULL,
  `column_num` int(11) NOT NULL,
  `carton_id` int(11) NOT NULL,
  `active` varchar(1) COLLATE utf8_bin DEFAULT 'Y',
  PRIMARY KEY (`carton_sample_id`),
  UNIQUE KEY `sample_id` (`sample_id`),
  KEY `carton_id` (`carton_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `container_type`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}container_type`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}container_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `container_type` varchar(100) COLLATE utf8_bin NOT NULL,
  `capacity` float NOT NULL,
  `number_of_rows` int(11) NOT NULL,
  `number_of_columns` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}domains`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}domains` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `domain_name` (`domain_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `laboratory`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}laboratory`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}laboratory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `laboratory_id` varchar(20) NOT NULL,
  `domain_name` varchar(100) DEFAULT NULL COMMENT 'Here just to be backwards compatible. Use domain_id.',
  `domain_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `laboratory_id` (`laboratory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `laboratory_personnel`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}laboratory_personnel`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}laboratory_personnel` (
  `lab_id` int(11) DEFAULT NULL,
  `person_id` int(11) NOT NULL,
  `auth_level_id` int(11) DEFAULT NULL,
  `active` varchar(1) COLLATE utf8_bin DEFAULT 'Y',
  `start_date_timestamp` bigint(20) DEFAULT NULL,
  `default_flag` varchar(1) COLLATE utf8_bin DEFAULT 'Y',
  KEY `lab_id` (`lab_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}modules`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}modules` (
  `module_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `file` varchar(64) NOT NULL COMMENT 'xml config file',
  `active` int(2) NOT NULL,
  PRIMARY KEY (`module_id`),
  UNIQUE KEY `name` (`name`),
  KEY `file` (`file`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `module_galaxy_users`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}module_galaxy_users`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}module_galaxy_users` (
  `person_id` int(11) NOT NULL,
  `url` varchar(64) NOT NULL,
  `api_key` varchar(64) NOT NULL,
  `history` varchar(64) NOT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}person`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}person` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `passwd` varchar(128) NOT NULL COMMENT 'sha256 hash',
  `organization_id` int(11) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `email_address` varchar(100) DEFAULT NULL,
  `middle_name` varchar(10) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `address_1` varchar(100) DEFAULT NULL,
  `address_2` varchar(100) DEFAULT NULL,
  `address_3` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state_or_province` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postal_code` varchar(15) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Table structure for table `person_datetime`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}person_prefs`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}person_prefs` (
  `person_id` int(11) NOT NULL,
  `date_format_id` int(2) NOT NULL DEFAULT '1',
  `time_format_id` int(2) NOT NULL DEFAULT '1',
  `units_id` int(11) NOT NULL COMMENT 'preferred unit system',
  PRIMARY KEY (`person_id`),
  KEY `date_format` (`date_format_id`),
  KEY `units_id` (`units_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `person_skills`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}person_skills`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}person_skills` (
  `person_id` int(11) NOT NULL DEFAULT '0',
  `skill_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}projects`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}projects` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `person_id` int(11) NOT NULL COMMENT 'project owner',
  `start_date` bigint(20) NOT NULL COMMENT 'unix timestamp',
  `end_date` bigint(20) NOT NULL COMMENT 'unix timestamp',
  `active` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`project_id`),
  UNIQUE KEY `name_uniq` (`name`),
  KEY `owner_id` (`person_id`,`start_date`,`end_date`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1037 ;

-- --------------------------------------------------------

--
-- Table structure for table `projects_files`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}projects_files`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}projects_files` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) NOT NULL COMMENT 'The file owner',
  `project_id` int(11) NOT NULL,
  `file_name` varchar(256) NOT NULL,
  `comment` text NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  PRIMARY KEY (`file_id`),
  KEY `person_id` (`person_id`,`project_id`,`timestamp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Table structure for table `projects_members`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}projects_members`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}projects_members` (
  `project_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  KEY `project_id` (`project_id`,`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sample`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}sample`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}sample` (
  `sample_id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_type_id` int(11) DEFAULT 0,
  `identification` varchar(128) NOT NULL,
	`concentration` float DEFAULT 0.0,
  `concentration_units_id` int(11) DEFAULT 0,
  `comments` varchar(100) DEFAULT NULL,
  `control_flag` varchar(1) DEFAULT 'N',
  `exhausted_flag` varchar(1) DEFAULT 'N',
  `volume` float DEFAULT 0.0,
  `volume_units_id` int(11) DEFAULT 0,
  PRIMARY KEY (`sample_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- Table structure for table `sample_type`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}sample_type`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}sample_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_type` varchar(64) CHARACTER SET utf8 NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}skill`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}skill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skill` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `skill` (`skill`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

-- --------------------------------------------------------

--
-- Table structure for table `storage_device`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}storage_device`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}storage_device` (
  `storage_id` int(11) NOT NULL AUTO_INCREMENT,
  `environment_description` varchar(64) COLLATE utf8_bin NOT NULL,
  `storage_type_id` int(11) NOT NULL,
  `laboratory_id` int(11) NOT NULL,
  `equipment_id` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `identifier` varchar(64) CHARACTER SET utf8 NOT NULL,
  `internal_barcode` varchar(64) COLLATE utf8_bin NOT NULL,
  `external_barcode` varchar(64) COLLATE utf8_bin NOT NULL,
  UNIQUE KEY `storage_id` (`storage_id`),
  KEY `storage_type_id` (`storage_type_id`,`laboratory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=141 ;

-- --------------------------------------------------------

--
-- Table structure for table `storage_shelf`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}storage_shelf`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}storage_shelf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shelf_number` varchar(32) COLLATE utf8_bin NOT NULL,
  `storage_id` int(11) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `storage_id` (`storage_id`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `storage_type`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}storage_type`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}storage_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `storage_type` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `{COLAB_PREFIX}units`;
CREATE TABLE IF NOT EXISTS `{COLAB_PREFIX}units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_type` varchar(20) NOT NULL,
  `unit_sys_id` int(11) NOT NULL,
  `unit_type_id` int(11) NOT NULL,
  `units` varchar(50) NOT NULL,
  `conversion_factor` double NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `unit_sys_id` (`unit_sys_id`),
  KEY `unit_type_id` (`unit_type_id`),
  KEY `conversion_factor` (`conversion_factor`),
  KEY `units` (`units`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
