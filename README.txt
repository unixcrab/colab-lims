COLAB README

System requirements

CO·LAB is a web based system and requires the following to function:
PHP 5
At least version 5.3. Ideally 5.4 (developed on 5.4.6)
Apache 2
Only tested and known to work on 2.2.16 - 2.2.22 but any version 2.* should work
MySQL 5
Most versions in the 5.0 branch should work. Tested on version 5.1.66 and developed on 5.5.29
The simplest way to run it is on a current Linux distribution with up-to-date L.A.M.P. packages installed.

Step 1: Setup Apache/PHP

 If the Apache webserver is not already installed, install it along with PHP5. There are various ways to do this but if you are unsure ask an administrator. Ideally the entire L.A.M.P. (Linux-Apache-MySQL-PHP) stack should be installed. 

In Debian/Ubuntu these commands may be enough:
sudo apt-get install php5 php5-mysql

Step 2: Setup MySQL

 Install MySQL and create a database (or get your administrator to do it if you don't have permission) with a username and password that COLAB will use. 

In Debian/Ubuntu:
sudo apt-get install mysql-server

Important!
Ensure that the database character set and collation are set to utf8. Without this a lot of characters will be corrupted.

You can check with these commands when logged into MySQL: 
show variables like "character_set_database";
show variables like "collation_database"; 

The command to create a utf8 database is: create DATABASE limsdb CHARACTER SET utf8 COLLATE utf8_bin;

Step 3: Download COLAB

Use GIT: Change directory on the webserver to where you want to run CO·LAB and execute     git clone https://bitbucket.org/unixcrab/colab-lims.git

Or click the download link at https://bitbucket.org/unixcrab/colab-lims and extract the files to a directory on your webserver.

Step 4: Configure COLAB

Change to the directory where you installed or extracted the files.
Edit the configuration file: core/config.common.php and change the database configuration options to use the MySQL username, password, database and hostname.
Set the $site_root variable to the directory relative to the webserver root directory.
Set the $serv_root variable to the full path where COLAB is installed.
Run the install and database initialisation script from the command line: php5 ./install.php

Step 4: Configure COLAB

If the install script completes without error then you should be able to load the page at your webserver address, taking into account the $site_root variable you set. If your configuration has $site_root="/lims" set (the default), then the website address will be something like http://your_webserver_address/lims 

Login with the default profile: Username is 'admin' and password is 'admin'.


