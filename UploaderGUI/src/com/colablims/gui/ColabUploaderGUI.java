package com.colablims.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;
import javax.swing.JButton;

import java.awt.Font;

import javax.swing.JList;
import javax.swing.AbstractListModel;

import java.awt.Color;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JCheckBox;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.validator.UrlValidator;

import java.awt.Toolkit;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;

public class ColabUploaderGUI {

	private JFrame frmColabDataUpload;
	private JTextField projectID;
	private JTextField colabHost;
	private JList<String> uploadList; 
	private Properties limsProps = new Properties();
	private String limsHost;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ColabUploaderGUI window = new ColabUploaderGUI();
					window.frmColabDataUpload.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ColabUploaderGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			limsProps.load(new FileInputStream("../agent/lims.properties"));
			limsHost = limsProps.getProperty("limsHost","http://127.0.0.1/lims");
			
			
		} catch ( IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		uploadList = new JList<String>();
		frmColabDataUpload = new JFrame();
		frmColabDataUpload.setIconImage(Toolkit.getDefaultToolkit().getImage(ColabUploaderGUI.class.getResource("/com/colablims/gui/favicon.ico")));
		frmColabDataUpload.setResizable(false);
		frmColabDataUpload.setTitle("COLAB data upload");
		frmColabDataUpload.getContentPane().setFont(new Font("Arial", Font.PLAIN, 12));
		SpringLayout springLayout = new SpringLayout();
		frmColabDataUpload.getContentPane().setLayout(springLayout);
		
		final JButton btnUpload = new JButton("Upload");
		btnUpload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.err.println("Upload");
				boolean uploadGood = false;
				int pID = 0; 
				
				// validate limsHost
				String[] schemes = {"http","https"};
				@SuppressWarnings("deprecation")
				UrlValidator urlValidator = new UrlValidator(schemes);
				if (urlValidator.isValid(limsHost)==false) {
				   System.err.println(limsHost+" url is invalid");
				   JOptionPane.showMessageDialog(null,
						    "Please enter a valid COLAB URL in the form http://hostname.",
						    "Invalid COLAB URL",
						    JOptionPane.WARNING_MESSAGE);
				}
				else {
					System.err.println(limsHost+" url is valid");
					uploadGood = true;
				}
				
				// validate pID
				try {
					pID = Integer.parseInt( projectID.getText() );
				}
				catch(NumberFormatException nfe) {
					uploadGood = false;
					System.err.println("invalid pID "+nfe.getLocalizedMessage());
					JOptionPane.showMessageDialog(null,
						    "Please enter a valid project ID as a number.",
						    "Invalid project ID",
						    JOptionPane.WARNING_MESSAGE);
				}
				System.err.println("pID="+pID);
				
				
				if(uploadGood) btnUpload.setEnabled(false);
			}
		});
		
		springLayout.putConstraint(SpringLayout.SOUTH, btnUpload, -10, SpringLayout.SOUTH, frmColabDataUpload.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnUpload, -10, SpringLayout.EAST, frmColabDataUpload.getContentPane());
		frmColabDataUpload.getContentPane().add(btnUpload);
		
		JButton btnCancel = new JButton("Close");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnCancel, 0, SpringLayout.NORTH, btnUpload);
		springLayout.putConstraint(SpringLayout.EAST, btnCancel, -22, SpringLayout.WEST, btnUpload);
		frmColabDataUpload.getContentPane().add(btnCancel);
		
		projectID = new JTextField();
		springLayout.putConstraint(SpringLayout.EAST, projectID, -356, SpringLayout.EAST, frmColabDataUpload.getContentPane());
		frmColabDataUpload.getContentPane().add(projectID);
		projectID.setColumns(10);
		
		JLabel lblProjectId = new JLabel("Project ID");
		springLayout.putConstraint(SpringLayout.NORTH, projectID, -2, SpringLayout.NORTH, lblProjectId);
		springLayout.putConstraint(SpringLayout.EAST, lblProjectId, -482, SpringLayout.EAST, frmColabDataUpload.getContentPane());
		lblProjectId.setVerticalAlignment(SwingConstants.BOTTOM);
		lblProjectId.setLabelFor(projectID);
		frmColabDataUpload.getContentPane().add(lblProjectId);
		
		colabHost = new JTextField();
		colabHost.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void changedUpdate(DocumentEvent arg0) {
				limsHost = colabHost.getText();
				System.err.println("new limsHost="+limsHost);
			}
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				// TODO Auto-generated method stub
				limsHost = colabHost.getText();
				System.err.println("new limsHost="+limsHost);
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				// TODO Auto-generated method stub
				limsHost = colabHost.getText();
				System.err.println("new limsHost="+limsHost);
			}
			});
		
		colabHost.setText(limsHost);
		springLayout.putConstraint(SpringLayout.WEST, projectID, 0, SpringLayout.WEST, colabHost);
		springLayout.putConstraint(SpringLayout.EAST, colabHost, -158, SpringLayout.EAST, frmColabDataUpload.getContentPane());
		frmColabDataUpload.getContentPane().add(colabHost);
		colabHost.setColumns(10);
		
		JLabel lblColabHost = new JLabel("COLAB host");
		springLayout.putConstraint(SpringLayout.NORTH, lblProjectId, 21, SpringLayout.SOUTH, lblColabHost);
		springLayout.putConstraint(SpringLayout.WEST, lblProjectId, 0, SpringLayout.WEST, lblColabHost);
		springLayout.putConstraint(SpringLayout.NORTH, colabHost, -2, SpringLayout.NORTH, lblColabHost);
		springLayout.putConstraint(SpringLayout.WEST, colabHost, 23, SpringLayout.EAST, lblColabHost);
		springLayout.putConstraint(SpringLayout.EAST, lblColabHost, 83, SpringLayout.WEST, frmColabDataUpload.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, lblColabHost, 10, SpringLayout.NORTH, frmColabDataUpload.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblColabHost, 10, SpringLayout.WEST, frmColabDataUpload.getContentPane());
		frmColabDataUpload.getContentPane().add(lblColabHost);
		
		JButton btnClearFileList = new JButton("Clear list");
		springLayout.putConstraint(SpringLayout.WEST, btnClearFileList, 314, SpringLayout.WEST, frmColabDataUpload.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnClearFileList, 0, SpringLayout.EAST, colabHost);
		btnClearFileList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				uploadList.setListData(new String[] {});
			}
		});
		frmColabDataUpload.getContentPane().add(btnClearFileList);
		
		JButton btnBrowseFiles = new JButton("Browse...");
		springLayout.putConstraint(SpringLayout.NORTH, btnBrowseFiles, 312, SpringLayout.NORTH, frmColabDataUpload.getContentPane());
		springLayout.putConstraint(SpringLayout.NORTH, btnClearFileList, 0, SpringLayout.NORTH, btnBrowseFiles);
		springLayout.putConstraint(SpringLayout.WEST, btnBrowseFiles, 419, SpringLayout.WEST, frmColabDataUpload.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnBrowseFiles, 0, SpringLayout.EAST, btnUpload);
		btnBrowseFiles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.err.println("Browsing...");

				JFileChooser c = new JFileChooser();
				c.setMultiSelectionEnabled(true);
				int rVal = c.showOpenDialog(c);
				if (rVal == JFileChooser.APPROVE_OPTION) {
					File[] files = c.getSelectedFiles();
					String[] fileNames = new String[files.length];
					int s = 0;
					for(File f:files) {
						fileNames[s] = f.getName();
						s++;
					} 
					
					System.err.println("filename= "+c.getSelectedFile().getName());
					System.err.println(c.getCurrentDirectory().toString());
					uploadList.setListData(fileNames);
				}
				if (rVal == JFileChooser.CANCEL_OPTION) {
					System.err.println("You pressed cancel");
				}


			}
		});
		frmColabDataUpload.getContentPane().add(btnBrowseFiles);
		
		JScrollPane scrollPane = new JScrollPane();
		springLayout.putConstraint(SpringLayout.NORTH, scrollPane, 19, SpringLayout.SOUTH, projectID);
		springLayout.putConstraint(SpringLayout.SOUTH, scrollPane, -116, SpringLayout.SOUTH, frmColabDataUpload.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, scrollPane, 10, SpringLayout.WEST, frmColabDataUpload.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, scrollPane, 0, SpringLayout.EAST, btnUpload);
		frmColabDataUpload.getContentPane().add(scrollPane);
		
		
		scrollPane.setViewportView(uploadList);
		springLayout.putConstraint(SpringLayout.NORTH, uploadList, 28, SpringLayout.SOUTH, colabHost);
		springLayout.putConstraint(SpringLayout.WEST, uploadList, 372, SpringLayout.WEST, frmColabDataUpload.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, uploadList, 0, SpringLayout.EAST, btnUpload);
		uploadList.setValueIsAdjusting(true);
		uploadList.setBackground(new Color(255, 255, 255));
		uploadList.setModel(new AbstractListModel() {
			String[] values = new String[] {};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		springLayout.putConstraint(SpringLayout.SOUTH, uploadList, -105, SpringLayout.SOUTH, btnBrowseFiles);
		
		JCheckBox chckbxSaveConfig = new JCheckBox("Save config on upload");
		chckbxSaveConfig.setSelected(true);
		springLayout.putConstraint(SpringLayout.WEST, chckbxSaveConfig, 0, SpringLayout.WEST, lblProjectId);
		springLayout.putConstraint(SpringLayout.SOUTH, chckbxSaveConfig, -10, SpringLayout.SOUTH, frmColabDataUpload.getContentPane());
		frmColabDataUpload.getContentPane().add(chckbxSaveConfig);
		
		JLabel lblLogo = new JLabel("logo");
		springLayout.putConstraint(SpringLayout.NORTH, lblLogo, 8, SpringLayout.NORTH, frmColabDataUpload.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, lblLogo, 0, SpringLayout.WEST, btnBrowseFiles);
		springLayout.putConstraint(SpringLayout.SOUTH, lblLogo, -22, SpringLayout.SOUTH, projectID);
		springLayout.putConstraint(SpringLayout.EAST, lblLogo, 0, SpringLayout.EAST, btnUpload);
		lblLogo.setIcon(new ImageIcon(ColabUploaderGUI.class.getResource("/com/colablims/gui/colab_logo.png")));
		frmColabDataUpload.getContentPane().add(lblLogo);
		frmColabDataUpload.setBounds(100, 100, 571, 450);
		frmColabDataUpload.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
