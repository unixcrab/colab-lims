<!DOCTYPE html>
<html lang="en">
<?php

$user_logged_in = false;
//session_cache_expire(360);
session_start();

include "config.common.php";

if( isset($_SESSION['lims']) && isset($_SESSION['lims']['lang']) && isset($_SESSION['lims']['logged_in'] ) && ( $_SESSION['lims']['logged_in'] == 1 ) ) {
	$user_logged_in = true;
	}

if( ($_SERVER['REQUEST_URI'] != $site_root."/index.php") && ($_SERVER['REQUEST_URI'] != $site_root."/index.php?l=f") )
	$_SESSION['lims']['request_uri'] = $_SERVER['REQUEST_URI'];
else 
	unset($_SESSION['lims']['request_uri']);
	
$time = time();
?>
<head>
<meta charset="utf-8">
<title>CO&middot;LAB</title>
<link href="<?php echo $site_root; ?>/config/style/bootstrap/css/bootstrap.css?t=<? print $time; ?>" rel="stylesheet" type="text/css">
<link href="<?php echo $site_root; ?>/config/style/font-awesome/font-awesome.min.css?t=<? print $time; ?>" rel="stylesheet" type="text/css">
<link href="<?php echo $site_root; ?>/config/style/datepicker/css/datepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo $site_root; ?>/config/style/timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
<!--
<link href="<?php echo $site_root; ?>/config/style/timepicker/jquery.ui.timepicker.css" rel="stylesheet" type="text/css">
<link href="<?php echo $site_root; ?>/config/style/ui/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css">
-->
<!--
<link href="<?php echo $site_root; ?>/config/style/bootstrap/css/bootstrap.css?t=<? print $time; ?>" rel="stylesheet" type="text/css">
<link href="<?php echo $site_root; ?>/config/style/font-awesome/font-awesome.min.css?t=<? print $time; ?>" rel="stylesheet" type="text/css">
<link href="<?php echo $site_root; ?>/config/style/datepicker/css/datepicker.css?t=<? print $time; ?>" rel="stylesheet" type="text/css">
-->
<link href="<?php echo $site_root; ?>/config/style/blueimp/jquery.fileupload-ui.css" rel="stylesheet" type="text/css">
<link href="<?php echo $site_root; ?>/core/js/__jquery.tablesorter/themes/blue/style.css" rel="stylesheet" type="text/css">
<!-- JS -->
<script>
  var site_root = <?php print json_encode($site_root); ?>;
  var curr_uri = <?php print json_encode($_SERVER['REQUEST_URI']); ?>;
  var curr_lang = <?php 
    $curr_lang = "\"en-GB\"";
    if( isset($_SESSION['lims']['lang']) && ($_SESSION['lims']['lang']=="gb") ) $curr_lang = "\"en-GB\"";
    elseif( isset($_SESSION['lims']['lang'] ) ) $curr_lang = json_encode($_SESSION['lims']['lang']); 
//     else print "\"en-GB\"";
    print $curr_lang;
  ?>;
  var date_format = <?php
    if( isset($_SESSION['lims']['dateformat']) ) print $_SESSION['lims']['dateformat'];
    else print $site_default_dateformat;
  ?>;
  var curr_projid = null;

	
</script>

<script type="text/javascript" src="<?print $site_root.'/core/js/jquery-1.9.1.min.js';?>"></script>
<!--<script type="text/javascript" src="<?print $site_root.'/core/js/jquery-ui-1.10.3.custom.min.js';?>"></script>
-->

<link rel="icon" type="image/png" href="<?php echo $site_root; ?>/img/html/favicon.ico?v=2" />

<style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 250px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
			
			.nav-header {
				font-size: 12px;  
				/*color:#666666;*/  
				}  

			.navbar .brand {
				font-size: 15px;  
				/*color:#666666;*/  
				}  
      .cartonCellUpOcc  {
				background-color:#ff9999;
				text-align:center;
				border: 1px solid red;
			}
			.cartonCellUpFree  {
				background-color:#99ff99;
				text-align:center;
			}
			.cartonCellDown  {
				background-color:#ffffff;
				text-align:center;
			}
	

</style>
    
</head>
