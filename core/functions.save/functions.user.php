<?php
include "config.common.php";
include_once "classes.php";
include_once "functions.db_connect.php";

function cleanUsername($str) {
	//error_log($str);
	$str = str_replace("\xe2\x82\xac","",$str);
	return preg_replace('/[,$|\s!"#�%&\/()\[\]=?\'\*\`\^\~\<\>\\\\]/', "", $str);
}

function cleanName($str) {
	//error_log($str);
	$str = str_replace("\xe2\x82\xac","",$str);
	return preg_replace('/[$|#�%\/()\[\]=?\*\`\^\~\<\>\\\\]/', "", $str);
}

function addUser($u) {
  global $dbtbl_person, $mysqli;
  $sql = "insert into ".$dbtbl_person." (username,first_name,last_name,email_address) values('".mysqli_real_escape_string($mysqli,$u->username)."','".mysqli_real_escape_string($mysqli,$u->first_name)."','".mysqli_real_escape_string($mysqli,$u->last_name)."','".mysqli_real_escape_string($mysqli,$u->email_address)."')";
//   error_log($sql);
  $r = dbq($sql);
  if( $person_id = mysqli_insert_id($mysqli) ) {
    return $person_id;
    }
  else return 0;
}

function delUser($uid) {
  global $dbtbl_person;
  $sql = "update ".$dbtbl_person." set active=0 where person_id=".$uid;
  dbq($sql);
}

function addLabPersonnel($lab_id,$person_id,$auth_level_id,$active,$start_date_timestamp,$default_flag) {
  global $dbtbl_laboratory_personnel;
  $sql = "insert into ".$dbtbl_laboratory_personnel." (lab_id,person_id,auth_level_id,active,start_date_timestamp,default_flag) values(".$lab_id.",".$person_id.",".$auth_level_id.",'".$active."',".$start_date_timestamp.",'".$default_flag."')";
	//error_log($sql);
  dbq($sql);
}

function delLabPersonnel($lab_id,$person_id) {
	global $dbtbl_laboratory_personnel;
	$sql = "delete from ".$dbtbl_laboratory_personnel." where person_id=".$person_id." and lab_id=".$lab_id;
	//error_log($sql);
	dbq($sql);
}

function getUserLabAccess($person_id) {
  global $dbtbl_laboratory_personnel;
  $retarr = array();
//   $sql = "select * from ".$dbtbl_laboratory_personnel." where person_id=".$person_id." and active='Y' order by lab_id asc";
  $sql = "select * from ".$dbtbl_laboratory_personnel." where person_id=".$person_id." order by lab_id asc";
//   error_log($sql);
  $r = dbq($sql);
  while( $la = $r->fetch_object() ) {
    $retarr[$la->lab_id] = $la;
  }
  return $retarr;
}

function authUser($username, $clr_passwd) {
  global $auth_types, $ad_domain, $ad_domain_controllers,$ad_base_dn;
  $enc_passwd = hash("sha256", $clr_passwd );
  
  $sql = "select * from person where username = '".$username."' limit 1";
// 	error_log($sql);  
  $r = dbq($sql);
  $u = $r->fetch_object();
  
  if($r->num_rows==1) { // username actually exists so try authenticate it
    foreach( $auth_types as $auth_type ) {
      //error_log("auth attempt to ".$auth_type);
      
      if( ($auth_type=="local") && ($u->passwd == $enc_passwd ) ) {
				$u->login_method = $auth_type;
				return $u; //->person_id;
				}
      
      if( $auth_type=="adldap" ) {
				include("adLDAP/src/adLDAP.php");
				try {
							$options = array("base_dn"=>$ad_base_dn,"use_tls"=>false,"use_ssl"=>false,"account_suffix"=>"@".$ad_domain, "domain_controllers"=>$ad_domain_controllers);
							$adldap = new adLDAP($options);

							if( $adldap->authenticate($username,$clr_passwd) ) { $u->login_method = $auth_type; return $u; }
					}
				catch (adLDAPException $e) {
			// 	      echo $e;
					error_log("auth attempt to ".$auth_type." exception for user '".$username."': ".$e);
			// 	      exit();  
					return null;
					}
      }
    }
  }
  
  else {
  error_log("authentication attempt for ".$username." failed!");
  return null;
  }
}

function changeUserPasswd($username, $enc_passwd) {
  global $dbtbl_person;
  $sql = "update ".$dbtbl_person." set passwd='".$enc_passwd."' where username='".$username."'";
  error_log($sql);
  dbq($sql);
}

function changeUserPasswdByID($person_id, $enc_passwd) {
  global $dbtbl_person;
  $sql = "update ".$dbtbl_person." set passwd='".$enc_passwd."' where person_id=".$person_id;
  error_log($sql);
  dbq($sql);
}

function getUserLang($person_id) {
  global $site_default_lang;
  $sql = "select lang from person_lang where person_id=".$person_id;
  // error_log($sql);
  // $res = mysql_query($sql);
  $r = dbq($sql);
  if( ! $r ) return $site_default_lang;
  
  // $l = mysql_fetch_object($res);
  $l = $r->fetch_object();
  if( $l->lang != "" ) return $l->lang;
  else return $site_default_lang;
}


function getUserLab($person_id) {
  global $dbtbl_laboratory_personnel;
  $sql = "select lab_id from ".$dbtbl_laboratory_personnel." where person_id=".$person_id." and default_flag='Y'";
  // error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows != 1 ) return null;
  
  $l = $r->fetch_object();
  return $l->lab_id;
//   if( $l->lang != "" ) return $l->lang;
//   else return $site_default_lang;
}

function setUserDefaultLab($person_id, $lab_id) {
  global $dbtbl_laboratory_personnel;
  $sql = "update ".$dbtbl_laboratory_personnel." set default_flag='N' where person_id=".$person_id;
//   error_log($sql);
  dbq($sql);
  $sql = "update ".$dbtbl_laboratory_personnel." set default_flag='Y' where person_id=".$person_id." and lab_id=".$lab_id;
//   error_log($sql);
  dbq($sql);
}

function getActiveUsers($order_by="last_name") {
  global $dbtbl_person;
  $retarr = array();
  $sql = "select * from ".$dbtbl_person." where active=1 order by ".$order_by." limit 1000";
//   error_log($sql);
  $r = dbq($sql);
  while( $u = $r->fetch_object('LimsUser') ) {
    $retarr[] = $u;
  }
  return $retarr;
}

function getAllUsers($order_by="last_name") {
  global $dbtbl_person;
  $retarr = array();
  $sql = "select * from ".$dbtbl_person." order by ".$order_by." limit 1000";
//   error_log($sql);
  $r = dbq($sql);
  while( $u = $r->fetch_object('LimsUser') ) {
    $retarr[] = $u;
  }
  return $retarr;
}

function getAllUsersInLab($lab_id, $order_by="last_name") {
	global $dbtbl_person;
	global $dbtbl_laboratory_personnel;
	$retarr = array();
	$sql = "select * from ".$dbtbl_person." where person_id in (select person_id from ".$dbtbl_laboratory_personnel." where lab_id=".$lab_id.") order by ".$order_by."";
	//error_log($sql);
  $r = dbq($sql);
  while( $u = $r->fetch_object('LimsUser') ) {
    $retarr[$u->person_id] = $u;
  }
  return $retarr;
}

function getUserDetails($person_id) {
  global $dbtbl_person;
  $sql = "select * from ".$dbtbl_person." where person_id = ".$person_id." limit 1";
  //error_log($sql);
  $r = dbq($sql);
	if( $r->num_rows == 1 ) return $r->fetch_object('LimsUser');
  //$u = $r->fetch_object('LimsUser');
  //return $u;
	else return null;
}

function getUserFromUname($username) {
  global $dbtbl_person;
  $sql = "select * from ".$dbtbl_person." where username like '".$username."' limit 1";
  //error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows == 1 ) {
    $u = $r->fetch_object('LimsUser');
    return $u;
    }
  else
    return null;
}



function updateUserDetails($u) {
  global $dbtbl_person;
  //global $dblink;
	global $mysqli;
  $sql = "update ".$dbtbl_person." set username='".mysqli_real_escape_string($mysqli,$u->username)."', first_name='".mysqli_real_escape_string($mysqli,$u->first_name)."', last_name='".mysqli_real_escape_string($mysqli,$u->last_name)."', email_address='".mysqli_real_escape_string($mysqli,$u->email_address)."', phone='".mysqli_real_escape_string($mysqli,$u->phone)."', active=".$u->active." where person_id='".$u->person_id."'";
  //error_log($sql);
  // mysql_query($sql);
  $r = dbq($sql);
//   var_dump($r);
  // $affrows = mysql_affected_rows($dblink);
  // error_log("affected rows=".$affrows);
  // if( $affrows != 1 ) return false;
  // else return true;
  //$_SESSION['lims']['first_name'] = $u->first_name;
  //$_SESSION['lims']['last_name'] = $u->last_name;
  //$_SESSION['lims']['email_address'] = $u->email_address;
}


function getUserDateFormat($person_id) {
  global $dblink;
  global $site_default_dateformat;
  
  if(!$person_id) return $site_default_dateformat;
  
  $sql = "select * from person_datetime where person_id=".$person_id." limit 1";
//   error_log($sql);
  $r = dbq($sql);
  if($r->num_rows==1) return $r->fetch_object()->date_format_id;
  else return $site_default_dateformat;
  
}

function setUserDateTimeFormat($person_id,$dformat,$tformat) {
//   global $dblink;
//   global $site_default_timeformat;
  $ret = 0;
  
  $r = dbq("select * from person_datetime where person_id=".$person_id." limit 1");
  if( $r->num_rows == 1 ) {
    $sql = "update person_datetime set date_format_id=".$dformat.", time_format_id=".$tformat." where person_id=".$person_id;
//     error_log($sql);
    dbq($sql );
//     error_log("r2 affected_rows ".mysqli_affected_rows($dblink) );
//     if( mysqli_affected_rows($dblink) == 1 ) 
    $ret = 1;
    }
  else {
    $sql = "insert into person_datetime(person_id,date_format_id,time_format_id) values(".$person_id.",".$dformat.",".$tformat.")";
//     error_log($sql);
    dbq($sql);
//     error_log("r2 affected_rows ".mysqli_affected_rows($dblink) );
//     if( mysqli_affected_rows($dblink) == 1 ) 
    $ret = 1;
  }
  
  $_SESSION['lims']['dateformat'] = $dformat;
  
  return $ret;
}

function addUserAlert($person_id, $lang_index, $ref_id) {
  global $dbtbl_alerts;
  $sql = "insert into ".$dbtbl_alerts."(person_id,lang_index,timestamp,ref_id) values(".$person_id.",'".$lang_index."',".time().",".$ref_id.")";
  dbq($sql);
}

function getUserAlerts($person_id) {
  global $dbtbl_alerts;
  $retarr = array();
  $sql = "select * from ".$dbtbl_alerts." where person_id=".$person_id." and ack=0 order by timestamp desc";
//   error_log($sql);
  $r = dbq($sql);
  while( $a = $r->fetch_object('UserAlert') ) {
    $retarr[] = $a;
  }
  return $retarr;
}

function getUserAlertsAll($person_id) {
  global $dbtbl_alerts;
  $retarr = array();
  $sql = "select * from ".$dbtbl_alerts." where person_id=".$person_id." order by timestamp desc";
//   error_log($sql);
  $r = dbq($sql);
  while( $a = $r->fetch_object('UserAlert') ) {
    $retarr[] = $a;
  }
  return $retarr;
}

function ackUserAlert($person_id, $alert_id) {
  global $dbtbl_alerts;
  $sql = "update ".$dbtbl_alerts." set ack=1 where person_id=".$person_id." and alert_id=".$alert_id;
//   error_log($sql);
  dbq($sql);
}

function getAuthLevels() {
	return $_SESSION['lims']['langdata']['auth_levels'];
	/*
  global $dbtbl_auth_levels, $max_mysql_results;
  $retarr = array();
  $sql = "select * from ".$dbtbl_auth_levels." order by descr limit ".$max_mysql_results;
  $r = dbq($sql);
  while( $a = $r->fetch_object() ) {
    $retarr[$a->auth_level_id] = $a;
  }
  return $retarr;
	*/
}
function getAuthLevel2($person_id, $lab_id) {
  global $dbtbl_laboratory_personnel;
  $sql = "select auth_level_id from ".$dbtbl_laboratory_personnel." where person_id=".$person_id." and lab_id=".$lab_id;
	//error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows==1) {
    $a = $r->fetch_object();
    return $a;
  }
  else
    return null;
}

function getAuthLevelHighest($person_id) {
  global $dbtbl_laboratory_personnel;
  $sql = "select auth_level_id from ".$dbtbl_laboratory_personnel." where person_id=".$person_id." order by auth_level_id DESC limit 1";
	//error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows==1) {
    $a = $r->fetch_object();
    return $a;
  }
  else
    return null;
}

/*
function getAuthLevel($person_id, $lab_id) {
  global $dbtbl_auth_levels, $dbtbl_laboratory_personnel;
  $retarr = array();
  $sql = "select * from ".$dbtbl_auth_levels." where auth_level_id in (select auth_level_id from ".$dbtbl_laboratory_personnel." where person_id=".$person_id." and lab_id=".$lab_id.")";
  $r = dbq($sql);
  if( $r->num_rows==1) {
    $a = $r->fetch_object();
    return $a;
  }
  else
    return null;
}
*/
?>