<?php
include_once "constants.php";
include "config.common.php";
include_once "classes.php";
include_once "functions.db_connect.php";


// TODO: ADD mysqli_real_escape_string($mysqli, to all inserts!

function delContainer($cid) {
  global $dbtbl_container_types;
  $sql = "update ".$dbtbl_container_types." set active=0 where id=".$cid;
  dbq($sql);
}

function updateContainer($c) {
  global $dbtbl_container_types;
  global $mysqli;
  $sql = "update ".$dbtbl_container_types." set container_type='".mysqli_real_escape_string($mysqli,$c->container_type)."', capacity=".$c->capacity.", number_of_rows=".$c->number_of_rows.",number_of_columns=".$c->number_of_columns." where id=".$c->id;
//   error_log($sql);
  dbq($sql);
}

function addContainer($newc) {
  global $dbtbl_container_types;
  global $mysqli;
  $sql = "insert into ".$dbtbl_container_types."(container_type,capacity,number_of_rows,number_of_columns) values('".mysqli_real_escape_string($mysqli,$newc->container_type)."',".$newc->capacity.",".$newc->number_of_rows.",".$newc->number_of_columns.")";
//   error_log($sql);
  dbq($sql);
}

function getContainerType($id) {
  global $dbtbl_container_types;
  global $max_mysql_results;
  $retarr = array();
  
  $sql = "select * from ".$dbtbl_container_types." where id=".$id;
	//error_log($sql);
  $r = dbq($sql);
  return $r->fetch_object('Container');
//   while( $c = $r->fetch_object('Container') ) {
// 	  $retarr[$c->id] = $c;
//   }
//   return $retarr;
}

function getContainerTypeFromSampleID($sid) {
  global $dbtbl_container_types, $dbtbl_carton, $dbtbl_carton_sample;
  global $max_mysql_results;
  $retarr = array();
  
//   $sql = "select * from ".$dbtbl_container_types." where id=".$id;
  $sql = "select * from ".$dbtbl_container_types." where id in (select container_type_id from ".$dbtbl_carton." where carton_id=(select carton_id from ".$dbtbl_carton_sample." where sample_id=".$sid." ) ) limit 1";
  //error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object('Container');
  else
    return null;
//   while( $c = $r->fetch_object('Container') ) {
// 	  $retarr[$c->id] = $c;
//   }
//   return $retarr;
}

function getContainerTypes() {
  global $dbtbl_container_types;
  global $max_mysql_results;
  $retarr = array();
  
  $sql = "select * from ".$dbtbl_container_types." where active=1 order by capacity desc limit ".$max_mysql_results;
//   error_log($sql);
  $r = dbq($sql);
  while( $c = $r->fetch_object('Container') ) {
	  $retarr[$c->id] = $c;
  }
  return $retarr;
}

function containerTypeInUse($container_type_id) {
	global $dbtbl_carton;
	// 1 usage is sufficient to count as 'in use'
	$sql = "select carton_id from ".$dbtbl_carton." where container_type_id=".$container_type_id." limit 1";
	$r = dbq($sql);
	if( $r->num_rows == 1 ) return true;
	else return false;
}

function getContainerStats($cid, $laboratory_id) {
	//if(!is_int((int)$cid)) {error_log("non int passed to getContainerStats(): ".$cid); return null;}
	global $dbtbl_carton_sample;
	$carton = getCarton($cid, $laboratory_id);
	$ctype = getContainerType($carton->container_type_id);
	$cap_total = $ctype->number_of_rows*$ctype->number_of_columns;
	//error_log("cap_total ".$cap_total);
	if($cap_total==0) return null;
	$sql = "select count(sample_id) as cnt from ".$dbtbl_carton_sample." where carton_id=".$cid;
	$r = dbq($sql);
	$stats = new Statistic();
	if( $r->num_rows == 1 ) {
		$cnt = $r->fetch_object();
		//error_log("cnt ".$cnt->cnt);
		$stats->average = (float)(($cnt->cnt/$cap_total)*100.0);
		//error_log($stats->average);
		}
	return $stats;
}


function storageDeviceInUse($sd_id) {
	global $dbtbl_carton;
	// 1 usage is sufficient to count as 'in use'
	$sql = "select carton_id from ".$dbtbl_carton." where storage_id=".$sd_id." limit 1";
	$r = dbq($sql);
	if( $r->num_rows == 1 ) return true;
	else return false;
}

function delStorageDevice($sd_id) {
  global $dbtbl_storage_device;
  $sql = "delete from ".$dbtbl_storage_device." where storage_id=".$sd_id;
  dbq($sql);
}

function updateStorageDevice($sd) {
  global $dbtbl_storage_device;
  global $mysqli;
  $sql = "update ".$dbtbl_storage_device." set storage_type_id=".$sd->storage_type_id.", laboratory_id=".$sd->laboratory_id.", identifier='".mysqli_real_escape_string($mysqli,$sd->identifier)."' where storage_id=".$sd->storage_id;
//   error_log($sql);
  dbq($sql);
}

function addStorageDevice($sd) {
  global $dbtbl_storage_device;
  global $mysqli;
  $sql = "insert into ".$dbtbl_storage_device."(environment_description,storage_type_id,laboratory_id,identifier) values('".
    mysqli_real_escape_string($mysqli,$sd->environment_description)."',".
    $sd->storage_type_id.",".
    $sd->laboratory_id.",".
    "'".mysqli_real_escape_string($mysqli,$sd->identifier)."'".
    ")";
//   error_log($sql);
  dbq($sql);
  
}

function getStorageDevices() {
	global $dbtbl_storage_device, $max_mysql_results;
	$retarr = array();
	$sql = "select * from ".$dbtbl_storage_device." order by identifier asc limit ".$max_mysql_results;
// 	error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	while( $d = $r->fetch_object('StorageDevice') ) {
		$retarr[$d->storage_id] = $d;
	}
	return $retarr;
}

function getStorageDevicesForLab($lab_id=0) {
	global $dbtbl_storage_device, $max_mysql_results;
	$retarr = array();
	if( $lab_id == 0 ) return $retarr;
	
	$sql = "select * from ".$dbtbl_storage_device." where laboratory_id=".$lab_id." order by identifier limit ".$max_mysql_results;
// 	error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	while( $d = $r->fetch_object('StorageDevice') ) {
		$retarr[$d->storage_id] = $d;
	}
	return $retarr;
}

function getStorageShelves($storage_id) {
  global $dbtbl_storage_shelf, $max_mysql_results;
  $retarr = array();
  $sql = "select * from ".$dbtbl_storage_shelf." where storage_id=".$storage_id." and active=1 order by shelf_number limit ".$max_mysql_results;
//   error_log($sql);
  $r = dbq($sql);
  while( $s = $r->fetch_object('StorageShelf') ) {
		if( storageShelfInUse($s->id) ) $s->occupied = 1;
	  $retarr[$s->id] = $s;
  }
  return $retarr;
}

function getStorageShef($shelf_id) {

}
 
function getStorageShelvesAll() {
  global $dbtbl_storage_shelf, $max_mysql_results;
  $retarr = array();
  $sql = "select * from ".$dbtbl_storage_shelf." order by id";
//   error_log($sql);
  $r = dbq($sql);
  while( $s = $r->fetch_object('StorageShelf') ) {
	  $retarr[$s->id] = $s;
  }
  return $retarr;
}

function addStorageShelf($shelf) {
  global $dbtbl_storage_shelf;
  global $mysqli;
  
  // don't allow same shelf name in same storage device
  $sql = "select id from ".$dbtbl_storage_shelf." where active=1 and shelf_number like '".$shelf->shelf_number."' and storage_id=".$shelf->storage_id;
//   error_log($sql);
  $r = dbq($sql);
  if( $s = $r->fetch_object('StorageShelf') ) {
// 	  $retarr[$d->id] = $d;
    //error_log("Not adding duplicate shelf ".$shelf->shelf_number." to storage_id ".$shelf->storage_id);
    return;
  }
  
  $sql = "insert into ".$dbtbl_storage_shelf."(shelf_number,storage_id,active) values('".mysqli_real_escape_string($mysqli,$shelf->shelf_number)."',".$shelf->storage_id.",1)";
//   error_log($sql);
  dbq($sql);
}

function delStorageShelf($shelf_id) {
  global $dbtbl_storage_shelf;
  $sql = "update ".$dbtbl_storage_shelf." set active=0 where id=".$shelf_id;
//   error_log($sql);
  dbq($sql);
}

function storageShelfInUse($shelf_id) {
	global $dbtbl_carton;
	// 1 usage is sufficient to count as 'in use'
	$sql = "select carton_id from ".$dbtbl_carton." where shelf_id=".$shelf_id." limit 1";
	//error_log($sql);
	$r = dbq($sql);
	if( $r->num_rows == 1 ) return true;
	else return false;
}

function getStorageTypes() {
	global $dbtbl_storage_type, $max_mysql_results;
	$retarr = array();
	$sql = "select * from ".$dbtbl_storage_type." order by storage_type limit ".$max_mysql_results;
// 	error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	while( $d = $r->fetch_object('StorageType') ) {
		$retarr[$d->id] = $d;
	}
	return $retarr;
}

function getSkills() {
	global $dbtbl_skills;
	$retarr = array();
	$sql = "select * from ".$dbtbl_skills." order by skill";
	// error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	// while( $s = mysqli_fetch_object($res, 'Skill') ) {
	while( $s = $r->fetch_object('Skill') ) {
		$retarr[] = $s;
	}
	return $retarr;
}

function addSkill($s) {
	global $dbtbl_skills;
	global $mysqli;
	$sql = "insert into ".$dbtbl_skills."(skill) values('".mysqli_real_escape_string($mysqli,$s)."')";
	// error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function delSkill($id) {
	global $dbtbl_skills;
	$sql = "delete from ".$dbtbl_skills." where id=".$id;
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function updateSkill($id, $s) {
	global $dbtbl_skills;
	global $mysqli;
	$sql = "update ".$dbtbl_skills." set skill='".mysqli_real_escape_string($mysqli,$s)."' where id=".$id;
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}




///////////// SAMPLES
function getSampleType($stid) {
  global $dbtbl_sample_type;
  $sql = "select * from ".$dbtbl_sample_type." where id=".$stid." limit 1";
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object("SampleType");
  else
    return null;
}

function getSampleTypes() {
	global $dbtbl_sample_type;
	$retarr = array();
	$sql = "select * from ".$dbtbl_sample_type." where active=1 order by sample_type ASC";
// 	error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	// while( $s = mysqli_fetch_object($res, 'Skill') ) {
	while( $s = $r->fetch_object('SampleType') ) {
		$retarr[$s->id] = $s;
	}
	return $retarr;
}

function addSampleType($s) {
	global $dbtbl_sample_type;
	$sql = "insert into ".$dbtbl_sample_type."(sample_type,active) values('".$s."',1)";
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function delSampleType($id) {
	global $dbtbl_sample_type;
	$sql = "update ".$dbtbl_sample_type." set active=0 where id=".$id;
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function updateSampleType($id, $s) {
	global $dbtbl_sample_type;
	$sql = "update ".$dbtbl_sample_type." set sample_type='".$s."' where id=".$id;
	//error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}


// function addCartonSample(
function getCartonSamples($carton_id) {
  global $dbtbl_carton_sample;
  global $max_mysql_results;
  $retarr = array(array());
  $sql = "select * from ".$dbtbl_carton_sample." where carton_id=".$carton_id." order by row_num desc,column_num desc limit ".$max_mysql_results;
//   error_log($sql);
  $r = dbq($sql);
  while( $s = $r->fetch_object('CartonSample') ) {
// 	  $retarr[$s->carton_sample_id] = $s;
    $retarr[$s->row_num][$s->column_num] = $s;
  }
  return $retarr;
}

function getCartonSampleFromSID($sid) { // from sample_id
  global $dbtbl_carton_sample;
  $sql = "select * from ".$dbtbl_carton_sample." where sample_id=".$sid." limit 1";
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object("CartonSample");
  else
    return null;
}

function getCartonSampleFromRC($r, $c, $carton_id) { // from row & col
  global $dbtbl_carton_sample;
  $sql = "select * from ".$dbtbl_carton_sample." where row_num=".$r." and column_num=".$c." and carton_id=".$carton_id." limit 1";
//   error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object("CartonSample");
  else
    return null;
}

function addCartonSample($cs) {
  global $dbtbl_carton_sample;
  global $mysqli;
  $sql = "insert into ".$dbtbl_carton_sample."(sample_id,row_num,column_num,carton_id,active) values(".$cs->sample_id.",".$cs->row_num.",".$cs->column_num.",".$cs->carton_id.",'".$cs->active."')";
//   error_log($sql);
  dbq($sql);
  if( $csid = mysqli_insert_id($mysqli) ) {
    return $csid;
    }
  else return null;
}

function updateCartonSample($cs) {
  global $dbtbl_carton_sample;
  global $mysqli;
	$setstr = sprintf(AUDIT_CARTON_SAMPLE_MOD_FORMAT,$cs->sample_id,$cs->row_num,$cs->column_num,$cs->carton_id,$cs->active);
	//$auditstr = sprintf(AUDIT_CARTON_SAMPLE_MOD_FORMAT,$cs->sample_id,$cs->row_num,$cs->column_num,$cs->carton_id,$cs->active);
	//$setstr = "sample_id=".$cs->sample_id.",row_num=".$cs->row_num.",column_num=".$cs->column_num.",carton_id=".$cs->carton_id.",active='".$cs->active."'";
	
  $sql = "update ".$dbtbl_carton_sample." set ".$setstr." where carton_sample_id=".$cs->carton_sample_id;
  error_log($sql);
  dbq($sql);
	$num_rows = mysqli_affected_rows($mysqli);
	//auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_CARTON_SAMPLE,AUDIT_SAMPLE_MOD,$cs->sample_id,$auditstr,time());
	if($num_rows>0)
		auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_CARTON_SAMPLE,AUDIT_ACTION_CARTON_SAMPLE_MOD,$cs->sample_id,$cs,time());
  return $num_rows;
}

function addSample($s) {
  global $dbtbl_sample;
  global $mysqli;
  $sql = "insert into ".$dbtbl_sample."(identification,sample_type_id) values('".mysqli_real_escape_string($mysqli,$s->identification)."',".$s->sample_type_id.")";
//   error_log($sql);
  dbq($sql);
  if( $sid = mysqli_insert_id($mysqli) ) {
		$s->sample_id = $sid;
		auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_SAMPLE,AUDIT_ACTION_SAMPLE_CREATED,$sid,$s,time());
    return $sid;
    }
  else return null;
//   return $sid;
}

function getSample($sid) {
  global $dbtbl_sample;
  $sql = "select * from ".$dbtbl_sample." where sample_id=".$sid." limit 1";
//   error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object('Sample');
  else
    return null;
}

function updateSample($s) {
  global $dbtbl_sample;
  global $mysqli;
	//include "constants.audit.php";
  $setstr = "sample_type_id=".$s->sample_type_id.",identification='".mysqli_real_escape_string($mysqli,$s->identification).
  "',concentration_units_id=".$s->concentration_units_id.",concentration=".$s->concentration.
  ",comments='".mysqli_real_escape_string($mysqli,$s->comments)."',control_flag='".$s->control_flag.
  "',exhausted_flag='".$s->exhausted_flag."',volume=".$s->volume.",volume_units_id=".$s->volume_units_id;
  $sql = "update ".$dbtbl_sample." set ".$setstr." where sample_id=".$s->sample_id;
  //error_log($sql);
  $r = dbq($sql);
  $num_rows = mysqli_affected_rows($mysqli);
  //auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_SAMPLE,AUDIT_SAMPLE_MOD,$s->sample_id,$setstr,time());
	if($num_rows>0)
		auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_SAMPLE,AUDIT_ACTION_SAMPLE_MOD,$s->sample_id,$s,time());
  return $num_rows;
}
//////////////////////////






function getDomain($id) {
	global $dbtbl_domains;
	// $d = new Domain();
	$sql = "select * from ".$dbtbl_domains." where id=".$id." limit 1";
	// error_log($sql);
	// if( !$res = mysqli_query($sql) ) {
		// return null;
	// }
	$r = dbq($sql);
	// $d = mysqli_fetch_object($res, 'Domain');
	$d = $r->fetch_object('Domain');
	return $d;
}

function getDomains() {
	global $dbtbl_domains, $max_domains;
	$retarr = array();
	$sql = "select * from ".$dbtbl_domains." order by domain_name limit ".$max_domains;
	// error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	while( $d = $r->fetch_object('Domain') ) {
		$retarr[] = $d;
	}
	return $retarr;
}

function addDomain($d) {
	global $dbtbl_domains;
	global $mysqli;
	$sql = "insert into ".$dbtbl_domains."(domain_name) values('".mysqli_real_escape_string($mysqli,$d)."')";
	// if( !mysqli_query($sql) ) {
	if( !$r = dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function delDomain($id) {
    // first check if domain is in use by a lab!
	global $dbtbl_domains;
	$sql = "delete from ".$dbtbl_domains." where id=".$id;
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function updateDomain($id, $d) {
	global $dbtbl_domains;
	global $mysqli;
	$sql = "update ".$dbtbl_domains." set domain_name='".mysqli_real_escape_string($mysqli,$d)."' where id=".$id;
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function getLabs() {
  // return an array of all labs
  global $dbtbl_labs, $max_labs;
  $retarr = array();
  $sql = "select * from ".$dbtbl_labs." order by name limit ".$max_labs;
  // error_log($sql);
  $r = dbq($sql);
  // $u = $r->fetch_object();
  
  while( $l = $r->fetch_object('Lab') ) {
	$d = getDomain($l->domain_id);
	$l->domain_name = $d->domain_name;
	$retarr[$l->id] = $l;
	}
  return $retarr;

}


function getLab($lab_id) {
  global $dbtbl_labs;
  $sql = "select * from ".$dbtbl_labs." where id=".$lab_id." limit 1";
   //error_log($sql);
  $r = dbq($sql);
	if( $r->num_rows == 1 )
		return $r->fetch_object('Lab');
  //return $l;
	else
		return null;
}


function addLab($l) {
	global $dbtbl_labs;
	$sql = "insert into ".$dbtbl_labs."(laboratory_id, domain_id, name) values('".$l->id."','".$l->domain_id."','".$l->name."')";
	// error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
	
}

function delLab($lab_id) {
	// MUST CHECK IF LAB IS IN USE!!! or use mysql foreign keys
	global $dbtbl_labs;
	$sql = "delete from ".$dbtbl_labs." where id=".$lab_id;
	// error_log($sql);
	if( ! dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function updateLab($l) {
	global $dbtbl_labs;
	$sql = "update ".$dbtbl_labs." set laboratory_id='".$l->name."', name='".$l->name."', domain_id=".$l->domain_id." where id=".$l->id;
	// error_log($sql);
	if( ! dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function getUnits() {
  global $dbtbl_units;
  $retarr = array();
  
  $sql = "select * from ".$dbtbl_units;
//   error_log($sql);
  $r = dbq($sql);
  while( $u = $r->fetch_object('Unit') ) {
	$retarr[$u->id] = $u;
	}
  return $retarr;
}

function updateCarton($c) {
  global $dbtbl_carton, $mysqli;
	//include "constants.audit.php";
	$setstr = sprintf(CARTON_MOD_FORMAT,mysqli_real_escape_string($mysqli,$c->carton_identifier),$c->create_date_timestamp,$c->storage_id,$c->shelf_id,mysqli_real_escape_string($mysqli,$c->internal_barcode),$c->owner_id,$c->allow_details_edit,$c->allow_contents_edit);
	//error_log($setstr);
	
	//$auditstr = sprintf(AUDIT_CARTON_MOD_FORMAT,mysqli_real_escape_string($mysqli,$c->carton_identifier),$c->create_date_timestamp,$c->storage_id,$c->shelf_id,mysqli_real_escape_string($mysqli,$c->internal_barcode),$c->owner_id);
	//error_log("updateCarton ".$auditstr);
	
  /*$setstr = "carton_identifier='".mysqli_real_escape_string($mysqli,$c->carton_identifier)."',".
    "create_date_timestamp=".$c->create_date_timestamp.",".
    "storage_id=".$c->storage_id.",".
    "shelf_id=".$c->shelf_id.",".
    "internal_barcode='".mysqli_real_escape_string($mysqli,$c->internal_barcode)."',".
		"owner_id=".$c->owner_id."";*/
  $sql = "update ".$dbtbl_carton." set ".$setstr." where carton_id=".$c->carton_id;
   //error_log($sql);
  dbq($sql);
  auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_CARTON,AUDIT_ACTION_CARTON_MOD,$c->carton_id,$c,time() );
  return 1;
}

function getCarton($cid, $laboratory_id) {
  global $dbtbl_carton;
	global $dbtbl_storage_device;
//   $retarr = array();
  
  $sql = "select * from ".$dbtbl_carton." where carton_id=".$cid." and storage_id in (select storage_id from ".$dbtbl_storage_device." where laboratory_id = ".$laboratory_id." ) limit 1";
   //error_log($sql);
  $r = dbq($sql);
  if( isset($r->num_rows) && ($r->num_rows == 1) )
    return $r->fetch_object('Carton');
  else
    return null;
}

function addCarton($c) {
  global $dbtbl_carton, $mysqli;
	$c->create_date_timestamp = time();
	//$sql = "insert into ".$dbtbl_carton."(container_type_id,carton_identifier,create_date_timestamp,storage_id,internal_barcode,owner_id) values(".$c->container_type_id.",'".mysqli_real_escape_string($mysqli,$c->carton_identifier)."',".$c->create_date_timestamp.",".$c->storage_id.",'000000',".$c->owner_id.")";
	$sql = "insert into ".$dbtbl_carton."(container_type_id,carton_identifier,create_date_timestamp,storage_id,internal_barcode,owner_id) values(".$c->container_type_id.",'".mysqli_real_escape_string($mysqli,$c->carton_identifier)."',".$c->create_date_timestamp.",".$c->storage_id.",'000000',".$c->owner_id.")";
	
   //error_log($sql);
  dbq($sql);
//   $cid = mysqli_insert_id($mysqli);
  if( $cid = mysqli_insert_id($mysqli) ) {
    // set a temporary barcode
    dbq("update ".$dbtbl_carton." set internal_barcode='C".$c->create_date_timestamp."-".$cid."' where carton_id=".$cid );
    $c->carton_id = $cid;
    auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_CARTON,AUDIT_ACTION_CARTON_CREATED,$cid,$c,time());
    }
//   return mysqli_insert_id($mysqli);
  return $c;
}

function getCartonFromSID($sid) { // from sample_id
  global $dbtbl_carton, $dbtbl_carton_sample;
  $sql = "select * from ".$dbtbl_carton." where carton_id=(select carton_id from ".$dbtbl_carton_sample." where sample_id=".$sid." limit 1) limit 1";
//   error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object("Carton");
  else
    return null;
}

function getCartons() {
  global $dbtbl_carton;
  $retarr = array();
  
  $sql = "select * from ".$dbtbl_carton;
  $r = dbq($sql);
  while( $c = $r->fetch_object('Carton') ) {
	$retarr[$c->carton_id] = $c;
	}
  return $retarr;
}

function getCartonsForUser($owner_id, $laboratory_id, $limit=20) {
  global $dbtbl_carton;
	global $dbtbl_storage_device;
  $retarr = array();
  
  //$sql = "select * from ".$dbtbl_carton." where laboratory_id=".$laboratory_id." and owner_id=".$owner_id." order by create_date_timestamp desc limit ".$limit;
	$sql = "select * from ".$dbtbl_carton." where storage_id in (select storage_id from ".$dbtbl_storage_device." where laboratory_id = ".$laboratory_id." ) and owner_id=".$owner_id." order by create_date_timestamp desc limit ".$limit;
	//error_log($sql);
  $r = dbq($sql);
  while( $c = $r->fetch_object('Carton') ) {
	$retarr[$c->carton_id] = $c;
	}
  return $retarr;
}


function attrTypeInUse($attr, $attr_id) {
	global $dbtbl_sample, $dbtbl_labs, $dbtbl_person_skills;
	$sql = "";
	if( $attr == "sample_types" ) 
		$sql = "select sample_id from ".$dbtbl_sample." where sample_type_id=".$attr_id." limit 1";
	if( $attr == "groups" )
		$sql = "select id from ".$dbtbl_labs." where domain_id=".$attr_id." limit 1";
	if( $attr == "skills" )
		$sql = "select person_id from ".$dbtbl_person_skills." where skill_id=".$attr_id." limit 1";
	
	//error_log($sql);
	$r = dbq($sql);
	if( $r->num_rows == 1 ) return true;
	else return false;
}


// search functions
function searchCartons($sterm, $laboratory_id) {
  global $dbtbl_carton;
	global $dbtbl_storage_device;
  global $max_mysql_results;
	global $mysqli;
	$sterm = mysqli_real_escape_string($mysqli,$sterm);
  $retarr = array();
  $sql = "select * from ".$dbtbl_carton." where storage_id in (select storage_id from ".$dbtbl_storage_device." where laboratory_id = ".$laboratory_id." ) and (carton_identifier like '%".$sterm."%' or internal_barcode like '%".$sterm."%') limit ".$max_mysql_results;
  $r = dbq($sql);
  while( $c = $r->fetch_object('Carton') ) {
	$retarr[$c->carton_id] = $c;
	}
  return $retarr;
}

function searchSamples($sterm) {
  global $dbtbl_sample;
  global $max_mysql_results;
	global $mysqli;
	$sterm = mysqli_real_escape_string($mysqli,$sterm);
  $retarr = array();
  $sql = "select * from ".$dbtbl_sample." where identification like '%".$sterm."%' or comments like '%".$sterm."%' limit ".$max_mysql_results;
//   error_log($sql);
  $r = dbq($sql);
  while( $s = $r->fetch_object('Sample') ) {
	$retarr[$s->sample_id] = $s;
	}
  return $retarr;
}


function auditOp($person_id,$type,$key,$id,$audit_descr,$timestamp) {
	
  global $dbtbl_audit, $mysqli;
	$audit_descr = json_encode($audit_descr,JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
	error_log("audit_descr: ".$audit_descr);
  $sql = "insert into ".$dbtbl_audit."(person_id,type,audit_key,affected_id,audit_descr,timestamp) values(".$person_id.",".$type.",".$key.",".$id.",'".mysqli_real_escape_string($mysqli,$audit_descr)."',".$timestamp.")";
  //
  dbq($sql);
  
}

function getAuditLog($type,$affected_id,$time_direction="asc",$limit=200) {
	global $dbtbl_audit;
	$retarr = array();
	//error_log($time_direction);
	if(($time_direction!="asc")&&($time_direction!="desc")) $time_direction="asc";
	$sql = "select * from ".$dbtbl_audit." where type=".$type." and affected_id=".$affected_id." order by timestamp ".$time_direction." limit ".$limit;
	//error_log($sql);
	$r = dbq($sql);
  while( $a = $r->fetch_object('Audit') ) {
	$retarr[$a->id] = $a;
	}
  return $retarr;
}
?>