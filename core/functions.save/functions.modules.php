<?php
include "config.common.php";
include_once "classes.php";
include_once "functions.db_connect.php";

function getEnabledModules() {
  global $dbtbl_modules;
  $retarr = array();
  $sql = "select * from ".$dbtbl_modules." where active=1 order by name limit 500";
//   error_log($sql);
  $r = dbq($sql);
  while( $u = $r->fetch_object('Module') ) {
    $retarr[] = $u;
  }
  return $retarr;
}

function delModule($mname) {
  global $dbtbl_modules;
//   $sql = "delete from ".$dbtbl_modules." where name like '".$mname."'";
  $sql = "update ".$dbtbl_modules." set active=0 where name like '".$mname."'";
//   error_log($sql);
  $r = dbq($sql);
  
  return 1;
}

function addModule($mname, $fname) {
  global $dbtbl_modules;
  global $mysqli;
  
  $sql = "select module_id from ".$dbtbl_modules." where name like '".$mname."' limit 1";
  $r = dbq($sql);
  if( $r->num_rows == 1 ) {
    $sql = "update ".$dbtbl_modules." set active=1 where name like '".$mname."'";
//     error_log($sql);
    dbq($sql);
  }
  else {
    $sql = "insert into ".$dbtbl_modules."(name,file,active) values('".mysqli_real_escape_string($mysqli,$mname)."','".mysqli_real_escape_string($mysqli,$fname)."',1)";
//     error_log($sql);
    dbq($sql);
  }
  return 1;
}

function updateModuleUserConf($person_id,$mname,$vals) {
  $modtbl = "module_".$mname."_users";
  
  $sql = "select * from ".$modtbl." where person_id=".$person_id;
  $r = dbq($sql);
  error_log("modtbl=".$r->num_rows);
  
  if( $r->num_rows == 0 ) {
    $sql1 = "insert into ".$modtbl."(person_id,";
    $sql2 = " values(".$_SESSION['lims']['person_id'].",";
    foreach($vals as $k=>$v) {
      $sql1 .= $k.",";
      $sql2 .= "'".$v."',";
    }
    $sql1 = rtrim($sql1,",");
    $sql2 = rtrim($sql2,",");
    $sql1 = $sql1.")";
    $sql2 = $sql2.")";
    dbq($sql1.$sql2);
    return 1;
  }
  
  $sql = "update ".$modtbl." set ";
  foreach($vals as $k=>$v) {
    $sql .= $k."='".$v."', ";
  }
  $sql = rtrim($sql, ", ");
  $sql .= " where person_id=".$_SESSION['lims']['person_id'];
//   error_log($sql);
  if( $r = dbq($sql) ) return 1;
  else return 0;
}
?>