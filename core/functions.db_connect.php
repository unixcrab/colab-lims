<?php
include "config.common.php";
// include $serv_root."/config/db.php";

$mysqli = mysqli_connect($dbHost,$dbUser,$dbPass,$dbDB);

if ( mysqli_connect_error() ) {
    die('Connect Error (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
}

/* change character set to utf8 */
if (!$mysqli->set_charset("utf8")) {
    printf("Error loading character set utf8: %s\n", $mysqli->error);
	}


// query wrapper
function dbq($sql) {
	global $mysqli;
	$r = $mysqli->query($sql);
	// $o = $r->fetch_object();
	// var_dump($r);
	return $r;
}

// result wrapper
function dbr() {

}

function db_get_col($tbl, $col, $where) {
//   global $mysqli;
  $sql = "select ".$col." from ".$tbl." where ".$where." limit 1";
//   error_log($sql);
  $r = dbq($sql);
//   $row = mysqli_fetch_row($r);
  if( $r ) {
    $row = $r->fetch_row();
    return $row[0];
    }
  else return "";
}
?>