<?php

$sterm = $_POST['searchTop'];
if( $sterm == "" ) return;

include "functions.lims.php";
$cres = searchCartons($sterm, $_SESSION['lims']['lab_id']);
$num_cartons = count($cres);

$sres = searchSamples($sterm);
$num_samples = count($sres);

// redirect if only 1 found and exact match
if( ($num_cartons==1) && ($num_samples==0) ) {
	reset($cres);
	$first_key = key($cres);
	//error_log("REDIRECT TO CARTON: index.php?p=inv&sp=cartons.mine&s=edit&cid=".$first_key);
	print "<script>window.location = 'index.php?p=inv&sp=cartons.mine&s=edit&cid=".$first_key."'</script>";
}

if( ($num_cartons==0) && ($num_samples==1) ) {
	reset($sres);
	$first_key = key($sres);
	//error_log("REDIRECT TO SAMPLE");
	print "<script>window.location = 'index.php?p=inv&sp=sample.open&s=edit&sid=".$first_key."'</script>";
}

if( $num_cartons > 0 ) {
		$lab_sdevices = getStorageDevicesForLab($_SESSION['lims']['lab_id']);
		$lab_users = getAllUsersInLab($_SESSION['lims']['lab_id']);
		print "<h4>".$_SESSION['lims']['langdata']['cartons'].": ".$num_cartons."</h4>";
		?>
		<table id='cartonResTable' class='table table-hover table-condensed tablesorter'>
		<thead>
		<tr><th><?php print $_SESSION['lims']['langdata']['carton']; ?></th><th><?php print $_SESSION['lims']['langdata']['barcode']; ?></th><th><?php print $_SESSION['lims']['langdata']['limsadmin_storage_device']; ?></th><th><?php print $_SESSION['lims']['langdata']['owner']; ?></th></tr>
		</thead>
		<tbody>
		<?php
		foreach($cres as $c) {
			$carton_identifier = myhtmlentities($c->carton_identifier);
			$internal_barcode = myhtmlentities($c->internal_barcode);
			$storage_name = "-";
			if( isset( $lab_sdevices[$c->storage_id] ) ) $storage_name = myhtmlentities($lab_sdevices[$c->storage_id]->identifier);
			$owner_name = "-";
			if( isset( $lab_users[$c->owner_id] ) ) $owner_name = myhtmlentities($lab_users[$c->owner_id]->first_name." ".$lab_users[$c->owner_id]->last_name);
			$carton_identifier = str_replace($sterm,"<strong>".$sterm."</strong>", $carton_identifier);
			$internal_barcode = str_replace($sterm,"<strong>".$sterm."</strong>", $internal_barcode);
			print "<tr><td><a href='index.php?p=inv&sp=cartons.mine&s=edit&cid=".$c->carton_id."'>".$carton_identifier."</td><td>".$internal_barcode."</td><td>".$storage_name."</td><td>".$owner_name."</td></tr>\n";
		}
		?>
		</tbody>
		</table>
		<?php
}
?>
<hr>
<?php
//$sres = searchSamples($sterm);
//$num_samples = count($sres);

if( $num_samples > 0 ) {
	print "<h4>".$_SESSION['lims']['langdata']['samples'].": ".$num_samples."</h4>";
  ?>
	<table id='sampleResTable' class='table table-hover table-condensed tablesorter'>
	<thead>
	<tr><th><?php print $_SESSION['lims']['langdata']['sample']; ?></th><th><?php print $_SESSION['lims']['langdata']['type']; ?></th><th><?php print $_SESSION['lims']['langdata']['carton']; ?></th><th><?php print $_SESSION['lims']['langdata']['location']; ?></th></tr>
	</thead>
	<tbody>
	<?php
		$c_uri = "index.php?p=inv&sp=cartons.mine&s=edit&cid=";
		foreach($sres as $s) {
			//error_log("s->sample_type_id=".$s->sample_type_id);
			 $stype = getSampleType($s->sample_type_id);
			 $sample_type = "-";
			 if( isset($stype->sample_type) ) $sample_type = $stype->sample_type;
			 $cs = getCartonSampleFromSID($s->sample_id);
			 if( $cs == null ) $cs = new CartonSample();
			 $carton = getCarton($cs->carton_id, $_SESSION['lims']['lab_id']);
			 if( $carton == null ) $carton = new Carton();
			 $ident = str_replace($sterm,"<strong>".myhtmlentities($sterm)."</strong>", $s->identification);
			 $carton_name = "-";
			 if( isset( $carton->carton_identifier ) ) $carton_name = myhtmlentities($carton->carton_identifier);
			 print "<tr><td><a href='index.php?p=inv&sp=sample.open&s=edit&sid=".$s->sample_id."'>".$ident."</a></td>".
			 "<td>".myhtmlentities($sample_type)."</td>".
			 "<td><a href='".$c_uri.$carton->carton_id."'>".$carton_name."</a></td>".
			 "<td><a href='".$c_uri.$cs->carton_id."&show=well&row=".$cs->row_num."&col=".$cs->column_num."'>".chr(64+$cs->row_num)." ".$cs->column_num."</a></td></tr>\n";
		}
	?>
	</tbody>
	</table>
  <?php
}
?>

<script src="core/js/__jquery.tablesorter/jquery.tablesorter.min.js"></script>

<script>
	$(function() {
		$("#cartonResTable").tablesorter();
		$("#sampleResTable").tablesorter();
	});
</script>