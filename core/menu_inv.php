<?php

include("functions.modules.php");

?>
<div id="left_menu_inv" data-spy="affix" data-offset-top="20">
	<ul class="nav nav-list" >
		<li class="nav-header"><? print $_SESSION['lims']['langdata']['cartons']; ?></li>
		<li <?php if( $subpage == "cartons.mine" ) print "class='active'";?>>
			<a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=cartons.mine" id="limsadmin_cartons_mine"><i class="icon-user"></i> <? print $_SESSION['lims']['langdata']['cartons_mine']; ?></a>
		</li>
		
		<li <?php if( $subpage == "cartons.new" ) print "class='active'";?>>
			<a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=cartons.new" id="limsadmin_cartons_new"><i class="icon-plus"></i> <? print $_SESSION['lims']['langdata']['new']; ?></a>
		</li>
		
		<li <?php if( $subpage == "cartons.search" ) print "class='active'";?>>
			<a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=cartons.search" id="limsadmin_cartons_search"><i class="icon-search"></i> <? print $_SESSION['lims']['langdata']['search']; ?></a>
		</li>
		
	<li class="divider"></li>
		
		<li class="nav-header"><? print $_SESSION['lims']['langdata']['samples']; ?></li>
		<li <?php if( $subpage == "samples.search" ) print "class='active'";?>>
			<a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=samples.search" id="limsadmin_samples_search"><i class="icon-search"></i> <? print $_SESSION['lims']['langdata']['search']; ?></a>
		</li>
		
	<li class="divider"></li>
		
	<li class="nav-header"><? print $_SESSION['lims']['langdata']['laboratory']; ?></li>
		<?php
		$mods = getModulesForMenu(MENU_INVENTORY, MODULE_COMPONENT_APP_CONFIG, MENU_SECTION_LAB, $_SESSION['lims']['auth_level']);
		foreach($mods as $mod) {
			//if( $_SESSION['lims']['auth_level'] < $mod->min_access_level_id ) continue;
			$file_short = str_replace(".xml","",$mod->file);
			?>
			<li <?php if( $subpage == "account:module:".$file_short ) print "class='active'";?>><a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=account:module:<?php print $file_short; ?>" id="account_module"><? print myhtmlentities($mod->name); ?></a>
			</li>
			<?php
			}
		?>
		
		
		
		
	</ul>
</div>

<script>
$(function() {
	$('#left_menu_inv').affix();
  
  });
</script>

<!-- load common generated JS functions -->
<script>
		function loadAuditLog(type, affected_id, time_dir) {
			$('#auditLogTableFooter').html("<i class='icon-spinner icon-spin icon-large'></i>");
			$("#auditLogTable > tbody").html("");
			$.getJSON('core/ajax_audit.php?m=get&type='+type+'&affected_id='+affected_id+'&sort='+time_dir, function(adata) {
				//console.log("\n\n\n\n\n\n");
				$.each(adata, function(k,v) {
					//console.log(k+' -- '+v.audit_descr);
					//console.log(k+' -- '+v.audit_changes_str);
					var audit_key_icon = "";
					if((v.audit_key==<?php print AUDIT_ACTION_CARTON_CREATED;?>)|(v.audit_key==<?php print AUDIT_ACTION_SAMPLE_CREATED;?>)) 
						//audit_key_icon="<i class='icon-plus' title='"+v.action_str+"'></i>";
						audit_key_icon=v.action_str;
					if((v.audit_key==<?php print AUDIT_ACTION_CARTON_MOD;?>)|(v.audit_key==<?php print AUDIT_ACTION_SAMPLE_MOD;?>)) 	
						//audit_key_icon="<i class='icon-edit' title='"+v.action_str+"'></i>";
						audit_key_icon=v.action_str;
					$('#auditLogTable > tbody:last').append('<tr><td>'+audit_key_icon+'</td><td><small>'+v.timestamp+'</small></td><td><small>'+v.username+'</small></td><td>'+v.audit_changes_str+'</td></tr>');
				});
				$('#auditLogTableFooter').html("");
			});
		}
</script>