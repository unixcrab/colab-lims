<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "functions.db_connect.php";
include "functions.user.php";

// error_log(__FILE__." m=".$_GET['m']);
//error_log("ALERTS=".DBTBL_alerts);

if( isset($_GET['m']) ) {
  if( $_GET['m'] == "g" ) { // get
    $ualerts = getUserAlerts($_SESSION['lims']['person_id']);
    print json_encode( $ualerts );
  }
    
  if( $_GET['m'] == "a" ) { // ack
    if( isset( $_GET['aid'] ) )
      ackUserAlert($_SESSION['lims']['person_id'], $_GET['aid'] );
  }

}

?>