<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "functions.db_connect.php";
include "functions.modules.php";

$person_id = $_SESSION['lims']['person_id'];
$modname=stripslashes( $_POST['limsmod'] );
// error_log("lab_id=".$lab_id);

if( file_exists("../modules/".$modname."/functions.php") )
  include_once "../modules/".$modname."/functions.php";
  
$vals = array();
foreach($_POST as $pk=>$pv) {
  if($pk=="limsmod") continue;
//   error_log("mod update: ".$pk." ".$pv);
  $vals[$pk] = $pv;
  }

$r = 1;
$r = updateModuleUserConf($person_id,$modname,$vals);

if( $r == 1 ) {
	print verifyModuleConfig();
// 	print json_encode( array("resp"=>1) );
	// return;
	}
else
	print json_encode( array("resp"=>0) );
?>