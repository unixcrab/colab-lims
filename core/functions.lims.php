<?php
//include "constants.php";
include "config.common.php";
include_once "classes.php";
include_once "functions.db_connect.php";


// TODO: ADD mysqli_real_escape_string($mysqli, to all inserts!

function getInstruments($laboratory_id) {
	$retarr = array();
	$sql = "select * from ".DBTBL_instruments." where laboratory_id=".$laboratory_id." order by name ASC";
	$r = dbq($sql);
	while( $i = $r->fetch_object('Instrument') ) {
	  $retarr[$i->id] = $i;
  }
  return $retarr;
}

function delContainer($cid) {
  $sql = "update ".DBTBL_container_types." set active=0 where id=".$cid;
  dbq($sql);
}

function updateContainer($c) {
  global $mysqli;
  $sql = "update ".DBTBL_container_types." set container_type='".mysqli_real_escape_string($mysqli,$c->container_type)."', capacity=".$c->capacity.", number_of_rows=".$c->number_of_rows.",number_of_columns=".$c->number_of_columns." where id=".$c->id;
//   error_log($sql);
  dbq($sql);
}

function addContainer($newc) {
  global $mysqli;
  $sql = "insert into ".DBTBL_container_types."(container_type,capacity,number_of_rows,number_of_columns) values('".mysqli_real_escape_string($mysqli,$newc->container_type)."',".$newc->capacity.",".$newc->number_of_rows.",".$newc->number_of_columns.")";
//   error_log($sql);
  dbq($sql);
}

function getContainerType($id) {
  global $max_mysql_results;
  $retarr = array();
  
  $sql = "select * from ".DBTBL_container_types." where id=".$id;
	//error_log($sql);
  $r = dbq($sql);
  return $r->fetch_object('Container');
//   while( $c = $r->fetch_object('Container') ) {
// 	  $retarr[$c->id] = $c;
//   }
//   return $retarr;
}

function getContainerTypeFromSampleID($sid) {
  global $max_mysql_results;
  $retarr = array();
  
//   $sql = "select * from ".DBTBL_container_types." where id=".$id;
  $sql = "select * from ".DBTBL_container_types." where id in (select container_type_id from ".DBTBL_carton." where carton_id=(select carton_id from ".DBTBL_carton_sample." where sample_id=".$sid." ) ) limit 1";
  //error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object('Container');
  else
    return null;
//   while( $c = $r->fetch_object('Container') ) {
// 	  $retarr[$c->id] = $c;
//   }
//   return $retarr;
}

function getContainerTypes() {
  global $max_mysql_results;
  $retarr = array();
  
  $sql = "select * from ".DBTBL_container_types." where active=1 order by capacity desc limit ".$max_mysql_results;
//   error_log($sql);
  $r = dbq($sql);
  while( $c = $r->fetch_object('Container') ) {
	  $retarr[$c->id] = $c;
  }
  return $retarr;
}

function containerTypeInUse($container_type_id) {
	// 1 usage is sufficient to count as 'in use'
	$sql = "select carton_id from ".DBTBL_carton." where container_type_id=".$container_type_id." limit 1";
	$r = dbq($sql);
	if( $r->num_rows == 1 ) return true;
	else return false;
}

function getContainerStats($cid, $laboratory_id) {
	$carton = getCarton($cid, $laboratory_id);
	$ctype = getContainerType($carton->container_type_id);
	$cap_total = $ctype->number_of_rows*$ctype->number_of_columns;
	//error_log("cap_total ".$cap_total);
	if($cap_total==0) return null;
	$sql = "select count(sample_id) as cnt from ".DBTBL_carton_sample." where carton_id=".$cid;
	$r = dbq($sql);
	$stats = new Statistic();
	if( $r->num_rows == 1 ) {
		$cnt = $r->fetch_object();
		//error_log("cnt ".$cnt->cnt);
		$stats->average = (float)(($cnt->cnt/$cap_total)*100.0);
		//error_log($stats->average);
		}
	return $stats;
}


function storageDeviceInUse($sd_id) {
	// 1 usage is sufficient to count as 'in use'
	$sql = "select carton_id from ".DBTBL_carton." where storage_id=".$sd_id." limit 1";
	$r = dbq($sql);
	if( $r->num_rows == 1 ) return true;
	else return false;
}

function delStorageDevice($sd_id) {
  $sql = "delete from ".DBTBL_storage_device." where storage_id=".$sd_id;
  dbq($sql);
}

function updateStorageDevice($sd) {
  global $mysqli;
  $sql = "update ".DBTBL_storage_device." set storage_type_id=".$sd->storage_type_id.", laboratory_id=".$sd->laboratory_id.", identifier='".mysqli_real_escape_string($mysqli,$sd->identifier)."' where storage_id=".$sd->storage_id;
//   error_log($sql);
  dbq($sql);
}

function addStorageDevice($sd) {
  global $mysqli;
  $sql = "insert into ".DBTBL_storage_device."(environment_description,storage_type_id,laboratory_id,identifier) values('".
    mysqli_real_escape_string($mysqli,$sd->environment_description)."',".
    $sd->storage_type_id.",".
    $sd->laboratory_id.",".
    "'".mysqli_real_escape_string($mysqli,$sd->identifier)."'".
    ")";
//   error_log($sql);
  dbq($sql);
  
}

function getStorageDevices() {
	global $max_mysql_results;
	$retarr = array();
	$sql = "select * from ".DBTBL_storage_device." order by identifier asc limit ".$max_mysql_results;
// 	error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	while( $d = $r->fetch_object('StorageDevice') ) {
		$retarr[$d->storage_id] = $d;
	}
	return $retarr;
}

function getStorageDevicesForLab($lab_id=0) {
	global $max_mysql_results;
	$retarr = array();
	if( $lab_id == 0 ) return $retarr;
	
	$sql = "select * from ".DBTBL_storage_device." where laboratory_id=".$lab_id." order by identifier limit ".$max_mysql_results;
// 	error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	while( $d = $r->fetch_object('StorageDevice') ) {
		$retarr[$d->storage_id] = $d;
	}
	return $retarr;
}

function getStorageShelves($storage_id) {
	global $max_mysql_results;
  $retarr = array();
  $sql = "select * from ".DBTBL_storage_shelf." where storage_id=".$storage_id." and active=1 order by shelf_number limit ".$max_mysql_results;
//   error_log($sql);
  $r = dbq($sql);
  while( $s = $r->fetch_object('StorageShelf') ) {
		if( storageShelfInUse($s->id) ) $s->occupied = 1;
	  $retarr[$s->id] = $s;
  }
  return $retarr;
}

function getStorageShef($shelf_id) {

}
 
function getStorageShelvesAll() {
	global $max_mysql_results;
  $retarr = array();
  $sql = "select * from ".DBTBL_storage_shelf." order by id";
//   error_log($sql);
  $r = dbq($sql);
  while( $s = $r->fetch_object('StorageShelf') ) {
	  $retarr[$s->id] = $s;
  }
  return $retarr;
}

function addStorageShelf($shelf) {
  global $mysqli;
  
  // don't allow same shelf name in same storage device
  $sql = "select id from ".DBTBL_storage_shelf." where active=1 and shelf_number like '".$shelf->shelf_number."' and storage_id=".$shelf->storage_id;
//   error_log($sql);
  $r = dbq($sql);
  if( $s = $r->fetch_object('StorageShelf') ) {
// 	  $retarr[$d->id] = $d;
    //error_log("Not adding duplicate shelf ".$shelf->shelf_number." to storage_id ".$shelf->storage_id);
    return;
  }
  
  $sql = "insert into ".DBTBL_storage_shelf."(shelf_number,storage_id,active) values('".mysqli_real_escape_string($mysqli,$shelf->shelf_number)."',".$shelf->storage_id.",1)";
//   error_log($sql);
  dbq($sql);
}

function delStorageShelf($shelf_id) {
  $sql = "update ".DBTBL_storage_shelf." set active=0 where id=".$shelf_id;
//   error_log($sql);
  dbq($sql);
}

function storageShelfInUse($shelf_id) {
	// 1 usage is sufficient to count as 'in use'
	$sql = "select carton_id from ".DBTBL_carton." where shelf_id=".$shelf_id." limit 1";
	//error_log($sql);
	$r = dbq($sql);
	if( $r->num_rows == 1 ) return true;
	else return false;
}

function getStorageTypes() {
	global $max_mysql_results;
	$retarr = array();
	$sql = "select * from ".DBTBL_storage_type." order by storage_type limit ".$max_mysql_results;
// 	error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	while( $d = $r->fetch_object('StorageType') ) {
		$retarr[$d->id] = $d;
	}
	return $retarr;
}

function getSkills() {
	$retarr = array();
	$sql = "select * from ".DBTBL_skills." order by skill";
	// error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	// while( $s = mysqli_fetch_object($res, 'Skill') ) {
	while( $s = $r->fetch_object('Skill') ) {
		$retarr[] = $s;
	}
	return $retarr;
}

function addSkill($s) {
	global $mysqli;
	$sql = "insert into ".DBTBL_skills."(skill) values('".mysqli_real_escape_string($mysqli,$s)."')";
	// error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function delSkill($id) {
	$sql = "delete from ".DBTBL_skills." where id=".$id;
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function updateSkill($id, $s) {
	global $mysqli;
	$sql = "update ".DBTBL_skills." set skill='".mysqli_real_escape_string($mysqli,$s)."' where id=".$id;
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}




///////////// SAMPLES
function getSampleType($stid) {
  $sql = "select * from ".DBTBL_sample_type." where id=".$stid." limit 1";
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object("SampleType");
  else
    return null;
}

function getSampleTypes() {
	$retarr = array();
	$sql = "select * from ".DBTBL_sample_type." where active=1 order by sample_type ASC";
// 	error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	// while( $s = mysqli_fetch_object($res, 'Skill') ) {
	while( $s = $r->fetch_object('SampleType') ) {
		$retarr[$s->id] = $s;
	}
	return $retarr;
}

function addSampleType($s) {
	$sql = "insert into ".DBTBL_sample_type."(sample_type,active) values('".$s."',1)";
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function delSampleType($id) {
	$sql = "update ".DBTBL_sample_type." set active=0 where id=".$id;
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function updateSampleType($id, $s) {
	$sql = "update ".DBTBL_sample_type." set sample_type='".$s."' where id=".$id;
	//error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}


// function addCartonSample(
function getCartonSamples($carton_id) {
  global $max_mysql_results;
  $retarr = array(array());
  $sql = "select * from ".DBTBL_carton_sample." where carton_id=".$carton_id." order by row_num desc,column_num desc limit ".$max_mysql_results;
//   error_log($sql);
  $r = dbq($sql);
  while( $s = $r->fetch_object('CartonSample') ) {
// 	  $retarr[$s->carton_sample_id] = $s;
    $retarr[$s->row_num][$s->column_num] = $s;
  }
  return $retarr;
}

function getCartonSampleFromSID($sid) { // from sample_id
  $sql = "select * from ".DBTBL_carton_sample." where sample_id=".$sid." limit 1";
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object("CartonSample");
  else
    return null;
}

function getCartonSampleFromRC($r, $c, $carton_id) { // from row & col
  $sql = "select * from ".DBTBL_carton_sample." where row_num=".$r." and column_num=".$c." and carton_id=".$carton_id." limit 1";
//   error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object("CartonSample");
  else
    return null;
}

function addCartonSample($cs) {
  global $mysqli;
  $sql = "insert into ".DBTBL_carton_sample."(sample_id,row_num,column_num,carton_id,active) values(".$cs->sample_id.",".$cs->row_num.",".$cs->column_num.",".$cs->carton_id.",'".$cs->active."')";
//   error_log($sql);
  dbq($sql);
  if( $csid = mysqli_insert_id($mysqli) ) {
    return $csid;
    }
  else return null;
}

function updateCartonSample($cs) {
  global $mysqli;
	$setstr = sprintf(AUDIT_CARTON_SAMPLE_MOD_FORMAT,$cs->sample_id,$cs->row_num,$cs->column_num,$cs->carton_id,$cs->active);
	//$auditstr = sprintf(AUDIT_CARTON_SAMPLE_MOD_FORMAT,$cs->sample_id,$cs->row_num,$cs->column_num,$cs->carton_id,$cs->active);
	//$setstr = "sample_id=".$cs->sample_id.",row_num=".$cs->row_num.",column_num=".$cs->column_num.",carton_id=".$cs->carton_id.",active='".$cs->active."'";
	
  $sql = "update ".DBTBL_carton_sample." set ".$setstr." where carton_sample_id=".$cs->carton_sample_id;
  //error_log($sql);
  dbq($sql);
	$num_rows = mysqli_affected_rows($mysqli);
	//auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_CARTON_SAMPLE,AUDIT_SAMPLE_MOD,$cs->sample_id,$auditstr,time());
	if($num_rows>0)
		auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_CARTON_SAMPLE,AUDIT_ACTION_CARTON_SAMPLE_MOD,$cs->sample_id,$cs,time());
  return $num_rows;
}

function addSample($s) {
  global $mysqli;
  $sql = "insert into ".DBTBL_sample."(identification,sample_type_id) values('".mysqli_real_escape_string($mysqli,$s->identification)."',".$s->sample_type_id.")";
//   error_log($sql);
  dbq($sql);
  if( $sid = mysqli_insert_id($mysqli) ) {
		$s->sample_id = $sid;
		auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_SAMPLE,AUDIT_ACTION_SAMPLE_CREATED,$sid,$s,time());
    return $sid;
    }
  else return null;
//   return $sid;
}

function getSample($sid) {
  $sql = "select * from ".DBTBL_sample." where sample_id=".$sid." limit 1";
//   error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object('Sample');
  else
    return null;
}

function updateSample($s) {
  global $mysqli;
	//include "constants.audit.php";
  $setstr = "sample_type_id=".$s->sample_type_id.",identification='".mysqli_real_escape_string($mysqli,$s->identification)."',concentration=".$s->concentration.
  ",concentration_units_id=".$s->concentration_units_id.
  ",comments='".mysqli_real_escape_string($mysqli,$s->comments)."',control_flag='".$s->control_flag.
  "',exhausted_flag='".$s->exhausted_flag."',volume=".$s->volume.",volume_units_id=".$s->volume_units_id;
  $sql = "update ".DBTBL_sample." set ".$setstr." where sample_id=".$s->sample_id;
  //error_log($sql);
  $r = dbq($sql);
  $num_rows = mysqli_affected_rows($mysqli);
  //auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_SAMPLE,AUDIT_SAMPLE_MOD,$s->sample_id,$setstr,time());
	if($num_rows>0)
		auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_SAMPLE,AUDIT_ACTION_SAMPLE_MOD,$s->sample_id,$s,time());
  return $num_rows;
}
//////////////////////////






function getDomain($id) {
	$sql = "select * from ".DBTBL_domains." where id=".$id." limit 1";
	// error_log($sql);
	$r = dbq($sql);
	// $d = mysqli_fetch_object($res, 'Domain');
	$d = $r->fetch_object('Domain');
	return $d;
}

function getDomains() {
	global $max_domains;
	$retarr = array();
	$sql = "select * from ".DBTBL_domains." order by domain_name limit ".$max_domains;
	// error_log($sql);
	// $res = mysqli_query($sql);
	$r = dbq($sql);
	while( $d = $r->fetch_object('Domain') ) {
		$retarr[] = $d;
	}
	return $retarr;
}

function addDomain($d) {
	global $mysqli;
	$sql = "insert into ".DBTBL_domains."(domain_name) values('".mysqli_real_escape_string($mysqli,$d)."')";
	// if( !mysqli_query($sql) ) {
	if( !$r = dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function delDomain($id) {
    // first check if domain is in use by a lab!
	$sql = "delete from ".DBTBL_domains." where id=".$id;
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function updateDomain($id, $d) {
	global $mysqli;
	$sql = "update ".DBTBL_domains." set domain_name='".mysqli_real_escape_string($mysqli,$d)."' where id=".$id;
// 	error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function getLabs() {
  // return an array of all labs
	global $max_labs;
  $retarr = array();
  $sql = "select * from ".DBTBL_labs." order by name limit ".$max_labs;
  // error_log($sql);
  $r = dbq($sql);
  // $u = $r->fetch_object();
  
  while( $l = $r->fetch_object('Lab') ) {
	$d = getDomain($l->domain_id);
	$l->domain_name = $d->domain_name;
	$retarr[$l->id] = $l;
	}
  return $retarr;

}


function getLab($lab_id) {
  $sql = "select * from ".DBTBL_labs." where id=".$lab_id." limit 1";
   //error_log($sql);
  $r = dbq($sql);
	if( $r->num_rows == 1 )
		return $r->fetch_object('Lab');
  //return $l;
	else
		return null;
}


function addLab($l) {
	$sql = "insert into ".DBTBL_labs."(laboratory_id, domain_id, name) values('".$l->id."','".$l->domain_id."','".$l->name."')";
	// error_log($sql);
	if( !dbq($sql) ) {
		return 0;
	}
	else
		return 1;
	
}

function delLab($lab_id) {
	// MUST CHECK IF LAB IS IN USE!!! or use mysql foreign keys
	$sql = "delete from ".DBTBL_labs." where id=".$lab_id;
	// error_log($sql);
	if( ! dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function updateLab($l) {
	$sql = "update ".DBTBL_labs." set laboratory_id='".$l->name."', name='".$l->name."', domain_id=".$l->domain_id." where id=".$l->id;
	// error_log($sql);
	if( ! dbq($sql) ) {
		return 0;
	}
	else
		return 1;
}

function getUnitsByType($unit_system,$unit_type) {
  $retarr = array();
  $sql = "select * from ".DBTBL_units." where unit_sys_id=".$unit_system." and unit_type_id=".$unit_type;
//   error_log($sql);
  $r = dbq($sql);
  while( $u = $r->fetch_object('Unit') ) {
	$retarr[$u->id] = $u;
	}
  return $retarr;
}

function getUnitsAll($unit_system) {
  $retarr = array();
  $sql = "select * from ".DBTBL_units." where unit_sys_id=".$unit_system;
//   error_log($sql);
  $r = dbq($sql);
  while( $u = $r->fetch_object('Unit') ) {
	$retarr[$u->id] = $u;
	}
  return $retarr;
}

function getUnitsAllSystems() {
  $retarr = array();
  $sql = "select * from ".DBTBL_units;
//   error_log($sql);
  $r = dbq($sql);
  while( $u = $r->fetch_object('Unit') ) {
	$retarr[$u->id] = $u;
	}
  return $retarr;
}

function updateCarton($c) {
	global $mysqli;
	$setstr = sprintf(CARTON_MOD_FORMAT,mysqli_real_escape_string($mysqli,$c->carton_identifier),$c->create_date_timestamp,$c->storage_id,$c->shelf_id,mysqli_real_escape_string($mysqli,$c->internal_barcode),$c->owner_id,$c->allow_details_edit,$c->allow_contents_edit);
	//error_log($setstr);
	
	//$auditstr = sprintf(AUDIT_CARTON_MOD_FORMAT,mysqli_real_escape_string($mysqli,$c->carton_identifier),$c->create_date_timestamp,$c->storage_id,$c->shelf_id,mysqli_real_escape_string($mysqli,$c->internal_barcode),$c->owner_id);
	//error_log("updateCarton ".$auditstr);
	
  /*$setstr = "carton_identifier='".mysqli_real_escape_string($mysqli,$c->carton_identifier)."',".
    "create_date_timestamp=".$c->create_date_timestamp.",".
    "storage_id=".$c->storage_id.",".
    "shelf_id=".$c->shelf_id.",".
    "internal_barcode='".mysqli_real_escape_string($mysqli,$c->internal_barcode)."',".
		"owner_id=".$c->owner_id."";*/
  $sql = "update ".DBTBL_carton." set ".$setstr." where carton_id=".$c->carton_id;
   //error_log($sql);
  dbq($sql);
  auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_CARTON,AUDIT_ACTION_CARTON_MOD,$c->carton_id,$c,time() );
  return 1;
}

function getCarton($cid, $laboratory_id) {
  $sql = "select * from ".DBTBL_carton." where carton_id=".$cid." and storage_id in (select storage_id from ".DBTBL_storage_device." where laboratory_id = ".$laboratory_id." ) limit 1";
  //error_log($sql);
  $r = dbq($sql);
  if( isset($r->num_rows) && ($r->num_rows == 1) )
    return $r->fetch_object('Carton');
  else
    return null;
}

function addCarton($c) {
	global $mysqli;
	$c->create_date_timestamp = time();
	//$sql = "insert into ".DBTBL_carton."(container_type_id,carton_identifier,create_date_timestamp,storage_id,internal_barcode,owner_id) values(".$c->container_type_id.",'".mysqli_real_escape_string($mysqli,$c->carton_identifier)."',".$c->create_date_timestamp.",".$c->storage_id.",'000000',".$c->owner_id.")";
	$sql = "insert into ".DBTBL_carton."(container_type_id,carton_identifier,create_date_timestamp,storage_id,internal_barcode,owner_id) values(".$c->container_type_id.",'".mysqli_real_escape_string($mysqli,$c->carton_identifier)."',".$c->create_date_timestamp.",".$c->storage_id.",'000000',".$c->owner_id.")";
	
   //error_log($sql);
  dbq($sql);
//   $cid = mysqli_insert_id($mysqli);
  if( $cid = mysqli_insert_id($mysqli) ) {
    // set a temporary barcode
    dbq("update ".DBTBL_carton." set internal_barcode='C".$c->create_date_timestamp."-".$cid."' where carton_id=".$cid );
    $c->carton_id = $cid;
    auditOp($_SESSION["lims"]["person_id"],AUDIT_TYPE_CARTON,AUDIT_ACTION_CARTON_CREATED,$cid,$c,time());
    }
//   return mysqli_insert_id($mysqli);
  return $c;
}

function getCartonFromSID($sid) { // from sample_id
  $sql = "select * from ".DBTBL_carton." where carton_id=(select carton_id from ".DBTBL_carton_sample." where sample_id=".$sid." limit 1) limit 1";
//   error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows == 1 )
    return $r->fetch_object("Carton");
  else
    return null;
}

function getCartons() {
  $retarr = array();
  
  $sql = "select * from ".DBTBL_carton;
  $r = dbq($sql);
  while( $c = $r->fetch_object('Carton') ) {
	$retarr[$c->carton_id] = $c;
	}
  return $retarr;
}

function getCartonsForUser($owner_id, $laboratory_id, $limit=20) {
  $retarr = array();
  //$sql = "select * from ".DBTBL_carton." where laboratory_id=".$laboratory_id." and owner_id=".$owner_id." order by create_date_timestamp desc limit ".$limit;
	$sql = "select * from ".DBTBL_carton." where storage_id in (select storage_id from ".DBTBL_storage_device." where laboratory_id = ".$laboratory_id." ) and owner_id=".$owner_id." order by create_date_timestamp desc limit ".$limit;
	//error_log($sql);
  $r = dbq($sql);
  while( $c = $r->fetch_object('Carton') ) {
	$retarr[$c->carton_id] = $c;
	}
  return $retarr;
}


function attrTypeInUse($attr, $attr_id) {
	$sql = "";
	if( $attr == "sample_types" ) 
		$sql = "select sample_id from ".DBTBL_sample." where sample_type_id=".$attr_id." limit 1";
	if( $attr == "groups" )
		$sql = "select id from ".DBTBL_labs." where domain_id=".$attr_id." limit 1";
	if( $attr == "skills" )
		$sql = "select person_id from ".DBTBL_person_skills." where skill_id=".$attr_id." limit 1";
	
	//error_log($sql);
	$r = dbq($sql);
	if( $r->num_rows == 1 ) return true;
	else return false;
}


// search functions
function searchCartons($sterm, $laboratory_id) {
  global $max_mysql_results;
	global $mysqli;
	$sterm = mysqli_real_escape_string($mysqli,$sterm);
  $retarr = array();
  $sql = "select * from ".DBTBL_carton." where storage_id in (select storage_id from ".DBTBL_storage_device." where laboratory_id = ".$laboratory_id." ) and (carton_identifier like '%".$sterm."%' or internal_barcode like '%".$sterm."%') limit ".$max_mysql_results;
  $r = dbq($sql);
  while( $c = $r->fetch_object('Carton') ) {
	$retarr[$c->carton_id] = $c;
	}
  return $retarr;
}

function searchSamples($sterm) {
  global $max_mysql_results;
	global $mysqli;
	$sterm = mysqli_real_escape_string($mysqli,$sterm);
  $retarr = array();
  $sql = "select * from ".DBTBL_sample." where identification like '%".$sterm."%' or comments like '%".$sterm."%' limit ".$max_mysql_results;
//   error_log($sql);
  $r = dbq($sql);
  while( $s = $r->fetch_object('Sample') ) {
	$retarr[$s->sample_id] = $s;
	}
  return $retarr;
}


function auditOp($person_id,$type,$key,$id,$audit_descr,$timestamp) {
	global $mysqli;
	$audit_descr = json_encode($audit_descr,JSON_UNESCAPED_UNICODE|JSON_NUMERIC_CHECK);
	//error_log("audit_descr: ".$audit_descr);
  $sql = "insert into ".DBTBL_audit."(person_id,type,audit_key,affected_id,audit_descr,timestamp) values(".$person_id.",".$type.",".$key.",".$id.",'".mysqli_real_escape_string($mysqli,$audit_descr)."',".$timestamp.")";
  //
  dbq($sql);
  
}

function getAuditLog($type,$affected_id,$time_direction="asc",$limit=200) {
	$retarr = array();
	//error_log($time_direction);
	if(($time_direction!="asc")&&($time_direction!="desc")) $time_direction="asc";
	$sql = "select * from ".DBTBL_audit." where type=".$type." and affected_id=".$affected_id." order by timestamp ".$time_direction." limit ".$limit;
	//error_log($sql);
	$r = dbq($sql);
  while( $a = $r->fetch_object('Audit') ) {
	$retarr[$a->id] = $a;
	}
  return $retarr;
}
?>