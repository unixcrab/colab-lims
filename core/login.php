<?php 
include "constants.php";

$show_loginfail = false;
if( isset($_GET['l']) && $_GET['l']=='f' ) {
        // print "login failed.";
        $show_loginfail = true;
        }

if( ! $user_logged_in && isset($_SESSION['lims']['lang']) ) {
				include "lang/lang.".$_SESSION['lims']['lang'].".php";
        $_SESSION['lims']['langdata'] = $langdata;
        }
else if( ! $user_logged_in && isset($_COOKIE["colablang"]) ) {
        include "lang/lang.".$_COOKIE["colablang"].".php";
        $_SESSION['lims']['lang'] = $_COOKIE["colablang"];
        $_SESSION['lims']['langdata'] = $langdata;
        }		
else {
        include "lang/lang.".$site_default_lang.".php";
        $_SESSION['lims']['lang'] = $site_default_lang;
        $_SESSION['lims']['langdata'] = $langdata;
}

$domain_info = "";
if( $auth_types[0] == "ad" ) $domain_info = $_SESSION['lims']['langdata']['domain'].": ".$ad_domain;

?>




<div class="span8 hero-unit">
<h2><? print $_SESSION['lims']['langdata']['login']; ?></h2>
<br/>
<?php
if( $show_loginfail ) {
?>
  <div class="alert alert-error">
    <? print $_SESSION['lims']['langdata']['login_failed']; ?>
  </div>
  <?php
}
?>

  <form id="loginForm" class="form-horizontal" action="<?php print $site_root;?>/core/auth_user.php" method="POST">
    <h3><? print $_SESSION['lims']['langdata']['username']; ?></h3> <input type="text"  name="username" id="username">
    <br/>
    <h3><? print $_SESSION['lims']['langdata']['password']; ?></h3> <input type="password" name="passwd" id="passwd">
    <br/>
    <br/>
    <button type="submit" class="btn btn-primary" id="loginSubmit"><? print $_SESSION['lims']['langdata']['login']; ?></button> <?php print $domain_info;?>
		<input type="hidden" id="passSHA256" name="passSHA256" value="">
  </form>
</div>

<script src="<?print $site_root; ?>/core/js/sha256.js"></script>
<script>
$(function() {
	$('#loginSubmit').click(function(){
		$('#loginSubmit').attr('disabled','disabled');
		$('#loginSubmit').html("<? print $_SESSION['lims']['langdata']['login']; ?> <i class='icon-spinner icon-spin'></i>");
		var clrp=$('#passwd').val();
		passSHA256 = CryptoJS.SHA256(clrp).toString(CryptoJS.enc.Hex);
		$('#passSHA256').val(passSHA256);
		$('#loginForm').submit();
		return false;
	});
});
</script>
