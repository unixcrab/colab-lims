<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION["lims"]["logged_in"]) ) {
  //error_log("non logged in user details change attempt!");
  return;
  }

include "config.common.php";
include "constants.php";
include "functions.db_connect.php";
include "functions.user.php";

$u = new LimsUser();
// $username = $_SESSION['lims']['username'];
// $euro = utf8_encode("€");
// $first_name = str_replace( "€", $euro, stripslashes( $_POST['first_name'] ) );

$first_name=stripslashes( urldecode($_POST['first_name']) );
$last_name=stripslashes( urldecode($_POST['last_name']) );
$email_address=stripslashes( urldecode($_POST['email_address']) );
$phone=stripslashes( urldecode($_POST['phone']) );

$u->username = $_SESSION['lims']['username'];
$u->person_id = $_SESSION['lims']['person_id'];
$u->first_name = cleanName($first_name);
$u->last_name = cleanName($last_name);
$u->email_address = $email_address;
$u->phone = $phone;

// error_log("ajax u=".$u->username." f=[".$first_name."] nl=[".$last_name."] e=".$email_address." p=".$phone);

if( updateUserDetails($u,true) ) {
  print json_encode( array("resp"=>3) );
  }
else
  print json_encode( array("resp"=>0) );
?>
