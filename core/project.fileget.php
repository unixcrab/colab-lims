<?php
session_start();
if( !isset($_SESSION['lims']['logged_in']) ) {
  header("Location: ../index.php");
//   return;
}

include "config.common.php";
include "functions.db_connect.php";
include "functions.projects.php";

$fid = 0;
$pid = 0;

if( isset($_GET['fid']) ) $fid = $_GET['fid'];
if( isset($_GET['pid']) ) $pid = $_GET['pid'];

if( ($fid == 0) || ($pid == 0) ) return;

$pf = getProjectFile($fid);

// return if file does not exist
if( ! file_exists( $projects_dir.$pid."/files/".$pf->file_name ) ) {
  error_log(__FILE__.": file ".$pf->file_name." does not exist!");
  return;
}


header('Content-disposition: attachment; filename='.$pf->file_name);
// header('Content-type: application/pdf');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Type: application/octet-stream');
header('Content-Length: ' . filesize($projects_dir.$pid."/files/".$pf->file_name));
readfile($projects_dir.$pid."/files/".$pf->file_name);
?>