<table class="table  table-hover">
<thead>
<tr>
<th></th>
<th><? print $_SESSION['lims']['langdata']['date']; ?></th>
<th><? print $_SESSION['lims']['langdata']['event']; ?></th>
<!-- <th></th> -->
</tr>
</thead>

<tbody>
<?php
$ua = getUserAlertsAll($person_id);

foreach($ua as $a) {
    if( $a->lang_index == "alert_project_invite" ) {
    $p = getProject($person_id, $a->ref_id);
    $ackStr = "";
    if( $a->ack == 1 ) $ackStr = "<i class='icon-check'></i>";
    print "<tr id='arow-".$a->alert_id."'><td>".$ackStr."</td><td>".makeDateString($a->timestamp)." ".makeTimeString($a->timestamp)."</td><td>".$_SESSION['lims']['langdata'][$a->lang_index].": <a href='index.php?p=bionfprojs&sp=projects.open&pid=".$p->project_id."'><i><b>".myhtmlentities($p->name)."</b></i></a></td></tr>\n";
    }
  }
?>
</tbody>
</table>