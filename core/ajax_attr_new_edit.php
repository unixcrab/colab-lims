<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "constants.php";
include "functions.db_connect.php";
include "functions.lims.php";

//foreach($_POST as $k=>$v) error_log(__FILE__." ".$k." -> ".$v);

$action = null;
$nattr = null;
if( isset($_POST['a']) ) $action = $_POST['a'];
if( isset($_POST['e']) ) $nattr = $_POST['e']; // attribute

if( !$action || !$nattr ) { 
	//error_log(__FILE__.": no action or attribute!");
	print json_encode( array("resp"=>0) );
	return;
	}

$ret = 0;
if( $action == "n" ) {
	// new attribute/value
// 	$nattr = null;
	$attrv = null;
// 	if( isset($_POST['e']) ) $nattr = $_POST['e']; // attribute
	if( isset($_POST['v']) ) $attrv = $_POST['v']; // value
	if( !$nattr || !$attrv ) {
		print json_encode( array("resp"=>0) );
		return;
		}
	
	$ret = 0;
	if( $nattr == "skills" ) {
		$ret = addSkill($attrv);
		}
	if( $nattr == "groups" ) {
		$ret = addDomain($attrv);
		}
	if( $nattr == "sample_types" ) {
		$ret = addSampleType($attrv);
		}
	}

if( $action == "d" ) {
    // delete
    $attr_id = null;
    if( isset($_POST['i']) ) $attr_id = $_POST['i']; // ID
    if( ! $attr_id ) {
      print json_encode( array("resp"=>0) );
      return;
      }
      
     
    $ret = 0;
    if( $nattr == "skills" ) {
		$ret = delSkill($attr_id);
		}
    if( $nattr == "groups" ) {
		$ret = delDomain($attr_id);
		}
    if( $nattr == "sample_types" ) {
		$ret = delSampleType($attr_id);
		}
	}
    
 
if( $action == "e" ) {
    // edit
    $attr_id = null;
    $attrv = null;
    if( isset($_POST['i']) ) $attr_id = trim($_POST['i']); // ID
    if( isset($_POST['v']) ) $attrv = trim($_POST['v']); // value
    if( ! $attr_id || ! $attrv ) {
      print json_encode( array("resp"=>0) );
      return;
      }
      
     
    $ret = 0;
    if( $nattr == "skills" ) {
		$ret = updateSkill($attr_id, $attrv);
		}
    if( $nattr == "groups" ) {
		$ret = updateDomain($attr_id, $attrv);
		}
    if( $nattr == "sample_types" ) {
		$ret = updateSampleType($attr_id, $attrv);
		}
	}


print json_encode( array("resp"=>$ret) );
?>