<?php

class Attribute {
	public $id;
	public $value = array();
	public function __construct($i, $v) { $this->id = $i; $this->value = $v; }
}

class LimsUser {
  public $person_id;
  public $username;
  public $first_name;
  public $last_name;
  public $email_address;
  public $phone;
  public $active = 1;
}

class LimsUserPrefs {
	public $person_id;
	public $date_format_id;
	public $time_format_id;
	public $units_id;
}

class Module {
  public $name;
  public $file; // name of the XML config file in modules/ folder
}

class ModuleMenu {
  public $module_id;
	public $component_id;
  public $menu_id;
	public $section_id;
	public $min_access_level_id;
}

class Domain {
  public $id;
  public $domain_name;
}

class SampleType {
  public $id;
  public $sample_type;
}

class StorageType {
  public $id;
  public $storage_type;
}

class Container {
  public $id;
  public $container_type;
  public $capacity;
  public $number_of_rows;
  public $number_of_columns;
//   public function __construct($type, $cap, $r, $c) { $this->container_type = $type; $this->capacity = $cap; $this->number_of_rows=$r; $this->number_of_columns=$c; }
//   public function __construct() {};
}

class StorageDevice{
  public $id;
  public $storage_id;
  public $environment_description = "";
  public $storage_type_id;
  public $laboratory_id;
  public $identifier;
}

class StorageShelf {
  public $id;
  public $shelf_number;
  public $storage_id;
  public $active = 1;
	public $occupied = 0;
}

class Lab {
  public $id;
  public $laboratory_id;
  public $domain_name;
  public $domain_id;
  public $name;
}

class Skill {
  public $id;
  public $skill;
}

class ProjectFile {
  public $file_id;
  public $person_id;
  public $project_id;
  public $file_name;
  public $timestamp;
  public $file_size;
  public $comment = "comments...";
}

class Project {
  public $project_id;
  public $name;
  public $person_id;
  public $start_date;
  public $end_date;
}

class UserAlert {
  public $alert_id;
  public $person_id;
  public $lang_index;
  public $timestamp;
}

class Unit {
  public $id;
  public $unit_type;
	public $unit_sys_id;
	public $unit_type_id;
  public $units;
  public $conversion_factor;
}

class Carton {
  public $carton_id = 0;
	//public $laboratory_id = 0;
  public $container_type_id = 0;
  public $carton_identifier = "";
  public $create_date_timestamp = 0;
  public $storage_id = 0;
  public $shelf_id = 0;
  //public $shelf_number = 0;
  public $internal_barcode = "";
	public $owner_id = 0;
	public $allow_details_edit = 1; // allow edits by non-owners
	public $allow_contents_edit = 1;
}

class CartonSample {
  public $carton_sample_id;
  public $sample_id;
  public $row_num;
  public $column_num;
  public $carton_id;
  public $active = "Y";
}

class Sample {
  public $sample_id;
  public $sample_type_id;
  public $identification;
  public $concentration;
	public $concentration_units_id;
  public $comments;
  //public $derived_from;
  //public $investigator_id;
  public $control_flag = "N";
  public $exhausted_flag = "N";
  public $volume;
  public $volume_units_id;
  //public $template;
  //public $primer;
}

class SearchResult {
  public $res_object = null;
  public $search_str = "";
  public $match_str = "";
}

class Statistic {
	public $average = 0.0;
}

class Instrument {
	public $id;
	public $name;
	public $laboratory_id;
}

class Audit {
	public $id=0;
	public $person_id=0;
	public $type=0;
	public $audit_key=0;
	public $affected_id=0;
	public $audit_descr="";
	public $timestamp=0;
}
?>