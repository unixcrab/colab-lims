<?php
include "functions.lims.php";
$sample_types = getSampleTypes();
?>
<h4><?php print $_SESSION['lims']['langdata']['samples_search']; ?></h4>
<hr>

<form class="form-horizontal" id="sampleSearchForm">

  <div class="control-group">
    <label class="control-label" for="searchName"><?php print $_SESSION['lims']['langdata']['name']; ?></label>
    <div class="controls">
      <input type="text" name="searchName" id="searchName">
    </div>
  </div>
	
  <div class="control-group">
    <label class="control-label" for="searchSType"><?php print $_SESSION['lims']['langdata']['type']; ?></label>
    <div class="controls">
      <select id="searchSType">
			<option value=0></option>
			<?php
			foreach($sample_types as $st_id=>$stype) {
				print "<option value=".$st_id.">".myhtmlentities($stype->sample_type)."</option>\n";
				}
			?>
			</select>
    </div>
  </div>
	
  <div class="control-group">
    <div class="controls">
      <button type="submit" class="btn btn-primary" id="searchSubmit"><?php print $_SESSION['lims']['langdata']['search']; ?></button>
			<button class="btn" id="searchReset"><?php print $_SESSION['lims']['langdata']['reset']; ?></button>
    </div>
  </div>
	
</form>

<div id="searchOutputContainer">
	<div class="alert alert-info" id="searchOutputStats" style="display:none;"></div>
	<div class="" id="searchOutput">
	<!--<div class="" id="searchOutput" style="height: 400px; overflow: auto;">-->
		<table id='resTable'></table>
	</div>
</div>

<script src="core/js/__jquery.tablesorter/jquery.tablesorter.min.js"></script>

<script>
	$(function() {
		$('#searchReset').click( function() {
			resetForm( $('#sampleSearchForm') ); 
			return false;
		});
		
		$('#searchSubmit').click( function() {
			$('#searchOutputStats').slideUp(100);
			$('#searchOutput').html("");
			
			var name = $('#searchName').val();
			var stype = $('#searchSType :selected').val();
			
			if( (name=="")&&(stype==0) ) return false;
			
			$('#searchSubmit').attr('disabled','disabled');
			$('#searchSubmit').html("<?php print $_SESSION['lims']['langdata']['search']; ?>  <i class='icon-spinner icon-spin'></i>");
			
			$.getJSON('core/ajax_search.php?obj=sample&m=adv&name='+name+'&stype='+stype, function(data) {
				var outStr = "<table id='resTable' class='table table-hover table-condensed'>";
				outStr += "<thead><tr><th><?php print $_SESSION['lims']['langdata']['sample']; ?></th><th><?php print $_SESSION['lims']['langdata']['type']; ?></th><th><?php print $_SESSION['lims']['langdata']['carton']; ?></th><th><?php print $_SESSION['lims']['langdata']['location']; ?></th></tr></thead><tbody>";
				$.each(data.options, function(key, sample) {
					var sample_name = sample[0];
					var sample_type = sample[1];
					var cid = sample[2];
					var carton_name = sample[3];
					outStr += "<tr id='srow-"+key+"'>";
					outStr += "<td><a href='index.php?p=inv&sp=sample.open&s=edit&sid="+key+"'>"+sample[0]+ "</a></td><td>"+sample_type+"</td>"+"</td><td><a href='index.php?p=inv&sp=cartons.mine&s=edit&cid="+cid+"'>"+carton_name+"</a></td>"+"</td><td><a href='index.php?p=inv&sp=cartons.mine&s=edit&cid="+cid+"&show=well&row="+sample[4]+"&col="+sample[5]+"'>"+String.fromCharCode(64+parseInt(sample[4]))+" "+sample[5]+"</a></td>";
					outStr += "</tr>";
				});
				outStr += "</tbody></table>";
				
				$('#searchOutputStats').slideDown(100, function(){
						if( data.numresults > 0 ) $('#searchOutput').html(outStr);
						$('#searchOutputStats').html("<strong><?php print $_SESSION['lims']['langdata']['search_results']; ?></strong>: <span class='badge badge-success'> "+data.numresults+' </span>');
						$('#searchSubmit').html("<?php print $_SESSION['lims']['langdata']['search']; ?>");
						$('#searchSubmit').removeAttr('disabled');
				});
			});
			return false;
			});
			
			
			
			
			
		});
		
</script>