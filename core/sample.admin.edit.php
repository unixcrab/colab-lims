<?php


include "functions.lims.php";
$s = getSample($sid);
if( $s == null ) {
  error_log("Missing sample: sid=".$sid."!");
  ?>
  <div class="alert alert-error">
  <i class="icon-exclamation-sign"></i>  <strong><?php print $_SESSION['lims']['langdata']['err_not_found']; ?>!</strong>
  <hr>
  <p><?php print $_SESSION['lims']['langdata']['err_sample_not_found']." ".$sid; ?>.
  <p><?php print $_SESSION['lims']['langdata']['err_contact_admin']; ?>.
  </div>
  <?php
  return;
  }

$carton_sample = getCartonSampleFromSID($s->sample_id);
$container_type = getContainerTypeFromSampleID($s->sample_id);
$carton = getCarton($carton_sample->carton_id,$_SESSION['lims']['lab_id']);
if( $carton == null ) {
	error_log("Denied attempt to access carton in lab_id=".$_SESSION['lims']['lab_id']." from user ".$_SESSION['lims']['person_id']);
	?>
	<div class="alert">
  <?php print $_SESSION['lims']['langdata']['page_access_denied'];?>
	</div>
	<?php
	return;
	}
// $carton = getCartonFromSID($s->sample_id);
// $s_type = getSampleType($s->sample_type_id);
$stypes = getSampleTypes();
//$units = getUnits();
$conc_units = getUnitsByType(UNIT_SYS_SI,UNIT_TYPE_CONC);
$vol_units = getUnitsByType(UNIT_SYS_SI,UNIT_TYPE_VOLUME);

$cartons_uri = "index.php?p=inv&sp=cartons.mine&s=edit";
?>

<div class="navbar">
  <div class="navbar-inner">
    <span class="brand" ><strong><?php print mb_strtoupper($_SESSION['lims']['langdata']['sample']); ?></strong>: <i><?php print myhtmlentities($s->identification); ?></i></span>
		<ul class="nav">
			
			<li id="navpill_li-sampleEditDetails" <?php if($show_audit_logs) print ""; else print "class='active'";?>><a href="#" id ="sampleEditDetails"><i class="icon-list icon-white"></i><?php print $_SESSION['lims']['langdata']['details']; ?></a></li>
			
			<li id="navpill_li-sampleAuditLogs" <?php if($show_audit_logs) print "class='active'"; ?>><a href="#" id="sampleAuditLogs"><i class="icon-stethoscope"></i><?php print $_SESSION['lims']['langdata']['audit_logs']; ?></a></li>
		</ul>		
  </div>
</div>

<div id="user_feedback" style="display:none;"></div>

<?php
if(($carton->owner_id!=$_SESSION['lims']['person_id'])&&($carton->allow_contents_edit!=1)) { ?>
	<div class="alert">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <strong><?php print $_SESSION['lims']['langdata']['warning']; ?>!</strong> <?php print $_SESSION['lims']['langdata']['err_no_edit_perm']; ?>.
</div>
 <?php } ?>
 
<div id="formContainer">
		<form class="form-horizontal">

			<div class="control-group">
				<label class="control-label" for="sampleAttrName"><?php print $_SESSION['lims']['langdata']['name']; ?></label>
				<div class="controls">
					<input type="text" id="sampleAttrName" value="<?php print myhtmlentities($s->identification);?>" style="width: 300px;" disabled="disabled">
				</div>
			</div>
			
			
			<div class="control-group">
				<label class="control-label" for="sampleAttrType"><?php print $_SESSION['lims']['langdata']['type']; ?></label>
				<div class="controls">
					<select id="sampleAttrType" disabled="disabled">
					<?php 
			foreach( $stypes as $stype ) {
				$selStr = "";
				if( $s->sample_type_id == $stype->id ) $selStr = "selected";
				print "<option value='".$stype->id."' ".$selStr.">".myhtmlentities($stype->sample_type)."</option>\n";
			}
					?>
					</select>
				</div>
			</div>
			
			
			<div class="control-group">
				<label class="control-label" for="sampleAttrCon"><?php print $_SESSION['lims']['langdata']['concentration']; ?></label>
				<div class="controls">
					<input type="text" class="input-medium" id="sampleAttrCon" value="<?php print $s->concentration; ?>" disabled="disabled">
					<select id="sampleAttrConUnits" class="input-small" disabled="disabled">
					<?php 
			foreach( $conc_units as $unit ) {
				$selStr = "";
				if( $s->concentration_units_id == $unit->id ) $selStr = "selected";
				print "<option value='".$unit->id."' ".$selStr.">".$unit->units."</option>\n";
			}
					?>
					</select>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="sampleAttrVol"><?php print $_SESSION['lims']['langdata']['volume']; ?></label>
				<div class="controls">
					<input type="text" class="input-medium" id="sampleAttrVol" value="<?php print $s->volume; ?>" disabled="disabled">
					<select id="sampleAttrVolUnits" class="input-small" disabled="disabled">
					<?php 
			foreach( $vol_units as $unit ) {
				$selStr = "";
				if( $s->volume_units_id == $unit->id ) $selStr = "selected";
				print "<option value='".$unit->id."' ".$selStr.">".$unit->units."</option>\n";
			}
					?>
					</select>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="sampleAttrWell"><?php print $_SESSION['lims']['langdata']['well']; ?></label>
				<div class="controls">
					
					<select id="sampleAttrWellR" class="input-mini" disabled="disabled">
					<?php 
					if( $container_type != null )
			for($r=1; $r<=$container_type->number_of_rows;$r++) {
				$selStr = "";
				if( $carton_sample->row_num == $r ) $selStr = "selected";
				print "<option value='".$r."' ".$selStr.">".chr(64+$r)."&nbsp;</option>\n";
			}
					?>
					</select>
					
					<select id="sampleAttrWellC" class="input-mini" disabled="disabled">
					<?php 
					if( $container_type != null )
			for($c=1; $c<=$container_type->number_of_columns;$c++) {
				$selStr = "";
				if( $carton_sample->column_num == $c ) $selStr = "selected";
				print "<option value='".$c."' ".$selStr.">".$c."</option>\n";
			}
					?>
					</select>
					
					<?php 
					if( $container_type == null )
			print "<span class='alert alert-error'><i class='icon-exclamation-sign'></i> ".$_SESSION['lims']['langdata']['err_not_found']."</span>\n";
					?>
					
					<?php print " <strong>".$_SESSION['lims']['langdata']['carton']." </strong> <a href='".$cartons_uri."&cid=".$carton->carton_id."'>".myhtmlentities($carton->carton_identifier)."</a>"; ?>
					
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="sampleAttrComments"><?php print $_SESSION['lims']['langdata']['comment']; ?></label>
				<div class="controls">
					<textarea id="sampleAttrComments" rows="4" style="width: 300px;" disabled="disabled"><?php print myhtmlentities($s->comments); ?></textarea>
				</div>
			</div>
			
			<div class="control-group">
				<label class="control-label" for="sampleAttrProps"></label>
				<div class="controls">
					
				<label class="checkbox inline">
				<input type="checkbox" id="sampleAttrActive" disabled="disabled" <?php if($carton_sample->active=="Y") print "checked";?>> <i class='icon-circle' style='color: green; font-size: 15px;'></i> <?php print $_SESSION['lims']['langdata']['active']; ?>
					</label>
					<label class="checkbox inline">
					<input type="checkbox" id="sampleAttrControl" disabled="disabled" <?php if($s->control_flag=="Y") print "checked";?>> <i class='icon-circle' style='color: yellow; font-size: 15px;'></i> <?php print $_SESSION['lims']['langdata']['control']; ?>
					</label>
					<label class="checkbox inline">
			<input type="checkbox" id="sampleAttrDepleted" disabled="disabled" <?php if($s->exhausted_flag=="Y") print "checked";?>> <i class='icon-circle' style='color: red; font-size: 15px;'></i> <?php print $_SESSION['lims']['langdata']['depleted']; ?>
					</label>
					

				</div>
			</div>
			 
			<?php if(($carton->owner_id==$_SESSION['lims']['person_id'])||($carton->allow_contents_edit==1)) { ?>
			<div class="control-group">
				<div class="controls">
					<button class="btn btn-warning" id="sampleAttrEditBtn"><i class="icon-lock icon-white"></i> <?php print $_SESSION['lims']['langdata']['unlock']; ?></button>
					<button type="submit" class="btn btn-primary" id="sampleAttrSaveBtn" style="display:none;"><?php print $button_save; ?></button>
				</div>
			</div>
			<?php } ?>
			
		</form>
</div>

<div id="auditLogContainer" style="display:none;">
	<table id="auditLogTable" class="table table-hover table-condensed" style="table-layout: fixed; width: 100%">
		<thead>
		<tr>
		<th style='width:80px;'></th>
		<th style='width:90px;'><?php print $_SESSION['lims']['langdata']['date']; ?></th>
		<th style='width:100px;'><?php print $_SESSION['lims']['langdata']['user']; ?></th>
		<th><?php print $_SESSION['lims']['langdata']['details']; ?></th>
		</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	<div id="auditLogTableFooter"></div>
</div>

<?php if(($carton->owner_id==$_SESSION['lims']['person_id'])||($carton->allow_contents_edit==1)) { ?>
<script>
$(function() {

		$('#sampleAuditLogs').click(function(){
				$('[id^=navpill_li-]').removeClass("active");
				$('#navpill_li-sampleAuditLogs').addClass("active");
				if( $('#formContainer').css('display') != 'none') {
					$('#formContainer').slideUp(200, function() {
						$('#auditLogContainer').slideDown(200);
					});
				}
				<?php print "loadAuditLog(".AUDIT_TYPE_SAMPLE.",".$s->sample_id.");\n"; ?>
				return false;
			});

		$('#sampleEditDetails').click( function() {
			$('[id^=navpill_li-]').removeClass("active");
			$('#navpill_li-sampleEditDetails').addClass("active");
			if( $('#auditLogContainer').css('display') != 'none') {
				$('#auditLogContainer').slideUp(200, function() {
					$('#formContainer').slideDown(200);
				});
			}
		});
	
	
	
    $('#sampleAttrSaveBtn').click( function() {
      var sn = $('#sampleAttrName').val();
      var st = $('#sampleAttrType').val();
      var sconc = $('#sampleAttrCon').val();
      var sconcu = $('#sampleAttrConUnits').val();
      var svol = $('#sampleAttrVol').val();
      var svolu = $('#sampleAttrVolUnits').val();
      var sr = $('#sampleAttrWellR').val();
      var sc = $('#sampleAttrWellC').val();
      var scom = $('#sampleAttrComments').val();
      
//       var sact = $('#sampleAttrActive').val();
      var sact = 0;
      if( $('#sampleAttrActive').is(':checked') ) sact = 1;
      var sctrl = 0;
      if( $('#sampleAttrControl').is(':checked') ) sctrl = 1;
      var sdep = 0;
      if( $('#sampleAttrDepleted').is(':checked') ) sdep = 1;
      
      if(sn=="") {
				userFeedbackL('alert-error', 'err_enter_name');
				return false;
      }
      
      if(!isFloat(sconc) && !isInt(sconc) ) {userFeedbackL('alert-error', 'err_concentration'); return false;}
      if(!isFloat(svol) && !isInt(svol) ) {userFeedbackL('alert-error', 'err_volume'); return false;}
      
			$(this).attr('disabled','disabled');
			$(this).html("<? print $button_save; ?> <i class='icon-spinner icon-spin'></i>");
      $.post('core/ajax_sample.php', {m:'u', sid:<?php print $sid; ?>, cid:<?php print $carton_sample->carton_id; ?>, sn:sn, st:st, sconc:sconc, sconcu:sconcu, svol:svol, svolu:svolu, sr:sr, sc:sc, scom:scom, sact:sact, sctrl:sctrl, sdep:sdep}, function(r) {
				var j = jQuery.parseJSON(r);
				if(j.resp<0) {
					userFeedbackL('alert-error', j.err_index);
					$('#sampleAttrSaveBtn').removeAttr('disabled');
					$('#sampleAttrSaveBtn').html("<? print $button_save; ?>");
					return false;
				}
				if(j.resp==0) {
					userFeedbackL('alert-warn', 'err_no_changes');
			// 	  $('[id^=sampleAttr]').attr('disabled','disabled');
					return false;
				}
				if(j.resp==1) {
			// 	  userFeedbackL('alert-success', 'err_saved');
			// 	  $('[id^=sampleAttr]').attr('disabled','disabled');
					window.location.replace('index.php?p=inv&sp=sample.open&s=edit&sid=<?php print $sid; ?>');

					return false;
				}
      });
      return false;
    });
		
    $('#sampleAttrEditBtn').click( function() {
      $('[id^=sampleAttr]').removeAttr('disabled');
      $(this).hide( 0, function(){
				$('#sampleAttrSaveBtn').show(0);
				return false;
      });
      return false;
      });
			
			
			
			
  });
</script>
<?php } ?>

<!-- <pre> -->
<?php
// var_dump($s);
?>
<!-- </pre> -->