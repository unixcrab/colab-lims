<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "constants.php";

//foreach($_POST as $k=>$v) error_log($k." ".$v);

if( $_SESSION['lims']['auth_level'] < PERM_MANAGER ) {
  error_log("Unauthorized attempt from person_id=".$_SESSION['lims']['person_id']." (auth level ".$_SESSION['lims']['auth_level'].") to access ".__FILE__);
  return;
  }

$mode = "";
if( isset( $_POST['m'] ) ) $mode = $_POST['m'];
elseif( isset( $_GET['m'] ) ) $mode = $_GET['m'];
else return;



include "functions.db_connect.php";
include "functions.user.php";
include "functions.lims.php";
// date_default_timezone_set($site_timezone);

if( $mode == "u" ) { // update
  //$u = new LimsUser();
  $acc_arr = null;
  $auth_arr = null;
  $lab_default = null;
  
  $uid = 0;
  if( isset($_POST['u'] ) ) $uid = $_POST['u'];
  else { error_log(__FILE__." : NO UID"); return; }// can't do anything without uid
  $u = getUserDetails($uid);
  
  if( isset($_POST['uname'] ) ) $u->username = strip_tags($_POST['uname']);
  if( isset($_POST['fname'] ) ) $u->first_name = strip_tags($_POST['fname']);
  if( isset($_POST['lname'] ) ) $u->last_name = strip_tags($_POST['lname']);
  if( isset($_POST['email'] ) ) $u->email_address = strip_tags($_POST['email']);
  
  if( isset($_POST['acc_arr'] ) ) $acc_arr = $_POST['acc_arr'];
  if( isset($_POST['auth_arr'] ) ) $auth_arr = $_POST['auth_arr'];
  if( isset($_POST['def'] ) ) $lab_default = $_POST['def'];
	if( isset($_POST['a'] ) ) $u->active = $_POST['a'];
	if( isset($_POST['u'] ) ) $u->person_id = $_POST['u'];

	$u->username = cleanUsername($u->username);
	if($u->username == "") { print json_encode(array("resp"=>1)); return; }
	$u->first_name = cleanName($u->first_name);
	$u->last_name = cleanName($u->last_name);
  
	updateUserDetails($u);
	
	if( isset($_POST['p'] ) && ($_POST['p']!="") ) {
		changeUserPasswdByID($uid, $_POST['p']);
		}
		
  $userlabs = getUserLabAccess($uid);
  //if( ! isset( $userlabs[$lab_default] ) ) 
		//addLabPersonnel($lab_default,$uid,$auth_arr[$lab_default],'Y',time(),'Y');
	
	foreach($acc_arr as $lab_id=>$v) {
		//if( $lab_id == $lab_default ) continue;
		//error_log("acc_arr ".$lab_id." ".$v);
		delLabPersonnel($lab_id,$uid);
		//if( $v == 0 ) delLabPersonnel($lab_id,$uid);
		if( $acc_arr[$lab_id] == 1 ) {
			//error_log("ADDING lab_id=".$lab_id);
			//if( isset($userlabs[$lab_id]) && $userlabs[$lab_id] )
			addLabPersonnel($lab_id,$uid,$auth_arr[$lab_id],'Y',time(),'Y');
		}
	}
	
	setUserDefaultLab($uid, $lab_default);
  print json_encode(array("resp"=>1));
}

if( $mode == "d" ) { // inactivate user
  if( isset($_POST['u'] ) ) $uid = $_POST['u'];
  delUser($uid);
}

if( $mode == "new" ) {
  $u = new LimsUser();
  if( isset($_POST['uname'] ) ) $u->username = strip_tags($_POST['uname']);
  if( isset($_POST['fname'] ) ) $u->first_name = strip_tags($_POST['fname']);
  if( isset($_POST['lname'] ) ) $u->last_name = strip_tags($_POST['lname']);
  if( isset($_POST['email'] ) ) $u->email_address = strip_tags($_POST['email']);
  if( isset($_POST['labs'] ) ) $labs = $_POST['labs'];
//   foreach($labs as $l) error_log("lab ".$l);
//   $labs_ids = explode(",",$labs);
  
	//$u->username = preg_replace('/[\s!"#�%&\/()\[\]=?\'\*\`\^\~\<\>]/', "", $u->username);
	$u->username = cleanUsername($u->username);
	if($u->username == "") { print json_encode(array("resp"=>1)); return; }
	
	$u->first_name = cleanName($u->first_name);
	$u->last_name = cleanName($u->last_name);
	
  $person_id = addUser($u);
  if( $person_id != 0 ) {
    $addCount = 1;
    foreach($labs as $lab_id) {
      if( $addCount==1 ) addLabPersonnel($lab_id,$person_id,0,'Y',time(),'Y');
      else addLabPersonnel($lab_id,$person_id,0,'Y',time(),'N');
      $addCount++;
      }
      
    print json_encode(array("uid"=>$person_id));
    }
  else
    print json_encode(array("uid"=>0));
    
}

if( $mode == "verify" ) {
  if( isset($_GET['attr']) && isset($_GET['value']) && ($_GET['attr'] == "username") ) $username = stripslashes($_GET['value']);
  $u = getUserFromUname($username);
  if( $u != null )
    print json_encode(array("available"=>0));
  else
    print json_encode(array("available"=>1));
}
?>