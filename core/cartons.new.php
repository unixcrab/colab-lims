<?php
include "functions.lims.php";
$container_types = getContainerTypes();
$storage_devices = getStorageDevicesForLab($_SESSION['lims']['lab_id']);

?>
<h4><?php print $_SESSION['lims']['langdata']['new_container']; ?></h4>
<hr>

<div id="user_feedback" style="display:none;"></div>

<form>
  <fieldset>
<!--     <legend>Legend</legend> -->
    <label><?php print $_SESSION['lims']['langdata']['uniq_name']; ?></label>
    <input type="text" id="newCartonName">
<!--     <span class="help-block">Example block-level help text here.</span> -->

    <label><?php print $_SESSION['lims']['langdata']['container_type']; ?></label>
    <select id="newCartonContType">
    <?php
    foreach($container_types as $ct) {
      $descr = "";
      if( $ct->capacity > 1 ) $descr = "(".$ct->number_of_rows."x".$ct->number_of_columns.")";
      print "<option value='".$ct->id."'>".myhtmlentities($ct->container_type." ".$descr)."</option>\n";
    }
    ?>
    </select>
    
		<label><?php print $_SESSION['lims']['langdata']['limsadmin_storage_device']; ?> (<?php print $_SESSION['lims']['langdata']['laboratory']; ?>: <?php print $_SESSION['lims']['lab_name']; ?>)</label>
		<select id="newCartonSDevice">
		<?php
		foreach($storage_devices as $sdkey=>$sd) {
			print "<option value=".$sdkey.">".myhtmlentities($sd->identifier)."</option>\n";
		}
		?>
		</select>
		
    <br/>
		<button type="submit" class="btn btn-primary" id="newCartonSave"><?php print $button_save; ?></button>
    <!--<button type="submit" class="btn btn-primary" id="newCartonSave"><i class="icon-plus icon-white"></i> <?php print $_SESSION['lims']['langdata']['create']; ?></button>-->
  </fieldset>
</form>

<script>
$(function() {
  $('#newCartonSave').click( function() {
    
    var cident = $('#newCartonName').val();
    var ctid = $('#newCartonContType').val();
		var sdid = $('#newCartonSDevice').val();
    if( cident == "" ) {
      userFeedbackL('alert-error','err_enter_name');
      return false;
      }
    $(this).attr('disabled','disabled');
		$(this).html("<? print $button_save; ?> <i class='icon-spinner icon-spin'></i>");
    //$('body').css('cursor', 'wait');
    $.post('core/ajax_carton.php', {m:'n', pid:<?php print $_SESSION['lims']['person_id'];?>,lid:<?php print $_SESSION['lims']['lab_id'];?>, cident:cident, ctid:ctid, sdid:sdid}, function(r) {
      var job = jQuery.parseJSON(r);
      $('body').css('cursor', 'auto');
      if( (job.resp == 1) && isInt(job.cid) )
				window.location.replace('index.php?p=inv&sp=cartons.mine&s=edit&direct=1&cid='+job.cid);
      });
    return false;
  });

  });
</script>