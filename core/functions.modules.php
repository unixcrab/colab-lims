<?php
include "config.common.php";
include_once "constants.php";
include_once "classes.php";
include_once "functions.db_connect.php";

function getModulesAll() {
  $retarr = array();
  //$sql = "select * from ".DBTBL_modules." where active=1 order by name limit 500";
	$sql = "select * from ".DBTBL_modules." where true order by name limit 500";
//   error_log($sql);
  $r = dbq($sql);
  while( $m = $r->fetch_object('Module') ) {
    $retarr[$m->module_id] = $m;
  }
  return $retarr;
}

function getModulesForMenu($menu_id,$component_id,$section_id,$min_access_level_id) {
	$retarr = array();
	$sql = "select * from ".DBTBL_modules." where active=1 and module_id in (select module_id from ".DBTBL_modules_menus." where menu_id=".$menu_id." and component_id=".$component_id." and section_id=".$section_id." and min_access_level_id<".$min_access_level_id.")";
	error_log($sql);
	$r = dbq($sql);
  while( $m = $r->fetch_object('Module') ) {
    $retarr[$m->module_id] = $m;
  }
  return $retarr;
}

function delModule($module_name) {
  //$sql = "update ".DBTBL_modules." set active=0 where name like '".$mname."'";
	$sql = "update ".DBTBL_modules." set active=0 where file like '".$module_name.".xml'";
   //error_log($sql);
  $r = dbq($sql);
  
  return 1;
}

function addModule($mname, $fname) {
  global $mysqli;
  $module_id = 0;
	
  $sql = "select module_id from ".DBTBL_modules." where name like '".$mname."' limit 1";
  $r = dbq($sql);
  if( $r->num_rows == 1 ) {
		$m = $r->fetch_object();
		$module_id = $m->module_id;
    $sql = "update ".DBTBL_modules." set active=1 where name like '".$mname."'";
    //error_log($sql);
    dbq($sql);
  }
  else {
    $sql = "insert into ".DBTBL_modules."(name,file,active) values('".mysqli_real_escape_string($mysqli,$mname)."','".mysqli_real_escape_string($mysqli,$fname)."',1)";
    //error_log($sql);
		
    dbq($sql);
		$module_id = mysqli_insert_id($mysqli);
  }
  return $module_id;
}

function setModuleMenus($module_id,$component_id,$menus_array) {
	if($module_id==0) return 0;
	if(count($menus_array)==0) return 0;
	
	$sql = "delete from ".DBTBL_modules_menus." where module_id=".$module_id." and component_id=".$component_id;
	//error_log($sql);
	dbq($sql);
	foreach($menus_array as $k=>$v) {
		$sql = "insert into ".DBTBL_modules_menus."(module_id,component_id,menu_id,section_id,min_access_level_id) values(".$module_id.",".$component_id.",".$k.",".$v.",".PERM_TECHNICIAN.")";
		//error_log($sql);
		dbq($sql);
	}
	return 1;
}

function getModuleMenus($module_id,$component_id) {
	if($module_id==0) return;
	$retarr = array();
	
	$sql = "select * from ".DBTBL_modules_menus." where module_id=".$module_id." and component_id=".$component_id;
	//error_log($sql);
	$r = dbq($sql);
	while( $m = $r->fetch_object('ModuleMenu') ) {
    $retarr[$m->menu_id] = $m;
  }
	return $retarr;
}


function updateModuleUserConf($person_id,$mname,$vals) {
	global $tablePrefix;
  $modtbl = $tablePrefix."module_".$mname."_user";
  
  $sql = "select * from ".$modtbl." where person_id=".$person_id;
	error_log($sql);
  $r = dbq($sql);
  //error_log("modtbl=".$r->num_rows);
  
  if( isset($r->num_rows) && ($r->num_rows == 0) ) {
    $sql1 = "insert into ".$modtbl."(person_id,";
    $sql2 = " values(".$_SESSION['lims']['person_id'].",";
    foreach($vals as $k=>$v) {
      $sql1 .= $k.",";
      $sql2 .= "'".$v."',";
    }
    $sql1 = rtrim($sql1,",");
    $sql2 = rtrim($sql2,",");
    $sql1 = $sql1.")";
    $sql2 = $sql2.")";
		error_log($sql1.$sql2);
    dbq($sql1.$sql2);
    return 1;
  }
  
  $sql = "update ".$modtbl." set ";
  foreach($vals as $k=>$v) {
    $sql .= $k."='".$v."', ";
  }
  $sql = rtrim($sql, ", ");
  $sql .= " where person_id=".$_SESSION['lims']['person_id'];
  error_log($sql);
  if( $r = dbq($sql) ) return 1;
  else return 0;
}
?>