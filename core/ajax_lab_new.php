<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "constants.php";
include "functions.db_connect.php";
include "functions.lims.php";

$l = new Lab();
$newlab_name=stripslashes( $_POST['lab_name'] );
$newlab_domain_id=stripslashes( $_POST['lab_domain_id'] );

$l->id = $newlab_name;
// $l->domain_name = $newlab_domain;
$l->domain_id = $newlab_domain_id;
$l->name = $newlab_name;

$r = addLab($l);
if( $r != 1 ) {
	print json_encode( array("resp"=>0) );
	return;
	}
print json_encode( array("resp"=>1) );
?>