<?php
// include "functions.projects.php";
$user_p = getActiveProjects($_SESSION['lims']['person_id']);
$mem_p = getActiveProjectsForMember($_SESSION['lims']['person_id']);
$ps = array_merge($user_p, $mem_p);

$now = time();
?>
<table class="table table-hover">
<thead>
<tr>
<th><? print $_SESSION['lims']['langdata']['projects_name']; ?></th>
<th><? print $_SESSION['lims']['langdata']['projects_startdate']; ?></th>
<th><? print $_SESSION['lims']['langdata']['projects_enddate']; ?></th>
<th></th>
</tr>
</thead>

<tbody>
<tr class='info'><td colspan=3><strong><? print $_SESSION['lims']['langdata']['projects_my']; ?></strong></td></tr>
  <?php
  $myprojs = true;
  foreach($ps as $p) {
  
    $pname = myhtmlentities($p->name);
    
    if( ($p->person_id != $_SESSION['lims']['person_id']) and ($myprojs==true) ) {
      print "<tr class='info'><td colspan=3><strong>".$_SESSION['lims']['langdata']['projects_involved']."</strong></td></tr>\n";
      $myprojs = false;
      }
    
    $start_date = "";
    $end_date = "";
    $expire_alert_icon = "";
//     $alarm_alert_icon = "";
    if( ($p->end_date - $day_len) <= $now ) $expire_alert_icon = "<i class='icon-bell'></i>";
    if( $p->end_date < $now ) $expire_alert_icon = "<i class='icon-warning-sign'></i>";
    
    $start_date = makeDateString($p->start_date);
    $end_date = makeDateString($p->end_date);
    
    print "<tr>".
    "<td><a href='index.php?p=bionfprojs&sp=projects.open&pid=".$p->project_id."'>".$pname."</a></td>".
    "<td>".$start_date."</td>".
    "<td>".$end_date." ".$expire_alert_icon."</td>".
//     "<td><i class='icon-edit'></i>".
    "</tr>\n";
  }
  ?>
</tbody>

</table>
