<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

if( isset( $_POST['m'] ) && ($_POST['m']=="u" ) ) $mode = "update";
else return;

include "config.common.php";
include "constants.php";
include "functions.db_connect.php";
include "functions.user.php";
include "functions.lims.php";

if( $mode == "update" ) {
	$prefs = new LimsUserPrefs();
  $lab_id = stripslashes($_POST['lab_id']);
  $_SESSION['lims']['lab_id'] = $lab_id;
  $_SESSION['lims']['lab_name'] = getLab( $_SESSION['lims']['lab_id'] )->name;
  $save_default = stripslashes($_POST['lab_save']);

  //$dformat = stripslashes( $_POST['df'] );
  //$tformat = $site_default_timeformat;
	$prefs->date_format_id = stripslashes( $_POST['df'] );
	$prefs->time_format_id = $site_default_timeformat;
	$prefs->units_id = $_POST['us'];
	
  if( $save_default==1 ) setUserDefaultLab($_SESSION['lims']['person_id'], $lab_id);
  //$r = setUserDateTimeFormat($_SESSION['lims']['person_id'],$dformat,$tformat);
	$r = setUserPrefs($_SESSION['lims']['person_id'],$prefs);
	
  if( $r == 1 ) {
    print json_encode( array("resp"=>1) );
  }
  else
    print json_encode( array("resp"=>0) );
}
?>