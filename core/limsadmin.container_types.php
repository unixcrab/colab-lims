<div id="user_feedback" class="alert alert-info" style="display: none;">
<? print $_SESSION['lims']['langdata']['please_complete']; ?>
</div>
<div id="containerTypeList">
<h4><?php print $_SESSION['lims']['langdata']['limsadmin_container_types']; ?></h4>
<hr>
<table id="containerTypesTable" class="table table-hover table-condensed tablesorter" style="width: 80%;">
<thead>
<tr>
<th><?php print $_SESSION['lims']['langdata']['name']; ?></th>
<th><?php print $_SESSION['lims']['langdata']['rows']; ?></th>
<th><?php print $_SESSION['lims']['langdata']['columns']; ?></th>
<th><?php print $_SESSION['lims']['langdata']['capacity']; ?></th>
<th></th>
</tr>
</thead>
<tbody>
<?php
include "functions.lims.php";

$cts = getContainerTypes();

if( count($cts) == 0 )
  print "<strong>".$_SESSION['lims']['langdata']['limsadmin_no_storage_devices']."</strong>";
  
foreach($cts as $ct) {
	$ctype_del_link = "<a href='#' id='containerTypeDel-".$ct->id."'>".$icon_trash."</a>";
	if( containerTypeInUse($ct->id) ) $ctype_del_link = "<i class='icon-trash' title='".$_SESSION['lims']['langdata']['cannot_delete'].": ".$_SESSION['lims']['langdata']['err_in_use']."' rel='tooltip' data-placement='right'></i>";
  print "<tr><td id='containerTypeName-".$ct->id."'>".myhtmlentities($ct->container_type)."</td><td id='containerTypeRows-".$ct->id."'>".$ct->number_of_rows."</td><td id='containerTypeColumns-".$ct->id."'>".$ct->number_of_columns."</td><td id='containerTypeCapacity-".$ct->id."'>".$ct->capacity."</td><td><a href='#' id='containerTypeEdit-".$ct->id."'>".$icon_edit."</a>&nbsp;&nbsp;&nbsp;".$ctype_del_link."</td></tr>\n";
	//print "<tr><td id='containerTypeName-".$ct->id."'>".$ct->container_type."</td><td id='containerTypeRows-".$ct->id."'>".$ct->number_of_rows."</td><td id='containerTypeColumns-".$ct->id."'>".$ct->number_of_columns."</td><td id='containerTypeCapacity-".$ct->id."'>".$ct->capacity."</td><td><a href='#' id='containerTypeEdit-".$ct->id."'><i class='icon-edit' title='".$_SESSION['lims']['langdata']['edit']."' data-placement='right' rel='tooltip'></i></a>&nbsp;&nbsp;&nbsp;<a href='#'><i class='icon-trash' id='containerTypeDel-".$ct->id."' title='".$_SESSION['lims']['langdata']['delete']."' data-placement='right'></i></a></td></tr>\n";
}

?>
</tbody>
</table>
<!-- <br/> -->
<hr>
</div>

<div id="newContainerTypeDef" style="display:none;">
  <h4><?php print $_SESSION['lims']['langdata']['limsadmin_new_container_type']; ?></h4>
  <hr/>
  <form>
  <label><?php print $_SESSION['lims']['langdata']['type']; ?>:</label>
  <input type="text" id="newContainerTypeName">
  <label><?php print $_SESSION['lims']['langdata']['rows']; ?>:</label>
  <input type="text" id="newContainerTypeRows" value="1">
  <label><?php print $_SESSION['lims']['langdata']['columns']; ?>:</label>
  <input type="text" id="newContainerTypeColumns" value="1">
  <label><?php print $_SESSION['lims']['langdata']['capacity']; ?>:</label>
  <input type="text" id="newContainerTypeCapacity" value="1" disabled="disabled">

  <br/>
  <button type="submit" class="btn btn-primary" id="newContainerTypeSave"><?php print $button_save; ?></button>
  </form>
</div>

<button class="btn btn-primary" id="newContainerTypeBtn"><i class="icon-plus icon-white"></i> <?php print $_SESSION['lims']['langdata']['limsadmin_new_container_type']; ?></button>

<!-- Edit Modal -->
<div id="containerTypeEditModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="containerTypeEditModalLabel" aria-hidden="true">
  <div class="modal-header">
    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
    <h4 id="containerTypeEditModalLabel"></h4>
  </div>
  <div class="modal-body">
  <form>
  <label class="control-label" for="containerTypeEditName" id="containerTypeEditNameLabel"><?php print $_SESSION['lims']['langdata']['name']; ?>:</label>
  <input type="text" id="containerTypeEditName">
  
  <label class="control-label" for="containerTypeEditRows" id="containerTypeEditRowsLabel"><?php print $_SESSION['lims']['langdata']['rows']; ?>:</label>
  <input type="text" id="containerTypeEditRows">
  
  <label class="control-label" for="containerTypeEditColumns" id="containerTypeEditColumnsLabel"><?php print $_SESSION['lims']['langdata']['columns']; ?>:</label>
  <input type="text" id="containerTypeEditColumns">

  <label class="control-label" for="containerTypeEditCapacity" id="containerTypeEditCapacityLabel"><?php print $_SESSION['lims']['langdata']['capacity']; ?>:</label>
  <input type="text" id="containerTypeEditCapacity" disabled="disabled">

  </form>
  </div>
  <div class="modal-footer">
    <button class="btn btn-warning" id="containerTypeEditCancel"><?php print $button_cancel; ?></button>
    <button class="btn btn-primary" id="containerTypeEditSave"><?php print $button_save; ?></button>
  </div>
</div>


<!-- Delete Modal -->
<div id="containerTypeDelModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="containerTypeDelModalLabel" aria-hidden="true">
  <div class="modal-header">
    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
    <h4 id="containerTypeDelModalLabel"><i class="icon-trash"></i> <?php print $_SESSION['lims']['langdata']['delete']; ?></h4>
  </div>
  <div class="modal-body" id="containerTypeDelModalBody">
  </div>
  <div class="modal-footer">
    <button class="btn btn-warning" id="containerTypeDelCancel"><?php print $button_cancel; ?></button>
    <button class="btn btn-danger" id="containerTypeDelConfirm"><?php print $button_trash; ?></button>
  </div>
</div>


<script src="core/js/__jquery.tablesorter/jquery.tablesorter.min.js"></script>
<script>
$(function() {
	$("#containerTypesTable").tablesorter({headers:{4:{sorter:false}}});
  
	$('#containerTypeDelCancel').click( function() {
			$('#containerTypeDelConfirm').unbind('click');
			$('#containerTypeDelModal').modal('hide');
		});
		
	$('#containerTypeEditCancel').click( function() {
			$('#containerTypeEditSave').unbind('click');
			$('#containerTypeEditModal').modal('hide');
		});
		
  $('#newContainerTypeBtn').click(function(){
      $(this).slideUp(200);
      $('#containerTypeList').slideUp( 200, function() { $('#newContainerTypeDef').slideDown(200);  });
    });
    
  $('#newContainerTypeSave').click( function(){
    var newCTName = encodeURIComponent( $('#newContainerTypeName').val() );
    var newCTCap = encodeURIComponent( $('#newContainerTypeCapacity').val() );
    var newCTRows = encodeURIComponent( $('#newContainerTypeRows').val() );
    var newCTCols = encodeURIComponent( $('#newContainerTypeColumns').val() );
    
    if( newCTName == "" ) {
      userFeedback("alert-error","<?php print $_SESSION['lims']['langdata']['err_enter_name']; ?>.");
      return false;
    }
    if( ! isInt(newCTCap) ) { userFeedback("alert-error","<?php print $_SESSION['lims']['langdata']['error']." (".$_SESSION['lims']['langdata']['capacity']."): ".$_SESSION['lims']['langdata']['err_int_required']; ?>"); return false; }
    if( ! isInt(newCTRows) ) { userFeedback("alert-error","<?php print $_SESSION['lims']['langdata']['error']." (".$_SESSION['lims']['langdata']['rows']."): ".$_SESSION['lims']['langdata']['err_int_required']; ?>"); return false; }
    if( ! isInt(newCTCols) ) { userFeedback("alert-error","<?php print $_SESSION['lims']['langdata']['error']." (".$_SESSION['lims']['langdata']['columns']."): ".$_SESSION['lims']['langdata']['err_int_required']; ?>"); return false; }

//     $.post("core/ajax_devices.php", { m: "nsd", n: newSDName, l: newSDLab, e: newSDEnv, t: newSDType }, function(){ location.reload(); } );
		$('#newContainerTypeSave').attr('disabled','disabled');
		$('#newContainerTypeSave').html("<? print $button_save; ?> <i class='icon-spinner icon-spin'></i>");
    $.post("core/ajax_devices.php", { m: "nc", n: newCTName, cap: newCTCap, r: newCTRows, c: newCTCols }, function(){ location.reload(); } );
    
    return false;
  });
  
  // calculate the capacity
  $('#newContainerTypeRows').on('input', function(){
    if( ! isInt($('#newContainerTypeRows').val()) ) return false;
    var r = parseInt( $('#newContainerTypeRows').val() );
    var c = parseInt( $('#newContainerTypeColumns').val() );
    $('#newContainerTypeCapacity').val(r*c);
    });
  
  $('#newContainerTypeColumns').on('input', function(){
    if( ! isInt($('#newContainerTypeColumns').val()) ) return false;
    var r = parseInt( $('#newContainerTypeRows').val() );
    var c = parseInt( $('#newContainerTypeColumns').val() );
    $('#newContainerTypeCapacity').val(r*c);
    });
  $('#containerTypeEditRows').on('input', function(){
    if( ! isInt($('#containerTypeEditRows').val()) ) return false;
    var r = parseInt( $('#containerTypeEditRows').val() );
    var c = parseInt( $('#containerTypeEditColumns').val() );
    $('#containerTypeEditCapacity').val(r*c);
    });
  
  $('#containerTypeEditColumns').on('input', function(){
    if( ! isInt($('#containerTypeEditColumns').val()) ) return false;
    var r = parseInt( $('#containerTypeEditRows').val() );
    var c = parseInt( $('#containerTypeEditColumns').val() );
    $('#containerTypeEditCapacity').val(r*c);
    });
    
  $("[id^=containerTypeDel-]").click( function() {
		// check if in use first
    var cid = this.id.split("-")[1];
    var sdname = $('#containerTypeName-'+cid).text();
    $('#containerTypeDelModalBody').html('<p><?php print $_SESSION['lims']['langdata']['delete']; ?> <i>'+sdname+'</i>?</p>');
    
    $('#containerTypeDelConfirm').click(function() {
    $.post("core/ajax_devices.php", { m: "dc", i: cid }, function(){
			$('#containerTypeDelModal').modal('hide');
			$('#containerTypeDelModal').on('hidden', function () {	location.reload() });
			});
      
    });

    $('#containerTypeDelModal').modal();
  });
  
  $("[id^=containerTypeEdit-]").click(function(){
    var cid = this.id.split("-")[1];
    var cname = $('#containerTypeName-'+cid).text();

    $('#containerTypeEditName').val( $('#containerTypeName-'+cid).text() );
    $('#containerTypeEditCapacity').val( $('#containerTypeCapacity-'+cid).text() );
    $('#containerTypeEditRows').val( $('#containerTypeRows-'+cid).text() );
    $('#containerTypeEditColumns').val( $('#containerTypeColumns-'+cid).text() );
    
    $('#containerTypeEditModalLabel').html("<i class='icon-edit'></i> <?php print $_SESSION['lims']['langdata']['limsadmin_edit_container_type']; ?>: <i>"+cname+"</i>");
    $('#containerTypeEditModal').modal();
//     $('#containerTypeEditModal').on('hidden', function() { location.reload(); });
    $('#containerTypeEditSave').click( function(){
      var bValid = true;
      if( $('#containerTypeEditName').val() == "" ) { $('#containerTypeEditName').css('background', '#ffdddd'); $('#containerTypeEditNameLabel').css('color', 'red'); bValid=false;}
      if( !isInt($('#containerTypeEditCapacity').val() ) ) { $('#containerTypeEditCapacity').css('background', '#ffdddd'); $('#containerTypeEditCapacityLabel').css('color', 'red'); bValid=false;}
      if( !isInt($('#containerTypeEditRows').val() ) ) { $('#containerTypeEditRows').css('background', '#ffdddd'); $('#containerTypeEditRowsLabel').css('color', 'red'); bValid=false;}
      if( !isInt($('#containerTypeEditColumns').val() ) ) { $('#containerTypeEditColumns').css('background', '#ffdddd'); $('#containerTypeEditColumnsLabel').css('color', 'red'); bValid=false;}
      if( bValid ) {
		$.post("core/ajax_devices.php", { m: "uc", i: cid, n: $('#containerTypeEditName').val(), cap: $('#containerTypeEditCapacity').val(), r: $('#containerTypeEditRows').val(), c: $('#containerTypeEditColumns').val() }, function(){
			$('#containerTypeEditModal' ).modal( "hide" ); 
			$('#containerTypeEditModal').on('hidden', function () {	location.reload() });
			});
	}
    });
  });
  
	$("[rel=tooltip]").tooltip();
});
</script>