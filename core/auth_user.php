<?php
include "config.common.php";
include "functions.db_connect.php";
include "functions.user.php";
include "functions.lims.php";

session_start();
$username = stripslashes( $_POST['username'] );
// $passwd = hash("sha256", stripslashes($_POST['passwd']) );
$passwd = stripslashes($_POST['passwd']);
//$passwd = stripslashes($_POST['passSHA256']);

$u = authUser($username,$passwd);

// print "auth=".$auth."<br/>";
if( $u!=null && $u->person_id > 0 ) {
	$user_prefs = getUserPrefs($u->person_id);
  $_SESSION['lims']['logged_in'] = 1;
  $_SESSION['lims']['person_id'] = $u->person_id;
  $_SESSION['lims']['first_name'] = $u->first_name;
  $_SESSION['lims']['last_name'] = $u->last_name;
  $_SESSION['lims']['username'] = $u->username;
  $_SESSION['lims']['email_address'] = $u->email_address;
  $_SESSION['lims']['phone'] = $u->phone;
  $_SESSION['lims']['login_method'] = $u->login_method;
  $_SESSION['lims']['lab_id'] = getUserLab($u->person_id);
  if( $_SESSION['lims']['lab_id'] != null )
    $_SESSION['lims']['lab_name'] = getLab( $_SESSION['lims']['lab_id'] )->name;
  else
    $_SESSION['lims']['lab_name'] = "NOLAB";
  $al = getAuthLevel2($u->person_id, $_SESSION['lims']['lab_id']);
  if( $al != null )
    $_SESSION['lims']['auth_level'] = $al->auth_level_id;
  else
    $_SESSION['lims']['auth_level'] = 0;
  
  //$_SESSION['lims']['dateformat'] = getUserDateFormat($u->person_id); //$user_df;
	$_SESSION['lims']['dateformat'] = $user_prefs->date_format_id; //$user_df;
	$_SESSION['lims']['unit_system'] = $user_prefs->units_id;
	$_SESSION['lims']['timeformat'] = $user_prefs->time_format_id;
  

  // TODO: last visited page and redirect to it after login
  if( isset( $_SESSION['lims']['request_uri'] ) ) 
		header("Location: ".$_SESSION['lims']['request_uri'] );
  else 
		//header("Location: ".$site_root."/index.php");
		header("Location: ".$start_page);
  }
else if ( $u == null ){
  $_SESSION['lims']['person_id'] = 0;
  header("Location: ".$site_root."/index.php?l=f");
  }

?>