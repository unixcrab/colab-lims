<?php

if( !isset($_SESSION['lims']) ) session_start();

// print "table ".$modXML->db->table['name']."<p>";
//print "<pre>";
//var_dump($modXML);
//print "</pre>";

if( file_exists("modules/".$modname."/functions.php") ) {
	include "modules/".$modname."/functions.php";
	if( function_exists("customModuleRender") ) {
		include "functions.lims.php";
		include "common.variables.php";
		include "functions.module.api.php";
		//error_log("customModuleRender exists -> exiting");
		customModuleRender();
		return;
		}
	}

$modXML = simplexml_load_file("modules/".$modname.".xml");

// table fields
if( $modXML->db->table ) {

  ?>
  <div id="user_feedback" class="alert alert-info"><? print $_SESSION['lims']['langdata']['please_provide']; ?></div>
  <?php

  print "\n\n<form name='limsmodule__user__config' action=''>\n";
  print "<input type='hidden' id='limsmodule__modname' name='modname' value='".$modname."'>\n";

  foreach($modXML->db->table->field as $field) {
    $ue = false;
    if( $field['user_edit'] ) {
      $ue = true; 
//       error_log("USER EDITABLE FIELD_ ".$field['descr']); 
//       print db_get_col($modXML->db->table['name'],$field['name'],"person_id=".$_SESSION['lims']['person_id'])."<p>";
      
      $field_val = myhtmlentities(db_get_col($modXML->db->table['name'], $field['name'], "person_id=".$_SESSION['lims']['person_id']));
      
      if( ($field_val == "") && isset($field["default"]) ) $field_val = $field["default"];
      
      print "<label for='limsmodule__user__".$modname."' id='limsmodule_label_".$modname."'><strong>".$field['descr']."</strong> :</label>\n";
      print "<input class='input-xlarge' type='text' name='limsmodule__user__".$modname."__".$field['name']."' id='limsmodule__user__".$modname."__".$field['name']."' value='".$field_val."' />\n";
      }
    }
    
    ?>
    <br/>
    <button type="submit" class="btn btn-primary" id="limsmodule_submit_update"><? print $button_save; ?></button>
    </form>
    <?php
}
else {
  ?>
  <div id="user_feedback" class="alert alert-warn"><? print $_SESSION['lims']['langdata']['module_no_options']; ?></div>
  <?php
}

