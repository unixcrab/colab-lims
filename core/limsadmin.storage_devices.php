<div id="user_feedback" class="alert alert-info" style="display: none;">
<? print $_SESSION['lims']['langdata']['please_complete']; ?>
</div>
<div id="storageDeviceList">
<h4><?php print $_SESSION['lims']['langdata']['limsadmin_storage_devices']; ?></h4>
<hr>
<?php
include "functions.lims.php";
$labs = getLabs();
$sts = getStorageTypes();
$sds = getStorageDevices();
?>
<script>
var shelves = new Array();
var shelves_occ = new Array();
var shelfOcc = new Array();
<?php
foreach($sds as $sd) {
	$shelves = getStorageShelves($sd->storage_id);
	$shJSArr = "{";
	$shOccJSArr = "{";
	foreach( $shelves as $k=>$v) {
		$shJSArr .= $k.":'".$v->shelf_number."',"; 
		$shOccJSArr .= $k.":".$v->occupied.",";
		print "shelfOcc[".$k."] = ".$v->occupied.";\n";
	}
	$shJSArr = rtrim($shJSArr,",");
	$shOccJSArr = rtrim($shOccJSArr,",");
	$shJSArr .= "}";
	$shOccJSArr .= "}";
	print "shelves[".$sd->storage_id."] = ".$shJSArr.";\n";
	print "shelves_occ[".$sd->storage_id."] = ".$shOccJSArr.";\n";
}
?>
</script>
<table id="storageDevicesTable" class="table table-hover table-condensed tablesorter" style="width: 80%;">
<thead>
<tr>
<th><?php print $_SESSION['lims']['langdata']['name']; ?></th>
<th><?php print $_SESSION['lims']['langdata']['type']; ?></th>
<th><?php print $_SESSION['lims']['langdata']['shelves']; ?></th>
<th><?php print $_SESSION['lims']['langdata']['laboratory']; ?></th>
<th></th>
</tr>
</thead>
<tbody>
<?php


if( count($sds) == 0 )
  print "<strong>".$_SESSION['lims']['langdata']['limsadmin_no_storage_devices']."</strong>";
foreach($sds as $sd) {
  
	$sd_del_link = "<a href='#' id='storageDeviceDel-".$sd->storage_id."'>".$icon_trash."</a>";
	if( storageDeviceInUse($sd->storage_id) ) $sd_del_link = "<i class='icon-trash' title='".$_SESSION['lims']['langdata']['cannot_delete'].": ".$_SESSION['lims']['langdata']['err_in_use']."' rel='tooltip' data-placement='right'></i>";
	
  $shelves = getStorageShelves($sd->storage_id);
  $nshelves = count($shelves);
  print "<tr><td id='storageDeviceName-".$sd->storage_id."'>".myhtmlentities($sd->identifier)."</td><td id='storageDeviceType-".$sd->storage_id."-".$sd->storage_type_id."'>".myhtmlentities($sts[$sd->storage_type_id]->storage_type)."</td><td>".$nshelves."</td><td id='storageDeviceLab-".$sd->storage_id."-".$sd->laboratory_id."'>".myhtmlentities($labs[$sd->laboratory_id]->name)."</td><td><a href='#' id='storageDeviceEdit-".$sd->storage_id."'>".$icon_edit."</a>&nbsp;&nbsp;&nbsp;".$sd_del_link."</td></tr>\n";

}

?>
</tbody>
</table>
<!-- <br/> -->
<hr>
</div>

<div id="newStorageDeviceDef" style="display:none;">
  <h4><?php print $_SESSION['lims']['langdata']['limsadmin_new_storage_device']; ?></h4>
  <hr/>
  <form>
  <label><?php print $_SESSION['lims']['langdata']['name']; ?>:</label>
  <input type="text" id="newStorageDeviceName">

  <label><?php print $_SESSION['lims']['langdata']['laboratory']; ?>:</label>
  <select id="newStorageDeviceLab">
  <?php
  foreach($labs as $lab) {
    print "<option value='".$lab->id."'>".$lab->name."</option>\n";
  }
  ?>
  </select>

<!--  <label><?php print $_SESSION['lims']['langdata']['environment']; ?>:</label>
  <input type="text" id="newStorageDeviceEnv">-->

  <label><?php print $_SESSION['lims']['langdata']['type']; ?>:</label>
  <select id="newStorageDeviceType">
  <?php
  foreach($sts as $st) {
    print "<option value='".$st->id."'>".$st->storage_type."</option>\n";
  }
  ?>
  </select>

  <br/>
  <button type="submit" class="btn btn-primary" id="newStorageDeviceSave"><?php print $button_save; ?></button>
  </form>
</div>

<button class="btn btn-primary" id="newStorageDeviceBtn"><i class="icon-plus icon-white"></i> <?php print $_SESSION['lims']['langdata']['limsadmin_new_storage_device']; ?></button>

<!-- Edit Modal -->
<div id="storageDeviceEditModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="storageDeviceEditModalLabel" aria-hidden="true">
  <div class="modal-header">
    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
    <h4 id="storageDeviceEditModalLabel"></h4>
  </div>
  <div class="modal-body">
	
    <form class="form-horizontal">
		
			<div class="control-group">
					<label class="control-label" for="storageDeviceEditName" id="storageDeviceEditNameLabel"><?php print $_SESSION['lims']['langdata']['name']; ?>:</label>
					<div class="controls"><input type="text" id="storageDeviceEditName"></div>
			</div>
			
			<div class="control-group">
					<label class="control-label" for="storageDeviceEditLab"><?php print $_SESSION['lims']['langdata']['laboratory']; ?>:</label>
					<div class="controls">
						<select id="storageDeviceEditLab">
						<?php
						foreach($labs as $lab) {
							print "<option value='".$lab->id."'>".myhtmlentities($lab->name)."</option>\n";
						}
						?>
						</select>
					</div>
			</div>

			<div class="control-group">
					<label class="control-label" for="storageDeviceEditType"><?php print $_SESSION['lims']['langdata']['type']; ?>:</label>
					<div class="controls">
						<select id="storageDeviceEditType">
						<?php
						foreach($sts as $st) {
							print "<option value='".$st->id."'>".myhtmlentities($st->storage_type)."</option>\n";
						}
						?>
						</select>
					</div>
			</div>
			
	
			<div class="control-group">
				<label class="control-label" for="storageDeviceEditShelfNew"><?php print $_SESSION['lims']['langdata']['limsadmin_new_shelf']; ?>:</label>
				<div class="controls">
					<input type="text" id="storageDeviceEditShelfNew" class="input-small">
					&nbsp;&nbsp;<button class="btn btn-primary" id="storageDeviceEditShelfAddBtn"> <?php print $button_add; ?></button>
				</div>
			</div>
		
			<div class="control-group">
					<label class="control-label" for="storageDeviceEditShelves"><?php print $_SESSION['lims']['langdata']['shelves']; ?>: </label>
					<div class="controls">
						<select size=5 id="storageDeviceEditShelves"></select>
						&nbsp;&nbsp;
						<button class="btn btn-danger" id="storageDeviceEditShelfDelBtn"><i class="icon-remove icon-white"></i> <?php print $_SESSION['lims']['langdata']['remove']; ?></button>
					</div>
			</div>
  </form>
  
  </div>
  <div class="modal-footer">
    <button class="btn btn-warning" id="storageDeviceEditCancel"><?php print $button_cancel; ?></button>
    <button class="btn btn-primary" id="storageDeviceEditConfirm"><?php print $button_save; ?></button>
  </div>
</div>

<!-- Delete Modal -->
<div id="storageDeviceDelModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="storageDeviceDelLabel" aria-hidden="true">
  <div class="modal-header">
    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
    <h4 id="storageDeviceDelLabel"><i class="icon-trash"></i> <?php print $_SESSION['lims']['langdata']['delete']; ?></h4>
  </div>
  <div class="modal-body">
    <p id="storageDeviceDelModalBody"></p>
  </div>
  <div class="modal-footer">
    <button class="btn btn-warning" id="storageDeviceDelCancel"><?php print $button_cancel; ?></button>
    <button class="btn btn-danger" id="storageDeviceDelConfirm"><?php print $button_trash; ?></button>
  </div>
</div>

<script src="core/js/__jquery.tablesorter/jquery.tablesorter.min.js"></script>
<script>
$(function() {
	$("[rel=tooltip]").tooltip();
	$("#storageDevicesTable").tablesorter({headers:{4:{sorter:false}}});
	
	$('#storageDeviceEditCancel').click( function() {
		$('#storageDeviceEditConfirm').unbind('click');
		$('#storageDeviceEditModal').modal('hide');
	});
	
	$('#storageDeviceDelCancel').click( function() {
		$('#storageDeviceDelConfirm').unbind('click');
		$('#storageDeviceDelModal').modal('hide');
	});
	
  $('#newStorageDeviceBtn').click(function(){
      $(this).slideUp(200);
      $('#storageDeviceList').slideUp( 200, function() { $('#newStorageDeviceDef').slideDown(200);  });
    });
    
  $('#newStorageDeviceSave').click( function(){
    var newSDName = encodeURIComponent( $('#newStorageDeviceName').val() );
    var newSDLab = $('#newStorageDeviceLab').val();
    var newSDType = $('#newStorageDeviceType').val();
    
    if( newSDName == "" ) {
      userFeedback("alert-error","<?php print $_SESSION['lims']['langdata']['err_enter_name']; ?>.");
      
      return false;
    }

		$('#newStorageDeviceSave').attr('disabled','disabled');
		$('#newStorageDeviceSave').html("<? print $button_save; ?> <i class='icon-spinner icon-spin'></i>");
    $.post("core/ajax_devices.php", { m: "nsd", n: newSDName, l: newSDLab, t: newSDType }, function(){ location.reload(); } );
    
    return false;
  });
  

  
  $("[id^=storageDeviceDel-]").click( function() {
    // TODO: must add check for dependencies!
    var sdid = this.id.split("-")[1];
    var sdname = $('#storageDeviceName-'+sdid).text();
    $('#storageDeviceDelModalBody').html('<?php print $_SESSION['lims']['langdata']['delete']; ?> <i>'+sdname+'</i>?');
    $('#storageDeviceDelConfirm').click( function(){
      $.post("core/ajax_devices.php", { m: "d", i: sdid }, function(){
				$('#storageDeviceDelModal').modal( 'hide' );
				$('#storageDeviceDelModal').on('hidden', function () {	location.reload() });
				//location.reload(); 
				});
    });

    $('#storageDeviceDelModal').modal();
  });

	function fillShelfList(sdid) {
		$('#storageDeviceEditShelves option').remove(); // clear list first
    $.each(shelves[sdid], function(k,v) {
			if(typeof v == 'undefined') return;
			var disStr = "";
			var occStr = "";
			if( shelfOcc[k] == 1 ) {
				disStr = "disabled";
				occStr = " (<?php print $_SESSION['lims']['langdata']['err_in_use'];?>)";
				}
      $('#storageDeviceEditShelves').append('<option value='+k+' id="shelfSel-'+k+'" '+disStr+'>'+v+occStr+'</option>');
      });
	}
	
  function ajaxGetShelves(sdid) {
    $.getJSON('core/ajax_devices.php?m=gs&sdid='+sdid, function(data) {
      shelves[sdid] = new Array();
			shelfOcc = new Array();
      $.each(data, function(k,v) {
				shelves[sdid][k] = v.shelf_number;
				shelfOcc[k] = v.occupied;
      });
			fillShelfList(sdid);
    });
  }
  
  $("[id^=storageDeviceEdit-]").click(function() {
    var sdid = this.id.split("-")[1];
//     var name = $('#storageDeviceName-'+sdid).text();
    var selLabId = $('[id^=storageDeviceLab-'+sdid+'-]').attr('id').split('-')[2];
    $('#storageDeviceEditLab').val(selLabId).attr('selected',true);
    
    var selTypeId = $('[id^=storageDeviceType-'+sdid+'-]').attr('id').split('-')[2];
    $('#storageDeviceEditType').val(selTypeId).attr('selected',true);
    
    $('#storageDeviceEditName').val( $('#storageDeviceName-'+sdid).text() );
    
		fillShelfList(sdid);
    
    $('#storageDeviceEditShelfDelBtn').attr('disabled','disabled');
		
    $('[id^=shelfSel-]').click(function(){
			var shid = this.id.split("-")[1];
			if( shelfOcc[shid]==0 )
				$('#storageDeviceEditShelfDelBtn').removeAttr('disabled');
			else
				$('#storageDeviceEditShelfDelBtn').attr('disabled','disabled');
			});
		
    $('#storageDeviceEditShelfDelBtn').click( function(){
      var shid = $('#storageDeviceEditShelves option:selected').attr('id').split('-')[1];
      $.post("core/ajax_devices.php", {m:"dsh", shid:shid}, function() {
				ajaxGetShelves(sdid);
				});
      return false;
      });
    
    $('#storageDeviceEditShelfAddBtn').click( function(){
      var sn = encodeURIComponent( $('#storageDeviceEditShelfNew').val() );
      if( sn == "" ) return false;
			$('#storageDeviceEditShelfAddBtn').attr('disabled','disabled');
			$('#storageDeviceEditShelfAddBtn').html("<? print $button_add; ?> <i class='icon-spinner icon-spin'></i>");
      $.post("core/ajax_devices.php", {m:"nsh", sn:sn, sdid:sdid}, function() {
				ajaxGetShelves(sdid);
				$('#storageDeviceEditShelfNew').val('');
				$('#storageDeviceEditShelfAddBtn').removeAttr('disabled');
				$('#storageDeviceEditShelfAddBtn').html("<? print $button_add; ?>");
				});
      return false;
    });
    
    
    $('#storageDeviceEditModal').modal();
		
    $('#storageDeviceEditConfirm').click( function() {
      var bValid = true;
      if( $('#storageDeviceEditName').val() == "" ) { $('#storageDeviceEditName').css('background', '#ffdddd'); $('#storageDeviceEditNameLabel').css('color', 'red'); bValid=false;}
      if( bValid ) {
				$('#storageDeviceEditConfirm').attr('disabled','disabled');
				$('#storageDeviceEditConfirm').html("<? print $button_save; ?> <i class='icon-spinner icon-spin'></i>");
				$.post("core/ajax_devices.php", { m: "u", i: sdid, n: $('#storageDeviceEditName').val(), l: $('#storageDeviceEditLab').val(), t: $('#storageDeviceEditType').val() }, function(){ 
				$('#storageDeviceEditModal').modal('hide'); 
				$('#storageDeviceEditModal').on('hidden', function () {	location.reload() });
				//location.reload(); 
				});
			}
    });
    
    $('#storageDeviceEditModalLabel').html('<i class="icon-edit"></i> <?php print $_SESSION['lims']['langdata']['limsadmin_edit_storage_device']; ?>: <i>'+$('#storageDeviceName-'+sdid).text()+'</i>');

  });
  
});
</script>
