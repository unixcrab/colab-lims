<?php
include "config.common.php";
include_once "constants.php";
include_once "classes.php";
include_once "functions.db_connect.php";

function getModuleData($modname,$where_clause="true",$order_by="") {
	global $tablePrefix;
	
	$table = $tablePrefix."module_".$modname;
	//if( ($table=="DBTBL_person") || ($table=="DBTBL_laboratory_personnel") ) return null;
	$retarr = array();
	
	if(($order_by!="") || ($order_by!=null)) 
		$sql = "select * from ".$table." where ".$where_clause." order by ".$order_by;
	else
		$sql = "select * from ".$table." where ".$where_clause;
	//error_log($sql);
	$r = dbq($sql);
	while( $u = $r->fetch_object() ) {
    $retarr[$u->id] = $u;
  }
  return $retarr;
}


function addModuleData($modname, $data_array) {
	global $tablePrefix;
	global $mysqli;
	$table = $tablePrefix."module_".$modname;
	$fields = "(".implode(",",array_keys($data_array)).")";
	foreach($data_array as $k=>$v) {
		if(is_string($v)) $data_array[$k] = "'".mysqli_real_escape_string($mysqli,$v)."'";
	}
	$vals = "(".implode(",",array_values($data_array)).")";
	$sql = "insert into ".$table.$fields." values".$vals;
	error_log($sql);
	if($r = dbq($sql)) return 1;
	else return 0;
}

function delModuleData($modname, $data_array) {
	global $tablePrefix;
	global $mysqli;
	$table = $tablePrefix."module_".$modname;
	
	$sql = "delete from ".$table." where ";
	
	foreach($data_array as $k=>$v) {
		if(is_string($v)) {
			$v = "'".mysqli_real_escape_string($mysqli,$v)."'";
			$sql .= $k." like ".$v." and ";
			}
		else {
			$sql .= $k."=".$v." and ";
		}
	}
	$sql.="true";
	
	error_log("delModuleData() ".$sql);
	
	if($r = dbq($sql)) return 1;
	else return 0;
}




?>