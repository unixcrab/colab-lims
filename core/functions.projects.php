<?php
include "config.common.php";
include_once "constants.php";
include_once "classes.php";
include_once "functions.db_connect.php";

function addProjectMember($project_id, $person_id) {
  //global DBTBL_projects_members;
  $sql = "select * from ".DBTBL_projects_members." where project_id=".$project_id." and person_id=".$person_id;
//   error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows >= 1 ) return -1;
  
  $sql = "insert into ".DBTBL_projects_members."(project_id,person_id) values(".$project_id.",".$person_id.")";
//   error_log($sql);
  dbq($sql);
  return 1;
}

function removeProjectMember($project_id, $person_id) {
  //global DBTBL_projects_members;
  $sql = "delete from ".DBTBL_projects_members." where project_id=".$project_id." and person_id=".$person_id;
//   error_log($sql);
  $r = dbq($sql);
  return 1;
}

function getProjectMembers($project_id) {
  //global DBTBL_projects_members;
  $retarr = array();
  $sql = "select person_id from ".DBTBL_projects_members." where project_id=".$project_id;
  //error_log($sql);
  $r = dbq($sql);
  while( $p = $r->fetch_object() ) {
//     print $p->person_id."<br/>";
    $u = getUserDetails($p->person_id);
//     print "<br/>".$u->first_name;
		if( $u != null ) $retarr[] = $u;
  }
  return $retarr;
}

function getProject($person_id, $project_id) {
  //global DBTBL_projects;
  
  $pmems = getProjectMembers($project_id);
  $pids = array();
  foreach($pmems as $pmem) {
    $pids[] = $pmem->person_id;
  }
  
  $sql = "select * from ".DBTBL_projects." where active=1 and person_id=".$person_id." and project_id=".$project_id." limit 1";
//   error_log($sql);
  $r = dbq($sql);
  
  if( $r->num_rows == 1 ) {
    $p = $r->fetch_object('Project');
    return $p;
    }
  
  if( in_array( $person_id, $pids ) ) {
    // it may not be the owner but rather a member, so get it.
    $sql = "select * from ".DBTBL_projects." where active=1 and project_id=".$project_id." limit 1";
//     error_log($sql);
    $r = dbq($sql);
    if( $r->num_rows == 1 ) {
      $p = $r->fetch_object('Project');
      return $p;
    }
  }
  
  return null;
}

function getActiveProjects($person_id) {
  //global DBTBL_projects;
  global $projects_max_listsize;
  $retarr = array();
  
  $sql = "select * from ".DBTBL_projects." where active=1 and person_id=".$person_id." order by name limit ".$projects_max_listsize;
//   error_log($sql);
  $r = dbq($sql);
  while( $p = $r->fetch_object('Project') ) {
    $retarr[] = $p;
  }
  return $retarr;
}

function getActiveProjectsForMember($person_id) {
  //global DBTBL_projects;
  //global DBTBL_projects_members;
  global $projects_max_listsize;
  
//   $pids = array();
//   $sql = "SELECT project_id FROM ".DBTBL_projects_members." WHERE person_id=".$person_id;
//   $r = dbq($sql);
//   while( $p = $r->fetch_object() ) {
//     $pids[] = (int)($p->project_id);
//   }
//   var_dump($pids);

  $retarr = array();
//   $inlist = implode(",", $pids);
//   $sql = "select * from ".DBTBL_projects." where active=1 and project_id in (".$inlist.") order by name limit ".$projects_max_listsize;
  $sql = "select * from ".DBTBL_projects." where active=1 and project_id in (SELECT project_id FROM ".DBTBL_projects_members." WHERE person_id=".$person_id.") order by name limit ".$projects_max_listsize;
//   print $sql;
  $r = dbq($sql);
  
//   print "num_rows ".$r->num_rows;
  if( isset($r->num_rows) && ($r->num_rows > 0) ) {
    while( $p = $r->fetch_object('Project') ) {
      $retarr[] = $p;
    }
  }
  return $retarr;
}

function addProject($person_id,$projname,$start_date,$end_date) {
  //global DBTBL_projects;
  global $mysqli;
  
  $sql = "select * from ".DBTBL_projects." where name like '".mysqli_real_escape_string($mysqli,$projname)."' limit 1";
//   error_log($sql);
  $r = dbq($sql);
  if( $r->num_rows == 1 ) {
//     $sql = "update ".DBTBL_modules." set active=1 where name like '".$mname."'";
//     error_log($sql);
//     dbq($sql);
    return -1;
  }
  else {
    $sql = "insert into ".DBTBL_projects."(person_id,name,start_date,end_date,active) values(".$person_id.",'".mysqli_real_escape_string($mysqli,$projname)."',".$start_date.",".$end_date.",1)";
//     error_log($sql);
    dbq($sql);
    return mysqli_insert_id($mysqli); // return new project id
  }
  return 0;
}

function addProjectFile($person_id, $project_id, $file_name, $timestamp) {
  //global DBTBL_projects_files;
  global $mysqli;
  
  $sql = "insert into ".DBTBL_projects_files."(person_id,project_id,file_name,timestamp) values(".$person_id.",".$project_id.",'".mysqli_real_escape_string($mysqli,$file_name)."',".$timestamp.")";
  dbq($sql);
//   error_log($sql);
}

function getProjectFile($file_id) {
  //global DBTBL_projects_files;
  $sql = "select * from ".DBTBL_projects_files." where file_id=".$file_id;
  $r = dbq($sql);
  return $r->fetch_object('ProjectFile');
}

function getProjectFiles($project_id) {
  //global DBTBL_projects_files;
  global $projects_max_files;
//   global $mysqli;

  $retarr = array();
  $sql = "select * from ".DBTBL_projects_files." where project_id=".$project_id." order by timestamp desc limit ".$projects_max_files;
//   error_log($sql);
  
  $r = dbq($sql);
  while( $f = $r->fetch_object('ProjectFile') ) {
    $retarr[$f->file_id] = $f;
  }
  return $retarr;
}

function deleteProjectFile($person_id,$file_id) {
  //global DBTBL_projects_files;
  global $projects_dir;
  
  //error_log(__FILE__." deleting ".$file_id." for ".$person_id);
  
  $pf = getProjectFile($file_id);
  $proj = getProject($person_id, $pf->project_id);
  
  $sql = "";
  if( $person_id == $pf->person_id ) // if file is owned by person
    $sql = "delete from ".DBTBL_projects_files." where file_id=".$file_id." and person_id=".$person_id;
  if( $person_id == $proj->person_id ) // if project is owned by person
    $sql = "delete from ".DBTBL_projects_files." where file_id=".$file_id." and project_id=".$proj->project_id;
    
  //error_log($sql);
  
  $r = dbq($sql);
  if( isset($r->num_rows) && ($r->num_rows > 0) ) return 1; // something was deleted
  
  // unlink the actual file
  if( file_exists($projects_dir.$pf->project_id."/files/".$pf->file_name) ) {
    //error_log("deletig ".$projects_dir.$pf->project_id."/files/".$pf->file_name);
    unlink($projects_dir.$pf->project_id."/files/".$pf->file_name);
    }
  
  return 0;
}
?>