<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "constants.php";

$l = "en-GB";
if( isset($_GET['l']) ) $l = $_GET['l'];

$i = "";
if( isset($_GET['i']) ) $i = $_GET['i'];
if( $i == "" ) return;

if( ($l=="en-GB") && file_exists("lang/lang.gb.php") ) include "lang/lang.gb.php";
elseif( file_exists("lang/lang.".$l.".php") ) include "lang/lang.".$l.".php";

//error_log(__FILE__." returning ".$langdata[$i]);
header('Content-type: application/json');
print json_encode(array("s"=>$langdata[$i]));
return;
?>