<?php
$pid = stripslashes($_GET['pid']);
$uri = "index.php?p=bionfprojs&sp=projects.open&pid=".$pid;
$section = "members";

if( isset($_GET['s']) ) $section = $_GET['s'];
// print "section=".$section;

$person_id = $_SESSION['lims']['person_id'];

$proj = getProject($person_id, $pid);

if( $proj == null ) {
  print "<div class='well'>".$_SESSION['lims']['langdata']['page_access_denied']."</div>";
  return;
}

$pname = myhtmlentities($proj->name);
$_SESSION['lims']['current_project_id'] = (int)($pid);

$proj_owner = true;
if( $proj->person_id != $person_id ) $proj_owner = false;
// print "OWNER: ".$proj_owner;
?>
<script>curr_projid=<?php print $pid; ?>;</script>

<table>
<tr>
<td>
  <?php print "<h4>".$pname."</h4>\n"; ?>
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td>
  <div class="pagination">
    <ul>
      <li id="projMemberAdminLink"><a href="<?php print $uri; ?>&s=members"><? print $_SESSION['lims']['langdata']['members']; ?></a></li>
      <li id="projFilesAdminLink"><a href="<?php print $uri; ?>&s=files"><? print $_SESSION['lims']['langdata']['files']; ?></a></li>
     <!-- <li id="projFlowAdminLink" ><a href="<?php print $uri; ?>&s=stages"><? print $_SESSION['lims']['langdata']['projects_flow']; ?></a></li> -->

    </ul>
  </div>
</td>
</tr>
</table>

<?php
include "project.admin.".$section.".php";
// if( $section == "members" ) include "project.admin.members.php";
// if( $section == "files" ) include "project.admin.files.php";
?>
<!--<div id="projAdminMembers" style="border: 0px solid;" >
  <?php
//   include "project.admin.members.php";
  ?>
</div>

<div id="projAdminFiles" style="border: 0px solid;display: none;">
  <?php
//   include "project.admin.files.php";
  ?>
</div>

<div id="projAdminFlow" style="border: 0px solid;display: none;">
  <?php
//   include "project.admin.stages.php";
  ?>
</div>-->
