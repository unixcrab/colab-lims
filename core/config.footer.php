<?php
// include "config.common.php";
// error_log("footer: ".$site_root);

$request_uri = $_SERVER['REQUEST_URI'];
if( isset( $_SESSION['lims']['lang'] ) ) $lang = $_SESSION['lims']['lang'];
else $lang = $site_default_lang;



?>

<footer>
<div class="navbar navbar-fixed-bottom">
	<div class="navbar-inner">
		<div class="container-fluid">

<!-- 			&copy; Company 2012 -->

		    <?php if( $user_logged_in ) { 
				include_once "functions.user.php";
				include_once "functions.lims.php";
				$labs_acc = getUserLabAccess($_SESSION['lims']['person_id']);
			?>
				
				<ul class="nav nav-pills dropup pull-left">
					<li class="dropdown" id="menuLabs">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#menuLabs">
						<i class="icon-beaker"></i> <?php print myhtmlentities($_SESSION['lims']['lab_name']); ?> <b class="caret"></b>
					</a>
					<?php if( count($labs_acc)>1 ) {
						print "<ul class='dropdown-menu'>";
							
							foreach($labs_acc as $la) {
								$thislab = getLab($la->lab_id);
								if( $thislab == null ) continue;
								if( $la->lab_id == $_SESSION['lims']['lab_id'] ) continue;
								$defStr = "";
								if( $la->default_flag == 'Y' ) $defStr = "(".strtolower($_SESSION['lims']['langdata']['default']).")";
								print "<li id='lab_sel-".$la->lab_id."'><a href='core/lab.switch.php?uri=".urlencode($request_uri)."&lid=".$la->lab_id."'><i class='icon-beaker'></i> ".myhtmlentities($thislab->name)." ".$defStr."</a></li>\n";
							}
						
						print "</ul>";
						}
						?>
					
			
					
					
					</li>
		    </ul>
		    <?php } ?>
		      
			<ul class="nav nav-pills dropup pull-right">
			  <li class="dropdown" id="menu1">
			    <a class="dropdown-toggle" data-toggle="dropdown" href="#menu1">
						<? print $lang; ?> <img src="<?php print $site_root."/img/flags/".$lang.".png"; ?>"/>
						<b class="caret"></b>
			    </a>
			    <ul class="dropdown-menu">
			      <li id="lang_sel_gb"><a href="core/lang.switch.php?uri=<? print urlencode($request_uri); ?>&lang=gb">English <img src="<?php print $site_root; ?>/img/flags/gb.png"/></a></li>
			      <li id="lang_sel_de"><a href="core/lang.switch.php?uri=<? print urlencode($request_uri); ?>&lang=de">Deutsch <img src="<?php print $site_root; ?>/img/flags/de.png"/></a></li>
			      <li id="lang_sel_no"><a href="core/lang.switch.php?uri=<? print urlencode($request_uri); ?>&lang=no">Norsk <img src="<?php print $site_root; ?>/img/flags/no.png"/></a></li>
			    </ul>
			  </li>
			</ul>
			
</div>
	</div>
</div>			
			
</footer>



<script type="text/javascript" src="<?print $site_root.'/core/js/bootstrap.js';?>"></script>
<script type="text/javascript" src="<?print $site_root.'/core/js/jquery-functions.js';?>"></script>
<script type="text/javascript" src="<?print $site_root.'/core/js/jquery.cookie.js';?>"></script>
	
<script>
	$(function() {
	 $.cookie("colablang", "<?php print $lang; ?>", { expires : 14 });
	});
</script>


<?php
 
 //print "<pre>";
 //var_dump($_COOKIE['colablang']);
 //var_dump($_SESSION['lims']);
 //print "</pre>";
 
?>
