<?php
include "functions.lims.php";

// $pairTables = array("domains" => "domains");
// var_dump($_GET);

$attr = "groups";
if( isset($_GET['attr'] ) ) $attr = $_GET['attr'];

$attrArr = array();
$header = "Attribute";
$new_button_text = "New"; // $_SESSION['lims']['langdata']['limsadmin_new_domain'];
$new_attr_label = "Name";
$edit_modal_header = "";

if( $attr == "sample_types" ) {
  $sdata = getSampleTypes();
  $header = $_SESSION['lims']['langdata']['limsadmin_sample_types'];
  $new_button_text = $_SESSION['lims']['langdata']['limsadmin_new_sample_type'];
  $new_attr_label = $_SESSION['lims']['langdata']['limsadmin_new_sample_type'];
	$edit_modal_header = $_SESSION['lims']['langdata']['limsadmin_edit_sample_type'];
  ?>
  <script>var editAttribute = "sample_types";</script>
  <?php
  foreach($sdata as $s) {
    $attrValArr = array($s->sample_type);
    $a = new Attribute($s->id, $attrValArr);
    $attrArr[] = $a;
  }
		
}

if( $attr == "groups" ) {
	$ddata = getDomains();
	$header = $_SESSION['lims']['langdata']['limsadmin_domains'];
	$new_button_text = $_SESSION['lims']['langdata']['limsadmin_new_domain'];
	$new_attr_label = $_SESSION['lims']['langdata']['limsadmin_new_domain'];
	$edit_modal_header = $_SESSION['lims']['langdata']['limsadmin_edit_domain'];
	?>
	<script>var editAttribute = "groups";</script>
	<?php
	
	foreach($ddata as $d) {
		$attrValArr = array($d->domain_name);
		$a = new Attribute($d->id, $attrValArr);
		$attrArr[] = $a;
		}
		
	}
	
if( $attr == "skills" ) {
	$sdata = getSkills();
	$header = $_SESSION['lims']['langdata']['limsadmin_skills'];
	$new_button_text = $_SESSION['lims']['langdata']['limsadmin_new_skill'];
	$new_attr_label = $_SESSION['lims']['langdata']['limsadmin_new_skill'];
	$edit_modal_header = $_SESSION['lims']['langdata']['limsadmin_edit_skill'];
	?>
	<script>var editAttribute = "skills";</script>
	<?php
	
	foreach($sdata as $s) {
		$attrValArr = array($s->skill);
		$a = new Attribute($s->id, $attrValArr);
		$attrArr[] = $a;
		}
	}
?>
<h4><? print $header; ?></h4>
<hr>
<div  id="lab_list_div">
	<table id="attrTable" class="table table-hover table-condensed tablesorter" style="width: 60%;">
	<thead>
	<tr>
	<th><?php print $_SESSION['lims']['langdata']['description']; ?></th>
	<th></th>
	</tr>
	</thead>
	<tbody>
	<?php	
	
$attrCount = 1;
foreach( $attrArr as $a ) {
	$attr_del_link = "<a href='#' id='btn_delattr-".$a->id."'>".$icon_trash."</a>";
	if( attrTypeInUse($attr, $a->id) ) $attr_del_link = "<i class='icon-trash' title='".$_SESSION['lims']['langdata']['cannot_delete'].": ".$_SESSION['lims']['langdata']['err_in_use']."' rel='tooltip' data-placement='right'></i>";
	
	print "<tr><td><div id='attr_edit-".$a->id."'>".myhtmlentities($a->value[0])."</div></td>".
	"<td><a href='#' id='btn_editattr-".$a->id."'>".$icon_edit."</a>&nbsp;&nbsp;&nbsp;".$attr_del_link."</td></tr>\n";
	

	$attrCount += 1;
	}
	
?>
	</tbody>
	</table>
</div>

<!-- Edit Modal -->
<div id="attrEditModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="attrEditModalLabel" aria-hidden="true" data-keyboard="false">
  <div class="modal-header">
    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
    <h4 id="attrEditModalLabel"></h4>
  </div>
  <div class="modal-body">
  <form>
  <label class="control-label" for="attrEditValue" id="attrEditValueLabel"><?php print $_SESSION['lims']['langdata']['description']; ?>:</label>
  <input class="input-xlarge" type="text" id="attrEditValue">

  </form>
  </div>
  <div class="modal-footer">
    <!--<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true" id="attrEditCancel"><?php print $button_cancel; ?></button>-->
		<button class="btn btn-warning" id="attrEditCancel"><?php print $button_cancel; ?></button>
    <button type="submit" class="btn btn-primary" id="attrEditSave"><?php print $button_save; ?></button>
  </div>
</div>

<!-- Delete Modal -->
<div id="attrDelModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="attrDelModalLabel" aria-hidden="true" data-keyboard="false">
  <div class="modal-header">
    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
    <h4 id="attrDelModalLabel"><i class="icon-trash"></i> <?php print $_SESSION['lims']['langdata']['delete']; ?></h4>
  </div>
  <div class="modal-body" id="attrDelModalBody"></div>
  <div class="modal-footer">
    <!--<button class="btn btn-warning" data-dismiss="modal" aria-hidden="true"><?php print $button_cancel; ?></button>-->
		<button class="btn btn-warning" id="attrDelCancel"><?php print $button_cancel; ?></button>
    <button type="submit" class="btn btn-danger" id="attrDelConfirm"><?php print $button_trash; ?></button>
  </div>
</div>

<!-- <button class="btn" id="button_newattr"><? print $new_button_text; ?></button> -->
<hr>
<!-- <div id="new_attr_container"   style="display:none;"> -->
<div id="new_attr_container" style="width: 60%;">
	
	<form id="form_newattr" name="form_newattr" action="" class="well well-small form-inline">
	<fieldset>
		<label for="newattr_val" id="newattr_val_label"><strong><? print $new_attr_label ?></strong></label>
	  &nbsp;<input type="text"  name="newattr_val" id="newattr_val" size="15" value="" />
	  
	  &nbsp;
	  <button type="submit" class="btn btn-primary" id="newattr_submit"><? print $button_save; ?></button>
	</fieldset>
	</form>
</div>


<script src="core/js/__jquery.tablesorter/jquery.tablesorter.min.js"></script>
<script>
$(function() {
	$('#attrEditModal').modal('hide');
	$('#attrDelModal').modal('hide');
	$("#attrTable").tablesorter({headers:{1:{sorter:false}}});

	$("[rel=tooltip]").tooltip();
	
	$('#attrDelCancel').click( function() {
		$('#attrDelConfirm').unbind('click');
		$('#attrDelModal').modal('hide');
	});
	
	$('*[id^=btn_delattr-]').click( function() {
		$('#attrDelModal').modal('show');
		//delete aid;
		var aid = this.id.split("-")[1];
		var aval = $('#attr_edit-'+aid).text();
		$('#attrDelModalBody').text("<?php print $_SESSION['lims']['langdata']['delete']; ?> "+aval+"?");
		
		$('#attrDelConfirm').click( function(){
			$.post("core/ajax_attr_new_edit.php", { a:'d', e:editAttribute, i:aid }, function(){
						$('#attrDelModal' ).modal( "hide" ); 
						$('#attrDelModal').on('hidden', function () {	location.reload() });
					});
			});
	});
	
	$('#attrEditCancel').click( function() {
		$('#attrEditSave').unbind('click');
		$('#attrEditModal').modal('hide');
	});
	
	$("[id^=btn_editattr-]").click(function() {
		delete aid;
		delete aval;
		delete ndescr;
			
		var aid = this.id.split("-")[1];
		var aval = $('#attr_edit-'+aid).text();
		$('#attrEditValue').val( aval );
		$('#attrEditModalLabel').html('<i class="icon-edit"></i>  <?php print $edit_modal_header; ?>: <i>'+aval+'</i>');
		$('#attrEditModal').modal();
		
		$('#attrEditSave').click( function(){
			var ndescr = $('#attrEditValue').val();
			
			if( ndescr == "" ) return false;
			
			if( ndescr == aval ) {
				$('#attrEditModal').modal('hide'); // no change;
				$('#attrEditSave').unbind('click');
				}
			else {
				$('#attrEditSave').attr('disabled','disabled');
				$('#attrEditSave').html("<? print $button_save; ?> <i class='icon-spinner icon-spin'></i>");
				$.post("core/ajax_attr_new_edit.php", { a:'e', e:editAttribute, i:aid, v:ndescr}, function(){
						$('#attrEditModal' ).modal( "hide" ); 
						$('#attrEditModal').on('hidden', function () {	location.reload() });
					});
					
			}
			
			});
			
			return false;
		});
  });
</script>

