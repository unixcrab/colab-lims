<?php

session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "functions.db_connect.php";
include "functions.user.php";

$username = $_SESSION['lims']['username'];
$old_passwd=stripslashes( $_POST['old_pw'] );
$new_passwd=stripslashes( $_POST['new_pw'] );
//error_log("ajax u=".$username." o=[".$old_passwd."] n=[".$new_passwd."]");

if( $new_passwd == "" ) { print json_encode( array("resp"=>1) ); return; }

$old_enc_passwd = hash("sha256",$old_passwd);
$new_enc_passwd = hash("sha256",$new_passwd);

// error_log("ajax u=".$username." o=".$old_enc_passwd." n=".$new_enc_passwd);

//if( authUser($username,$old_enc_passwd) == null ) {
if( authUser($username,$old_passwd) == null ) {
  error_log("change ".$username." passwd with ".$old_enc_passwd." failed. Old incorrect.");
  print json_encode( array("resp"=>2) );
  return;
}

//changeUserPasswd($username, $new_enc_passwd);
changeUserPasswdByID($_SESSION['lims']['person_id'], $new_enc_passwd);
print json_encode( array("resp"=>3) );
?>
