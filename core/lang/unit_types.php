<?php
$unit_types = array(
	"gb" => array( "Temperature", "Mass", "Volume", "Length", "Pressure",  "Concentration"),
	"de" => array( "Temperatur", "Masse", "Volumen", "Länge", "Druck", "Konzentration"),
	"no" => array( "Temperatur", "Mass", "Volum", "Lengde", "Press", "Konsentrasjon")
	);
	
$unit_bases = array( "°C", "Kg", "L", "m", "Pa", "N", "mol/L");
?>