<?php
//include_once("core/constants.php");

$langdata = array();

// generic terms
$langdata['edit'] = "Ändern";
$langdata['delete'] = "Löschen";
$langdata['cannot_delete'] = "nicht löschbar";
$langdata['delete_file'] = "Datei löschen";
$langdata['save'] = "Speichern";
$langdata['new'] = "Neu";
$langdata['confirm'] = "Bestätigen";
$langdata['cancel'] = "Abbrechen";
$langdata['configure'] = "Konfigurieren";
$langdata['domain'] = "Gruppe";
$langdata['laboratory'] = "Labor";
$langdata['laboratories'] = "Labore";
$langdata['units'] = "Einheiten";
$langdata['unit_system'] = "Einheitensystem";
$langdata['description'] = "Beschreibung";
$langdata['enable'] = "Aktivieren";
$langdata['enabled'] = "Aktiviert";
$langdata['misc'] = "Sonstiges"; // miscellaneous
$langdata['prefs'] = "Einstellung";
$langdata['create'] = "Erstellen";
$langdata['member'] = "Mitglied";
$langdata['members'] = "Mitglieder";
$langdata['file'] = "Datei";
$langdata['files'] = "Dateien";
$langdata['invite'] = "Einladen";
$langdata['please_complete'] = "Bitte füllen Sie alle Felder aus";
$langdata['please_provide'] = "Bitte geben Sie so viele Informationen wie möglich";
$langdata['owner'] = "Besitzer/in";
$langdata['upload_date'] = "Uploaddatum";
$langdata['modified_date'] = "Änderungsdatum";
$langdata['create_date'] = "Erstellungsdatum";
$langdata['details'] = "Angaben";
$langdata['comment'] = "Kommentar";
$langdata['event'] = "Ereignis";
$langdata['events'] = "Ereignisse";
$langdata['date'] = "Datum";
$langdata['close'] = "Schließen";
$langdata['name'] = "Name";
$langdata['first_name'] = "Vorname";
$langdata['last_name'] = "Nachname";
$langdata['environment'] = "Umgebung";
$langdata['type'] = "Typ";
$langdata['capacity'] = "Fassungsvermögen";
$langdata['row'] = "Zeile";
$langdata['column'] = "Spalte";
$langdata['rows'] = "Zeilen";
$langdata['columns'] = "Spalten";
$langdata['error'] = "Fehler";
$langdata['shelf'] = "Regal";
$langdata['shelves'] = "Regale";
$langdata['sample'] = "Probe";
$langdata['samples'] = "Proben";
$langdata['carton'] = "Schachtel";
$langdata['cartons'] = "Schachteln";
$langdata['cartons_list'] = "Schachteln";
$langdata['cartons_search'] = "Schachteln suchen";
$langdata['samples_search'] = "Proben suchen";
$langdata['cartons_mine'] = "Meine Schachteln";
$langdata['supplies'] = "Vorrat";
$langdata['location'] = "Standort";
$langdata['barcode'] = "Strichcode";
$langdata['search'] = "Suche";
$langdata['search_results'] = "Suchergebnisse";
$langdata['add'] = "Hinzufügen";
$langdata['remove'] = "Entfernen";
$langdata['volume'] = "Inhalt";
$langdata['concentration'] = "Konzentration";
$langdata['well'] = "Näpfchen";
$langdata['control'] = "Kontrollprobe";
$langdata['depleted'] = "Aufgebraucht";
$langdata['active'] = "Aktiv";
$langdata['inactive'] = "Inaktiv";
$langdata['primer'] = "Primer";
$langdata['uniq_name'] = "Eindeutiger Name";
$langdata['lock'] = "Sperren";
$langdata['unlock'] = "Öffnen";
$langdata['empty'] = "Leer";
$langdata['container'] = "Gefäß";
$langdata['list'] = "Liste";
$langdata['user'] = "Nutzer/in";
$langdata['users'] = "Nutzer/innen";
$langdata['new_user'] = "Neu/e Nutzer/in";
$langdata['email_address'] = "Email-Adresse";
$langdata['phone'] = "Telefon";
$langdata['default'] = "Standardeinstellung";
$langdata['select'] = "Auswählen";
$langdata['access'] = "Zugang";
$langdata['level'] = "Niveau";
$langdata['warning'] = "Achtung";
$langdata['all'] = "Alles";
$langdata['reset'] = "zurücksetzen";
$langdata['contents'] = "Inhalt";
$langdata['utilisation'] = "Auslastung";
$langdata['action'] = "Aktion";
$langdata['permissions'] = "Erlaubnis";
$langdata['set'] = "Aktiviert";
$langdata['unset'] = "Deaktiviert";
$langdata['yes'] = "Ja";
$langdata['no'] = "Nein";
$langdata['hour'] = "Stunde";
$langdata['minute'] = "Minuten";
$langdata['reserve'] = "Reservieren";
$langdata['reserved'] = "Reserviert";
$langdata['from'] = "von";
$langdata['to'] = "bis";
$langdata['by'] = "von";
$langdata['day'] = "Tag";
$langdata['week'] = "Woche";
$langdata['month'] = "Monat";
$langdata['menu'] = "Menü";
$langdata['menus'] = "Menüs";
$langdata['section'] = "Teil";

$langdata['reset_password'] = "Passwort zurücksetzen";
$langdata['account_admin'] = "Profil";
$langdata['login'] = "Anmelden";
$langdata['logout'] = "Abmelden";
$langdata['username'] = "Benutzername";
$langdata['password'] = "Passwort";
$langdata['login_failed'] = "Fehler bei der Anmeldung. Bitte Überprüfen Sie Benutzernamen und Passwort.";
$langdata['profile_details'] = "Meine Daten";
$langdata['page_access_denied'] = "Zugang zu dieser Seite verweigert";
$langdata['current_password'] = "Aktuelles Passwort";
$langdata['new_password'] = "Neues Passwort";
$langdata['repeat_password'] = "Passwort wiederholen";
$langdata['ldap_ad_ident'] = "LDAP/Active Directory Benutzername";

$langdata['topmenu_inventory'] = "Inventar";
$langdata['topmenu_projects'] = "Projekte";
$langdata['topmenu_admin'] = "Admin";
$langdata['topmenu_bioinformatics_projects'] = "Bioinformatik-Projekte";

// modules
$langdata['module_name'] = "Modulname";
$langdata['module_no_options'] = "Kann nicht vom Benutzer konfiguriert werden";
$langdata['module_choose_section'] = "Bitte wählen Sie einen Teil";
$langdata['module_choose_menu'] = "Bitte wählen Sie ein Menü";

// projects
$langdata['projects'] = "Projekte";
$langdata['projects_current'] = "Aktuelle";
$langdata['projects_new'] = $langdata['new'];
$langdata['projects_all'] = "Alle";
$langdata['projects_name'] = "Projektname";
$langdata['projects_startdate'] = "Startdatum";
$langdata['projects_enddate'] = "Erwartetes Ende";
$langdata['projects_flow'] = "Projektphasen";
$langdata['projects_my'] = "Meine Projekte";
$langdata['projects_involved'] = "Projekte, an denen ich beteiligt bin";
$langdata['projects_file_del_info'] = "<strong>Datei-Upload/löschen Rechte:</strong><br><ul><li>Alle Projektbeteiligten können Dateien hochladen.</li><li>Nur der/die (Besitzer/in) einer Datei oder eines Projekts kann Dateien löschen.</li></ul>";


// galaxy fields
$langdata['galaxy_details'] = "Galaxy config";

// lims admin
$langdata['limsadmin_modules'] = "Module";
$langdata['limsadmin_inventory'] = "Inventar";
$langdata['limsadmin_sample_types'] = "Probenarten";
$langdata['limsadmin_container_types'] = "Schachtelart";
$langdata['limsadmin_labs'] = "Labore";
$langdata['limsadmin_domains'] = "Gruppen";
$langdata['limsadmin_infrastructure'] = "Infrastruktur";
$langdata['limsadmin_personnel'] = "Personal";
$langdata['limsadmin_skills'] = "Fähigkeiten";
$langdata['limsadmin_storage_devices'] = "Aufbewahrungsgeräte";
$langdata['limsadmin_storage_device'] = "Aufbewahrungsgerät";
$langdata['limsadmin_edit_user'] = "Benutzer bearbeiten";
$langdata['limsadmin_enable_user'] = "Benutzer/in aktivieren";
$langdata['limsadmin_lab_access'] = "Labor Zugang";

// lims admin LABS
$langdata['limsadmin_new_lab'] = "Neues Labor";
$langdata['limsadmin_lab_name'] = "Laborname";
$langdata['limsadmin_lab_domain'] = "Laborgruppe";

// lims admin Sample Types
$langdata['limsadmin_new_sample_type'] = "Neue Probenarten";
$langdata['limsadmin_edit_sample_type'] = "Probenarten bearbeiten";

// lims admin Domains
$langdata['limsadmin_new_domain'] = "Neue Gruppe";
$langdata['limsadmin_edit_domain'] = "Gruppe bearbeiten";

// lims admin Skills
$langdata['limsadmin_new_skill'] = "Neue Fähigkeit";
$langdata['limsadmin_edit_skill'] = "Fähigkeit bearbeiten";

// lims admin Storage Devices
$langdata['limsadmin_new_storage_device'] = "Neues Aufbewahrungsgerät";
$langdata['limsadmin_edit_storage_device'] = "Aufbewahrungsgerät bearbeiten";
$langdata['limsadmin_no_storage_devices'] = "Keine Aufbewahrungsgeräte definiert";
$langdata['limsadmin_new_container_type'] = "Neue Schachtelart";
$langdata['limsadmin_edit_container_type'] = "Schachtelart bearbeiten";
$langdata['container_type'] = "Schachtelart";
$langdata['limsadmin_new_shelf'] = "Neues Regal";

// account prefs
$langdata['account_dateformat'] = "Datumsformat";
$langdata['account_dateformat_mdy'] = "monat/tag/jahr";
$langdata['account_dateformat_dmy'] = "tag/monat/jahr";

// lims admin cartons
$langdata['new_container'] = "Neue Schachtel";
$langdata['edit_wells'] = "Näpfchen bearbeiten";
$langdata['new_sample'] = "Neue Probe";
$langdata['add_new_sample'] = "Klicken Sie auf <i class=\"icon-plus-sign-alt\"></i> um eine neue Probe zu diesem Näpfchen hinzuzufügen";
$langdata['well_occupied'] = "Dieses Näpfchen ist besetzt";
$langdata['scan_barcode'] = "Strichcode scannen oder eingeben";
$langdata['allow_nonowner_edit'] = "Andere dürfen die Daten bearbeiten";
$langdata['allow_nonowner_well_edit'] = "Andere dürfen die Näpfchen/Proben bearbeiten";

// alerts
$langdata['alert_project_invite'] = "Sie wurden eingeladen, sich an einem Projekt zu beteiligen";

// errors
$langdata['err_passwords_no_match'] = "Passwörter stimmen nicht überein";
$langdata['err_current_password_incorrect'] = "Aktuelles Passwort nicht korrekt";
$langdata['err_all_fields_required'] = "Alle Felder sind Pflichtfelder";
$langdata['err_enter_first_name'] = "Bitte geben Sie einen Vornamen ein";
$langdata['err_enter_last_name'] = "Bitte geben Sie einen Nachnamen ein";
$langdata['err_enter_name'] = "Bitte geben Sie einen Namen ein";
$langdata['err_enter_username'] = "Bitte geben Sie einen Benutzernamen ein";
$langdata['err_select_lab'] = "Bitte wählen Sie mindestens 1 Labor";
$langdata['err_int_required'] = "Ganzzahl erforderlich";
$langdata['err_not_found'] = "Nicht gefunden";
$langdata['err_sample_not_found'] = "Kann Probe mit id nicht finden";
$langdata['err_contact_admin'] = "Bitte wenden Sie sich an den Administrator";
$langdata['err_no_changes'] = "Keine Änderungen";
$langdata['err_saved'] = "Änderungen gespeichert";
$langdata['err_concentration'] = "Ungültige Konzentration";
$langdata['err_volume'] = "Ungültige Inhalt";
$langdata['err_not_available'] = "Nicht verfügbar";
$langdata['err_email_invalid'] = "Email-Adresse ist nicht gültig";
$langdata['err_invalid_data_requested'] = "Ungültige oder nicht existierende Daten angefordert";
$langdata['err_in_use'] = "In Betrieb";
$langdata['err_lab_select_access_level'] = "Bitte wählen Sie ein Zugangsniveau für jedes ausgewählte Labor";
$langdata['err_no_edit_perm'] = "Keine Berechtigung zum Bearbeiten";
$langdata['err_no_cartons_in_lab'] = "Sie haben in diesem Labor noch keine Schachteln";
$langdata['err_module_name_invalid'] = "Modul Dateiname ist nicht gültig";
$langdata['err_invalid_range'] = "Ungültige Abgrenzung";

// audit
$langdata['audit_logs'] = "Revisionsaufzeichnung";
$langdata['audit_object_created'] = "Erstellt";
$langdata['audit_object_modified'] = "Verändert";

// user types
$langdata['auth_levels'] = array(PERM_CUSTOMER=>"Kunde", PERM_TECHNICIAN=>"Laborant/in", PERM_SCIENTIST=>"Forscher/in", PERM_MANAGER=>"Manager/in",  PERM_ADMIN=>"Administrator/in");
?>