<?php
//include_once("core/constants.php");

$langdata = array();

// generic terms
$langdata['edit'] = "Edit";
$langdata['delete'] = "Delete";
$langdata['cannot_delete'] = "Cannot delete";
$langdata['delete_file'] = "Delete file";
$langdata['save'] = "Save";
$langdata['new'] = "New";
$langdata['confirm'] = "Confirm";
$langdata['cancel'] = "Cancel";
$langdata['configure'] = "Configure";
$langdata['domain'] = "Domain";
$langdata['laboratory'] = "Laboratory";
$langdata['laboratories'] = "Laboratories";
$langdata['units'] = "Units";
$langdata['unit_system'] = "Unit system";
$langdata['description'] = "Description";
$langdata['enable'] = "Enable";
$langdata['enabled'] = "Enabled";
$langdata['misc'] = "Misc."; // miscellaneous
$langdata['prefs'] = "Preferences";
$langdata['create'] = "Create";
$langdata['member'] = "Member";
$langdata['members'] = "Members";
$langdata['file'] = "File";
$langdata['files'] = "Files";
$langdata['invite'] = "Invite";
$langdata['please_complete'] = "Please complete all fields";
$langdata['please_provide'] = "Please provide as many details as possible.";
$langdata['owner'] = "Owner";
$langdata['upload_date'] = "Upload date";
$langdata['modified_date'] = "Modified date";
$langdata['create_date'] = "Create date";
$langdata['details'] = "Details";
$langdata['comment'] = "Comment";
$langdata['event'] = "Alert";
$langdata['events'] = "Alerts";
$langdata['date'] = "Date";
$langdata['close'] = "Close";
$langdata['name'] = "Name";
$langdata['first_name'] = "First name";
$langdata['last_name'] = "Last name";
$langdata['environment'] = "Environment";
$langdata['type'] = "Type";
$langdata['capacity'] = "Capacity";
$langdata['row'] = "Row";
$langdata['column'] = "Column";
$langdata['rows'] = "Rows";
$langdata['columns'] = "Columns";
$langdata['error'] = "Error";
$langdata['shelf'] = "Shelf";
$langdata['shelves'] = "Shelves";
$langdata['sample'] = "Sample";
$langdata['samples'] = "Samples";
$langdata['carton'] = "Carton";
$langdata['cartons'] = "Cartons";
$langdata['cartons_list'] = "List cartons";
$langdata['cartons_search'] = "Search cartons";
$langdata['samples_search'] = "Search samples";
$langdata['cartons_mine'] = "My cartons";
$langdata['supplies'] = "Supplies";
$langdata['location'] = "Location";
$langdata['barcode'] = "Barcode";
$langdata['search'] = "Search";
$langdata['search_results'] = "Search results";
$langdata['add'] = "Add";
$langdata['remove'] = "Remove";
$langdata['volume'] = "Volume";
$langdata['concentration'] = "Concentration";
$langdata['well'] = "Well";
$langdata['control'] = "Control";
$langdata['depleted'] = "Depleted";
$langdata['active'] = "Active";
$langdata['inactive'] = "Inactive";
$langdata['primer'] = "Primer";
$langdata['uniq_name'] = "Unique name";
$langdata['lock'] = "Lock";
$langdata['unlock'] = "Unlock";
$langdata['empty'] = "Empty";
$langdata['container'] = "Container";
$langdata['list'] = "List";
$langdata['user'] = "User";
$langdata['users'] = "Users";
$langdata['new_user'] = "New user";
$langdata['email_address'] = "Email address";
$langdata['phone'] = "Phone";
$langdata['default'] = "Default";
$langdata['select'] = "Select";
$langdata['access'] = "Access";
$langdata['level'] = "Level";
$langdata['warning'] = "Warning";
$langdata['all'] = "All";
$langdata['reset'] = "Reset";
$langdata['contents'] = "Contents";
$langdata['utilisation'] = "Utilisation";
$langdata['action'] = "Action";
$langdata['permissions'] = "Permissions";
$langdata['set'] = "Set";
$langdata['unset'] = "Unset";
$langdata['yes'] = "Yes";
$langdata['no'] = "No";
$langdata['hour'] = "Hour";
$langdata['minute'] = "Minute";
$langdata['reserve'] = "Reserve";
$langdata['reserved'] = "Reserved";
$langdata['from'] = "From";
$langdata['to'] = "To";
$langdata['by'] = "By";
$langdata['day'] = "Day";
$langdata['week'] = "Week";
$langdata['month'] = "Month";
$langdata['menu'] = "Menu";
$langdata['menus'] = "Menus";
$langdata['section'] = "Section";

$langdata['reset_password'] = "Reset password";
$langdata['account_admin'] = "Profile";
$langdata['login'] = "Login";
$langdata['logout'] = "Logout";
$langdata['username'] = "Username";
$langdata['password'] = "Password";
$langdata['login_failed'] = "Login failed. Please Check your username and password.";
$langdata['profile_details'] = "My details";
$langdata['page_access_denied'] = "Access to this page is denied.";
$langdata['current_password'] = "Current password";
$langdata['new_password'] = "New password";
$langdata['repeat_password'] = "Repeat password";
$langdata['ldap_ad_ident'] = "LDAP/Active Directory identifier";

$langdata['topmenu_inventory'] = "Inventory";
$langdata['topmenu_projects'] = "Lab Projects";
$langdata['topmenu_admin'] = "Admin";
$langdata['topmenu_bioinformatics_projects'] = "Bioinformatics Projects";

// modules
$langdata['module_name'] = "Module name";
$langdata['module_no_options'] = "There are no user configurable options for this module";
$langdata['module_choose_section'] = "Please choose a valid section";
$langdata['module_choose_menu'] = "Please choose a menu";

// projects
$langdata['projects'] = "Projects";
$langdata['projects_current'] = "Running";
$langdata['projects_new'] = $langdata['new'];
$langdata['projects_all'] = "All";
$langdata['projects_name'] = "Project name";
$langdata['projects_startdate'] = "Start date";
$langdata['projects_enddate'] = "Expected end date";
$langdata['projects_flow'] = "Project stages";
$langdata['projects_my'] = "My projects";
$langdata['projects_involved'] = "Projects I am involved in";
$langdata['projects_file_del_info'] = "<strong>File upload/delete rights:</strong><br><ul><li>All project members can upload files.</li><li>Only the file owner or project owner can delete files.</li></ul>";

// galaxy fields
$langdata['galaxy_details'] = "Galaxy config";

// lims admin
$langdata['limsadmin_modules'] = "Modules";
$langdata['limsadmin_inventory'] = "Inventory";
$langdata['limsadmin_sample_types'] = "Sample types";
$langdata['limsadmin_container_types'] = "Container types";
$langdata['limsadmin_labs'] = "Labs";
$langdata['limsadmin_domains'] = "Groups";
$langdata['limsadmin_infrastructure'] = "Infrastructure";
$langdata['limsadmin_personnel'] = "Personnel";
$langdata['limsadmin_skills'] = "Skills";
$langdata['limsadmin_storage_devices'] = "Storage Devices";
$langdata['limsadmin_storage_device'] = "Storage Device";
$langdata['limsadmin_edit_user'] = "Edit user";
$langdata['limsadmin_enable_user'] = "Enable user";
$langdata['limsadmin_lab_access'] = "Laboratory access";

// lims admin LABS
$langdata['limsadmin_new_lab'] = "New Laboratory";
$langdata['limsadmin_lab_name'] = "Laboratory name";
$langdata['limsadmin_lab_domain'] = "Laboratory group";

// lims admin Sample Types
$langdata['limsadmin_new_sample_type'] = "New sample type";
$langdata['limsadmin_edit_sample_type'] = "Edit sample type";

// lims admin Domains
$langdata['limsadmin_new_domain'] = "New group";
$langdata['limsadmin_edit_domain'] = "Edit group";

// lims admin Skills
$langdata['limsadmin_new_skill'] = "New Skill";
$langdata['limsadmin_edit_skill'] = "Edit skill";

// lims admin Storage Devices
$langdata['limsadmin_new_storage_device'] = "New storage device";
$langdata['limsadmin_edit_storage_device'] = "Edit storage device";
$langdata['limsadmin_no_storage_devices'] = "No storage devices defined";
$langdata['limsadmin_new_container_type'] = "New container type";
$langdata['limsadmin_edit_container_type'] = "Edit container type";
$langdata['container_type'] = "Container type";
$langdata['limsadmin_new_shelf'] = "New shelf";

// account prefs
$langdata['account_dateformat'] = "Date format";
$langdata['account_dateformat_mdy'] = "Month/Day/Year";
$langdata['account_dateformat_dmy'] = "Day/Month/Year";

// lims admin cartons
$langdata['new_container'] = "New container";
$langdata['edit_wells'] = "Edit wells";
$langdata['new_sample'] = "New sample";
$langdata['add_new_sample'] = "Click <i class=\"icon-plus-sign-alt\"></i> to add a new sample to this well";
$langdata['well_occupied'] = "This well is already occupied";
$langdata['scan_barcode'] = "Scan or type barcode";
$langdata['allow_nonowner_edit'] = "Allow others to edit details";
$langdata['allow_nonowner_well_edit'] = "Allow others to edit wells/samples";

// alerts
$langdata['alert_project_invite'] = "You have been invited to join a project";

// errors
$langdata['err_passwords_no_match'] = "Passwords do not match";
$langdata['err_current_password_incorrect'] = "Current password incorrect";
$langdata['err_all_fields_required'] = "All fields are required";
$langdata['err_enter_first_name'] = "Please enter a first name";
$langdata['err_enter_first_name'] = "Please enter a first name";
$langdata['err_enter_last_name'] = "Please enter a last name";
$langdata['err_enter_name'] = "Please enter a name";
$langdata['err_enter_username'] = "Please enter a username";
$langdata['err_select_lab'] = "Please select at least 1 laboratory";
$langdata['err_int_required'] = "Integer required";
$langdata['err_not_found'] = "Not found";
$langdata['err_sample_not_found'] = "Cannot find sample with id";
$langdata['err_contact_admin'] = "Please contact the administrator";
$langdata['err_no_changes'] = "No changes made";
$langdata['err_saved'] = "Changes saved";
$langdata['err_concentration'] = "Invalid concentration";
$langdata['err_volume'] = "Invalid volume";
$langdata['err_not_available'] = "Not available";
$langdata['err_email_invalid'] = "Email address is not valid";
$langdata['err_invalid_data_requested'] = "Invalid or non existent data requested";
$langdata['err_in_use'] = "In use";
$langdata['err_lab_select_access_level'] = "Please select an access level for each selected laboratory";
$langdata['err_no_edit_perm'] = "No permission to edit";
$langdata['err_no_cartons_in_lab'] = "You have no cartons in this lab yet";
$langdata['err_module_name_invalid'] = "Module filename invalid";
$langdata['err_invalid_range'] = "Invalid range";

// audit
$langdata['audit_logs'] = "Audit logs";
$langdata['audit_object_created'] = "Created";
$langdata['audit_object_modified'] = "Modified";

// user types
$langdata['auth_levels'] = array(PERM_CUSTOMER=>"Customer", PERM_TECHNICIAN=>"Technician", PERM_SCIENTIST=>"Scientist", PERM_MANAGER=>"Manager",  PERM_ADMIN=>"Administrator");
?>