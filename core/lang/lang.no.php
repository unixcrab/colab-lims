<?php
//include_once("constants.php");

$langdata = array();

// generic terms
$langdata['edit'] = "Rediger";
$langdata['delete'] = "Slett";
$langdata['cannot_delete'] = "Kann ikke slette";
$langdata['delete_file'] = "Slett fil";
$langdata['save'] = "Lagre";
$langdata['new'] = "Ny";
$langdata['confirm'] = "Bekreft";
$langdata['cancel'] = "Avbryte";
$langdata['configure'] = "Konfigurere";
$langdata['domain'] = "Gruppe";
$langdata['laboratory'] = "Laboratorium";
$langdata['laboratories'] = "Laboratorier";
$langdata['units'] = "Units";
$langdata['unit_system'] = "Mål system";
$langdata['description'] = "Beskrivelse";
$langdata['enable'] = "Aktiver";
$langdata['enabled'] = "Aktivert";
$langdata['misc'] = "Diverse"; // miscellaneous
$langdata['prefs'] = "Preferanser";
$langdata['create'] = "Skape";
$langdata['member'] = "Medlem";
$langdata['members'] = "Medlemmer";
$langdata['file'] = "Fil";
$langdata['files'] = "Filer";
$langdata['invite'] = "Invitere";
$langdata['please_complete'] = "Fyll ut alle feltene";
$langdata['please_provide'] = "Vennligst oppgi så mange detaljer som mulig";
$langdata['owner'] = "Eier";
$langdata['upload_date'] = "Opplastingsdato";
$langdata['modified_date'] = "Endringsdato";
$langdata['create_date'] = "Lagedato";
$langdata['details'] = "Detaljer";
$langdata['comment'] = "Kommentar";
$langdata['event'] = "Hendelse";
$langdata['events'] = "Hendelser";
$langdata['date'] = "Dato";
$langdata['close'] = "Lukk";
$langdata['name'] = "Navn";
$langdata['first_name'] = "Navn";
$langdata['last_name'] = "Etternavn";
$langdata['environment'] = "Miljø";
$langdata['type'] = "Typ";
$langdata['capacity'] = "Kapasitet";
$langdata['row'] = "Rad";
$langdata['column'] = "Kolonne";
$langdata['rows'] = "Rader";
$langdata['columns'] = "Kolonner";
$langdata['error'] = "Feil";
$langdata['shelf'] = "Hylle";
$langdata['shelves'] = "Hyller";
$langdata['sample'] = "Prøve";
$langdata['samples'] = "Prøver";
$langdata['carton'] = "Kartong";
$langdata['cartons'] = "Kartonger";
$langdata['cartons_list'] = "Liste kartonger";
$langdata['cartons_search'] = "Søk kartonger";
$langdata['samples_search'] = "Søk prøver";
$langdata['cartons_mine'] = "Min kartonger";
$langdata['supplies'] = "Forsyninger";
$langdata['location'] = "Plassering";
$langdata['barcode'] = "Strekkode";
$langdata['search'] = "Søk";
$langdata['search_results'] = "Søkeresultater";
$langdata['add'] = "Legg til";
$langdata['remove'] = "Fjern";
$langdata['volume'] = "Volum";
$langdata['concentration'] = "Konsentrasjon";
$langdata['well'] = "Hull";
$langdata['control'] = "Kontroll";
$langdata['depleted'] = "Tom";
$langdata['active'] = "Aktiv";
$langdata['inactive'] = "Uvirksom";
$langdata['primer'] = "Primer";
$langdata['uniq_name'] = "Unik navn";
$langdata['lock'] = "Lås";
$langdata['unlock'] = "Åpen";
$langdata['empty'] = "Tomt";
$langdata['container'] = "Beholder";
$langdata['list'] = "Liste";
$langdata['user'] = "Bruker";
$langdata['users'] = "Brukerer";
$langdata['new_user'] = "Ny bruker";
$langdata['email_address'] = "Epost adresse";
$langdata['phone'] = "Telefon";
$langdata['default'] = "Standard";
$langdata['select'] = "Velg";
$langdata['access'] = "Adgang";
$langdata['level'] = "Nivå";
$langdata['warning'] = "Advarsel";
$langdata['all'] = "Alt";
$langdata['reset'] = "Null";
$langdata['contents'] = "Innhold";
$langdata['utilisation'] = "Utnyttelse";
$langdata['action'] = "Aksjon";
$langdata['permissions'] = "Tillatelse";
$langdata['set'] = "Satt";
$langdata['unset'] = "Fjernet";
$langdata['yes'] = "Ja";
$langdata['no'] = "Nei";
$langdata['hour'] = "Time";
$langdata['minute'] = "Minutter";
$langdata['reserve'] = "Reservere";
$langdata['reserved'] = "Reservert";
$langdata['from'] = "fra";
$langdata['to'] = "t.o.m.";
$langdata['by'] = "av";
$langdata['day'] = "dag";
$langdata['week'] = "uke";
$langdata['month'] = "måned";
$langdata['menu'] = "Meny";
$langdata['menus'] = "Menyer";
$langdata['section'] = "Del";

$langdata['reset_password'] = "Nullstille passord";
$langdata['account_admin'] = "Profil";
$langdata['login'] = "Logg inn";
$langdata['logout'] = "Logg ut";
$langdata['username'] = "Brukernavn";
$langdata['password'] = "Passord";
$langdata['login_failed'] = "Pålogging mislyktes. Vennligst sjekk ditt brukernavn og passord.";
$langdata['profile_details'] = "Mine detaljer";
$langdata['page_access_denied'] = "Tilgang til denne siden er nektet.";
$langdata['current_password'] = "Aktuell passord";
$langdata['new_password'] = "Nytt passord";
$langdata['repeat_password'] = "Gjenta passord";
$langdata['ldap_ad_ident'] = "LDAP/Active Directory identifikasjon";

$langdata['topmenu_inventory'] = "Varelager";
$langdata['topmenu_projects'] = "Prosjekter";
$langdata['topmenu_admin'] = "Admin";
$langdata['topmenu_bioinformatics_projects'] = "Bioinformatikk Prosjekter";

// modules
$langdata['module_name'] = "Modulnavn";
$langdata['module_no_options'] = "Det finnes ingen konfigurerbare alternativer for denne modulen";
$langdata['module_choose_section'] = "Velg en gyldig seksjon";
$langdata['module_choose_menu'] = "Velg en meny";

// projects
$langdata['projects'] = "Prosjekter";
$langdata['projects_current'] = "Løpende";
$langdata['projects_new'] = $langdata['new'];
$langdata['projects_all'] = "Alt";
$langdata['projects_name'] = "Prosjekt navn";
$langdata['projects_startdate'] = "Start dato";
$langdata['projects_enddate'] = "Forventet slutt";
$langdata['projects_flow'] = "Prosjektfaser";
$langdata['projects_my'] = "Min prosjekter";
$langdata['projects_involved'] = "Prosjekter jeg er involvert i";
$langdata['projects_file_del_info'] = "<strong>Filopplasting/slette rettigheter:</strong><br><ul><li>Alle prosjektmedlemmer kan laste opp filer.</li><li>Bare filen eieren eller prosjekteier kan slette filer.</li></ul>";

// galaxy fields
$langdata['galaxy_details'] = "Galaxy detaljer";

// lims admin
$langdata['limsadmin_modules'] = "Moduler";
$langdata['limsadmin_inventory'] = "Varelager";
$langdata['limsadmin_sample_types'] = "Prøvetyper";
$langdata['limsadmin_container_types'] = "Container typer";
$langdata['limsadmin_labs'] = "Laboratorier";
$langdata['limsadmin_domains'] = "Grupper";
$langdata['limsadmin_infrastructure'] = "Infrastruktur";
$langdata['limsadmin_personnel'] = "Personell";
$langdata['limsadmin_skills'] = "Ferdigheter";
$langdata['limsadmin_storage_devices'] = "Lagringsenheter";
$langdata['limsadmin_storage_device'] = "Lagringsenhet";
$langdata['limsadmin_edit_user'] = "Rediger bruker";
$langdata['limsadmin_enable_user'] = "Aktiver bruker";
$langdata['limsadmin_lab_access'] = "Laboratorium tilgang";

// lims admin LABS
$langdata['limsadmin_new_lab'] = "Ny Laboratorium";
$langdata['limsadmin_lab_name'] = "Laboratorium navn";
$langdata['limsadmin_lab_domain'] = "Laboratorium gruppe";

// lims admin Sample Types
$langdata['limsadmin_new_sample_type'] = "Ny prøvetyp";
$langdata['limsadmin_edit_sample_type'] = "Redigere prøvetyp";

// lims admin Domains
$langdata['limsadmin_new_domain'] = "Ny gruppe";
$langdata['limsadmin_edit_domain'] = "Redigere gruppe";

// lims admin Skills
$langdata['limsadmin_new_skill'] = "Ny ferdighet";
$langdata['limsadmin_edit_skill'] = "Redigere ferdighet";

// lims admin Storage Devices
$langdata['limsadmin_new_storage_device'] = "Ny lagringsenhet";
$langdata['limsadmin_edit_storage_device'] = "Redigere lagringsenhet";
$langdata['limsadmin_no_storage_devices'] = "Ingen lagringsenheter definert";
$langdata['limsadmin_new_container_type'] = "Ny beholder typen";
$langdata['limsadmin_edit_container_type'] = "Redigere beholder";
$langdata['container_type'] = "Beholder typen";
$langdata['limsadmin_new_shelf'] = "Ny hylle";

// account prefs
$langdata['account_dateformat'] = "Datoformat";
$langdata['account_dateformat_mdy'] = "måned/dag/år";
$langdata['account_dateformat_dmy'] = "dag/måned/år";

// lims admin cartons
$langdata['new_container'] = "Ny beholder";
$langdata['edit_wells'] = "Rediger wells";
$langdata['new_sample'] = "Ny prøve";
$langdata['add_new_sample'] = "Klick på <i class=\"icon-plus-sign-alt\"></i> for å legge til en ny prøve til denne brønnen";
$langdata['well_occupied'] = "Denne brønn er allerede opptatt";
$langdata['scan_barcode'] = "Skann eller skriv strekkode";
$langdata['allow_nonowner_edit'] = "Tillat andre å redigere detaljer";
$langdata['allow_nonowner_well_edit'] = "ATillat andre å redigere brønner/prøver";

// alerts
$langdata['alert_project_invite'] = "Du har blitt invitert til å delta i et prosjekt";

// errors
$langdata['err_passwords_no_match'] = "Passordene er ikke enig";
$langdata['err_current_password_incorrect'] = "Nåværende passord er feil";
$langdata['err_all_fields_required'] = "Alle felt må fylles ut";
$langdata['err_enter_first_name'] = "Skriv inn et navn";
$langdata['err_enter_last_name'] = "Skriv inn et etternavn";
$langdata['err_enter_name'] = "Skriv inn et navn";
$langdata['err_enter_username'] = "Skriv inn et brukernavn";
$langdata['err_select_lab'] = "Velg minst ett laboratorium";
$langdata['err_int_required'] = "Heltall kreves";
$langdata['err_not_found'] = "Ikke funnet";
$langdata['err_sample_not_found'] = "Kan ikke finne prøven med id";
$langdata['err_contact_admin'] = "Vennligst kontakt administratoren";
$langdata['err_no_changes'] = "Ingen endringer er gjort";
$langdata['err_saved'] = "Endringene lagret";
$langdata['err_concentration'] = "Ugyldig konsentrasjon";
$langdata['err_volume'] = "Ugyldig volum";
$langdata['err_not_available'] = "Ikke tilgjengelig";
$langdata['err_email_invalid'] = "E-postadressen er ikke gyldig";
$langdata['err_invalid_data_requested'] = "Ugyldige eller ikke eksisterende data anmodet";
$langdata['err_in_use'] = "i bruk";
$langdata['err_lab_select_access_level'] = "Vennligst velg et tilgangsnivå for hver valgte laboratorium";
$langdata['err_no_edit_perm'] = "Ingen tillatelse til å redigere";
$langdata['err_no_cartons_in_lab'] = "Du har ingen kartonger i dette laboratoriet ennå";
$langdata['err_module_name_invalid'] = "Modul filnavn er ikke gyldig";
$langdata['err_invalid_range'] = "Ugyldig utvalg";

// audit
$langdata['audit_logs'] = "Revisjon logg";
$langdata['audit_object_created'] = "Opprettet";
$langdata['audit_object_modified'] = "Endret";

// user types
$langdata['auth_levels'] = array(PERM_CUSTOMER=>"Kunde", PERM_TECHNICIAN=>"Tekniker", PERM_SCIENTIST=>"Forsker", PERM_MANAGER=>"Daglig leder",  PERM_ADMIN=>"Administrator");
?>