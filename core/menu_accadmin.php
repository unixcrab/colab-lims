<?php
// $request_uri = $_SERVER['REQUEST_URI'];
// var_dump($_SESSION['lims']);

// echo $subpage;
include("functions.modules.php");
?>



<ul class="nav nav-list">
  <li class="nav-header"><?php print $_SESSION['lims']['langdata']['account_admin'];?></li>
  
  <li <?php if( $subpage == "account.passwd" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=account.passwd" id="myaccount_passwd"><? print $_SESSION['lims']['langdata']['password']; ?></a>
  </li>
  
  <li <?php if( $subpage == "account.details" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=account.details" id="myaccount_details"><? print $_SESSION['lims']['langdata']['profile_details']; ?></a>
  </li>
  
  <li <?php if( $subpage == "account.prefs" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=account.prefs" id="myaccount_prefs"><? print $_SESSION['lims']['langdata']['prefs']; ?></a>
  </li>
  
<!--  <li <?php if( $subpage == "account.lab" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=account.lab" id="myaccount_lab"><? print $_SESSION['lims']['langdata']['laboratory']; ?></a>
  </li>-->
  
  <li <?php if( $subpage == "account.alerts" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=account.alerts" id="myaccount_alerts"><? print $_SESSION['lims']['langdata']['events']; ?></a>
  </li>
  
	<li class="divider"></li>
	
  <li class="nav-header"><?php print $_SESSION['lims']['langdata']['limsadmin_modules'];?></li> 
<!--   <pre> -->
  <?php

	$mods = getModulesForMenu(MENU_USER_PROFILE, MODULE_COMPONENT_USER_CONFIG, 0, $_SESSION['lims']['auth_level']);
	
  foreach($mods as $mod) {
//     print $mod->name."\n";
    $file_short = str_replace(".xml","",$mod->file);
    ?>
    <li <?php if( $subpage == "account:module:".$file_short ) print "class='active'";?>><a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=account:module:<?php print $file_short; ?>" id="account_module"><? print myhtmlentities($mod->name); ?></a>
    </li>
    <?php
    }
  ?>

  
</ul>