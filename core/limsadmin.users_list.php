<?php if( $_SESSION['lims']['auth_level']<PERM_MANAGER ) return; ?>

<h4><?php print $_SESSION['lims']['langdata']['users']; ?></h4>
<hr>

<table class="table table-hover table-condensed">
<thead>
  <tr>
  <th></th>
  <th><?php print $_SESSION['lims']['langdata']['username']; ?></th>
  <th><?php print $_SESSION['lims']['langdata']['name']; ?></th>
  <th></th>
  <th><?php print $_SESSION['lims']['langdata']['email_address']; ?></th>
  <th></th>
  </tr>
</thead>
<tbody>
<?php
include "functions.lims.php";

$users = getAllUsers("username");
$ucount = 1;
foreach($users as $u) {
	$user_edit_auth_lvl = getAuthLevelHighest($u->person_id);
	
  $inactiveStr = "";
  $inactiveDelIconStr = "<a href='#'><i class='icon-trash' id='userDel-".$u->person_id."'></i></a>";
  if( $u->active == 0 ) {
    $inactiveStr = "(".strtolower($_SESSION['lims']['langdata']['inactive']).")";
    $inactiveDelIconStr = "";
    }
				
	$inactiveEditIconStr = "<a href='index.php?p=limsadmin&sp=limsadmin.user_edit&uid=".$u->person_id."'><span><i class='icon-edit' id='userEdit-".$u->person_id."' title='".$_SESSION['lims']['langdata']['edit']."' data-placement='right'></i></span></a>";
	
	if($user_edit_auth_lvl->auth_level_id>$_SESSION['lims']['auth_level']) {
		$inactiveEditIconStr = "";
		$inactiveDelIconStr = "";
		//$inactiveEditIconStr = "<i class='icon-edit' id='userEdit-X' title='".$_SESSION['lims']['langdata']['err_no_edit_perm']."' data-placement='right'></i>";
		}
	
  print "<tr><td>".$ucount.".</td><td id='username-".$u->person_id."'>".myhtmlentities($u->username)." ".$inactiveStr."</td><td id='first_name-".$u->person_id."'>".myhtmlentities($u->first_name)."</td><td  id='last_name-".$u->person_id."'>".myhtmlentities($u->last_name)."</td><td><a href='mailto:".myhtmlentities($u->email_address)."'>".myhtmlentities($u->email_address)."</a></td><td>".$inactiveEditIconStr."&nbsp;&nbsp;&nbsp;".$inactiveDelIconStr."</td></tr>\n";
  $ucount++;
  }
?>
</tbody>
</table>



<!-- DEL Modal -->
<div id="userDelModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="userDelModalLabel" aria-hidden="true">
  <div class="modal-header">
    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>-->
    <h4 id="userDelModalLabel"><i class="icon-trash"></i> <?php print $_SESSION['lims']['langdata']['delete']; ?></h4>
  </div>
  <div class="modal-body">
  <p id="userDelModalBody"></p>
  </div>

  <div class="modal-footer">
    <button class="btn btn-warning" id="userDelModalCancel"><?php print $button_cancel; ?></button>
    <button class="btn btn-primary" id="userDelConfirm"><?php print $button_trash; ?></button>
  </div>
</div>




<script>
$(function() {
  $("[id^=userEdit-]").tooltip();

	$('#userDelModalCancel').click( function() {
		$('#userDelConfirm').unbind('click');
		$('#userDelModal').modal('hide');
	});
	
  $("[id^=userDel-]").click(function() {
    var uid = this.id.split("-")[1];
    var uname = $('#username-'+uid).text();
    $('#userDelModalBody').html('<?php print $_SESSION['lims']['langdata']['delete']; ?> <i>'+uname+'</i>?');
//     $('#userEditUserName').val( uname );
    $('#userDelConfirm').click( function() {
      $.post("core/ajax_user.php", { m: "d", u: uid }, function(){
				$( '#userDelModal' ).modal( 'hide' ); 
				//location.reload(); 
				$('#userDelModal').on('hidden', function () {	location.reload() });
			});
    });
    $('#userDelModal').modal();
    });
});
</script>