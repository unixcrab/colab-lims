<?php
// print "cid=".$cid;
$carton = getCarton($cid, $_SESSION['lims']['lab_id']);
if( $carton == null ) {
	error_log("Denied attempt to access carton ".$cid." in lab_id ".$_SESSION['lims']['lab_id']." from user ".$_SESSION['lims']['person_id']);
	?>
	<div class="alert">
  <?php print $_SESSION['lims']['langdata']['err_invalid_data_requested'];?>
	</div>
	<?php
	return;
	}
$carton_samples = getCartonSamples($carton->carton_id);
$units = getUnitsAll(UNIT_SYS_SI);
$containers_all = getContainerTypes();
$storage_locs = getStorageDevicesForLab($_SESSION['lims']['lab_id']);
$shelves = getStorageShelves($carton->storage_id);
$lab_users = getAllUsersInLab($_SESSION['lims']['lab_id']);
$carton_stats = getContainerStats($carton->carton_id,$_SESSION['lims']['lab_id']);

$num_samples_in_carton = (count($carton_samples,COUNT_RECURSIVE)-count($carton_samples,0));
$carton_owner = false;
if($carton->owner_id == $_SESSION['lims']['person_id']) $carton_owner = true;

$cell_size = 55;
if( $containers_all[$carton->container_type_id]->number_of_columns > 12) $cell_size = 32;
$carton_right_pad = 22;
if( $containers_all[$carton->container_type_id]->number_of_columns > 12) $carton_right_pad = 14;
?>

<div class="navbar">
  <div class="navbar-inner">
    <span class="brand" ><strong><?php print mb_strtoupper($_SESSION['lims']['langdata']['carton']); ?></strong>: <i><?php print myhtmlentities($carton->carton_identifier); ?></i></span>
		<ul class="nav">
			
			<li id="navpill_li-cartonAttrEditDetails" <?php if(($show=="well")||($show_audit_logs)) print ""; else print "class='active'";?>><a href="#" id ="cartonAttrEditDetails"><i class="icon-list icon-white"></i><?php print $_SESSION['lims']['langdata']['details']; ?></a></li>
			<li id="navpill_li-cartonAttrEditWells" <?php if($show=="well") print "class='active'";?>  >
				<a href="#" id="cartonAttrEditWells"><i class="icon-th icon-white"></i><?php print $_SESSION['lims']['langdata']['edit_wells']; ?></a>
			</li>
			<li id="navpill_li-cartonAuditLogs" <?php if($show_audit_logs) print "class='active'"; ?>><a href="#" id="cartonAuditLogs"><i class="icon-stethoscope"></i><?php print $_SESSION['lims']['langdata']['audit_logs']; ?></a></li>
		</ul>		
  </div>
</div>


 
<!--
<ul class="nav nav-pills">
	<li class="disabled"><a href='#'><?php print mb_strtoupper($carton->carton_identifier); ?> <i class="icon-chevron-right"></i> </a></li>
	<li id="navpill_li-cartonAttrEditDetails" <?php if(($show=="well")||($show_audit_logs)) print ""; else print "class='active'";?>><a href="#" id ="cartonAttrEditDetails"><i class="icon-list icon-white"></i><?php print $_SESSION['lims']['langdata']['details']; ?></a></li>
  <li id="navpill_li-cartonAttrEditWells" <?php if($show=="well") print "class='active'";?>  >
    <a href="#" id="cartonAttrEditWells"><i class="icon-th icon-white"></i><?php print $_SESSION['lims']['langdata']['edit_wells']; ?></a>
  </li>
  <li id="navpill_li-cartonAuditLogs" <?php if($show_audit_logs) print "class='active'"; ?>><a href="#" id="cartonAuditLogs"><i class="icon-stethoscope"></i>Audit logs</a></li>
</ul>
-->

<script type="text/javascript" src="<?print $site_root.'/core/js/datepicker/bootstrap-datepicker.'.trim($curr_lang,'"').'.js';?>"></script>
<script>
var shs = Array();
<?php

	//$shelves_per_sdevice = array();
  foreach($storage_locs as $st) {
    $shelf_assoc = "{";
    $shelves_sdevice = getStorageShelves($st->storage_id);
		//$shelves_per_sdevice[$st->storage_id] = $shelves_sdevice;
    foreach($shelves_sdevice as $shelf) {
      $shelf_assoc .= $shelf->id.":'".$shelf->shelf_number."',";
    }
    $shelf_assoc = rtrim($shelf_assoc,",");
    $shelf_assoc .= "}";
    print "shs[".$st->storage_id."] = ".$shelf_assoc.";\n";
  }
	
	// print out JS num rows and cols for mouseovers
	print "var cartonNumRows=".$containers_all[$carton->container_type_id]->number_of_rows.";\n";
	print "var cartonNumCols=".$containers_all[$carton->container_type_id]->number_of_columns.";\n";
	
?>
</script>

<div id="auditLogContainer" style="display:none;">
<!--
<pre>
<?php
$auditlogs = null; //getAuditLog(AUDIT_TYPE_CARTON,$cid,"asc");
//error_log(current($auditlogs)->audit_descr);

//var_dump( (array)json_decode(current($auditlogs)->audit_descr));
?>
</pre>
-->

<!--
<label class="radio">
  <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
  Oldest first
</label>
<label class="radio">
  <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
  Newest first
</label>
-->

<table id="auditLogTable" class="table table-hover table-condensed" style="table-layout: fixed; width: 100%">
<thead>
<tr>
<th style='width:80px;'></th>
<th style='width:90px;'><?php print $_SESSION['lims']['langdata']['date']; ?></th>
<th style='width:100px;'><?php print $_SESSION['lims']['langdata']['user']; ?></th>
<th><?php print $_SESSION['lims']['langdata']['details']; ?></th>
</tr>
</thead>
<tbody>
</tbody>
</table>
<div id="auditLogTableFooter"></div>
</div>

<div id="formContainer" class="" style="padding-right: 40px; <?php if($show=="well") print "display:none;";?>">

	<?php
	if(($carton->owner_id!=$_SESSION['lims']['person_id'])&&($carton->allow_details_edit!=1)) { ?>
		<div class="alert">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<strong><?php print $_SESSION['lims']['langdata']['warning']; ?>!</strong> <?php print $_SESSION['lims']['langdata']['err_no_edit_perm']; ?>.
	</div>
	 <?php } ?>
 
  <form class="form-horizontal">
    <div class="control-group">
      <label class="control-label" for="cartonAttrIdent"><?php print $_SESSION['lims']['langdata']['name']; ?></label>
      <div class="controls">
				<input type="text" id="cartonAttrIdent" value="<?php print myhtmlentities($carton->carton_identifier);?>" style="width: 300px;" disabled="disabled">
      </div>
    </div>
    
    <div class="control-group">
      <label class="control-label" for="cartonAttrBarCode">
				<?php print $_SESSION['lims']['langdata']['barcode']; ?>
			</label>
      <div class="controls">
				<input type="text" id="cartonAttrBarCode" value="<?php print myhtmlentities($carton->internal_barcode);?>" style="width: 300px;" disabled="disabled">
      </div>
			<label class="control-label"></label>
			<!--<div id="barcodeContainer" class="controls" style="padding-top: 45px; padding-left: 20px; padding-right: 20px; padding-bottom: 10px; 20px; border: 1px solid #ccc; border-radius: 10px;<?php if($show=="well") print "display:none;";?>">
					<img src='core/barcode_gen.php?b=<?php print $carton->internal_barcode; ?>'>
			</div>-->
			<div id="barcodeContainer" class="controls" style="padding-top: 15px; padding-left: 20px; padding-right: 20px; padding-bottom: 10px; border: 0px solid #ccc; border-radius: 10px;<?php if($show=="well") print "display:none;";?>">
					<img src='core/barcode_gen.php?b=<?php print $carton->internal_barcode; ?>'>
			</div>
    </div>
    
    <div class="control-group">
      <label class="control-label" for="cartonAttrCreateDate">
				<?php print $_SESSION['lims']['langdata']['date']; ?>
			</label>
      <div class="controls">
				<input type="text" id="cartonAttrCreateDate" value="<?php print makeDateString($carton->create_date_timestamp)." ".makeTimeString($carton->create_date_timestamp);?>" disabled>
      </div>
    </div>
    
    <div class="control-group">
      <label class="control-label" for="cartonAttrStorageLoc">
				<?php print $_SESSION['lims']['langdata']['limsadmin_storage_device']; ?>
			</label>
      <div class="controls">
					<select id="cartonAttrStorageLoc" disabled="disabled">
					<?php 
						foreach( $storage_locs as $sd ) {
							$selStr = "";
							if( $sd->storage_id == $carton->storage_id ) $selStr = "selected";
							print "<option value='".$sd->storage_id."' ".$selStr.">".myhtmlentities($sd->identifier)."</option>\n";
						}
					
					?>
					</select>
      </div>
    </div>
    
    <div class="control-group">
      <label class="control-label" for="cartonAttrStorageShelf">
				<?php print $_SESSION['lims']['langdata']['shelf']; ?>
			</label>
      <div class="controls">
				<select id="cartonAttrStorageShelf" disabled="disabled">
				<?php 
					
					foreach( $shelves as $shelf ) {
						$selStr = "";
						if( $shelf->id == $carton->shelf_id ) $selStr = "selected"; 
						print "<option value='".$shelf->id."' ".$selStr.">".myhtmlentities($shelf->shelf_number)."</option>\n";
					}
				
				?>
				</select>
      </div>
    </div>
    
		<div class="control-group">
			<label class="control-label" for="cartonAttrOwner">
				<?php print $_SESSION['lims']['langdata']['owner']; ?>
			</label>
			<div class="controls">
			<select id="cartonAttrOwner" disabled="disabled">
			<?php
				foreach( $lab_users as $user ) {
					$selStr = "";
					if( $user->person_id == $carton->owner_id ) $selStr = "selected"; 
					print "<option value='".$user->person_id."' ".$selStr.">".myhtmlentities($user->first_name." ".$user->last_name)."</option>\n";
				}
			?>	
			</select>
			</div>
		</div>
		
		<?php if($carton_owner) { ?>
			<div class="control-group">
				<label class="control-label" for="cartonAttrOwner">
					<?php print $_SESSION['lims']['langdata']['permissions']; ?>
				</label>
				<div class="controls">
					<label class="checkbox">
						<input type="checkbox" id="cartonAttrAllowNonOwnerEdit" <?php if($carton->allow_details_edit==1) print "checked"; ?> disabled="disabled"> <?php print $_SESSION['lims']['langdata']['allow_nonowner_edit']; ?>
					</label>
					<label class="checkbox">
						<input type="checkbox" id="cartonAttrAllowNonOwnerWellEdit" <?php if($carton->allow_contents_edit==1) print "checked"; ?> disabled="disabled"> <?php print $_SESSION['lims']['langdata']['allow_nonowner_well_edit']; ?>
					</label>
				</div>
			</div>
		<?php } ?>
		
		<?php if(($carton->owner_id==$_SESSION['lims']['person_id'])||($carton->allow_details_edit==1)) { ?>
    <div class="control-group">
      <div class="controls">
				<button class="btn btn-warning" id="cartonAttrEditBtn"><i class="icon-lock icon-white"></i> <?php print $_SESSION['lims']['langdata']['unlock']; ?></button>
				<button type="submit" class="btn btn-primary" id="cartonAttrSaveBtn" style="display:none;"><?php print $button_save; ?></button>
      </div>
    </div>
		<?php } ?>
  </form>
  
	
			<div id="contentsContainer" class="clearfix" style="<?php if($show=="well") print "display:none;";?> border: 0px solid #ccc; width:80%">
					<h5>
					<?php 
					print $_SESSION['lims']['langdata']['contents'].": ".$num_samples_in_carton."&nbsp;&nbsp;"; 
					if($carton_stats!=null) {
						print "&nbsp;&nbsp;".$_SESSION['lims']['langdata']['utilisation'].": ";
						if(($carton_stats->average<1.0)&&($carton_stats->average>0.0)) print "<1%";
						else print number_format($carton_stats->average,1,'.','')."%";
						}
						?>
					</h5>
					<table id="contentsTable" class='table table-hover table-condensed tablesorter'>
					<thead>
					<tr><th><?php print $_SESSION['lims']['langdata']['sample']; ?></th><th style="width:100px;"><?php print $_SESSION['lims']['langdata']['location']; ?></th></tr>
					</thead>
					<tbody>

					<?php
					if( $num_samples_in_carton > 0 ) {
						for($r=1; $r<=$containers_all[$carton->container_type_id]->number_of_rows; $r++) {
							for($c=1; $c<=$containers_all[$carton->container_type_id]->number_of_columns; $c++) {
								if( isset( $carton_samples[$r][$c] ) ) {
									$s = getSample($carton_samples[$r][$c]->sample_id);
									print "<tr><td>".myhtmlentities($s->identification)."</td><td><a href='index.php?p=inv&sp=cartons.mine&s=edit&cid=".$cid."&show=well&row=".$r."&col=".$c."'>".chr(64+$r)." ".$c."</a></td></tr>\n";
								}
							}
						}
					}
					?>
					</tbody>
					</table>
			</div> <!-- END contentsContainer -->
	
	
	
	
</div> <!-- END formContainer -->






<!-- <div style="display:none;" id="wellEditor"> -->
<div id="plateContainer" style="<?php if($show!="well") print "display:none;";?> border:2px solid; border-radius:10px; width:<?php print ($containers_all[$carton->container_type_id]->number_of_columns*($cell_size+$carton_right_pad)); ?>px;"  class="well">
<table class="" id="plateContainerTable" style="background:#fff; table-layout: fixed; width: <?php print ($containers_all[$carton->container_type_id]->number_of_columns*$cell_size); ?>px;">
<?php
// print $containers_all[$carton->container_type_id]->number_of_rows."X".$containers_all[$carton->container_type_id]->number_of_columns;

// print column header
print "<tr><td  style='border: 1px solid black; padding:5px; width:30px; height:12px; background-color:#eeeeee;'></td>";
for($c=1; $c<=$containers_all[$carton->container_type_id]->number_of_columns; $c++) {
	print "<td id='cell__header_col-".$c."' style='text-align:center; border: 1px solid black; padding:5px; width:".$cell_size."px; height:12px; background-color:#eeeeee;'><strong>".$c."</strong></td>";
	}
print "</tr>\n";


$show_popover_for = null;
for($r=1; $r<=$containers_all[$carton->container_type_id]->number_of_rows; $r++) {
  print "<tr>\n<td id='cell__header_row-".$r."' style='text-align:center; border: 1px solid black; padding:10px; width:10px; height:".$cell_size."px; background-color:#eeeeee;'><strong>".chr($r+64)."</strong></td>\n";
  for($c=1; $c<=$containers_all[$carton->container_type_id]->number_of_columns; $c++) {
//     print "";
  
    //$well_border_col = "black";
    $well_bg_col = "#ffffff";
    //if( ($r==$row) && ($r<=$row) || ($c==$col) && ($c<=$col) ) $well_bg_col = "#ffefef";
//     if( ($c==$col) && ($c<=$col) ) $well_bg_col = "#ffefef";
		$popover_trigger = "hover";
    if( ($r==$row) && ($c==$col) ) {
      $well_bg_col = "#ffbcbc";
      //$well_border_col = "black";
			$show_popover_for = $carton_samples[$r][$c]->sample_id;
			//error_log('show_popover_for='.$show_popover_for);
			//if( $show_popover_for != null ) 
			$popover_trigger = "manual";
      }
	
    if( isset($carton_samples[$r][$c]) ) {
      $s = getSample($carton_samples[$r][$c]->sample_id);
      $s_name = "*";
      $s_type = "*";
      $s_comments = "*";
      $s_vol = "*";
      $s_vol_units = "*";
      $s_conc = "*";
      $s_conc_units = "*";
			$icon_active = "";
			$icon_control = "";
			$icon_depleted = "";
      if( $s != null ) {
					$s_name = $s->identification;
					$s_type = getSampleType($s->sample_type_id)->sample_type;
					$s_comments = $s->comments;
					$s_vol = $s->volume;
				// 	error_log("VOL ".$s->volume_units_id);
					if( isset($s->volume_units_id) && isset($units[$s->volume_units_id]) ) $s_vol_units = $units[$s->volume_units_id]->units;
					else $s_vol_units = "*";
					$s_conc = $s->concentration;
					if( isset($s->concentration_units_id ) && isset($units[$s->concentration_units_id]) ) $s_conc_units = $units[$s->concentration_units_id]->units;
					else $s_conc_units = "*";
					if($carton_samples[$r][$c]->active=="Y") $icon_active = "<i class='icon-circle' style='color: green; font-size: 10px;'></i>&nbsp;";
					if($s->control_flag=="Y") $icon_control = "<i class='icon-circle' style='color: yellow; font-size: 10px;'></i>&nbsp;";
					if($s->exhausted_flag=="Y") $icon_depleted = "<i class='icon-circle' style='color: red; font-size: 10px;'></i>&nbsp;";
      }
			$edit_sample_link = "#";
			if(($carton->owner_id==$_SESSION['lims']['person_id'])||($carton->allow_contents_edit==1))
				$edit_sample_link = "index.php?p=inv&sp=sample.open&s=edit&sid=".$carton_samples[$r][$c]->sample_id;
      print "<td class='cartonCellUpOcc' id='cell__-sample-".$carton_samples[$r][$c]->sample_id."' style='border: 1px solid; padding:0px 0px 0px 0px; width:".$cell_size."px; height:".$cell_size."px; '>".
			
      "<a id='sample-".$carton_samples[$r][$c]->sample_id."' ".
      "data-trigger='".$popover_trigger."' data-animation='true' data-placement='top' data-html='true' ".
      "data-title='<strong>".myhtmlentities($s_name)."</strong>' ".
      "data-content='<strong>".$_SESSION['lims']['langdata']['type'].":</strong> ".myhtmlentities($s_type)."<br/>".
      "<strong>".$_SESSION['lims']['langdata']['volume'].":</strong> ".$s_vol." ".$s_vol_units."<br/>".
      "<strong>".$_SESSION['lims']['langdata']['concentration'].":</strong> ".$s_conc." ".$s_conc_units."<br/>".
      "'".
      " href='".$edit_sample_link."'><small>".$carton_samples[$r][$c]->sample_id."</small></a>".
			
			"<div style='border: 0px solid; padding:0px 0px 0px 0px; height:8px; width:".$cell_size."; '>".
			$icon_active.$icon_control.$icon_depleted.
			"</div>".
			"</td>\n";
      }
    else {
			$add_sample_link = "";
			if(($carton->owner_id==$_SESSION['lims']['person_id'])||($carton->allow_contents_edit==1))
				$add_sample_link = "<a id='newsample-".$r."-".$c."' href='index.php?p=inv&sp=sample.open&s=new&cid=".$carton->carton_id."&r=".$r."&c=".$c."' data-trigger='hover' data-placement='top' data-html='true' data-title='<strong>".$_SESSION['lims']['langdata']['empty']."</strong>' data-content='".$_SESSION['lims']['langdata']['add_new_sample'].".'><i class='icon-plus-sign-alt'></i></a>";
      print "<td class='cartonCellDown' id='cell__-newsample-".$r."-".$c."' style='border: 1px solid black; padding:10px; width:".$cell_size."px; height:".$cell_size."px;' >".$add_sample_link."</td>\n";
			}
  }
  print "</tr>\n";
}


?>
</table>
</div>


<br/>

<script src="core/js/__jquery.tablesorter/jquery.tablesorter.min.js"></script>
<script>
$(function() {

	 <?php if($show_audit_logs) print "loadAuditLog(".AUDIT_TYPE_CARTON.",".$carton->carton_id.");\n"; ?>
	 
	/*
	$('[id^=cell__header_col-]').mouseenter( function() {
		var c = this.id.split("-")[1];
		//console.log('col '+c);
		$('[id^=cell__-newsample-]').addClass("cartonCellDown");
		for(var r=1;r<=cartonNumRows;r++) {
			//console.log('r '+r+' c '+c);
			$('[id^=cell__-newsample-'+r+'-'+c+']').removeClass("cartonCellDown");
			$('#cell__-newsample-'+r+'-'+c).addClass("cartonCellUpFree");
			//$('[id^=cell__-sample-]').removeClass("cartonCellDown");
			//$('[id^=cell__-sample-]').addClass("cartonCellUpOcc");
		}
	});
	*/
	
	$('[id^=cell__header_row-]').mouseenter( function() {
		var r = this.id.split("-")[1];
		//console.log('col '+c);
		$('[id^=cell__-newsample-]').addClass("cartonCellDown");
		for(var c=1;c<=cartonNumCols;c++) {
			//console.log('r '+r+' c '+c);
			$('[id^=cell__-newsample-'+r+'-'+c+']').removeClass("cartonCellDown");
			$('#cell__-newsample-'+r+'-'+c).addClass("cartonCellUpFree");
			//$('[id^=cell__-sample-]').removeClass("cartonCellDown");
			//$('[id^=cell__-sample-]').addClass("cartonCellUpOcc");
		}
	});
	
	$('[id^=cell__-newsample-]').mouseenter( function() {
		var r = this.id.split("-")[1];
		var c = this.id.split("-")[2];
		//console.log(r+' '+c);
		$('[id^=cell__-newsample-]').addClass("cartonCellDown");
		$(this).removeClass("cartonCellDown");
		$(this).addClass("cartonCellUpFree");
	});
	
	/*
	$('[id^=cell__-]').mouseout( function() {
		var r = this.id.split("-")[1];
		var c = this.id.split("-")[2];
		//console.log(r+' '+c);
		//$('[id^=cell__-]').addClass("cartonCellDown");
		$(this).removeClass("cartonCellUp");
		$(this).addClass("cartonCellDown");
	});
	*/
	
	$('#contentsTable').tablesorter();
	
  $('#cartonAttrBarCode').keyup(function() {
    var nbc = $('#cartonAttrBarCode').val();
    $('#barcodeContainer').html("<img src='core/barcode_gen.php?b="+nbc+"'>");
  });
	
  $('#cartonAttrEditDetails').click( function() {
		$('[id^=navpill_li-]').removeClass("active");
    $('#navpill_li-cartonAttrEditDetails').addClass("active");

		if( $('#plateContainer').css('display') != 'none') {
			$('#plateContainer').slideUp(200, function() {
				$('#formContainer').slideDown(200);
			});
		}
		if( $('#auditLogContainer').css('display') != 'none') {
			$('#auditLogContainer').slideUp(200, function() {
				$('#formContainer').slideDown(200);
			});
		}
  });
	
  $('#cartonAttrEditWells').click(function(){
    $('[id^=navpill_li-]').removeClass("active");
    $('#navpill_li-cartonAttrEditWells').addClass("active");
		if( $('#formContainer').css('display') != 'none') {
			$('#formContainer').slideUp(200, function() {
				$('#plateContainer').slideDown(200);
			});
		}
		if( $('#auditLogContainer').css('display') != 'none') {
			$('#auditLogContainer').slideUp(200, function() {
				$('#plateContainer').slideDown(200);
			});
		}
    return false;
  });
	
  $('#cartonAuditLogs').click(function(){
    $('[id^=navpill_li-]').removeClass("active");
    $('#navpill_li-cartonAuditLogs').addClass("active");
 
		if( $('#formContainer').css('display') != 'none') {
			$('#formContainer').slideUp(200, function() {
				$('#auditLogContainer').slideDown(200);
			});
		}
		
		if( $('#plateContainer').css('display') != 'none') {
			$('#plateContainer').slideUp(200, function() {
				$('#auditLogContainer').slideDown(200);
			});
		}
		
		<?php print "loadAuditLog(".AUDIT_TYPE_CARTON.",".$carton->carton_id.");\n"; ?>
		
    return false;
  });
	
	<?php
  if( isset($_GET['show_audit']) && ($_GET['show_audit']==1)) {
  ?>
  $('#plateContainer').hide();
  $('#formContainer').hide();
	$('#auditLogContainer').slideDown(200);
  <?php
  }
  ?>
	
  <?php
  if( isset($_GET['direct']) && ($_GET['direct']==1)) {
  ?>
  $('[id^=cartonAttr]').removeAttr('disabled');
	$('#cartonAttrCreateDate').attr('disabled','disabled');
  $('#cartonAttrSaveBtn').show(0);
  $('#cartonAttrEditBtn').hide(0);
  <?php
  }
  ?>
  
  $('[id^=sample-]').popover();
  $('[id^=newsample-]').popover();
	<?php
		if( $show_popover_for != null ) {
			print "$('#sample-".$show_popover_for."').popover('show');\n";
			//print "$('#sample-".$show_popover_for."').popover({trigger:'manual'});\n";
			}
			
	?>
  
	/*
  if( date_format == 1 ) {
    $( "#cartonAttrCreateDate" ).datepicker( { weekStart: 1, format: "dd/mm/yyyy"} );
  }
  if( date_format == 2 ) {
    $( "#cartonAttrCreateDate" ).datepicker( { weekStart: 1, format: "mm/dd/yyyy"} );
  }
  */
  

	
  $('#cartonAttrStorageLoc').change(function() {
    var sdid = $('#cartonAttrStorageLoc option:selected').val();

    $.getJSON('core/ajax_devices.php?m=gs&sdid='+sdid, function(shdata) {
      
      $('#cartonAttrStorageShelf option').remove(); // clear list first
      $.each(shdata, function(k,v) {
				if(typeof v === 'undefined') return;
				$('#cartonAttrStorageShelf').append('<option value='+k+'>'+v.shelf_number+'</option>');
      });
      
    });
  });
  
  $('#cartonAttrEditBtn').click( function() {
    $('[id^=cartonAttr]').removeAttr('disabled');
		$('#cartonAttrCreateDate').attr('disabled','disabled');
    $(this).hide( 0, function(){
      $('#cartonAttrSaveBtn').show(0);
      return false;
    });
    return false;
    });

  <?php if(($carton->owner_id==$_SESSION['lims']['person_id'])||($carton->allow_details_edit==1)) { ?>
  $('#cartonAttrSaveBtn').click( function() {
		var cident_raw = $('#cartonAttrIdent').val();
    var cident = encodeURIComponent(cident_raw);
    var ibc = encodeURIComponent($('#cartonAttrBarCode').val());
    var cd = $('#cartonAttrCreateDate').val();
    var sid = $('#cartonAttrStorageLoc option:selected').val();
    var shid = $('#cartonAttrStorageShelf option:selected').val();
		var owner = $('#cartonAttrOwner option:selected').val();
		var ade = 0;
		var ace = 0;
    if( $('#cartonAttrAllowNonOwnerEdit').is(':checked') ) ade = 1;
		if( $('#cartonAttrAllowNonOwnerWellEdit').is(':checked') ) ace = 1;
		
		//console.log(allow_det_edit+' '+allow_contents_edit);
		$(this).attr('disabled','disabled');
		$(this).html("<? print $button_save; ?> <i class='icon-spinner icon-spin'></i>");
    $.post('core/ajax_carton.php', {m:'u', pid:owner, cid:<?php print $carton->carton_id; ?>, ctid: <?php print $carton->container_type_id; ?>, cd: cd, cident:cident, ibc:ibc, sid:sid, shid:shid, ade:ade, ace:ace}, function(){
			if(cident_raw!='<?php print addslashes($carton->carton_identifier); ?>') {location.reload();}
			$('#cartonAttrSaveBtn').removeAttr('disabled');
			$('#cartonAttrSaveBtn').html("<? print $button_save; ?>");
      $('[id^=cartonAttr]').attr('disabled','disabled');
      $('[id^=cartonAttrEdit]').removeAttr('disabled');
			$('#cartonAttrCreateDate').attr('disabled','disabled');
      $('#cartonAttrSaveBtn').hide(0, function(){ $('#cartonAttrEditBtn').removeAttr('disabled'); $('#cartonAttrEditBtn').show(0); });
    });
    return false;
    });
    <?php } ?>
		
  });
</script>

<!-- <pre> -->
<?php 
// var_dump($carton_samples);
// var_dump($carton);  
// var_dump($shelves); 
?>
<!-- </pre> -->
