<?php
include "functions.lims.php";

if( $cid == null ) return;
if( ($r==null) || ($c==null) ) return;
$carton = getCarton($cid, $_SESSION['lims']['lab_id']);
if( $carton == null ) {
	error_log("Denied attempt to access carton in lab_id=".$_SESSION['lims']['lab_id']." from user ".$_SESSION['lims']['person_id']);
	?>
	<div class="alert">
  <?php print $_SESSION['lims']['langdata']['page_access_denied'];?>
	</div>
	<?php
	return;
	}
$stypes = getSampleTypes();

print "<h4>".$_SESSION['lims']['langdata']['new_sample']."</h4>\n";
print "<strong>".$_SESSION['lims']['langdata']['container'].":</strong> ".myhtmlentities($carton->carton_identifier)." <strong>".$_SESSION['lims']['langdata']['row'].":</strong> ".$r." <strong>".$_SESSION['lims']['langdata']['column'].":</strong> ".$c."\n";
?>
<hr>
<div id="user_feedback" style="display:none;"></div>

<form>
  <fieldset>
<!--     <legend>Legend</legend> -->
    <label><?php print $_SESSION['lims']['langdata']['name']; ?></label>
    <input type="text" id="newSampleName">

    <label><?php print $_SESSION['lims']['langdata']['type']; ?></label>
    <select id="newSampleType">
      <?php
      foreach($stypes as $st )
	print "<option value='".$st->id."'>".myhtmlentities($st->sample_type)."</option>\n";
      ?>
    </select>
    
    <br/>
    <button type="submit" class="btn btn-primary" id="newSampleSave"><?php print $button_save; ?></button>
  </fieldset>
</form>

<script>
$(function() {
  $('#newSampleSave').click( function() {
    var sn = $('#newSampleName').val();
    var st = $('#newSampleType').val();
    
    if( sn == "" ) {
      userFeedbackL('alert-error','err_enter_name');
      return false;
      }
      
		$(this).attr('disabled','disabled');
		$(this).html("<? print $button_save; ?> <i class='icon-spinner icon-spin'></i>");
    $.post('core/ajax_sample.php', {m:'n', sn:sn, st:st, cid:<?php print $cid; ?>, r:<?php print $r; ?>, c:<?php print $c; ?>}, function(r) {
      var j = jQuery.parseJSON(r);
      if( j.resp == -1 ) {
				userFeedbackL('alert-error',j.reason);
			}
      if( (j.resp==1) && isInt(j.sid) ) {
				window.location.replace('index.php?p=inv&sp=sample.open&s=edit&sid='+j.sid);
      }
      });
    return false;
    });
  });
</script>