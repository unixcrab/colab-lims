<?
require_once('class/BCGFont.php');
require_once('class/BCGColor.php');
require_once('class/BCGDrawing.php');
require("class/BCGcode39extended.barcode.php");


$color_black = new BCGColor(0, 0, 0);
$color_white = new BCGColor(255, 255, 255);

$font = new BCGFont('./class/font/Arial.ttf', 10);

$code = new BCGcode39extended(); // Or another class name from the manual
$code->setScale(1); // Resolution
$code->setThickness(30); // Thickness
$code->setForegroundColor($color_black); // Color of bars
$code->setBackgroundColor($color_white); // Color of spaces
$code->setFont($font); // Font (or 0)
$code->parse('12112000003aP'); // Text

$drawing = new BCGDrawing('', $color_white);
$drawing->setBarcode($code);
$drawing->draw();

header('Content-Type: image/png');

$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
?>
