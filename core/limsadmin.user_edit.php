<?php
if( $_SESSION['lims']['auth_level'] < PERM_MANAGER ) return;

if(isset($_GET['uid'])) $uid_edit = stripslashes($_GET['uid']);
if( ($uid_edit=="") || ($uid_edit<0) ) return;
// error_log("uid_edit=".$uid_edit);

include "functions.lims.php";

$u = getUserDetails($uid_edit);
if($u==null) return;

$user_edit_auth_lvl = getAuthLevelHighest($uid_edit);
if($user_edit_auth_lvl->auth_level_id>$_SESSION['lims']['auth_level']) return;

$lab_access = getUserLabAccess($uid_edit);
$auth_levels = getAuthLevels();
$labs = getLabs();
?>

<h4><?php print $_SESSION['lims']['langdata']['limsadmin_edit_user'].": <i>".myhtmlentities($u->username)."</i>"; ?></h4>
<hr>

<div id="user_feedback" class="alert alert-info" style="display:none;"></div>

<form>
  <?php
  if($u->active == 0) print "<label class='checkbox'><input type='checkbox' id='renableUser-".$uid_edit."'> ".$_SESSION['lims']['langdata']['limsadmin_enable_user']."</label><br/> \n";
  ?>
<!--   <br/> -->
  <label for="userUserName"><?php print $_SESSION['lims']['langdata']['username']; ?> (<?php print $_SESSION['lims']['langdata']['ldap_ad_ident']; ?>):</label>
  <input  type="text" id="userUserName" class="input-large" value="<?php print myhtmlentities( $u->username); ?>">
  
	<label for="passReset1"><?php print $_SESSION['lims']['langdata']['reset_password']; ?>:</label>
  <input  type="password" id="passReset1" class="input-medium" autocomplete="off">
	 <?php print $_SESSION['lims']['langdata']['repeat_password']; ?>: <input  type="password" id="passReset2" class="input-medium" autocomplete="off">
	
  <label for="userFirstName"><?php print $_SESSION['lims']['langdata']['first_name']; ?>:</label>
  <input  type="text" id="userFirstName" class="input-large" value="<?php print myhtmlentities( $u->first_name); ?>">

  <label for="userLastName"><?php print $_SESSION['lims']['langdata']['last_name']; ?>:</label>
  <input  type="text" id="userLastName" class="input-large" value="<?php print myhtmlentities( $u->last_name); ?>">

  <label for="userEmail"><?php print $_SESSION['lims']['langdata']['email_address']; ?>:</label>
  <input  type="text" id="userEmail" class="input-large" value="<?php print myhtmlentities($u->email_address); ?>">

  <br/>
  <table class="table table-hover  table-bordered" style="width:60%" >
  <caption><?php print $_SESSION['lims']['langdata']['limsadmin_lab_access'];?></caption>
  <thead>
  <tr><th></th>
	<th><?php print $_SESSION['lims']['langdata']['access']; ?></th>
	<th><?php print $_SESSION['lims']['langdata']['level']; ?></th>
	<th><?php print $_SESSION['lims']['langdata']['default']; ?></th></tr>
  </thead>
  <tbody>
  <?php
  foreach($labs as $lab) {
    $accessCheckStr = "";
    if( isset($lab_access[$lab->id] ) ) $accessCheckStr = "checked";
    $defaultCheckStr = "";
		$defaultClassStr = "";
    if( isset($lab_access[$lab->id] ) && ($lab_access[$lab->id]->default_flag=='Y') ) {$defaultCheckStr = "checked"; $defaultClassStr="class='info'";}
    print "<tr ".$defaultClassStr."><td id='lab_id-".$lab->id."'><strong>".myhtmlentities($lab->name)."</strong></td>".
    "<td><input type='checkbox' id='lab_access-".$lab->id."' ".$accessCheckStr."></td>".
    "<td>\n<select id='lab_authlevel-".$lab->id."'>\n<option value=0>".$_SESSION['lims']['langdata']['select']."</option>";
    foreach($auth_levels as $al_key=>$al_name) {
			if($al_key>$_SESSION['lims']['auth_level']) continue;
      $selStr = "";
      if( isset($lab_access[$lab->id]->auth_level_id) && ( $lab_access[$lab->id]->auth_level_id == $al_key ) ) $selStr = "selected";
      print "<option value=".$al_key." ".$selStr.">".$al_name."</option>\n";
			//print "<option value=".$al->auth_level_id." ".$selStr.">".$al->descr." (".$al->auth_level_id.")</option>\n";
      }
    print "</select></td>".
    "<td><input type='checkbox' id='lab_default-".$lab->id."' ".$defaultCheckStr."></td>".
    "</tr>\n";
    }
  ?>
  </tbody>
  </table>
  
	<div class="alert alert-error" style="display:none;"></div>
	
  <p>
	<button type="submit" class="btn btn-warning" id="editUserCancel"><? print $button_cancel; ?></button>&nbsp;&nbsp;&nbsp;
  <button type="submit" class="btn btn-primary" id="editUserSaveBtn"><? print $button_save; ?></button>
</form>


<script src="<?print $site_root; ?>/core/js/sha256.js"></script>
<script>
$(function() {
	$('#editUserCancel').click(function(){
		window.location.replace('index.php?p=limsadmin&sp=limsadmin.users_list');
		return false;
	});
	
  $('#editUserSaveBtn').click(function() {
		$("#user_feedback").slideUp(200); 
    var uid = <?php print $uid_edit; ?>;
    var uname = $('#userUserName').val();
    var fname = $('#userFirstName').val();
    var lname = $('#userLastName').val();
    var email = $('#userEmail').val();
    var active = <?php if($u->active == 0) print 0; else print 1;?>;
		
		var pass1 = $('#passReset1').val();
		var passSHA256 = "";
		if(pass1!="") {
			var pass2 = $('#passReset2').val();
			if(pass1!=pass2) { userFeedbackL('alert-error','err_passwords_no_match'); return false;}
			passSHA256 = CryptoJS.SHA256(pass1).toString(CryptoJS.enc.Hex);
			//console.log(passSHA256);
			//return false;
		}
		
		if( $('#renableUser-<?php print $uid_edit; ?>').is(':checked') ) { active = 1; }
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if(!emailReg.test(email)) { userFeedbackL('alert-error','err_email_invalid'); return false;}
    
    var ab = {}; // access array
    $('[id^=lab_access-]').each( function(){
      var k = this.id.split("-")[1];
      var v = 0;
      if( this.checked ) v = 1;
      ab[k] = v;
    });
    
    var al = {}; // auth level array
    $('[id^=lab_authlevel-]').each( function(){
      var k = this.id.split("-")[1];
			var s = $('#lab_authlevel-'+k+' option:selected').val();
			al[k] = s;
    });

		var def = 0;
		$('[id^=lab_default-]').each( function(){
			if(this.checked) def = this.id.split("-")[1]; 
		});
	
		var oneLabSel = 0;
		var doUpdate = true;
		$.each(ab, function( key, value ) {
			if(value==1) oneLabSel = 1;
			if((value==1)&&(al[key]==0)) { 
				userFeedbackL("alert-error", 'err_lab_select_access_level'); 
				doUpdate = false; 
				}
			});
		if( oneLabSel==0) { userFeedbackL("alert-error", 'err_select_lab'); doUpdate = false; }
		
		//return false;
		if(doUpdate) {
			$('#editUserSaveBtn').attr('disabled','disabled');
			$('#editUserSaveBtn').html("<? print $button_save; ?> <i class='icon-spinner icon-spin'></i>");
			$.post('core/ajax_user.php', {m:'u', u:uid, uname:uname, fname:fname, lname:lname, email:email, 'acc_arr':ab, 'auth_arr':al, def:def, a:active, p:passSHA256}, function(r) {
				var j = jQuery.parseJSON(r);
				if( j.resp = 1 ) //return false; 
					window.location.replace('index.php?p=limsadmin&sp=limsadmin.users_list');
			});
			}
    return false;
  });
  
  $('[id^=lab_default-]').click(function(){
    var lid = this.id.split("-")[1];
    $('[id^=lab_default-]').prop('checked', false);
    $('#lab_default-'+lid).prop('checked', true);
    $('#lab_access-'+lid).prop('checked', true);
  });
  
});
</script>