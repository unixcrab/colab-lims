<?php
session_start();

$uri = $_GET['uri'];
$newlab = $_GET['lid'];

if( $newlab == "" ) {
	print "no lab request";
	return;
	}

include_once "functions.user.php";
include_once "functions.lims.php";

$labs = getUserLabAccess($_SESSION['lims']['person_id']);
if( isset($labs[$newlab]) ) {
	//error_log("setting lab to ".$newlab);
	$_SESSION['lims']['lab_id'] = $newlab;
	$_SESSION['lims']['lab_name'] = getLab( $newlab )->name;
	header("Location: ".$uri);
}
else {
	error_log("Unauthorised attempt to change to lab ".$newlab." from user ".$_SESSION['lims']['person_id']);
	return;
}
?>