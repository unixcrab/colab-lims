<?php
if( !isset($_SESSION['lims']) ) session_start();
include "common.variables.php";
// include_once "functions.user.php";
?>
<div id="user_feedback" class="alert alert-info">
<? print $_SESSION['lims']['langdata']['err_all_fields_required']; ?>
</div>

<form name="change_pw" action="">
<fieldset>
  <label for="current_passwd" id="current_passwd_label"><strong><? print $_SESSION['lims']['langdata']['current_password']; ?></strong></label>
  <input type="password"  name="current_passwd" id="current_passwd" size="20" value="" />
<!--   <span class="help-block">Enter your current password to confirm it's you.</span> -->
  <br/>
  <label for="new_passwd1" id="new_passwd1_label"><strong><? print $_SESSION['lims']['langdata']['new_password']; ?></strong></label>
  <input type="password" name="new_passwd1" id="new_passwd1" size="20" value="" />
<!--   <span class="help-block">Enter the new password.</span> -->
  <br/>
  <label for="new_passwd2" id="new_passwd2_label"><strong><? print $_SESSION['lims']['langdata']['repeat_password']; ?></strong></label>
  <input type="password" name="new_passwd2" id="new_passwd2" size="20" value="" />
  <br/>
<!--   <br/> -->
  <button type="submit" class="btn btn-primary" id="change_pw_submit"><? print $button_save; ?></button>

</fieldset>
</form>

<!-- </div> -->
