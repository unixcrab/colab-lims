<!-- <link href="<?php echo $site_root; ?>/config/style/blueimp/jquery.fileupload-ui.css" rel="stylesheet" type="text/css"> -->
<?php
//   print "<script type='text/javascript' src='".$site_root."/core/js/ui/i18n/jquery.ui.datepicker-".trim($curr_lang,'"').".js'></script>";
?>
<script type="text/javascript" src="<?print $site_root.'/core/js/datepicker/bootstrap-datepicker.'.trim($curr_lang,'"').'.js';?>"></script>

<div id="user_feedback" class="alert alert-info" style="display: none;">
<? print $_SESSION['lims']['langdata']['please_complete']; ?>
</div>

<form name="user_project_new" action="">

  <div>
    <label for="limsproj_name" id="limsproj_name_label"><strong><? print $_SESSION['lims']['langdata']['projects_name']; ?></strong>:</label>
    <input class="input-large" type="text" name="limsproj_name" id="limsproj_name"  value="" />
  </div>
  
  <div id="proj_startdate" >
    <label for="limsproj_start_picker" id="limsproj_start_picker_label"><strong><? print $_SESSION['lims']['langdata']['projects_startdate']; ?></strong>:</label>
    <input type="text" id="limsproj_start_picker" value="<?php print makeDateString( time() ); ?>">
  </div>
  
<!--  <div class="input-append date" id="proj_startdate" data-date="<?php print makeDateString( time() ); ?>" data-date-format="dd/mm/yyyy">
    <input class="span2" size="16" type="text" value="<?php print makeDateString( time() ); ?>" readonly>
    <span class="add-on"><i class="icon-calendar"></i></span>
  </div>-->
  
  
  <div id="proj_enddate" >
    <label for="limsproj_end_picker" id="limsproj_end_picker_label"><strong><? print $_SESSION['lims']['langdata']['projects_enddate']; ?></strong>:</label>
    <input type="text" id="limsproj_end_picker">
  </div>
  
<!--  <div id="proj_startdate" class="pull-left">
    <label for="limsproj_start_picker" id="limsproj_start_picker_label"><strong><? print $_SESSION['lims']['langdata']['projects_startdate']; ?></strong>:</label>
    <div id="limsproj_start_picker"></div>
  </div>
  
  <div id="proj_enddate" class="pull-left" style="margin-left:35px;">
    <label for="limsproj_end_picker" id="limsproj_end_picker_label"><strong><? print $_SESSION['lims']['langdata']['projects_enddate']; ?></strong>:</label>
    <div id="limsproj_end_picker"></div>
  </div>-->
  
  <button type="submit" class="btn btn-primary" id="limsproj_new_submit"><? print $_SESSION['lims']['langdata']['create']; ?></button>
</form>

<script>
$(document).ready(function(){
  
  if( date_format == 1 ) {
    $( "#limsproj_start_picker" ).datepicker( { weekStart: 1, format: "dd/mm/yyyy"} );
    $( "#limsproj_end_picker" ).datepicker( { weekStart: 1, format: "dd/mm/yyyy"} );
  }
  if( date_format == 2 ) {
    $( "#limsproj_start_picker" ).datepicker( { weekStart: 1, format: "mm/dd/yyyy"} );
    $( "#limsproj_end_picker" ).datepicker( { weekStart: 1, format: "mm/dd/yyyy"} );
  }

});
</script>
