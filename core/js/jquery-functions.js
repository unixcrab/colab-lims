
function userFeedback(level, str) {
  $("#user_feedback").removeClass("alert alert-error alert-info alert-success");
  $("#user_feedback").addClass("alert "+level);
  $("#user_feedback").html(str);
  $("#user_feedback").show("fast");
}

function userFeedbackL(level, lindex) { // Languagefied version
  $.getJSON('core/ajax_lang.php', {l:curr_lang,i:lindex}, function(d) {
		$("#user_feedback").slideUp(200, function() {; 
			$("#user_feedback").removeClass("alert alert-error alert-info alert-success");
			$("#user_feedback").addClass("alert "+level);
			$("#user_feedback").html(d.s+"."); 
			$("#user_feedback").slideDown(200); 
			});
  });
  
}

function resetForm($form) {
    $form.find('input:text, input:password, input:file, select, textarea').val('');
    $form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
}
/*
function getRowLetter(rownum) {
 //var letters = {1:'A',2:'B'}
 return String.fromCharCode(65 + rownum);
}
*/
function isInt(s) {
  var intRegex = /^\d+$/;
  if(intRegex.test(s)) return true;
  else return false;
}

function isFloat(val) {
  if(!val || (typeof val != "string" || val.constructor != String)) {
    return(false);
  }
  var isNumber = !isNaN(new Number(val));
  if(isNumber) {
    if(val.indexOf('.') != -1) {
      return(true);
    } else {
      return(false);
    }
  } else {
    return(false);
  }
}

$(document).on("click", "#change_pw_submit", function(){
  var old_pw=$('#current_passwd').val();
  var new_pw1=$('#new_passwd1').val();
  var new_pw2=$('#new_passwd2').val();

  if( new_pw1 != new_pw2 ) {
    userFeedbackL("alert-error","err_passwords_no_match");
    return false;
  }
  
	var jqxhr = $.post('core/ajax_change_pw.php', {old_pw:old_pw,new_pw:new_pw1}, function(data) {
		if( data.resp == 2 ) userFeedbackL("alert-error", "err_current_password_incorrect");
		if( data.resp == 3 ) userFeedbackL("alert-success", "err_saved");
		//console.log(data.resp);
	}, "json" );
	
	return false;  
});

$(document).on("click", "#limsmodule_submit_update", function(){
  var modname = $('#limsmodule__modname').val();
  var datastring = "limsmod="+modname; //+"&limstbl="+modtbl;
  $("[id^=limsmodule__user__]").each(function () {
    parts = this.name.split("__");
    datastring += "&"+parts[3]+"="+encodeURIComponent(this.value);
  });
  
    $.ajax({
    type: "post",
    url: "core/ajax_module_update.php",
    data: datastring,
    dataType: "text",
    contentType: "application/x-www-form-urlencoded",
    success: function(data) {
      var chresp = jQuery.parseJSON(data);

      if( chresp.resp == 1  ) {
				userFeedback("alert-success", "Update successful. "+chresp.verify_response);
      }
      else if( chresp.resp == -1  ) {
				userFeedback("alert-error", "Could not verify module details: "+chresp.error+"");
      }
      else {
				userFeedback("alert-warning", "No changes made.");
      }

    },
    error: function() {
      userFeedback("alert-error", "Update failed.");
    }
  });
  
  
  return false;
});

$(document).on("click", "#change_details_submit", function(){
  var first_name = encodeURIComponent( $('#first_name').val() );
  var last_name = encodeURIComponent( $('#last_name').val() );
  var email_address = $('#email_address').val();
  var phone=encodeURIComponent( $('#phone').val() );
  
  if( first_name == "" ) {
    userFeedbackL("alert-error", "err_enter_first_name");
    return false;
  }
  if( last_name == "" ) {
    userFeedbackL("alert-error", "err_enter_last_name");
    return false;
  }
	
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if(!emailReg.test(email_address)) { userFeedbackL('alert-error','err_email_invalid'); return false;}
	email_address = encodeURIComponent( email_address );
	
	var jqxhr = $.post('core/ajax_change_details.php', {first_name:first_name,last_name:last_name,email_address:email_address,phone:phone}, function() {
		userFeedbackL("alert-success", "err_saved");
		//location.reload();
	});

  return false;
});

$(document).on("click", "#newattr_submit", function() {
	var attr_val= encodeURIComponent( $('#newattr_val').val() );
	if( attr_val == "" ) return false;
	
	var datastring = "e="+editAttribute+"&a=n&v="+attr_val;
	
	$.ajax({
	  type: "post",
	  url: "core/ajax_attr_new_edit.php",
	  data: datastring,
	  dataType: "text",
	  contentType: "application/x-www-form-urlencoded",
	  success: function(data) {

	  var chresp = jQuery.parseJSON(data);

		if( chresp.resp == 1  ) {
		  window.location = curr_uri;
		}
		else {
			$("#user_feedback").removeClass("alert alert-info");
			$("#user_feedback").addClass("alert alert-warning");
			$("#user_feedback").html("Lab not saved.");
		}

	  },
	  error: function() {
		$("#user_feedback").removeClass("alert alert-info");
		$("#user_feedback").addClass("alert alert-error");
		$("#user_feedback").html("Laboratory addition failed.");
	  }
	});

	return false;
});

$(document).on("click", "#newlab_submit", function(){
  var lab_name= encodeURIComponent( $('#newlab_name').val() );
  var lab_domain_id = encodeURIComponent( $('#newlab_domain').val() );
  
  if( lab_name == "" ) {
	$("#user_feedback").removeClass("alert alert-info");
    $("#user_feedback").addClass("alert alert-error");
    $("#user_feedback").html("Lab name cannot be empty.");
    return false;
  }
 
  var datastring = 'lab_name='+lab_name+'&lab_domain_id='+lab_domain_id;
  
  $.ajax({
  type: "post",
  url: "core/ajax_lab_new.php",
  data: datastring,
  dataType: "text",
  contentType: "application/x-www-form-urlencoded",
  success: function(data) {

  var chresp = jQuery.parseJSON(data);

    if( chresp.resp == 1  ) {
      $("#user_feedback").removeClass("alert alert-error alert-info");
      $("#user_feedback").addClass("alert alert-success");
      $("#user_feedback").html("newlab successful.");

      $('#newlab_submit').attr("disabled", true);
	  window.location = curr_uri;
    }
    else {
        $("#user_feedback").removeClass("alert alert-info");
        $("#user_feedback").addClass("alert alert-warning");
        $("#user_feedback").html("Lab not saved.");
    }

  },
  error: function() {
    $("#user_feedback").removeClass("alert alert-info");
    $("#user_feedback").addClass("alert alert-error");
    $("#user_feedback").html("Laboratory addition failed.");
  }
});
  return false;
});

function ajaxGetNumUserAlerts(callback) {
  $.getJSON('core/ajax_alerts.php?m=g', function(data) {
      var numAlerts = parseInt(data.length);
      callback(numAlerts);
  });  
}

function ajaxAckAlert(aid) {
  if( (! aid) || (aid<1) ) return false;
  $.get("core/ajax_alerts.php", { m: "a", aid: aid } );
  return false;
  
}


function ajaxDelLab(lab_id) {
	var datastring = 'lab_id='+lab_id
	
	$.ajax({
	  type: "post",
	  url: "core/ajax_lab_del.php",
	  data: datastring,
	  contentType: "application/x-www-form-urlencoded",
	  success: function(data) {
		var chresp = jQuery.parseJSON(data);
		
		if( chresp.resp == 1  ) {
		  window.location = curr_uri;
		}
		else {
			return false;
		}
	  },
	  error: function() {
		return false;
	  }
	});

	return false;
}

function ajaxDelAttr(a, a_id) {
	var datastring = 'a=d&e='+a+'&i='+a_id;

	$.ajax({
	  type: "post",
	  url: "core/ajax_attr_new_edit.php",
	  data: datastring,
	  dataType: "text",
	  contentType: "application/x-www-form-urlencoded",
	  success: function(data) {

	  var chresp = jQuery.parseJSON(data);

		if( chresp.resp == 1  ) {
		  window.location = curr_uri;
		}
		else {
			$("#user_feedback").removeClass("alert alert-info");
			$("#user_feedback").addClass("alert alert-warning");
			$("#user_feedback").html("Lab not saved.");
		}

	  },
	  error: function() {
		$("#user_feedback").removeClass("alert alert-info");
		$("#user_feedback").addClass("alert alert-error");
		$("#user_feedback").html("Laboratory addition failed.");
	  }
	});
		
	return false;
}

function ajaxChangeAttr(a,a_id,a_v) {
  var datastring = 'a=e&e='+editAttribute+'&i='+a_id+'&v='+a_v;
  
  $.ajax({
	  type: "post",
	  url: "core/ajax_attr_new_edit.php",
	  data: datastring,
	  dataType: "text",
	  contentType: "application/x-www-form-urlencoded",
	  success: function(data) {

	  var chresp = jQuery.parseJSON(data);

		if( chresp.resp == 1  ) {
		  window.location = curr_uri;
		}
		else {
			$("#user_feedback").removeClass("alert alert-info");
			$("#user_feedback").addClass("alert alert-warning");
			$("#user_feedback").html("Not saved.");
		}

	  },
	  error: function() {
		$("#user_feedback").removeClass("alert alert-info");
		$("#user_feedback").addClass("alert alert-error");
		$("#user_feedback").html("Edit failed.");
	  }
	});
		
  return false;
	
}

function ajaxChangeLab(lab_id, lab_name, lab_domain_id) {
	var datastring = 'lab_id='+lab_id+'&lab_name='+lab_name+'&lab_domain_id='+lab_domain_id;

	$.ajax({
	  type: "post",
	  url: "core/ajax_lab_change.php",
	  data: datastring,
	  dataType: "text",
	  contentType: "application/x-www-form-urlencoded",
	  success: function(data) {

	  var chresp = jQuery.parseJSON(data);

		if( chresp.resp == 1  ) {
		  return 1;
		}
		else {
			return 0;
		}

	  },
	  error: function() {
		return 0;
	  }
	});
}

function ajaxUpdateModule(mfname, mstate, menus) {	
	var jqxhr = $.post('core/ajax_modules.php', {mf:mfname,s:mstate,menus:menus}, function(data) {
		if( data.resp == 1 ) userFeedbackL("alert-success", "err_saved");
		else if( data.resp == 10 ) userFeedbackL("alert-error", "err_module_name_invalid");
		else userFeedbackL("alert-error", "err_not_available");
		//console.log("ajaxUpdateModule() "+data.resp);
		return data.resp;
	}, "json" );

	return jqxhr;
}

function ajaxCreateProject(p, sdate, edate) {
	var datastring = 'm=n&p='+p+'&sd='+sdate+'&ed='+edate;
	
	$.ajax({
	  type: "post",
	  url: "core/ajax_proj.php",
	  data: datastring,
	  contentType: "application/x-www-form-urlencoded",
	  success: function(data) {
		var chresp = jQuery.parseJSON(data);
		
		if( chresp.resp > 0  ) {
		  $(location).attr("href","index.php?p=bionfprojs&sp=projects.open&pid="+chresp.resp);
		  return true;
		}
		if( chresp.resp == -1  ) {
		  userFeedback("alert-error","Project <strong><i>"+decodeURIComponent(p)+"</i></strong> already exists.");
		  return false;
		}
		if( chresp.resp == -10  ) {
		  userFeedback("alert-error","End date is before start date!");
		  return false;
		}
		else {
			return false;
		}
	  },
	  error: function() {
		return false;
	  }
	});

	return true;
}

function ajaxInvProjUsers(pid,u) {
  	var datastring = 'm=i&pid='+pid+'&u='+u;
	
	$.ajax({
	  type: "post",
	  url: "core/ajax_proj.php",
	  data: datastring,
	  contentType: "application/x-www-form-urlencoded",
	  success: function(data) {
		var chresp = jQuery.parseJSON(data);
		
		if( chresp.resp > 0  ) {
		  userFeedback("alert-success", "Member list updated.");
		  return false;
		}
		if( chresp.resp == 0  ) {
		  userFeedback("alert-info","No new users invited to the project.");
		  return false;
		}
		if( chresp.resp == -1  ) {
		  userFeedback("alert-error","Project "+pid+" does not exist.");
		  return false;
		}
		
		else {
		  userFeedback("alert-error","Failed adding users.");
		  return false;
		}
	  },
	  error: function() {
		return false;
	  }
	});

	return false;
  
}

$(document).ready(function(){

  
  $('.dropdown-toggle').dropdown();

  $("#myaccount_passwd").click(function(){
    $("#contentCol").load(site_root+"/core/account.passwd.php");
  });
  $("#myaccount_details").click(function(){
    $("#contentCol").load(site_root+"/core/account.details.php");
  });
  $("#adm_users").click(function(){
    $("#contentCol").load(site_root+"/core/adm_users.php");
  });

  $("#inv_supplies").click(function(){
    $("#contentCol").load(site_root+"/core/inv_supplies.php");
  });
  $("#inv_samples").click(function(){
    $("#contentCol").load(site_root+"/core/inv_samples.php");
  });


	$('*[id*=btn_dellab-]:visible').click( function() {
		lab_id = this.value;
		$(this).fadeOut( function() {
			$('#btn_dellab_confirm-'+lab_id).fadeIn();
			$('#btn_dellab_confirm-'+lab_id).click( function() {
				dlret = ajaxDelLab(lab_id);
				});
			});
		
	});

  $("[id^=module_enable_chkbox--]").click(function () {
    //var chkBoxId = this.id;
    var fname = this.id.split("module_enable_chkbox--")[1];
    //console.log("chkBoxId="+this.id+" fname="+fname);
		//console.log("this.value "+this.value);
    var ret = true;
    
    if( this.value == "checked" ) {
      this.value = "";
			//console.log("this.value2 "+this.value);
			$("[id^=moduleMenu--"+fname.replace(".","\\.")+"--]").attr('disabled','disabled');
      //ret = ajaxUpdateModule(fname, 0);
    }
    else {
      this.value = "checked";
			//console.log("this.value2 "+this.value);
			$("[id^=moduleMenu--"+fname.replace(".","\\.")+"--]").removeAttr('disabled');
			
			
      //ret = ajaxUpdateModule(fname, 1);
    }
					     
  });
  

  $('*[id*=btn_editlab-]:visible').click( function() {
    var lab_id = this.value;
    var currentLabName = $('#lab_name-'+lab_id).text();
    var currentDomainName = $('#lab_domain-'+lab_id).text();

    $('#btn_dellab-'+lab_id).attr("disabled", true); 
    $(this).fadeOut( function() {
	    $('#lab_name-'+lab_id).html("<input id='lab_name_changed-"+lab_id+"' type='text' value='"+$('#lab_name-'+lab_id).html()+"'>");
	    var selectDomainString = "<select id='lab_domain_changed-"+lab_id+"'>";
	    for( var index in LabDomains ) {
		    var selStr = "";
		    if( LabDomains[index] == currentDomainName ) selStr = " selected";;
		    selectDomainString += "<option value='"+index+"' "+selStr+">"+LabDomains[index]+"</option>";
		    }
	    selectDomainString += "</select>";
	    
	    $('#lab_domain-'+lab_id).html(selectDomainString);
	    
	    $('#btn_editlab_confirm-'+lab_id).fadeIn();
	    $('#btn_editlab_confirm-'+lab_id).click( function() {
		    var newLabName = $('#lab_name_changed-'+lab_id).val();
		    var newDomainID = $('#lab_domain_changed-'+lab_id).val();
		    var newDomainName = $('#lab_domain_changed-'+lab_id+' option[value='+newDomainID+']').text(); 		    
		    if( (currentLabName==newLabName) && (currentDomainName==newDomainName) ) { 
			    $('#lab_name-'+lab_id).html(newLabName);
			    $('#lab_domain-'+lab_id).html(newDomainName);
			    
			    $('#btn_editlab_confirm-'+lab_id).attr("disabled", true);
			    $('#btn_dellab-'+lab_id).attr("disabled", false);
			    }
		    else {
			    var cret = ajaxChangeLab(lab_id, newLabName, newDomainID); 
			    // set fields back to read only
			    $('#lab_name-'+lab_id).html(newLabName);
			    $('#lab_domain-'+lab_id).html(newDomainName);
			    $('#btn_editlab_confirm-'+lab_id).hide();
			    $('#btn_dellab-'+lab_id).attr("disabled", false);
			    }
		    });
	    });
		
  });
	
/*
  $("#lang_sel_gb").click(function(){
        $.cookies.del( 'xlimslang' );
        $.cookies.set( 'xlimslang', 'gb' );
  });

  $("#lang_sel_de").click(function(){
        $.cookies.del( 'xlimslang' );
        $.cookies.set( 'xlimslang', 'de' );
  });

  $("#lang_sel_no").click(function(){
        $.cookies.del( 'xlimslang' );
        $.cookies.set( 'xlimslang', 'no' );
  }); 
*/
	
  $('#limsproj_new_submit').click( function() {
    var pname = encodeURIComponent($('#limsproj_name').val());
    var sdate = encodeURIComponent($('#limsproj_start_picker').val());
    var edate = encodeURIComponent($('#limsproj_end_picker').val());
    if( pname=="" || sdate=="" || edate=="" ) {

      return false;
    } 
    
    var npn = ajaxCreateProject(pname, sdate, edate);
    return false;
  });
  
  $('#lims_users_prefs_submit').click( function() {
		var us = $('#lims_user_unit_system').val(); // units
    var df = $('#lims_user_date_format_sel').val(); //date format
    var lab_id = $('#lims_user_lab_sel').val();
    var slab = 0;
    if( $('#lims_user_lab_save').is(':checked') ) slab = 1;
    $.post('core/ajax_user_prefs.php', {m:'u', us:us, df:df, lab_id:lab_id, lab_save:slab}, function() {
			userFeedbackL("alert-success", "err_saved");
      //location.reload();
      return false;
    });
    
    return false;
  });
  
  $('#projMembersConfirm').click( function() {
    var u = "";
    $("[id^=limsproj__madd__]").each( function () {
      if($('#'+this.id).is(':checked')) {
	parts = this.id.split("__");
	u += parts[2]+",";
      }
    });
    
    ajaxInvProjUsers(curr_projid,u);
    return false;
  });
  
  // project pages effects
  var transitionType = "drop";
  $('#projMemberAdminLink').click( function() {
    $('*[id*=projAdmin]:visible').hide(transitionType, 'fast', function() { $('#projAdminMembers').show(transitionType, 'fast'); });
  });
  
  $('#projFilesAdminLink').click( function() {
    $('*[id*=projAdmin]:visible').hide(transitionType, 'fast', function() { $('#projAdminFiles').show(transitionType, 'fast'); });
  });
  
  $('#projFlowAdminLink').click( function() {
    $('*[id*=projAdmin]:visible').hide(transitionType, 'fast', function() { $('#projAdminFlow').show(transitionType,'fast'); }); 
  });
  
});
