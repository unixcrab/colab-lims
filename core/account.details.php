<?php
//session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "common.variables.php";
//$u = getUserDetails($_SESSION['lims']['person_id']);

// echo $subpage;
?>

<!--  MUST UPDATE SESSION WITH NEW DETAILS! -->

<div id="user_feedback" class="alert alert-info">
<? print $_SESSION['lims']['langdata']['please_provide']; ?>
</div>

<form name="change_details" action="" >

  <label for="first_name" id="first_name_label"><strong><? print $_SESSION['lims']['langdata']['first_name']; ?></strong></label>
  <input class="input-large" type="text"  name="first_name" id="first_name"  value="<?php print myhtmlentities($_SESSION['lims']['first_name']); ?>" />

  <label for="last_name" id="last_name_label"><strong><? print $_SESSION['lims']['langdata']['last_name']; ?></strong></label>
  <input class="input-large" type="text"  name="last_name" id="last_name"  value="<?php print myhtmlentities($_SESSION['lims']['last_name']); ?>" />

  <label for="email_address" id="email_address_label"><strong><? print $_SESSION['lims']['langdata']['email_address']; ?></strong></label>
  <input class="input-large" type="text"  name="email_address" id="email_address"  value="<?php print myhtmlentities($_SESSION['lims']['email_address']); ?>" />

  <label for="phone" id="phone_label"><strong><? print $_SESSION['lims']['langdata']['phone']; ?></strong></label>
  <input class="input-large" type="text"  name="phone" id="phone"  value="<?php print myhtmlentities($_SESSION['lims']['phone']); ?>" />
<!--   <span class="help-block">Cell or landline</span> -->
  <br/>
  <button type="submit" class="btn btn-primary" id="change_details_submit"><? print $button_save; ?></button>


</form>