<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "functions.db_connect.php";
include "functions.modules.php";

// $mname=stripslashes( $_POST['m'] );
$fname = stripslashes( $_POST['mf'] ); // module XML filename
$mstate = stripslashes( $_POST['s'] ); // 0 or 1
$menus_array = json_decode(stripslashes( $_POST['menus'] ) );

$module_name = mb_split(".xml",$fname)[0];

if( $space=mb_stristr($fname," ") ) {
	print json_encode( array("resp"=>10) );
	return;
	}

if($mstate == 0) {
	$r = delModule($module_name);
	print json_encode( array("resp"=>1) );
	return;
  }
	
$r = 1;

$modXML = simplexml_load_file("../".$modules_dir."/".$fname);


if($mstate == 1 ) {
	$module_id = addModule($modXML->name, $fname);
	
	// table fields
	$create_mod_app_config = false;
	$create_mod_user_config = false;

	foreach($modXML->db[0]->table as $xtable) {
		$mod_table_name = "";
		$create_sql = "";
		
		if($xtable['type']=="app_config") {
					// force table name from xml file name
					$mod_table_name = $tablePrefix."module_".$module_name;
					$create_mod_app_config = true;
					}
		else if($xtable['type']=="user_config") {
					$mod_table_name = $tablePrefix."module_".$module_name."_user";
					$create_mod_user_config = true;
					}
					
		error_log("mod_table_name=".$mod_table_name);

		if($mod_table_name == "") continue;
		
		$primary_key = "";
		$create_sql = "CREATE TABLE IF NOT EXISTS `".$mod_table_name."` (";
		
		foreach($xtable->field as $field) {
			if( $field["key"] == "primary" ) $primary_key = $field["name"];
			if( $field["type"] == "varchar" )
				$create_sql .= "`".$field["name"]."` varchar(64) NOT NULL,";
			elseif( $field["type"] == "int" )
				$create_sql .= "`".$field["name"]."` int(11) NOT NULL,";
				
			$ue = false;
			if( $field['user_edit'] ) {
				$ue = true; 
				}
			}

		if( $primary_key != "" ) $create_sql .= "PRIMARY KEY(`".$primary_key."`)";
		else $create_sql = rtrim($create_sql,",");
		
		$create_sql .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8";
		error_log("create module SQL=".$create_sql);
		
		dbq($create_sql);
		if($create_mod_app_config)
			$r = setModuleMenus($module_id,MODULE_COMPONENT_APP_CONFIG,$menus_array);
		if($create_mod_user_config) {
			$menus_array = array(MENU_USER_PROFILE=>0);
			$r = setModuleMenus($module_id,MODULE_COMPONENT_USER_CONFIG,$menus_array);
			}
	 }
 
}
//else {
  //$module_id = addModule($modXML->name, $fname);
	//error_log("module_id ".$module_id);
	
  //if( $create_sql!="") dbq($create_sql);
	
	//setModuleMenus($module_id,MODULE_COMPONENT_APP_CONFIG,$menus_array);
//  }


if( $r == 1 ) {
	print json_encode( array("resp"=>1) );
	}
else
	print json_encode( array("resp"=>0) );
?>