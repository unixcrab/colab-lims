<?php 
if( $_SESSION['lims']['auth_level'] < PERM_ADMIN ) return;

include_once "functions.modules.php";
$mods = getModulesAll();


$mod_id_arr = array();
foreach( $mods as $m ) {
	$mod_id_arr[$m->file] = $m->module_id;
	}

//print "<pre>";
//	var_dump($mod_id_arr);
//print "</pre>";
?>

<h4><?php print $_SESSION['lims']['langdata']['limsadmin_modules']; ?></h4>
<hr>

<div id="user_feedback" class="alert alert-info" style="display:none;"></div>

<div class="accordion" id="accordion2">

<?php
$tmp_mod_id = 1;
if ($handle = opendir($modules_dir)) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
						$ext = substr(strrchr($entry, '.'), 1);
            
            if( $ext != "xml" ) continue;
            
            $modXML = simplexml_load_file($modules_dir."/".$entry);
						
						//print "<pre>";
							//var_dump($modXML->db->table[0]['type']);
							//foreach($modXML->db->table[0]->field as $field) var_dump($field);
						//print "</pre>";


            $accord_name = str_replace(".","_",$entry);
						$mod_enabled = true;
						$chkString = "checked";
						//if( isset($mod_id_arr[$entry]) && ( $mods[$mod_id_arr[$entry]]->active==1 ) ) {
						if( isset($mod_id_arr[$entry]) ) {
							$mod_id = $mod_id_arr[$entry];
							$mod_menus = getModuleMenus($mod_id,MODULE_COMPONENT_APP_CONFIG);
							if($mods[$mod_id_arr[$entry]]->active==0) {
								$chkString = "";
								$mod_enabled = false;
								}
							}
						else {
							$mod_id = $tmp_mod_id;
							$mod_enabled = false;
							$chkString = "";
							}
						
						
            $descrLang = $modXML->descr->gb;
            if( $modXML->descr->$_SESSION['lims']['lang'] != "" ) $descrLang = $modXML->descr->$_SESSION['lims']['lang'];
            
						$menu_chkString = "";
						?>
						<div class="accordion-group">
							<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse-<?php print $accord_name; ?>">
								<?php print "<strong>".myhtmlentities($modXML->name)."</strong> (".myhtmlentities($descrLang).")"; ?>
							</a>
						</div>
						
						<div id="collapse-<?php print $accord_name; ?>" class="accordion-body collapse ">
							<div class="accordion-inner">
								<!-- MODULE CONFIG -->
								
								<label class="checkbox">
									<input type="checkbox" id='module_enable_chkbox--<?php print $entry; ?>' <?php print $chkString; ?> value='<?php print $chkString; ?>'>
									<?php print $_SESSION['lims']['langdata']['enable']; ?>
									</label>
									
								<form>
								<fieldset>
								<legend><?php print $_SESSION['lims']['langdata']['menus']; ?></legend>
		
									<?php if( isset($mod_menus[MENU_ADMIN]) ) $menu_chkString="checked"; else $menu_chkString="";  ?>
									<label class="checkbox"><input type="checkbox" id="moduleMenu--<?php print $entry; ?>--admin--<?php print MENU_ADMIN; ?>" <?php print $menu_chkString; ?>> <?php print $_SESSION['lims']['langdata']['topmenu_admin']; ?></label>
									
									<?php if( isset($mod_menus[MENU_USER_PROFILE]) ) $menu_chkString="checked"; else $menu_chkString="";  ?>
									<label class="checkbox"><input type="checkbox" id="moduleMenu--<?php print $entry; ?>--profile--<?php print MENU_USER_PROFILE; ?>" <?php print $menu_chkString; ?>> <?php print $_SESSION['lims']['langdata']['account_admin']; ?></label>
									
									
										<?php 
										$menu_section = 0;
										if( isset($mod_menus[MENU_INVENTORY]) ) {
											$menu_chkString="checked";
											$menu_section = $mod_menus[MENU_INVENTORY]->section_id;
											}
										else $menu_chkString="";
											?>
										<label class="checkbox">
											<input type="checkbox" id="moduleMenu--<?php print $entry; ?>--inv--<?php print MENU_INVENTORY; ?>" <?php print $menu_chkString; ?>> <?php print $_SESSION['lims']['langdata']['limsadmin_inventory']; ?>
										</label>
										
										<select id="moduleMenu--<?php print $entry; ?>--invSection">
											<option value=0><?php print $_SESSION['lims']['langdata']['section']; ?></option>
											<option value=<?php print MENU_SECTION_CARTONS; ?> <?php if($menu_section==MENU_SECTION_CARTONS) print "selected"; ?>>
											<?php print $_SESSION['lims']['langdata']['cartons']; ?>
											</option>
											<option value=<?php print MENU_SECTION_SAMPLES; ?> <?php if($menu_section==MENU_SECTION_SAMPLES) print "selected"; ?>>
											<?php print $_SESSION['lims']['langdata']['samples']; ?>
											</option>
											<option value=<?php print MENU_SECTION_LAB; ?> <?php if($menu_section==MENU_SECTION_LAB) print "selected"; ?>>
											<?php print $_SESSION['lims']['langdata']['laboratory']; ?>
											</option>
										</select>
									

									</fieldset>
									
									<button class="btn btn-primary" id="saveButton--<?php print $entry; ?>"><?php print $button_save; ?></button>
								</form>
								
								
								
								
							</div>
						</div>
						<?php

						
            $tmp_mod_id++;
						
						?>
						</div>
						<?php
						
						//if(!$mod_enabled) print "<script>$(function() { $('[id^=moduleMenu--reserve_instrument\\\.xml--]').attr('disabled','disabled'); });</script>";
						if(!$mod_enabled) print "<script>$(function() { $('[id^=moduleMenu--".str_replace(".","\\\.",$entry)."--]').attr('disabled','disabled'); });</script>";
          }
    }
    closedir($handle);
}
?>
</div>


<script type="text/javascript" src="<?print $site_root.'/core/js/json/jquery.json.min.js';?>"></script>
<script>
	$(function() {
		$("[id^=saveButton--]").click(function () {
			var saveMenus = true;
			var fname = this.id.split("--")[1];
			var fname_esc = fname.replace(".","\\.");
			//console.log("id="+this.id+" fname="+fname);
			
			var enMod = $("#module_enable_chkbox--"+fname_esc).val();
	
			var menuArray = {}; //new Array(); // key=menu id, value=section id
			$("[id*=moduleMenu--"+fname_esc+"--]").each(function(){
				if(this.checked) {
					var mid = this.id.split("--")[3];
					menuArray[mid] = 0;
					if(mid==<?php print MENU_INVENTORY;?>) {
						// get the section id
						var sid = parseInt($("#moduleMenu--"+fname_esc+"--invSection").val());
						menuArray[mid] = sid;
						console.log("sid="+sid);
						if((sid==0)&&(enMod=="checked")) {
							saveMenus = false;
							userFeedbackL("alert-error", "module_choose_section");
						}
					}
					console.log(mid+" checked");
					}
			}); 
			
			var numMenus = Object.keys(menuArray).length;
			console.log("menuArray length "+numMenus);
			if(numMenus==0) {
				saveMenus = false;
				userFeedbackL("alert-error", "module_choose_menu");
			}
			
			if(saveMenus) {
				var menuArrayJSON = $.toJSON(menuArray);
				console.log(menuArrayJSON);
				if(enMod=="checked") {
					ajaxUpdateModule(fname, 1, menuArrayJSON);
						}
					else {
						ajaxUpdateModule(fname, 0, menuArrayJSON);
					}
				
				
			}
			
			
			return false;
		});
	});
</script>