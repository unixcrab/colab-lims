<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "constants.php";
include "functions.db_connect.php";
include "functions.lims.php";

$mode = "";
if( isset($_POST['m']) ) $mode = $_POST['m'];
if( $mode == "" ) return;

date_default_timezone_set($site_timezone);

$c = new Carton();
if( isset($_POST['cid']) )
  $c->carton_id = stripslashes( $_POST['cid'] );
if( isset($_POST['ctid']) )
  $c->container_type_id = stripslashes( $_POST['ctid'] );
if( isset($_POST['sdid']) )
  $c->storage_id = stripslashes( $_POST['sdid'] );
if( isset($_POST['cident']) )
  $c->carton_identifier = strip_tags(urldecode(stripslashes( $_POST['cident'] )));
if( isset($_POST['ibc']) )
  $c->internal_barcode = strip_tags(urldecode(stripslashes( $_POST['ibc'] )));
if( isset($_POST['lid']) )
	$laboratory_id = stripslashes( $_POST['lid'] );
if( isset($_POST['pid']) )
  $c->owner_id = stripslashes( $_POST['pid'] );
if( isset($_POST['ade']) )
  $c->allow_details_edit = $_POST['ade'];
if( isset($_POST['ace']) )
  $c->allow_contents_edit = $_POST['ace'];

// error_log("lab_id=".$l->id."   ".$l->name."   ".$l->domain_id);

if( isset($_POST['cd']) ) {
  if( $_SESSION['lims']['dateformat'] == 1 ) 
    $c->create_date_timestamp = strtotime( str_replace('/', '-', stripslashes( $_POST['cd'] ) ) );
  else
    $c->create_date_timestamp = strtotime( stripslashes( $_POST['cd'] ) );
}
if( isset($_POST['sid']) )
  $c->storage_id = stripslashes( $_POST['sid'] );
if( isset($_POST['shid']) )
  $c->shelf_id = stripslashes( $_POST['shid'] );

$r = 0;
if( $mode == "u" ) { // update
	$curr_carton = getCarton($c->carton_id,$_SESSION['lims']['lab_id']);
	if(($curr_carton->owner_id!=$_SESSION['lims']['person_id'])) {
		// don't overwrite permissions
		$c->allow_details_edit = $curr_carton->allow_details_edit;
		$c->allow_contents_edit = $curr_carton->allow_contents_edit;
	}
	if(($curr_carton->owner_id==$_SESSION['lims']['person_id'])||($curr_carton->allow_details_edit==1)) {
		$r = updateCarton($c);
		print json_encode( array("resp"=>$r) );
		
	}
	return;
}
if( $mode == "n" ) { // new
//   sleep(1);
  $c = addCarton($c);
  if( $c->carton_id > 0 ) $r=1;
 
  print json_encode( array("resp"=>$r, "cid"=>$c->carton_id) );
  return;
}

// if( $r == 1 ) {
// 	print json_encode( array("resp"=>1) );
// 	}
// else
// 	print json_encode( array("resp"=>0) );
?>