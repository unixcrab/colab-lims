<?php
$icon_edit = "<i class='icon-edit' title='".$_SESSION['lims']['langdata']['edit']."' data-placement='right' rel='tooltip'></i>";
$icon_trash = "<i class='icon-trash' title='".$_SESSION['lims']['langdata']['delete']."' data-placement='right' rel='tooltip'></i>";

$button_save = "<i class='icon-save icon-white'></i> ".$_SESSION['lims']['langdata']['save'];
$button_trash = "<i class='icon-trash icon-white'></i> ".$_SESSION['lims']['langdata']['delete'];
$button_cancel = "<i class='icon-ban-circle icon-white'></i> ".$_SESSION['lims']['langdata']['cancel'];
$button_add = "<i class='icon-plus icon-white'></i> ".$_SESSION['lims']['langdata']['add'];
$button_close = "<i class='icon-remove icon-white'></i> ".$_SESSION['lims']['langdata']['close'];
$button_reserve = "<i class='icon-time icon-white'></i> ".$_SESSION['lims']['langdata']['reserve'];
	
?>