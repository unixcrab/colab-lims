<?php
if( $_SESSION['lims']['auth_level'] < PERM_ADMIN ) return;

include "functions.lims.php";

$labs = getLabs();
$domains = getDomains();

function loadListDiv() {
	global $labs, $domains;
	foreach($labs as $l ) {
			
			print "<tr><td>\n<div id='lab_name-".$l->id."'>".myhtmlentities($l->name)."</div></td> <td><div id='lab_domain-".$l->id."'>".myhtmlentities($l->domain_name)."</div></td>".
				"<td width='120'><div align='right'>\n".
				"<td><button class='btn btn-mini' id='btn_editlab-".$l->id."' value=".$l->id.">".$_SESSION['lims']['langdata']['edit']."</button>\n".
				"<button class='btn btn-mini btn-info' id='btn_editlab_confirm-".$l->id."' value=".$l->id." style='display:none;'>".$_SESSION['lims']['langdata']['save']."</button></td>\n".
				// "<td><button class='btn btn-mini btn-warning' id='btn_dellab-".$l->id."' value=".$l->id.">".$_SESSION['lims']['langdata']['delete']."</button>\n".
				
// 				"<td><button class='btn btn-mini btn-warning' id='btn_dellab-".$l->id."' value=".$l->id."><i class='icon-remove'></i></button>\n".
				"<td><button class='close' id='btn_dellab-".$l->id."' value=".$l->id.">&times;</button>\n".
				
// 				"<button class='btn btn-mini btn-danger' id='btn_dellab_confirm-".$l->id."' value=".$l->id." style='display:none;'>".$_SESSION['lims']['langdata']['confirm']."</button>".
				"<button class='btn btn-mini btn-danger' id='btn_dellab_confirm-".$l->id."' value=".$l->id." style='display:none; float: right;'>".$_SESSION['lims']['langdata']['confirm']."</button>".
				"</div>\n</td></tr></td>\n\n";
		}
}
?>

<script>
$(function() {
  $("[id^=btn_dellab-]").tooltip();
});
</script>

<script type="text/javascript">
var LabDomains = new Array();
<?php
	foreach($domains as $d) {
		print "LabDomains[".$d->id."] = '".$d->domain_name."'; ";
	  }
?>

</script>

<h4><? print $_SESSION['lims']['langdata']['limsadmin_labs']; ?></h4>

<div style="height: 300px; overflow: auto;" id="lab_list_div">

	<table class="table table-hover table-condensed" >

	<tbody>
	<?php
		loadListDiv();
	?>
	</tbody>
	</table>
</div>


<div id="newlab_container">
	<div id="user_feedback" class="alert alert-info">
	Enter details for the new laboratory.
	</div>
	<form name="form_newlab" action="" class=" well form-inline">
	<fieldset>
		<label for="newlab_name" id="newlab_name_label"><strong><? print $_SESSION['lims']['langdata']['limsadmin_lab_name']; ?></strong></label>
	  <input type="text"  name="newlab_name" id="newlab_name" size="15" value="" />
	  &nbsp;
	  <label for="newlab_domain" id="newlab_domain_label"><strong><? print $_SESSION['lims']['langdata']['domain']; ?></strong></label>
	  <select name="newlab_domain" id="newlab_domain" > <!-- style="width: 150px;" -->
	  <?php
	  foreach($domains as $d) {
		print "\n<option value='".$d->id."'>".$d->domain_name."</option>";
	  }
	  ?>
	  </select>
	  &nbsp;
	  <button type="submit" class="btn btn-primary" id="newlab_submit"><? print $button_save; ?></button>
	</fieldset>
	</form>
</div>

