<!-- <div class="pull-left"> -->
<div id="user_feedback" class="alert alert-info" style="display: none;">
  <? print $_SESSION['lims']['langdata']['please_complete']; ?>
</div>

<?php
$pmems = getProjectMembers($pid);
// print "<pre>";
// print var_dump($pmems);
// print "</pre>\n";

$uidarray = array();
foreach($pmems as $pmem) {
//   print " pmem: ".$pmem->first_name." ".$pmem->last_name;
  $uidarray[$pmem->person_id] = $pmem->person_id;
}

$users = getAllUsers();

$inviteCheckboxDisableStr = "";


foreach($users as $k=>$u) {
  $m = $k % 4;
//   if( $m == 0 ) print "<div class=""></div>\n";
//     print "<br/>\n";
  
  $checkStr = "";
  if( isset($uidarray[$u->person_id]) ) $checkStr = "checked";
  
  if( ! $proj_owner and ($u->person_id != $person_id) ) $inviteCheckboxDisableStr = "disabled";
  if( ! $proj_owner and ($u->person_id == $person_id) ) $inviteCheckboxDisableStr = "";

  $displayCss = "inline";
  if( $proj_owner and ($u->person_id == $person_id) ) $displayCss = "none";
  
//   value='".$checkStr."'
  print "<div style='display:".$displayCss.";position:relative;padding: 2px;margin-right: 10px;margin-bottom: 10px;height:60px;width:150px;border:1px solid #999999;box-shadow: 2px 2px 5px #888888;border-radius:3px;' class='pull-left'><i class='icon-user'></i> <strong>".myhtmlentities($u->first_name." ".$u->last_name)."</strong><hr style='margin:2px;'><label class='checkbox' style='position:absolute;bottom:0;'><input type='checkbox' id='limsproj__madd__".$u->person_id."' ".$checkStr." ".$inviteCheckboxDisableStr.">".$_SESSION['lims']['langdata']['member']."</label></div>\n";

}
?>
<!-- </div> -->

<!-- <br/> -->
<div >
<button id="projMembersConfirm" class="btn btn-primary"><?php print $_SESSION['lims']['langdata']['confirm']; ?></button>
<!-- <br/> -->
<!-- <div class='clearfix'> -->

<!-- </p> -->
</div>
