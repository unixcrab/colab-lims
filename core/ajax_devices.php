<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "constants.php";
include "functions.db_connect.php";
include "functions.lims.php";

//foreach($_POST as $k=>$v) error_log(__FILE__." POST ".$k." -> ".$v);
//foreach($_GET as $k=>$v) error_log(__FILE__." GET ".$k." -> ".$v);

$mode = "";
if( isset($_POST['m']) ) $mode = $_POST['m'];
elseif( isset($_GET['m']) ) $mode = $_GET['m'];
// error_log(__FILE__." m=".$mode);
if( $mode == "" ) return;

if( $mode == "gs" ) { // get shelves for storage_id
  $storage_id = $_GET['sdid'];
  if($storage_id == "") return;
  $shelves = getStorageShelves($storage_id);
  print json_encode($shelves);
  //error_log(__FILE__." ".json_encode($shelves) );
  return;
}

if( $mode == "nc" ) { // new container 
  $cname = trim(urldecode($_POST['n']));
  $cap = $_POST['cap'];
  $rows = $_POST['r'];
  $cols = $_POST['c'];
  
  //error_log(__FILE__." ".$cname." ".$cap." ".$rows." ".$cols);
//   $newc = new Container($cname,$cap,$rows,$cols);
  $newc = new Container();
  $newc->container_type = $cname;
  $newc->capacity = $cap;
  $newc->number_of_rows = $rows;
  $newc->number_of_columns = $cols;
  addContainer($newc);
}

if( $mode == "dc" ) { // disable container 
  $cid = $_POST['i'];
  if( $cid == "" ) return;
  delContainer($cid);
}

if( $mode == "uc" ) { // update container 
  $cid = $_POST['i'];
  $cname = trim(urldecode($_POST['n']));
  $cap = $_POST['cap'];
  $rows = $_POST['r'];
  $cols = $_POST['c'];
  
  if( $cid == "" || $cname == "" ) return;
  
  //error_log(__FILE__." ".$cid." ".$cname." ".$cap." ".$rows." ".$cols);
  $c = new Container();
  $c->id = $cid;
  $c->container_type = $cname;
  $c->capacity = $cap;
  $c->number_of_rows = $rows;
  $c->number_of_columns = $cols;
  updateContainer($c);
}

if( $mode == "dsh" ) { // delete shelf
  $shelf_id = $_POST['shid'];
  if( $shelf_id == "" ) return;
  delStorageShelf($shelf_id);
}

if( $mode == "nsh" ) { // new shelf
  $shelf_name = urldecode($_POST['sn']);
  $storage_id = $_POST['sdid'];
  if( $shelf_name == "" || $storage_id == "" ) return;
  $shelf = new StorageShelf();
  $shelf->shelf_number = $shelf_name;
  $shelf->storage_id = $storage_id;
  addStorageShelf($shelf);
}

if( $mode == "d" ) { // delete storage device
  $sd_id = 0;
  if( isset($_POST['i']) ) $sd_id = $_POST['i'];
  if( $sd_id == 0 ) return;
  
//   TODO: check sd_id dependencies!
  delStorageDevice($sd_id);
}

if( $mode == "u" ) { // update storage device
  $sd_id = 0;
  $name = "";
  $lab_id = 0;
  $type_id = 0;
  
  if( isset($_POST['i']) ) $sd_id = $_POST['i'];
  if( isset($_POST['n']) ) $name = trim(urldecode($_POST['n']));
  if( isset($_POST['l']) ) $lab_id = $_POST['l'];
  if( isset($_POST['t']) ) $type_id = $_POST['t'];
  
  if( $sd_id == 0 ) return;
  
  $nsd = new StorageDevice();
  $nsd->storage_id = $sd_id;
  $nsd->storage_type_id = $type_id;
  $nsd->laboratory_id = $lab_id;
  $nsd->identifier = $name;
  updateStorageDevice($nsd);
}

if( $mode == "nsd" ) { // new storage device
//   error_log(__FILE__." nsd");
  $name = "";
  $lab_id = 0;
//   $env = "";
  $type_id = 0;
  
  if( isset($_POST['n']) ) $name = urldecode($_POST['n']);
  if( isset($_POST['l']) ) $lab_id = $_POST['l'];
//   if( isset($_POST['e']) ) $env = $_POST['e'];
  if( isset($_POST['t']) ) $type_id = $_POST['t'];
  
  $nsd = new StorageDevice();
  $nsd->storage_id = 0;
//   $nsd->environment_description = $env;
  $nsd->storage_type_id = $type_id;
  $nsd->laboratory_id = $lab_id;
  $nsd->identifier = $name;
  addStorageDevice($nsd);
}
?>