<h4>
<?php
print $_SESSION['lims']['langdata']['projects_flow'];
?>
</h4>

<script>
    $(function() {
        $( "#stageSortableList" ).sortable({
            revert: true
        });
        $( "#draggable" ).draggable({
            connectToSortable: "#stageSortableList",
            helper: "clone",
            revert: "invalid"
        });
        $( "ul, li" ).disableSelection();
    });
    </script>

<hr>

Modules:
<ul style="list-style-type: none; margin: 0; padding: 0; margin-bottom: 10px;">
    <li id="draggable" class="ui-state-highlight" style="margin: 5px; padding: 5px; width: 150px;">Complex module</li>
</ul>

Stages: <button class="btn btn-primary" type="button"><i class="icon-plus icon-white"></i> Add stage</button>
<ul style="list-style-type: none; margin: 0; padding: 5px; margin-bottom: 10px; width: 250px; height: 60px;" id="stageSortableList">
    <li class="ui-state-default">Gather files</li>
    <li class="ui-state-default">Sequencing</li>
    <li class="ui-state-default">alignment/SNP calling</li>
    <li class="ui-state-default">QC</li>
    <li class="ui-state-default">Publish</li>
</ul>