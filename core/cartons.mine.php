<?php
include "functions.lims.php";

$uri = "index.php?p=inv&sp=cartons.mine";

$show = "details";
if( isset($_GET['show']) ) $show = $_GET['show'];
$row = 0;
if( isset($_GET['row']) ) $row = $_GET['row'];
$col = 0;
if( isset($_GET['col']) ) $col = $_GET['col'];
$climit = 20;
if( isset($_GET['climit']) ) $climit = (int)$_GET['climit'];
if( ! is_int($climit) || ($climit==0) ) $climit = 20;
$show_audit_logs = false;
if( isset($_GET['show_audit']) && ($_GET['show_audit']==1)) $show_audit_logs = true;

$cid = 0;
$section = "";
if( isset($_GET['s']) && isset($_GET['cid'])) {
  $section = $_GET['s'];
  $cid = $_GET['cid'];
  include "carton.admin.".$section.".php";
  return;
  }

$cartons = getCartonsForUser($person_id, $_SESSION['lims']['lab_id'], $climit);
$storage_devices = getStorageDevices();
$container_types = getContainerTypes();
$shelves = getStorageShelvesAll();
$num_cartons = count($cartons);
if($num_cartons==0) {
	print "<div class='alert alert-info'>".$_SESSION['lims']['langdata']['err_no_cartons_in_lab'].".</div>";
	return;
	}
?>
<!--<div class="pull-left"></div>-->
<h4><?php print $_SESSION['lims']['langdata']['cartons_mine']; ?></h4>
<hr>

<div id="showNumCartons" class="btn-large pull-right">
<a href="<?php print $uri; ?>&climit=20"><span class="label <?php if($climit==20) print 'label-info'; ?>">&nbsp;&nbsp;20&nbsp;&nbsp;</span></a>
<a href="<?php print $uri; ?>&climit=100"><span class="label <?php if($climit==100) print 'label-info'; ?>">&nbsp;&nbsp;100&nbsp;&nbsp;</span></a>
<a href="<?php print $uri; ?>&climit=10000" class="disabled"><span class="label <?php if($climit==10000) print 'label-info'; ?>">&nbsp;&nbsp;<?php print $_SESSION['lims']['langdata']['all']; ?>&nbsp;&nbsp;</span></a>
</div>


<table id="myCartonList" class="table table-condensed table-hover tablesorter">
<thead>
	<tr>
	<th><?php print $_SESSION['lims']['langdata']['carton']; ?></th>
	<th><?php print $_SESSION['lims']['langdata']['container_type']; ?></th>
	<th colspan=2><?php print $_SESSION['lims']['langdata']['limsadmin_storage_device']; ?></th>
	<th><?php print $_SESSION['lims']['langdata']['create_date']; ?></th>
	</tr>
</thead>
<tbody>
<?php
foreach( $cartons as $c ) {
  $shelf_number = "-";
  $storage_loc = "-";
  if( isset( $shelves[$c->shelf_id]->shelf_number ) ) $shelf_number = $shelves[$c->shelf_id]->shelf_number;
  if( isset( $storage_devices[$c->storage_id]->identifier ) ) $storage_loc = $storage_devices[$c->storage_id]->identifier;
	// $container_types[$c->container_type_id]->container_type
  print "<tr>".
  "<td><a id='cartonRow-".$c->carton_id."' href='".$uri."&s=edit&cid=".$c->carton_id."'>".myhtmlentities($c->carton_identifier)."</td>".
	"<td>".myhtmlentities($container_types[$c->container_type_id]->container_type)."</td>".
  "<td colspan=2>".myhtmlentities($storage_loc)." <strong>|</strong> <small>".myhtmlentities($shelf_number)."</small></td>".
  "<td>".makeDateString($c->create_date_timestamp)."</td>".
  "</tr>\n";
}
?>
</tbody>
</table>

<script src="core/js/__jquery.tablesorter/jquery.tablesorter.min.js"></script>
<script>
$(function() {
	$("#myCartonList").tablesorter();
//   $("[id^=cartonRow-]").popover();
  
  });
</script>