<?php
if( $_SESSION['lims']['auth_level'] < PERM_MANAGER ) return;

include "functions.lims.php";
// include "functions.user.php";

$labs = getLabs();

?>

<h4><?php print $_SESSION['lims']['langdata']['new_user']; ?></h4>
<hr>

<div id="user_feedback" class="alert alert-info">
<? print $_SESSION['lims']['langdata']['please_complete']; ?>
</div>

<form>

<label><?php print $_SESSION['lims']['langdata']['username']; ?>:</label>
<!-- <div class="input-append"> -->
<input class="input-large" type="text" id="userNewUsername" data-trigger="manual" data-html="true" data-placement="right" data-title="<i class='icon-exclamation-sign'></i> <? print $_SESSION['lims']['langdata']['username']; ?>" data-content="<strong style='color:red;'><? print $_SESSION['lims']['langdata']['err_not_available']; ?></strong>">

<label><?php print $_SESSION['lims']['langdata']['first_name']; ?>:</label>
<input  type="text" id="userFirstName" class="input-large">

<label><?php print $_SESSION['lims']['langdata']['last_name']; ?>:</label>
<input  type="text" id="userLastName" class="input-large">

<label><?php print $_SESSION['lims']['langdata']['email_address']; ?>:</label>
<input  type="text" id="userEmail" class="input-large">

<label><?php print $_SESSION['lims']['langdata']['laboratories']; ?>:</label>
<select multiple="multiple" id="labSelectList" name="labSelectList">
<?php
foreach($labs as $lab) {
  print "<option value=".$lab->id.">".myhtmlentities($lab->name)."</option>";
}
?>
</select>

<p>
<button type="submit" class="btn btn-primary" id="newUserSaveBtn"><? print $button_save; ?></button>
</form>


<script>
$(function() {
  $('#newUserSaveBtn').click(function() {
		$('#newUserSaveBtn').attr('disabled','disabled');
		$('#newUserSaveBtn').html("<? print $button_save; ?> <i class='icon-spinner icon-spin'></i>");
    var nuname = $('#userNewUsername').val();
    var fname = $('#userFirstName').val();
    var lname = $('#userLastName').val();
    var email = $('#userEmail').val();
    var labs=$('#labSelectList').val();

    if(nuname=="") {
			userFeedbackL('alert-error','err_enter_username'); 
			$('#newUserSaveBtn').removeAttr('disabled');
			$('#newUserSaveBtn').html("<? print $button_save; ?>");
			return false;
			}
    if(labs==null) {
			userFeedbackL('alert-error','err_select_lab');
			$('#newUserSaveBtn').removeAttr('disabled');
			$('#newUserSaveBtn').html("<? print $button_save; ?>");
			return false;
			}
		if(email=="") {
			userFeedbackL('alert-error','err_email_invalid'); 
			$('#newUserSaveBtn').removeAttr('disabled');
			$('#newUserSaveBtn').html("<? print $button_save; ?>");
			return false;
			}
			
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if(!emailReg.test(email)) { 
			userFeedbackL('alert-error','err_email_invalid');
			$('#newUserSaveBtn').removeAttr('disabled');
			$('#newUserSaveBtn').html("<? print $button_save; ?>");
			return false;
			}
    
    $('#newUserSaveBtn').attr('disabled','disabled');
    
    $.post('core/ajax_user.php', {m:'new', uname:nuname, fname:fname, lname:lname, email:email, labs:labs}, function(r) {
      var j = jQuery.parseJSON(r);
      if(j.uid!=0) window.location.replace('index.php?p=limsadmin&sp=limsadmin.user_edit&uid='+j.uid);
    });
    return false;
  });

  $('#userNewUsername').focusout( function(){
    var nuname = $('#userNewUsername').val();
    if( nuname == "" ) return false;
    $.getJSON('core/ajax_user.php?m=verify&attr=username&value='+nuname, function(data) {
      var avbl = parseInt(data.available);
      if( avbl != 1 ) {
				$('#userNewUsername').popover('show');
				$('#newUserSaveBtn').attr('disabled','disabled');
      }
      else {
				$('#userNewUsername').popover('hide');
				$('#newUserSaveBtn').removeAttr('disabled');
      }
    });  
  });

  
});
</script>