<?php
session_start();

unset($_SESSION['lims']);
session_destroy();
header("Location: ../index.php");
?>