<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "constants.php";
include "functions.db_connect.php";
include "functions.lims.php";

$l = new Lab();
$l->id = stripslashes( $_POST['lab_id'] );
$l->name = stripslashes( $_POST['lab_name'] );
$l->domain_id = stripslashes( $_POST['lab_domain_id'] );
// error_log("lab_id=".$l->id."   ".$l->name."   ".$l->domain_id);


$r = updateLab($l);
if( $r == 1 ) {
	print json_encode( array("resp"=>1) );
	}
else
	print json_encode( array("resp"=>0) );
?>