<?php

function myhtmlentities($string) {
	return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

function startsWith($haystack, $needle) {
    $length = strlen($needle);
    return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle) {
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

function makeDateString($timestamp) {
  global $site_timezone;
  date_default_timezone_set($site_timezone);
  if($_SESSION['lims']['dateformat']==1) {
      return date("d/m/Y", $timestamp);
      }
  if($_SESSION['lims']['dateformat']==2) {
      return date("m/d/Y", $timestamp);
      }
}

function makeTimeString($timestamp) {
  //return date("H:i:s",$timestamp);
	return date("H:i",$timestamp);
}

function parseAuditEntry($logstr) {
	$a_arr = array();
	preg_match_all('/{([^}]*)}/',$logstr,$matches);
	foreach($matches[1] as $attr_val) {
		$dpos = strpos($attr_val,"=");
		$logattr = substr($attr_val,0,$dpos);
		$logval = substr($attr_val,$dpos+1);
		$a_arr[$logattr] = $logval;
	}
	return $a_arr;
}
?>