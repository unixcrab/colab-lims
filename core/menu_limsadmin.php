<?php
$admin_attr = "";
if( isset($_GET['attr']) ) $admin_attr = $_GET['attr'];
// print $page." ".$subpage." ".$admin_attr;
?>

<div id="left_menu_admin" data-spy="affix" data-offset-top="20">
<ul class="nav nav-list">


<?php if($_SESSION['lims']['auth_level']>=PERM_MANAGER) { ?>
  <li class="nav-header"><i class="icon-user"></i> <? print $_SESSION['lims']['langdata']['limsadmin_personnel']; ?></li>
	<li <?php if( $subpage == "limsadmin.users_list" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=limsadmin.users_list" id="limsadmin_users"><i class="icon-angle-right"></i> <? print $_SESSION['lims']['langdata']['list']; ?></a>
  </li>
  <li <?php if( $subpage == "limsadmin.user_add" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=limsadmin.user_add" id="limsadmin_user_add"><i class="icon-angle-right"></i> <? print $_SESSION['lims']['langdata']['add']; ?></a>
  </li>
  <li <?php if( $admin_attr == "skills" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=limsadmin.edit&attr=skills" id="limsadmin_skills"><i class="icon-angle-right"></i> <? print $_SESSION['lims']['langdata']['limsadmin_skills']; ?></a>
  </li>
<?php } ?>

<li class="divider"></li>
  
<?php if( $_SESSION['lims']['auth_level']>=PERM_MANAGER ) { ?>
  <li class="nav-header">
    <i class="icon-list-ol"></i> <? print $_SESSION['lims']['langdata']['limsadmin_inventory']; ?>
  </li>
  <li <?php if($subpage == "limsadmin.container_types") print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=limsadmin.container_types" id="limsadmin_container_types"><i class="icon-angle-right"></i> <? print $_SESSION['lims']['langdata']['limsadmin_container_types']; ?></a>
  </li>
  <li <?php if($admin_attr == "sample_types") print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=limsadmin.edit&attr=sample_types" id="limsadmin_sample_types"><i class="icon-angle-right"></i> <? print $_SESSION['lims']['langdata']['limsadmin_sample_types']; ?></a>
  </li>
	<!--
  <li <?php if( $subpage == "limsadmin.units" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=limsadmin.units" id="limsadmin_units"><i class="icon-angle-right"></i> <? print $_SESSION['lims']['langdata']['units']; ?></a>
  </li>
	-->
<?php } ?>

<li class="divider"></li>

<?php if($_SESSION['lims']['auth_level']>=PERM_ADMIN) { ?>
  <li class="nav-header">
    <i class="icon-beaker"></i> <? print $_SESSION['lims']['langdata']['limsadmin_labs']; ?>
  </li>
  <li <?php if( $subpage == "limsadmin.labs" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=limsadmin.labs" id="limsadmin_labs"><i class="icon-angle-right"></i> <? print $_SESSION['lims']['langdata']['limsadmin_labs']; ?></a>
  </li>
  <li <?php if($admin_attr == "groups") print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=limsadmin.edit&attr=groups" id="limsadmin_domains"><i class="icon-angle-right"></i> <? print $_SESSION['lims']['langdata']['limsadmin_domains']; ?></a>
  </li>
<?php } ?>
  
<li class="divider"></li>

<?php if($_SESSION['lims']['auth_level']>=PERM_MANAGER) { ?>
  <li class="nav-header">
   <i class="icon-home"></i> <? print $_SESSION['lims']['langdata']['limsadmin_infrastructure']; ?>
  </li>
  <li <?php if( $subpage == "limsadmin.storage_devices" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=limsadmin.storage_devices" id="limsadmin_storage_devices"><i class="icon-angle-right"></i> <? print $_SESSION['lims']['langdata']['limsadmin_storage_devices']; ?></a>
  </li>
<?php } ?>
  
<li class="divider"></li>
  
<?php if( $_SESSION['lims']['auth_level']>=PERM_ADMIN ) { ?>
  <li class="nav-header">
    <i class="icon-cog"></i> <? print $_SESSION['lims']['langdata']['limsadmin_modules']; ?>
  </li>
  <li <?php if( $subpage == "limsadmin.modules" ) print "class='active'";?>>
    <a href="index.php?p=<? print $_SESSION['lims']['left_menu']; ?>&sp=limsadmin.modules" id="limsadmin_modules"><i class="icon-angle-right"></i> <? print $_SESSION['lims']['langdata']['configure']; ?></a>
  </li>
<?php } ?>

</ul>
</div>

<script>
$(function() {
	$('#left_menu_admin').affix();
  
  });
</script>
