<?php
//if( !isset($_SESSION['lims']) ) session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include_once "functions.user.php";
include_once "functions.lims.php";

//$ud = getUserDateFormat($_SESSION['lims']['person_id']);
$uprefs = getUserPrefs($_SESSION['lims']['person_id']);
$labs_acc = getUserLabAccess($_SESSION['lims']['person_id']);
// $default_lab = getUserLab($_SESSION['lims']['person_id']);
$default_lab = 0;

$dateformats = array(
  1=>$_SESSION['lims']['langdata']['account_dateformat_dmy'],
  2=>$_SESSION['lims']['langdata']['account_dateformat_mdy']
  );
?>

<!--  MUST UPDATE SESSION WITH NEW DETAILS! -->

<div id="user_feedback" class="alert alert-info">
<? print $_SESSION['lims']['langdata']['please_provide']; ?>
</div>

<form name="change_details" action="" >

	<label class="control-label" for="lims_user_lab_sel" id="lims_user_lab_label"><strong><? print $_SESSION['lims']['langdata']['laboratory']; ?></strong>:</label>
		<select id="lims_user_lab_sel">
		<?php
		foreach($labs_acc as $la) {
			$thislab = getLab($la->lab_id);
			if( $thislab == null ) continue;
			$selStr = "";
			$defStr = "";
			if( $la->lab_id == $_SESSION['lims']['lab_id'] ) $selStr = "selected";
			if( $la->default_flag == 'Y' ) $defStr = "(".strtolower($_SESSION['lims']['langdata']['default']).")";
			print "<option value=".$la->lab_id." ".$selStr.">".myhtmlentities($thislab->name)." ".$defStr."</option>\n";
			}
		?>
		</select>
		&nbsp;&nbsp;<label class="checkbox "><input type="checkbox" id="lims_user_lab_save"> <? print $_SESSION['lims']['langdata']['default']; ?></label>

<br/>

<label class="control-label" for="lims_user_date_format" id="lims_user_date_format_label"><strong><? print $_SESSION['lims']['langdata']['account_dateformat']; ?></strong>:</label>
		<select id="lims_user_date_format_sel">
			<?php
			foreach($dateformats as $k=>$v) {
				$selStr = "";
				//if( $k == $ud ) $selStr = "selected";
				if( $k == $uprefs->date_format_id ) $selStr = "selected";
				print "<option value=".$k." ".$selStr.">".$v."</option>\n";
				}
			?>
		</select>

<br/>
<br/>

<label for="lims_user_unit_system" id="lims_user_unit_system_label"><strong><? print $_SESSION['lims']['langdata']['unit_system']; ?></strong>:</label>
<select id="lims_user_unit_system">
	<option value="<?php print UNIT_SYS_SI;?>" <?php if($_SESSION['lims']['unit_system']==1) print "selected"; ?>>SI</option>
	<option value="<?php print UNIT_SYS_NON_SI;?>" <?php if($_SESSION['lims']['unit_system']==20) print "selected"; ?>>US</option>
</select>
  
  <br/>
  <button type="submit" class="btn btn-primary" id="lims_users_prefs_submit"><? print $button_save; ?></button>


</form>