<?php
include "functions.lims.php";
//include "functions.user.php";
$sdevices = getStorageDevicesForLab($_SESSION['lims']['lab_id']);
$lab_users = getAllUsersInLab($_SESSION['lims']['lab_id']);

?>
<h4><?php print $_SESSION['lims']['langdata']['cartons_search']; ?></h4>
<hr>

<form class="form-horizontal" id="cartonSearchForm">
  <div class="control-group">
    <label class="control-label" for="searchName"><?php print $_SESSION['lims']['langdata']['name']; ?></label>
    <div class="controls">
      <input type="text" name="searchName" id="searchName" data-provide="typeahead">
			<!--<span class="help-block"><strong>*</strong> = wildcard.</span>-->
    </div>
		
  </div>
	
  <div class="control-group">
    <label class="control-label" for="searchBC"><?php print $_SESSION['lims']['langdata']['barcode']; ?></label>
    <div class="controls">
      <input type="text" id="searchBC" placeholder="<?php print $_SESSION['lims']['langdata']['scan_barcode']; ?>">
    </div>
  </div>
	
  <div class="control-group">
    <label class="control-label" for="searchSDevice"><?php print $_SESSION['lims']['langdata']['limsadmin_storage_device']; ?></label>
    <div class="controls">
      <select id="searchSDevice">
			<option value=0></option>
			<?php
			foreach($sdevices as $sd_id=>$sd) {
				print "<option value=".$sd_id.">".myhtmlentities($sd->identifier)."</option>\n";
				}
			?>
			</select>
    </div>
  </div>
	
	
	<div class="control-group">
    <label class="control-label" for="searchOwner"><?php print $_SESSION['lims']['langdata']['owner']; ?></label>
    <div class="controls">
      <select id="searchOwner">
			<option value=0></option>
			<?php 
			foreach($lab_users as $luid=>$luser) {
				if(($luser->first_name=="")&&($luser->last_name=="")) continue;
				print "<option value=".$luid.">".myhtmlentities($luser->first_name." ".$luser->last_name)."</option>\n";
			}
			
			?>
			</select>
    </div>
  </div>
	
	
  <div class="control-group">
    <div class="controls">
      <button type="submit" class="btn btn-primary" id="searchSubmit"><?php print $_SESSION['lims']['langdata']['search']; ?></button>
			<button class="btn" id="searchReset"><?php print $_SESSION['lims']['langdata']['reset']; ?></button>
    </div>
  </div>
</form>

<div id="searchOutputContainer">
	<div class="alert alert-info" id="searchOutputStats" style="display:none;"></div>
	<div class="" id="searchOutput">
	<!--<div class="" id="searchOutput" style="height: 400px; overflow: auto;">-->
		<table id='resTable'></table>
	</div>
</div>

<script src="core/js/__jquery.tablesorter/jquery.tablesorter.min.js"></script>

<script>

	$(function() {
		$('#searchReset').click( function() {
			resetForm( $('#cartonSearchForm') ); 
			return false;
		});
		
		$('#searchSubmit').click( function() {
			$('#searchOutputStats').slideUp(200);
			$('#searchOutput').html("");
			
			var name = $('#searchName').val();
			var bc = $('#searchBC').val();
			var sdevice = $('#searchSDevice :selected').val();
			var owner = $('#searchOwner :selected').val();
			if( (name=="")&&(bc=="")&&(sdevice==0)&&(owner==0) ) return false;
			
			$('#searchSubmit').attr('disabled','disabled');
			$('#searchSubmit').html("<?php print $_SESSION['lims']['langdata']['search']; ?>  <i class='icon-spinner icon-spin'></i>");
			$.getJSON('core/ajax_search.php?obj=carton&m=adv&name='+name+'&bc='+bc+'&sdevice='+sdevice+'&owner='+owner, function(data) {
				var outStr = "<table id='resTable' class='table table-hover table-condensed tablesorter'>";
				outStr += "<thead><tr><th><?php print $_SESSION['lims']['langdata']['carton']; ?></th><th><?php print $_SESSION['lims']['langdata']['barcode']; ?></th><th><?php print $_SESSION['lims']['langdata']['limsadmin_storage_device']; ?></th><th><?php print $_SESSION['lims']['langdata']['owner']; ?></th></tr></thead><tbody>";
				$.each(data.options, function(key, carton) {
					outStr += "<tr id='crow-"+key+"'>";
					$.each(carton, function(ckey, cval) {
						outStr += "<td>"+cval + "</td>";
						});
					outStr += "</tr>";
				});
				//$('#searchOutput').append(outStr+"</table>");
				outStr += "</tbody></table>";
				
				$('#searchOutputStats').slideDown(200, function(){
						if( data.numresults > 0 ) $('#searchOutput').html(outStr);
						//else $('#searchOutput').html("");
						$('#searchOutputStats').html("<strong><?php print $_SESSION['lims']['langdata']['search_results']; ?></strong>: <span class='badge badge-success'> "+data.numresults+' </span>');
						$('#searchSubmit').html("<?php print $_SESSION['lims']['langdata']['search']; ?>");
						$('#searchSubmit').removeAttr('disabled');
						$("#resTable").tablesorter();
				});
			});
			return false;
		});
		
		//$('#resTable').css('cursor', 'pointer');
		$(document).on("mouseover", "[id^=crow-]", function(){
				$(this).css('cursor', 'pointer');
			});
		
		$(document).on("click", "[id^=crow-]", function(){
			//
			var cid = this.id.split("-")[1];
			document.location.href = "index.php?p=inv&sp=cartons.mine&s=edit&cid="+cid;
			});
		
	});
      

      
</script>