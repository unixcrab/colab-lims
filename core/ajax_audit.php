<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "functions.db_connect.php";
include "functions.lims.php";
include "functions.user.php";
include "functions.misc.php";

if( ! isset($_GET['m']) ) return;
$mode = $_GET['m'];

//$units_all = getUnitsAll($_SESSION['lims']['unit_system']);
$units_all = getUnitsAllSystems();
$container_types = getContainerTypes();
$sample_types = getSampleTypes();

if( $mode == "get" ) {
	//sleep(1);
	if( ! isset($_GET['type']) ) return;
	if( ! isset($_GET['affected_id']) ) return;
	if( ! isset($_GET['sort']) ) return;
	$type = $_GET['type'];
	$affected_id = $_GET['affected_id'];
	$sort = $_GET['sort'];
	
	$storage_locs = getStorageDevicesForLab($_SESSION['lims']['lab_id']);
	$shelves_all = getStorageShelvesAll();
	
	$auditlogs = getAuditLog($type,$affected_id,$sort,1000);
	//$prev_audit = (array)(new Audit());
	$prev_audit = array("primer"=>NULL,"investigator_id"=>NULL);
	foreach($auditlogs as $alog) {
		$audit_changes_str = "";
		$audit_details_str = "";
		$alog->timestamp = makeDateString($alog->timestamp)." ".makeTimeString($alog->timestamp);
		$this_audit = (array)json_decode($alog->audit_descr);
		foreach($this_audit as $k=>$v) {
			if($k=="sample_id") continue;
			if($k=="carton_id") continue;
			if($k=="create_date_timestamp") continue;
			//if( isset($prev_audit[$k]) ) 
			//error_log($k." ".$prev_audit[$k]." -> ".$this_audit[$k]);
			
			if( (!isset($prev_audit[$k])) || (isset($prev_audit[$k]) && ($prev_audit[$k]!=$this_audit[$k]) ) ) {
				$audit_details_str = $k;
				
				// carton attributes
				if($k=="storage_id") { 
					$audit_details_str = $_SESSION['lims']['langdata']['limsadmin_storage_device']; 
					if( isset($storage_locs[$v]) ) $v = myhtmlentities($storage_locs[$v]->identifier); 
					}
				if($k=="shelf_id") { 
					$audit_details_str = $_SESSION['lims']['langdata']['shelf']; 
					if(isset($shelves_all[$v])) $v = $shelves_all[$v]->shelf_number;
					else continue;
					}
				if($k=="owner_id") { 
					$audit_details_str = $_SESSION['lims']['langdata']['owner']; 
					$v = myhtmlentities(getUserDetails($v)->username); 
					}
				if($k=="carton_identifier") { 
					$audit_details_str = $_SESSION['lims']['langdata']['name'];
					$v = myhtmlentities($v);
					}
				if($k=="internal_barcode") { 
					$audit_details_str = $_SESSION['lims']['langdata']['barcode'];
					$v = myhtmlentities($v);
					}
				if($k=="container_type_id") {
					$audit_details_str = $_SESSION['lims']['langdata']['container_type'];
					if( isset($container_types[$v]) ) $v = myhtmlentities($container_types[$v]->container_type);
					else continue;
					}
				if($k=="allow_details_edit") {
					$audit_details_str = $_SESSION['lims']['langdata']['allow_nonowner_edit'];
					if($v==1) $v = $_SESSION['lims']['langdata']['yes'];
					else $v = $_SESSION['lims']['langdata']['no'];
					}
				if($k=="allow_contents_edit") {
					$audit_details_str = $_SESSION['lims']['langdata']['allow_nonowner_well_edit'];
					if($v==1) $v = $_SESSION['lims']['langdata']['yes'];
					else $v = $_SESSION['lims']['langdata']['no'];
					}
					
				// sample attributes
				if($k=="identification") { $audit_details_str = $_SESSION['lims']['langdata']['name']; }
				if($k=="comments") { $audit_details_str = $_SESSION['lims']['langdata']['comment']; }
				if($k=="control_flag") {
					$audit_details_str = $_SESSION['lims']['langdata']['control']; 
					if($v=='Y') $v = $_SESSION['lims']['langdata']['yes'];
					else $v = $_SESSION['lims']['langdata']['no'];
					}
				if($k=="exhausted_flag") {
					$audit_details_str = $_SESSION['lims']['langdata']['depleted']; 
					if($v=='Y') $v = $_SESSION['lims']['langdata']['yes'];
					else $v = $_SESSION['lims']['langdata']['no'];
					}
				if($k=="sample_type_id") {
					$audit_details_str = $_SESSION['lims']['langdata']['type'];
					if( isset($sample_types[$v]) ) $v = $sample_types[$v]->sample_type;
					else continue;
					}
				if($k=="concentration") {
					$audit_details_str = $_SESSION['lims']['langdata']['concentration'];
					}
				if($k=="concentration_units_id") {
					$audit_details_str = $_SESSION['lims']['langdata']['units']." (".$_SESSION['lims']['langdata']['concentration'].")";
					if( isset($units_all[$v]) ) $v = $units_all[$v]->units;
					else continue;
					}
				if($k=="volume") {
					$audit_details_str = $_SESSION['lims']['langdata']['volume'];
					}
				if($k=="volume_units_id") {
					$audit_details_str = $_SESSION['lims']['langdata']['units']." (".$_SESSION['lims']['langdata']['volume'].")";
					if( isset($units_all[$v]) ) $v = $units_all[$v]->units;
					else continue;
					}
				$audit_changes_str .= "<small><b> ".$audit_details_str.":</b> <span style='color:#c22;';>".$v."</span></small><br/>";
				}
			}

		
		$action_str = "";
		if( ($alog->audit_key==AUDIT_ACTION_CARTON_CREATED)||($alog->audit_key==AUDIT_ACTION_SAMPLE_CREATED) ) {
			$action_str = $_SESSION['lims']['langdata']['audit_object_created'];
			}
		if( ($alog->audit_key==AUDIT_ACTION_CARTON_MOD)||($alog->audit_key==AUDIT_ACTION_SAMPLE_MOD))
			$action_str = $_SESSION['lims']['langdata']['audit_object_modified'];
		
		$auser = getUserDetails($alog->person_id);
		$alog->action_str = $action_str;
		$alog->username = myhtmlentities($auser->username)."<br/>(".myhtmlentities($auser->first_name." ".$auser->last_name).")";
		$alog->audit_changes_str = $audit_changes_str;		
		
		$prev_audit = $this_audit;
	}
	
	print json_encode($auditlogs);
}
?>