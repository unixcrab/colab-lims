<?php
$bcStr = "000000";
if( isset($_GET['b'] ) ) $bcStr = $_GET['b'];

require_once('barcode/barcodegen.1d-php5.v2.2.0/class/BCGFont.php');
require_once('barcode/barcodegen.1d-php5.v2.2.0/class/BCGColor.php');
require_once('barcode/barcodegen.1d-php5.v2.2.0/class/BCGDrawing.php');
require("barcode/barcodegen.1d-php5.v2.2.0/class/BCGcode39extended.barcode.php");


$color_black = new BCGColor(0, 0, 0);
$color_white = new BCGColor(255, 255, 255);

$font = new BCGFont('./barcode/barcodegen.1d-php5.v2.2.0/class/font/Arial.ttf', 10);

$code = new BCGcode39extended(); // Or another class name from the manual
$code->setScale(1); // Resolution
$code->setThickness(40); // Thickness
$code->setForegroundColor($color_black); // Color of bars
$code->setBackgroundColor($color_white); // Color of spaces
$code->setFont($font); // Font (or 0)
$code->parse($bcStr); // Text

$drawing = new BCGDrawing('', $color_white);
$drawing->setBarcode($code);
$drawing->draw();

header('Content-Type: image/png');

$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
?>