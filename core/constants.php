<?php
// database tables
global $tablePrefix;
define("DBTBL_audit",$tablePrefix."audit");
define("DBTBL_person",$tablePrefix."person");
define("DBTBL_person_skills",$tablePrefix."person_skills");
define("DBTBL_person_prefs",$tablePrefix."person_prefs");
define("DBTBL_auth_levels",$tablePrefix."auth_levels");
define("DBTBL_laboratory_personnel",$tablePrefix."laboratory_personnel");
define("DBTBL_supplies",$tablePrefix."purchased_supplies");
define("DBTBL_domains",$tablePrefix."domains");
define("DBTBL_labs",$tablePrefix."laboratory");
define("DBTBL_lab_inv",$tablePrefix."laboratory_inventory");
define("DBTBL_skills",$tablePrefix."skill");
define("DBTBL_storage_type",$tablePrefix."storage_type");
define("DBTBL_storage_device",$tablePrefix."storage_device");
define("DBTBL_storage_shelf",$tablePrefix."storage_shelf");
define("DBTBL_modules",$tablePrefix."modules");
define("DBTBL_modules_menus",$tablePrefix."modules_menus");
define("DBTBL_projects",$tablePrefix."projects");
define("DBTBL_projects_members",$tablePrefix."projects_members");
define("DBTBL_projects_files",$tablePrefix."projects_files");
define("DBTBL_container_types",$tablePrefix."container_type");
define("DBTBL_sample",$tablePrefix."sample");
define("DBTBL_sample_type",$tablePrefix."sample_type");
define("DBTBL_units",$tablePrefix."units");
define("DBTBL_carton",$tablePrefix."carton");
define("DBTBL_carton_sample",$tablePrefix."carton_sample");
define("DBTBL_alerts",$tablePrefix."alerts");
define("DBTBL_instruments",$tablePrefix."instruments");

// modules
define("MODULE_COMPONENT_APP_CONFIG",1);
define("MODULE_COMPONENT_USER_CONFIG",2);

// left menus
define("MENU_INVENTORY",100);
define("MENU_BIOINF_PROJECTS",200);
define("MENU_ADMIN",300);
define("MENU_USER_PROFILE",400);

// menu sections
define('MENU_SECTION_CARTONS',100);
define('MENU_SECTION_SAMPLES',200);
define('MENU_SECTION_LAB',300);

// permission levels in ascending order
define("PERM_CUSTOMER",10);
define("PERM_TECHNICIAN",20);
define("PERM_SCIENTIST",30);
define("PERM_MANAGER",40);
define("PERM_ADMIN",100);

// Audit constants
define("AUDIT_TYPE_CARTON",10);
define("AUDIT_TYPE_SAMPLE",20);
define("AUDIT_TYPE_CARTON_SAMPLE",21);
define("AUDIT_TYPE_USER",30);

define("AUDIT_ACTION_CARTON_CREATED",100);
define("AUDIT_ACTION_CARTON_MOD",101);
define("AUDIT_ACTION_SAMPLE_CREATED",200);
define("AUDIT_ACTION_SAMPLE_MOD",201);
define("AUDIT_ACTION_CARTON_SAMPLE_MOD",202);
define("AUDIT_ACTION_USER_CREATED",300);
define("AUDIT_ACTION_USER_MOD",301);

// audit DB line formats
define("CARTON_MOD_FORMAT","carton_identifier='%s',create_date_timestamp=%d,storage_id=%d,shelf_id=%d,internal_barcode='%s',owner_id=%d,allow_details_edit=%d,allow_contents_edit=%d");

define("AUDIT_CARTON_SAMPLE_MOD_FORMAT","sample_id=%d,row_num=%d,column_num=%d,carton_id=%d,active='%s'");

// Units
define("UNIT_SYS_SI",1);
define("UNIT_SYS_NON_SI",20);

define("UNIT_TYPE_MASS",10);
define("UNIT_TYPE_WEIGHT",20);
define("UNIT_TYPE_CONC",30); // concentration
define("UNIT_TYPE_TEMP",40);
define("UNIT_TYPE_VOLUME",50);
define("UNIT_TYPE_PRESSURE",60);
define("UNIT_TYPE_LENGTH",70);

define("FORMAT_DATE_DMY",1);
define("FORMAT_DATE_MDY",2);
?>