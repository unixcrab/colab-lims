<?php
$site_default_lang = "gb";
$site_timezone = "Europe/Berlin"; // http://no1.php.net/manual/en/timezones.php
$site_root="/lims";
$serv_root="/var/www/lims/";
$start_page=$site_root."/index.php?p=inv&sp=cartons.mine";

// database config
$dbUser = "lims";
$dbPass = "lims";
$dbDB = "lims";
$dbHost = "localhost";
$tablePrefix = "colab_";

// $auth_types = array("local"); // First try AD with LDAP using the adLDAP library (http://adldap.sourceforge.net) then local DB if that fails
$auth_types = array("adldap", "local"); // First try AD with LDAP using the adLDAP library (http://adldap.sourceforge.net) then local DB if that fails
$ad_domain = "";
$ad_domain_controllers = array();
$ad_base_dn = "";

//limits
$max_labs = 20;
$max_domains = 20;
$max_mysql_results = 1000;

$modules_dir = "modules/";

$projects_dir = $serv_root."projects/";
$projects_max_listsize = 1000;
$projects_max_files = 500;

$site_default_dateformat = 1; // date formats: 1=dd/mm/yy, 2=mm/dd/yy
$site_default_timeformat = 1; // time formats: 1=24hr, 2=12hr
$site_default_unit_sys = 1;

$day_len = 86400; // seconds
?>
