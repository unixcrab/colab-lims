<?php
$person_id = null;
if( $user_logged_in ) {
  $person_id = $_SESSION['lims']['person_id'];
	include "constants.php";
  include "functions.user.php";
  include "functions.projects.php";
  include "functions.misc.php";
	include "common.variables.php";
	
  }
// print "person_id=".$person_id;
// var_dump( get_included_files() );
?>
<!--
<a href="<?php print $site_root; ?>/">Home</a>
-->


<div id="navbar-main" class="navbar navbar-fixed-top" >
	<div class="navbar-inner">
		<div class="container-fluid">
	      
	  <!--
 		<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> 
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
 		</a> 
	  -->
		
	  <!--   <a class="brand" href="index.php"><img src="<?php print $site_root; ?>/img/colab.svg"></a> -->
    <!--<a class="brand" href="index.php"><img src="<?php print $site_root; ?>/img/logo_colab_112x20.png"></a>-->
		<a class="brand" href="index.php"><img src="<?php print $site_root; ?>/img/colab_inkscape_115-1.png"></a>

		  <?php
		  if( $user_logged_in ) {
		      $ua = getUserAlerts($person_id);
					$inv_active = "";
					$bionfprojs_active = "";
					$limsadmin_active = "";
					if( $page == "inv" ) $inv_active = "class='active'";
					elseif ( $page == "bionfprojs" ) $bionfprojs_active = "class='active'";
					elseif ( $page == "limsadmin" ) $limsadmin_active = "class='active'";
		  ?>
              

				<ul class="nav nav-pills">
				
				  <li <?php print $inv_active; ?>>
						<a href="<?php print $site_root; ?>/index.php?p=inv&sp=cartons.mine"><i class="icon-list-ol"></i> <?php print $_SESSION['lims']['langdata']['topmenu_inventory'];?> </a>
					</li>
					
				  <!-- <li>
						<a href="<?php print $site_root; ?>/index.php?p=proj"><?php print $_SESSION['lims']['langdata']['topmenu_projects'];?>  </a>
					</li> -->
					
				  <li <?php print $bionfprojs_active; ?>>
						<a href="<?php print $site_root; ?>/index.php?p=bionfprojs&sp=projects.running"><i class="icon-time"></i> <?php print $_SESSION['lims']['langdata']['topmenu_bioinformatics_projects'];?>  </a>
					</li>
					
					<?php if( $_SESSION['lims']['auth_level'] >= PERM_MANAGER ) { ?>
						<li <?php print $limsadmin_active; ?>>
							<a href="<?php print $site_root; ?>/index.php?p=limsadmin"><i class="icon-cogs"></i> <?php print $_SESSION['lims']['langdata']['topmenu_admin'];?></a>
						</li>
					<?php } ?>
					
				</ul>

			  
			  
			  
		      <div class="btn-group pull-right">
						<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" id="user_details_menu_dropdown_top">
			      <i class="icon-user icon-white"></i> <?php print myhtmlentities($_SESSION['lims']['first_name'])." ".myhtmlentities($_SESSION['lims']['last_name']); ?> 
			      <?php
                  if( $_SESSION['lims']['login_method'] == "adldap" ) print " (".$_SESSION['lims']['username']."\\".$ad_domain.") ";
//                                 print " ".myhtmlentities($_SESSION['lims']['lab_name']);
                  ?>
                              <span class="badge badge-info" id="user_alert_tag"></span> <i class="caret"></i>
			    </a>
                            
                            <ul class="dropdown-menu no-collapse" id="user_dropdown"> 
                                
                                
                                <li><a href="index.php?p=accadmin&sp=account.details"><i class="icon-wrench"></i>  <? print $_SESSION['lims']['langdata']['account_admin']; ?> </a></li>
                                <li class="divider"></li>
                                <li><a href="core/logout.php"><i class="icon-off"></i>  <? print $_SESSION['lims']['langdata']['logout']; ?> </a></li>
                                
                                <?php
                                if( count($ua) >= 1 ) {
                                ?>
                                <li class="divider"></li>
                                <li><a href="#" class="clearitem"><i class="icon-flag"></i> <?php print $_SESSION['lims']['langdata']['events']; ?></a></li>
                                <?php
                                }
                                ?>
<!--                                 <li id="alert2"> alert 2</li> -->
                            </ul>
		      </div>
                
<!--		      <form class="navbar-search pull-left" action="index.php?p=search&sp=search.results" method="POST">
			  <input type="text" id="searchTop" data-provide="typeahead"  class="search-query" placeholder="<?php print $_SESSION['lims']['langdata']['search']; ?>" name="searchTop"> 
		      </form>-->
			
		      <form class="navbar-search" action="index.php?p=search&sp=search.results" method="POST">
			<div class='input-append'>
			  <input type="text" name="searchTop" autocomplete="off" id="searchTop" data-provide="typeahead"  placeholder="<?php print $_SESSION['lims']['langdata']['search']; ?>">
			  <button class='btn'><i class="icon-search"></i></button>
			</div>
		      </form>
				
		    <?php
		    }
		    ?>



		</div>
		
	</div>
<!-- 	<span class="icon-bar"></span> -->
</div>

<?php
if( $user_logged_in ) { ?>
      
  <!-- Modal -->
  <div id="alertsModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="alertsModalLabel" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3 id="alertsModalLabel"><?php print $_SESSION['lims']['langdata']['events']; ?></h3>
    </div>
    <div class="modal-body">
<!--       <p>One fine body…</p> -->

      <table class="table table-condensed table-hover">
      
	<thead>
	<tr>
	<th></th>
	<th><? print $_SESSION['lims']['langdata']['date']; ?></th>
	<th><? print $_SESSION['lims']['langdata']['event']; ?></th>
	<th></th>
	</tr>
	</thead>
	
      <tbody>
	<?php
        foreach($ua as $a) {
					if( $a->lang_index == "alert_project_invite" ) {
						$p = getProject($person_id, $a->ref_id);
						print "<tr id='arow-".$a->alert_id."'><td><i class='icon-plus'></i></td><td><small>".makeDateString($a->timestamp)." ".makeTimeString($a->timestamp)."</small></td><td>".$_SESSION['lims']['langdata'][$a->lang_index].": <a href='index.php?p=bionfprojs&sp=projects.open&pid=".$p->project_id."'><i><b>".myhtmlentities($p->name)."</b></i></a></td><td><i id='alertAck-".$a->alert_id."' class='icon-check'></i></td></tr>\n";
						}
          }
// 	  }
	?>
      </tbody>
      </table>


    </div>
    <div class="modal-footer">
      <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><?php print $button_close; ?></button>
<!--       <button class="btn btn-primary">Save changes</button> -->
    </div>
  </div>
      

      <script>
	$(function() {

	  $('#searchTop').typeahead({ minLength: 3,
	    source: function (query, process) {
	      return $.get('core/ajax_search.php', { q: query, m:'s' }, function (data) {
		return process(data.options);
		});
	    }
	  });
      
	$('#user_dropdown a.clearitem').click(function(){
	  
	  $('#alertsModal').modal();
	  return false;
	});
	
	ajaxGetNumUserAlerts( function(alerts){
	  if( alerts < 1 ) {
	    $("#user_alert_tag").html("");
	    $("#user_alert_tag").attr("class","badge");
	    return;
	    }
	  $("#user_alert_tag").attr("class","badge badge-important");
	  $("#user_alert_tag").html(alerts);
	});
	
	
        $("[id^=alertAck-]").click( function(){
	  var aid=this.id.split("-")[1]; 
	  ajaxAckAlert(aid);
	  $("#arow-"+aid).attr("class","success");

	  ajaxGetNumUserAlerts( function(alerts){
	    if( alerts < 1 ) {
	      $("#user_alert_tag").html("0");
	      $("#user_alert_tag").attr("class","badge");
	      $('#user_dropdown a.clearitem').parent().remove();
	      return;
	      }
	    $("#user_alert_tag").attr("class","badge badge-important");
	    $("#user_alert_tag").html(alerts);
	    
	  });
	  
	  
	});
	
	
	
      });
      

      
      </script>

<?php
  }
?>
