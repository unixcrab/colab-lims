<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "constants.php";
include "functions.db_connect.php";
include "functions.misc.php";

if( ! isset($_GET['m']) ) return;
$mode = $_GET['m'];
$obj = ""; // search objective
if( isset($_GET['obj']) ) $obj = $_GET['obj'];

if( ($obj == "sample" ) && ($mode == "adv") ) {
	$s_name = strtr($_GET['name'],"*","%");
	$s_type_id = $_GET['stype'];
	include "functions.lims.php";
	//include "functions.user.php";
	$sample_types = getSampleTypes();
	
	$s_name_sql = "false";
	if($s_name != "") $s_name_sql = "(identification like '".$s_name."')";
	$s_type_sql = "false";
	if($s_type_id != 0 ) $s_type_sql = "(sample_type_id=".$s_type_id.")";
	$sql = "select * from ".DBTBL_sample." where sample_id in (select sample_id from ".DBTBL_carton_sample." where carton_id in (select carton_id from ".DBTBL_carton." where storage_id in (select storage_id from ".DBTBL_storage_device." where laboratory_id=".$_SESSION['lims']['lab_id']."))) and (".$s_name_sql." or ".$s_type_sql.") order by sample_id DESC";
	//error_log($sql);
	$r = dbq($sql);
	$list = array();
	while( $s = $r->fetch_object() ) {
		$s_type_name = "-";
		if( isset( $sample_types[$s->sample_type_id] ) ) $s_type_name = myhtmlentities($sample_types[$s->sample_type_id]->sample_type);
		$carton_sample = getCartonSampleFromSID($s->sample_id);
		$carton = getCarton($carton_sample->carton_id,$_SESSION['lims']['lab_id'] );
		$list[$s->sample_id] = array( myhtmlentities($s->identification), $s_type_name, $carton_sample->carton_id, myhtmlentities($carton->carton_identifier), $carton_sample->row_num, $carton_sample->column_num );
	}
	$options = array();
	$options["numresults"] = count($list);
	$options["options"] = $list;
	//sleep (1);
	header('Content-type: application/json');
	print json_encode( $options );
}

if( ($obj == "carton" ) && ($mode == "adv") ) {
	$c_ident = strtr($_GET['name'],"*","%");
	$c_bc = strtr($_GET['bc'],"*","%");
	$c_sdevice_id = $_GET['sdevice'];
	$c_owner_id = $_GET['owner'];
	
	include "functions.lims.php";
	include "functions.user.php";
	$lab_sdevices = getStorageDevicesForLab($_SESSION['lims']['lab_id']);
	$lab_users = getAllUsersInLab($_SESSION['lims']['lab_id']);
	
	$c_ident_sql = "false";
	if( $c_ident != "" ) $c_ident_sql = "(carton_identifier like '".$c_ident."')";
	$c_bc_sql = "false";
	if( $c_bc != "" ) $c_bc_sql = "(internal_barcode like '".$c_bc."')";
	$c_sdevice_id_sql = "false";
	if( $c_sdevice_id != 0 ) $c_sdevice_id_sql = "(storage_id=".$c_sdevice_id.")";
	$c_owner_id_sql = "false";
	if( $c_owner_id != 0 ) $c_owner_id_sql = "(owner_id=".$c_owner_id.")";
	
	//$sql = "select * from ".$dbtbl_carton." where laboratory_id=".$_SESSION['lims']['lab_id']." and (".$c_ident_sql." or ".$c_bc_sql." or ".$c_sdevice_id_sql." or ".$c_owner_id_sql.")";
	$sql = "select * from ".DBTBL_carton." where storage_id in (select storage_id from ".DBTBL_storage_device." where laboratory_id = ".$_SESSION['lims']['lab_id']." ) and (".$c_ident_sql." or ".$c_bc_sql." or ".$c_sdevice_id_sql." or ".$c_owner_id_sql.")";
	//error_log($sql);
	$r = dbq($sql);
	$list = array();
	while( $s = $r->fetch_object() ) {
		$storage_name = "-";
		if( isset( $lab_sdevices[$s->storage_id] ) ) $storage_name = myhtmlentities($lab_sdevices[$s->storage_id]->identifier);
		$owner_name = "-";
		if( isset( $lab_users[$s->owner_id] ) ) $owner_name = myhtmlentities($lab_users[$s->owner_id]->first_name." ".$lab_users[$s->owner_id]->last_name);
		$list[$s->carton_id] = array(myhtmlentities($s->carton_identifier), myhtmlentities($s->internal_barcode), $storage_name, $owner_name);
	}
	$options = array();
	$options["numresults"] = count($list);
	$options["options"] = $list;
	//sleep (1);
	header('Content-type: application/json');
	print json_encode( $options );
}

if( $mode == "s" ) { // simple search for only names and barcodes
	if( ! isset($_GET['q']) ) return;
	$s = $_GET['q'];
	$list = array();
	global $mysqli;
	$s = mysqli_real_escape_string($mysqli,$s);
	
	$sql = "select carton_identifier as ci from ".DBTBL_carton." where storage_id in (select storage_id from ".DBTBL_storage_device." where laboratory_id = ".$_SESSION['lims']['lab_id']." ) and (carton_identifier like '%".$s."%' or internal_barcode like '%".$s."%')";
	//error_log($sql);
	$r = dbq($sql);
	while( $c = $r->fetch_object() ) {
		$list[] = $c->ci;
		}

	//$sql = "select identification as id from ".$dbtbl_sample." where identification like '%".$s."%' or comments like '%".$s."%'";
	$sql = "select identification as id from ".DBTBL_sample." where sample_id in (select sample_id from ".DBTBL_carton_sample." where carton_id in (select carton_id from ".DBTBL_carton." where storage_id in (select storage_id from ".DBTBL_storage_device." where laboratory_id=".$_SESSION['lims']['lab_id']."))) and ((identification like '%".$s."%')) order by identification";
	//error_log($sql);
	$r = dbq($sql);
	while( $s = $r->fetch_object() ) {
		$list[] = $s->id;
		}
		
	$options = array();
	$options["options"] = $list;

	header('Content-type: application/json');
	print json_encode( $options );
}
?>