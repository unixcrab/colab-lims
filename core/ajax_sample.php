<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;


include "config.common.php";
include "constants.php";
include "functions.db_connect.php";
include "functions.lims.php";

$mode = "";
if( isset($_POST['m']) ) $mode = $_POST['m'];
if( $mode == "" ) return;

//foreach($_POST as $k=>$v) error_log(__FILE__." ".$k." --> ".$v);

if( $mode == "u" ) { // update
  $sid = null;
  $cid = null;
  $r = null;
  $c = null;
  $sn = null;
  $st = null;
  $sconc = null;
  $sconcu = null;
  $svol = null;
  $svolu = null;
  $scom = null;
  $sact = null;
  $sctrl = null;
  $sdep = null;
  
  if( isset($_POST['sid'] ) ) $sid = $_POST['sid'];
  if( isset($_POST['cid'] ) ) $cid = $_POST['cid'];
  if( isset($_POST['sr'] ) ) $r = $_POST['sr'];
  if( isset($_POST['sc'] ) ) $c = $_POST['sc'];
  if( isset($_POST['sn'] ) ) $sn = $_POST['sn'];
  if( isset($_POST['st'] ) ) $st = $_POST['st'];
  if( isset($_POST['sconc'] ) ) $sconc = $_POST['sconc'];
  if( isset($_POST['sconcu'] ) ) $sconcu = $_POST['sconcu'];
  if( isset($_POST['svol'] ) ) $svol = $_POST['svol'];
  if( isset($_POST['svolu'] ) ) $svolu = $_POST['svolu'];
  if( isset($_POST['scom'] ) ) $scom = $_POST['scom'];
  if( isset($_POST['sact'] ) ) $sact = $_POST['sact'];
  if( isset($_POST['sctrl'] ) ) $sctrl = $_POST['sctrl'];
  if( isset($_POST['sdep'] ) ) $sdep = $_POST['sdep'];
  
  if( ($cid==null) || ($sid==null) || ($sn==null) ) {
    print json_encode( array("resp"=>-1, "err_index"=>"err_not_found") );
    return;
  }
  
	$carton = getCarton($cid,$_SESSION['lims']['lab_id']);
	//error_log("owner_id=".$carton->owner_id." person_id=".$_SESSION['lims']['person_id']." allow_contents_edit=".$carton->allow_contents_edit);
	if(($carton->owner_id != $_SESSION['lims']['person_id']) && ($carton->allow_contents_edit!=1)) {
		print json_encode( array("resp"=>-1, "err_index"=>"err_no_edit_perm") );
    return;
	}
	
	// remove potential badness
	//$sn = preg_replace( "/<[a-Z]>/", "*", $sn );
	$sn = strip_tags($sn);
	$scom = strip_tags($scom);
	
  // check if well already occupied
  $cs_curr = getCartonSampleFromSID($sid);
  if( ($cs_curr->row_num != $r) || ($cs_curr->column_num != $c) ) {
    $cs_new = getCartonSampleFromRC($r,$c,$cid);
    if( $cs_new != null ) {
//       $occ = getSample($cs_new->sample_id);
//       print json_encode( array("resp"=>0, "err_index"=>"well_occupied", "sid"=>$cs_new->sample_id, "sident"=>$occ->identification) );
      print json_encode( array("resp"=>-1, "err_index"=>"well_occupied") );
      return;
    }
  }
  
  $s = new Sample();
  $s->sample_id = $sid;
  $s->sample_type_id = $st;
  $s->identification = $sn;
  $s->concentration = $sconc;
  $s->concentration_units_id = $sconcu;
  $s->volume = $svol;
  $s->volume_units_id = $svolu;
  if( $sdep == 1 ) $s->exhausted_flag = "Y";
  if( $sctrl == 1 ) $s->control_flag = "Y";
  $s->comments = $scom;
  
  $cs_curr->row_num = $r;
  $cs_curr->column_num = $c;
  if( $sact == 1 ) $cs_curr->active = "Y";
  else $cs_curr->active = "N";
  
  $rs = updateSample($s);
  //error_log("rs=".$rs);
  $rcs = updateCartonSample($cs_curr);
  //error_log("rcs=".$rcs);
  
  if( ($rs==1)||($rcs==1) ) 
    print json_encode( array("resp"=>1) );
  else
    print json_encode( array("resp"=>0) );
  
} // end mode 'u'

if( $mode == "n" ) { // new
  $cid = null;
  $r = null;
  $c = null;
  $sn = null;
  $st = null;
  if( isset($_POST['cid'] ) ) $cid = $_POST['cid'];
  if( isset($_POST['r'] ) ) $r = $_POST['r'];
  if( isset($_POST['c'] ) ) $c = $_POST['c'];
  if( isset($_POST['sn'] ) ) $sn = $_POST['sn'];
  if( isset($_POST['st'] ) ) $st = $_POST['st'];
  
  if( ($cid==null) || ($r==null) || ($c==null) || ($sn==null) || ($st==null) ) {
    print json_encode( array("resp"=>0) );
    return;
  }
  
	$carton = getCarton($cid,$_SESSION['lims']['lab_id']);
	if(($carton->owner_id!=$_SESSION['lims']['person_id'])&&($carton->allow_contents_edit!=1)) {
		print json_encode( array("resp"=>-1, "err_index"=>"err_no_edit_perm") );
    return;
	}
	
  $cs = getCartonSampleFromRC($r, $c, $cid);
  
  if( ($cs!=null) && ($cs->sample_id>0) ) { // sample already in this well
    //error_log("cs=".$cs->sample_id);
//     print json_encode( array("resp"=>5, "cid"=>652) );
    print json_encode( array("resp"=>-1, "reason"=>"well_occupied", "sid"=>$cs->sample_id ) );
    return;
  }
  
  
  // should be ok to create it now
  $s = new Sample();
  $s->identification = $sn;
  $s->sample_type_id = $st;
  $sid = addSample($s);
  if( $sid != null ) {
    $cs = new CartonSample();
    $cs->sample_id = $sid;
    $cs->row_num = $r;
    $cs->column_num = $c;
    $cs->carton_id = $cid;
    if( ($csid=addCartonSample($cs)) != null )
      print json_encode( array("resp"=>1, "sid"=>$sid) );
    else
      print json_encode( array("resp"=>0 ) );
    }
  else
    print json_encode( array("resp"=>0 ) );
} // end mode 'n'
?>