<?php
session_start();
if(!isset($_SESSION["lims"]) && !isset($_SESSION['lims']['logged_in']) ) return;

include "config.common.php";
include "functions.db_connect.php";
include "functions.projects.php";
include "functions.user.php";

date_default_timezone_set($site_timezone);

$person_id = $_SESSION['lims']['person_id'];
$mode = "";
if( isset( $_POST['m'] ) )
  $mode = stripslashes( $_POST['m'] ); // n=new, u=update, i=invite users, df=delete file

// foreach($_POST as $k=>$v) error_log("ajax_proj: ".$k."->[".$v."]");

$r = 0;

if( $mode == "df" ) {  // delete project file
  if( !isset($_POST['fid']) ) {
    print json_encode( array("resp"=>$r) );
    return;
  }
  $fid = $_POST['fid'];
  $r = deleteProjectFile($person_id,$fid);
  print json_encode( array("resp"=>$r) );
}

if( $mode == "n" ) { // create new project
  $projname = stripslashes( $_POST['p'] );
  if( $_SESSION['lims']['dateformat'] == 1 ) {
    $sdate = strtotime( str_replace('/', '-', stripslashes( $_POST['sd'] ) ) );
    $edate = strtotime( str_replace('/', '-', stripslashes( $_POST['ed'] ) ) );
  }
  else {
    $sdate = strtotime( stripslashes( $_POST['sd'] ) );
    $edate = strtotime( stripslashes( $_POST['ed'] ) );
  }

//   error_log("dates ".$sdate." -> ".$edate);
  
  if($edate < $sdate) {
    print json_encode( array("resp"=>-10) );
    exit();
    }
  $r = addProject($_SESSION['lims']['person_id'],$projname,$sdate,$edate);
  
  // create the folder for uploaded files
  if( $r > 0 ) {
    error_log("creating ".$projects_dir.$r."/files" );
    mkdir( $projects_dir.$r."/files", 0755, true );
    }
}

if( $mode == "i" ) { // invite/uninvite users to project
  $pid = stripslashes( $_POST['pid'] );
  $users = trim( stripslashes( $_POST['u'] ) );
//   error_log($pid." --> ".$users);
  
  $pmems = getProjectMembers($pid);
  $uidarray = array();
  foreach($pmems as $pmem) 
    $uidarray[$pmem->person_id] = $pmem->person_id;
  
  $uarr = explode(",", $users);
  foreach($uarr as $uid) {
    if( $uid == "" ) continue;
//     error_log("uid:".$uid);
    
    if( isset( $uidarray[$uid] ) ) {
//       error_log("skipping already member ".$uid);
      continue;
      }
      
//     error_log("INVITING ".$uid);
    $r = addProjectMember($pid, $uid);
    if( $r == 1 ) addUserAlert($uid,"alert_project_invite", $pid);
    }
  
  // uninvite
  foreach($uidarray as $curr_mem) {
    if( ! in_array($curr_mem, $uarr) ) {
//       error_log("UNINVITING ".$curr_mem);
      $r = removeProjectMember($pid, $curr_mem);
    }      
  }
}

// error_log("ajax_proj: ".$r);

// if( $r > 0 ) {
print json_encode( array("resp"=>$r) );
// }
// else
//   print json_encode( array("resp"=>0) );
?>