<?php
include "core/config.common.php";

$sql_schema = "install_files/mysql_schema.sql";
$sql_schema_out = "install_files/mysql_schema_".$tablePrefix.".sql";
$init_data = "install_files/init_data.sql";
$init_data_out = "install_files/init_data_".$tablePrefix.".sql";

if( ! file_exists($sql_schema) ) {
	print "Error: file not found: ".$sql_schema."!\n\n";
	return;
	}
$output = passthru("sed -e 's/{COLAB_PREFIX}/".$tablePrefix."/g' ".$sql_schema." > ".$sql_schema_out);

if( ! file_exists($init_data) ) {
	print "Error: file not found: ".$init_data."!\n\n";
	return;
	}
$output = passthru("sed -e 's/{COLAB_PREFIX}/".$tablePrefix."/g' ".$init_data." > ".$init_data_out);

//  alter DATABASE ".$dbDB." CHARACTER SET 'utf8';
// alter DATABASE ".$dbDB." COLLATE utf8_bin;
// drop DATABASE colablims;
// create DATABASE colablims CHARACTER SET utf8 COLLATE utf8_bin;
/*
CREATE DATABASE colablims
 CHARACTER SET utf8
 DEFAULT CHARACTER SET utf8
 COLLATE utf8_general_ci
 DEFAULT COLLATE utf8_general_ci;
 
 ALTER DATABASE colablims2
 CHARACTER SET utf8
 DEFAULT CHARACTER SET utf8
 COLLATE utf8_general_ci
 DEFAULT COLLATE utf8_general_ci
 ;
 
 */
//$cmd_out = shell_exec("mysql -u".$dbUser." -p".$dbPass." -h ".$dbHost." -D ".$dbDB." < ".$sql_schema_out);
//$cmd_out = shell_exec("mysql -u".$dbUser." -p".$dbPass." -h ".$dbHost." -D ".$dbDB." < ".$init_data_out);

?>