package com.colablims.agent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ConnectException;

//import javax.ws.rs.client.Client;
//import javax.ws.rs.client.ClientBuilder;
//import javax.ws.rs.client.Entity;
//import javax.ws.rs.client.Invocation;
//import javax.ws.rs.client.WebTarget;
//import javax.ws.rs.core.Form;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;



import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
//import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.util.EntityUtils;

public class FileTransferThread implements Runnable {
	private LabInstrument labInstr;
	//private String filePath;
	private File sendFile;
	public FileTransferThread(File sendFile, LabInstrument instr) {
		this.labInstr = instr;
		this.sendFile = sendFile;
		//this.filePath = sendFile.getName();
	}

	@Override
	public void run() {
		
		try {
			LabInstrument.logOut("Uploading to "+labInstr.getLimsHost());
			System.err.println("Uploading to "+labInstr.getLimsHost());
			
			// get the file checksum to verify after the transfer
			//FileInputStream fis = new FileInputStream(new File(filePath));
			FileInputStream fis = new FileInputStream(sendFile);
			String md5 = DigestUtils.md5Hex(fis);
			LabInstrument.logOut("md5 checksum for "+sendFile.getName()+": "+md5);
			
			
			CloseableHttpClient httpclient = HttpClients.createMinimal();
			HttpPost httppost = new HttpPost( labInstr.getLimsHost() );
			InputStreamEntity reqEntity = new InputStreamEntity( new FileInputStream(sendFile), -1);
			reqEntity.setContentType("binary/octet-stream");
            reqEntity.setChunked(true);
            httppost.setEntity(reqEntity);
            System.err.println("executing request " + httppost.getRequestLine());
            CloseableHttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();
            System.err.println(response.getStatusLine()/*+" --> "+response.getEntity()*/);
            if (resEntity != null) {
                System.err.println("Response content length: " + resEntity.getContentLength());
                System.err.println("Chunked?: " + resEntity.isChunked());
                //System.err.println("code: " + resEntity.getContent());
            }
            EntityUtils.consume(resEntity);
			
//			final ClientConfig cConfig = new ClientConfig();
//			cConfig.register(MultiPartFeature.class);
//			Client limsClient = ClientBuilder.newClient(cConfig);
//			//String sContentDisposition = "attachment; filename=\"" + sendFile.getName()+"\"";
//			WebTarget webTarget = limsClient.target(labInstr.getLimsHost());
//			//webTarget.request().post(Entity.entity(sendFile, MediaType.APPLICATION_OCTET_STREAM_TYPE));
//			Form form = new Form();
//			form.param("uploadFile", "foo");
//			webTarget.request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(form, MediaType.MULTIPART_FORM_DATA_TYPE));
//			
			//Invocation.Builder invBuilder = webTarget.request(MediaType.TEXT_PLAIN_TYPE);
			//invBuilder.header("instrument-header-id", labInstr.getInstrumentID());
			//Response resp = invBuilder.get();
			//System.out.println("resp "+resp.getStatus());
			//System.out.println("resp "+resp.readEntity(String.class));
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ConnectException e) {
			System.err.println("Cannot connect to URL "+labInstr.getLimsHost());
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

}
