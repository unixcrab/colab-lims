package com.colablims.agent;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;


public class LabInstrument {
	private String limsHost;
	private String limsAPIKey;
	private String instrumentID ;
	private String instrumentDescr;
	private String instrumentStatus;
	private String instrumentOutputDir;
	private String instrumentOutputExtension;
	private int instrumentOutputMinAge;
	private String agentLogFile;
	private String agentProcessedDir;
	private int pollSecs;
	
	private Properties limsProps;
	
	private static DateFormat df;
	private static FileWriter logOutFile;
	
	/**
	 * @param  properties  A Java properties file with instrument details in it 
	 * @return  Nothing
	 * @see         Properties
	 */
	public LabInstrument(Properties limsprops) {
		this.limsProps = limsprops;
		limsHost = limsProps.getProperty("limsHost","http://127.0.0.1/lims");
		limsAPIKey = limsProps.getProperty("limsAPIKey");
		instrumentID = limsProps.getProperty("instrumentID");
		instrumentDescr = limsProps.getProperty("instrumentDescr","Lab Instrument");
		instrumentStatus = limsProps.getProperty("instrumentStatus");
		instrumentOutputDir = limsProps.getProperty("instrumentOutputDir", ".");
		instrumentOutputExtension = limsProps.getProperty("instrumentOutputExtension",".txt");
		instrumentOutputMinAge = Integer.parseInt( limsProps.getProperty("instrumentOutputMinAge", "60") );
		agentLogFile = limsProps.getProperty("agentLogFile","sendlog.dat");
		agentProcessedDir = limsProps.getProperty("agentProcessedDir","processed");
		pollSecs = Integer.parseInt(limsProps.getProperty("pollSecs","10"));
		
		df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			logOutFile = new FileWriter(agentLogFile,true);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		logOut("Ignoring files newer than "+instrumentOutputMinAge+" seconds");
	}
	
	public int getPollSecs() {
		return pollSecs;
	}

	public void setPollSecs(int pollSecs) {
		this.pollSecs = pollSecs;
	}

	public String getLimsHost() {
		return limsHost;
	}


	public void setLimsHost(String limsHost) {
		this.limsHost = limsHost;
	}


	public int getInstrumentOutputMinAge() {
		return instrumentOutputMinAge;
	}

	public void setInstrumentOutputMinAge(int instrumentOutputMinAge) {
		this.instrumentOutputMinAge = instrumentOutputMinAge;
	}

	public Properties getLimsProps() {
		return limsProps;
	}


	public String getLimsAPIKey() {
		return limsAPIKey;
	}


	public void setLimsAPIKey(String limsAPIKey) {
		this.limsAPIKey = limsAPIKey;
	}


	public void setLimsProps(Properties limsProps) {
		this.limsProps = limsProps;
	}


	public String getAgentProcessedDir() {
		return agentProcessedDir;
	}


	public void setAgentProcessedDir(String agentProcessedDir) {
		this.agentProcessedDir = agentProcessedDir;
	}


	public String getInstrumentID() {
		return instrumentID;
	}



	public void setInstrumentID(String instrumentID) {
		this.instrumentID = instrumentID;
	}



	public String getInstrumentDescr() {
		return instrumentDescr;
	}



	public void setInstrumentDescr(String instrumentDescr) {
		this.instrumentDescr = instrumentDescr;
	}



	public String getInstrumentStatus() {
		return instrumentStatus;
	}



	public void setInstrumentStatus(String instrumentStatus) {
		this.instrumentStatus = instrumentStatus;
	}



	public String getInstrumentOutputDir() {
		return instrumentOutputDir;
	}



	public void setInstrumentOutputDir(String instrumentOutputDir) {
		this.instrumentOutputDir = instrumentOutputDir;
	}



	public String getInstrumentOutputExtension() {
		return instrumentOutputExtension;
	}



	public void setInstrumentOutputExtension(String instrumentOutputExtension) {
		this.instrumentOutputExtension = instrumentOutputExtension;
	}



	public String getAgentLogFile() {
		return agentLogFile;
	}



	public void setAgentLogFile(String agentLogFile) {
		this.agentLogFile = agentLogFile;
	}



	public static void logOut(String log) {
		Date today = Calendar.getInstance().getTime();        
		String reportDate = df.format(today);
		try {
			logOutFile.write(reportDate+" "+log+"\n");
			logOutFile.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
