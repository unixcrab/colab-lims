package com.colablims.agent;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
//import org.apache.commons.io.*;

class FileWatcherThread implements Runnable {
	private String monitorDir = ".";
	private String monitorProcDir;
	private int minFileAge = 60;
	private DateFormat fileTimestamp;
	private long msecsNow;
	private LabInstrument labInstr;
	private int dirPollSecs;
	
	public FileWatcherThread(LabInstrument instr) throws IOException {
		this.labInstr = instr;
		monitorProcDir = instr.getAgentProcessedDir();
		monitorDir = instr.getInstrumentOutputDir();
		minFileAge = instr.getInstrumentOutputMinAge();
		dirPollSecs = instr.getPollSecs();
	}
	
	@Override
	public void run() {
		File instrDataFolder = new File(monitorDir);
		File procDataFolder = new File(monitorProcDir);
		fileTimestamp = new SimpleDateFormat("ddMMyyyy_HHmmss");
		
		// check output folder exists
		if( (!procDataFolder.exists()) || (!procDataFolder.isDirectory())) {
			// attempt to create it
			boolean mkResult = procDataFolder.mkdir();
			if(!mkResult) {
				// cannot make so output processed data into the instrument data folder
				LabInstrument.logOut("Cannot create folder: "+monitorProcDir);
				monitorProcDir = monitorDir;
			}
			else {
				LabInstrument.logOut("Created folder "+monitorProcDir);
			}
		}
		
		// check input folder is actually there
		if( instrDataFolder.exists() && instrDataFolder.isDirectory() ) {
		
			try {
				//System.out.println("Looking in "+monitorDir);
				LabInstrument.logOut("Looking in "+monitorDir);
				for(;;) {
					msecsNow = new Date().getTime();
					
					File[] fileList = instrDataFolder.listFiles();
					for(File sFile : fileList) {
						String fileName = sFile.getName();
						
						// check file is old enough to process
						if((sFile.lastModified()+(minFileAge*1000)) >= msecsNow) {
							//LabInstrument.logOut("Waiting for "+fileName+" to age "+minFileAge+" secs.");
							continue;
						}
						
						
						if(!fileName.endsWith(".lims_processed")) {
							LabInstrument.logOut("Found new file "+fileName);
							
							// rename the file so it doesn't get reprocessed
							Date today = Calendar.getInstance().getTime();        
							String procFileName = monitorProcDir+fileName+"_"+fileTimestamp.format(today)+".lims_processed";
							
							File renamedFile = new File(procFileName);
							if(!sFile.renameTo(renamedFile)) {
								LabInstrument.logOut("Problem renaming: "+procFileName+"!");
							}
							else{
								new Thread(new FileTransferThread(renamedFile, labInstr)).start();
								LabInstrument.logOut(fileName+" processed");
							}
						}
					}
					Thread.sleep(dirPollSecs*1000);
				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("finished");
			}
			catch (NullPointerException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		
		}
		else {
			System.out.println("Directory "+monitorDir+" does not exist!");
		}
		
	}
}
