package com.colablims.agent;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class LimsAgent {

	public static void main(String[] args) {
				
		Properties limsProps = new Properties();
		try {
			limsProps.load(new FileInputStream("lims.properties"));
			//limsHost = limsProps.getProperty("limsHost", "localhost");
			
			//LabInstrument instrument = new LabInstrument(instrumentID, instrumentDescr, instrumentStatus, instrumentOutputDir, instrumentOutputExtension, agentLogFile);
			LabInstrument instrument = new LabInstrument(limsProps);
			//System.out.println(limsHost);
			
			LabInstrument.logOut("Starting...");
			
			new Thread(new FileWatcherThread(instrument)).start();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	


	
}


