﻿<?php 
include "core/html_head.php"; 

$page="";
$subpage="";

if( $user_logged_in ) {

  if( isset($_GET['p'] ) ) {
    $page = $_GET['p'];
    $_SESSION['lims']['left_menu'] = $page;
    }
    
  // subpages
  if( isset($_GET['sp'] ) ) {
    $subpage = $_GET['sp'];
    $_SESSION['lims']['left_menu_subpage'] = $subpage;
    }
}

  
?>

<!--<body style="padding-top:45px; style="background-image: url('img/bg1.png'); background-repeat:no-repeat; background-position:right bottom;"  ">-->
<body data-spy="scroll" data-target=".subnav" data-offset="50">

<!-- display blocks -->

<!-- HEADER block -->

<?php 

$br_ua = strtoupper($_SERVER['HTTP_USER_AGENT']);
if( (strlen(trim(strstr($br_ua,"MSIE 6")))!=0) || (strlen(trim(strstr($br_ua,"MSIE 7")))!=0) ) {
//   print "Unsupported browser";
  ?>
  <div style="width:100%; align:center; background:#ffaaaa;">
  <h1>Browser unsupported.</h1>
  <p>Sorry, but this old browser is not supported. 
  
  <p>This version, <i><?php print $_SERVER['HTTP_USER_AGENT']; ?></i>, does not support modern HTML, CSS or javascript standards.
  
  <p>Please upgrade to a more recent version or consider using <b><a href="http://chrome.google.com">Google Chrome</a></b> or <b><a href="http://getfirefox.com">Mozilla Firefox</a></b>.
  
  </div>
  <?php
  exit(0);
  }
  
include "core/config.header.php"; 
// include( "core/functions.misc.php" );
?>

<div class="container-fluid">
    <div class="row-fluid">
    
    
    
      <div class="span2" id="leftCol" >
      <?php
      if ( $page == "" ) include "core/menu_blank.php";
      else include "core/menu_".$page.".php";
      ?>
      </div>
      
      
      
      
      <?php
	    
	    if( ! $user_logged_in ) {
		    print "\n<div class='span10' id='contentCol'>";
		    include "core/login.php";
		    print "\n</div>";
	    }
	    if( $subpage != "" ) {
				print "\n<div class='span9' id='contentCol'>";
				// load the page from the module
				if( $substart = startsWith($subpage,"account:module:") ) {
					$modname = str_replace("account:module:","",$subpage);

					include ("core/account.module_user.php");
					}
				else
					include "core/".$subpage.".php";
				print "\n</div>";

		}

      ?>
      
    </div>
</div>


			<?php include "core/config.footer.php"; ?>

<!-- end display blocks -->

</body>

</html>